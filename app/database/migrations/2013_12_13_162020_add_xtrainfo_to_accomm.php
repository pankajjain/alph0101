<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddXtrainfoToAccomm extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('accommodations', function(Blueprint $table)
		{
			$table->smallInteger('numberBars')->after('mainContactEmail');
			$table->smallInteger('numberRestaurant')->after('mainContactEmail');
			$table->smallInteger('numberRoom')->after('mainContactEmail');
			$table->smallInteger('numberFloor')->after('mainContactEmail');
			$table->string('roomVoltage')->after('mainContactEmail');
			$table->string('internetUsage')->after('mainContactEmail');
			$table->string('breakfastCharge')->after('mainContactEmail');
			$table->string('roomService')->after('mainContactEmail');
			$table->string('nonSmokingRoom')->after('mainContactEmail');
			$table->string('elevator')->after('mainContactEmail');
			$table->string('parking')->after('mainContactEmail');
			$table->string('parkingFee')->after('mainContactEmail');
			$table->string('receptionOpen')->after('mainContactEmail');
			$table->string('checkIn')->after('mainContactEmail');
			$table->string('checkOut')->after('mainContactEmail');
			$table->smallInteger('hotelBuilt')->after('mainContactEmail');
			$table->smallInteger('hotelRenovated')->after('mainContactEmail');
			$table->string('airportTransfer')->after('mainContactEmail');
			$table->string('airportTransferFee')->after('mainContactEmail');
			$table->smallInteger('distanceCity')->after('mainContactEmail');
			$table->smallInteger('distanceAirport')->after('mainContactEmail');
			$table->smallInteger('timeAirport')->after('mainContactEmail');
			$table->string('hotelDescription')->after('mainContactEmail');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('accommodations', function(Blueprint $table)
		{
			$table->dropColumn('numberBars');
			$table->dropColumn('numberRestaurant');
			$table->dropColumn('numberRoom');
			$table->dropColumn('numberFloor');
			$table->dropColumn('roomVoltage');
			$table->dropColumn('internetUsage');
			$table->dropColumn('breakfastCharge');
			$table->dropColumn('roomService');
			$table->dropColumn('nonSmokingRoom');
			$table->dropColumn('elevator');
			$table->dropColumn('parking');
			$table->dropColumn('parkingFee');
			$table->dropColumn('receptionOpen');
			$table->dropColumn('checkIn');
			$table->dropColumn('checkOut');
			$table->dropColumn('hotelBuilt');
			$table->dropColumn('hotelRenovated');
			$table->dropColumn('airportTransfer');
			$table->dropColumn('airportTransferFee');
			$table->dropColumn('distanceCity');
			$table->dropColumn('distanceAirport');
			$table->dropColumn('timeAirport');
			$table->dropColumn('hotelDescription');
		});
	}

}