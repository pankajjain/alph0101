<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVouchersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vouchers', function(Blueprint $table)
		{
			//new columns to be added 
			$table->string('voucherName')->after('accommID');
			$table->text('summary')->after('roomID');
			$table->text('highlights')->after('summary');
			$table->boolean('breakfastIncluded')->after('highlights');
			$table->decimal('voucherAmount', 5, 2)->after('originalPrice');
			$table->string('voucherDuration')->after('voucherAmount');
			$table->integer('minimumDaysPriorBooking');
			$table->timestamp('redemptionPeriodFrom');
			$table->timestamp('redemptionPeriodTo');

			//columns to be renamed
			$table->renameColumn('validityPeriodFrom', 'bookingPeriodFrom');
			$table->renameColumn('validityPeriodTo', 'bookingPeriodTo');

			//columns to be removed
			$table->dropColumn('bookingConditions');

		});
	}
	public function down()
	{
		Schema::table('vouchers', function(Blueprint $table)
		{
			$table->dropColumn('voucherName');
			$table->dropColumn('summary');
			$table->dropColumn('highlights');
			$table->dropColumn('breakfastIncluded');
			$table->dropColumn('voucherAmount');
			$table->dropColumn('voucherDuration');
			$table->dropColumn('minimumDaysPriorBooking');
			$table->dropColumn('redemptionPeriodFrom');
			$table->dropColumn('redemptionPeriodTo');

			$table->renameColumn( 'bookingPeriodFrom' , 'validityPeriodFrom');
			$table->renameColumn('bookingPeriodTo' ,'validityPeriodTo');

			$table->text('bookingConditions');


		});
	}

}