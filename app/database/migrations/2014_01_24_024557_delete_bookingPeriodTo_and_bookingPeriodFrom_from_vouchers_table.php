<?php

use Illuminate\Database\Migrations\Migration;

class DeleteBookingPeriodToAndBookingPeriodFromFromVouchersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vouchers', function($table)
		{
			$table->dropColumn('bookingPeriodFrom');
			$table->dropColumn('bookingPeriodTo');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
			$table->timestamp('bookingPeriodFrom');
			$table->timestamp('bookingPeriodTo');
		
	}

}