<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVouchersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vouchers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('accommID');
			$table->integer('roomID');
			$table->decimal('discountedPrice');
			$table->text('bookingConditions');
			$table->text('voucherPolicies');
			$table->timestamp('validityPeriodFrom');
			$table->timestamp('validityPeriodTo');
			$table->string('voucherID');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vouchers');
	}

}
