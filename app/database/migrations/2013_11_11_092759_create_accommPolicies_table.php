<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccommPoliciesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('accommpolicies', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('accommID');
			$table->integer('infantAgeUntil')->default(3);
			$table->integer('childrenAgeFrom')->default(12);
			$table->integer('minimumGuestAgeForStay')->default(0);
			$table->integer('extraBedRequiredFrom')->default(12);
			$table->boolean('childrenStayFree')->default(true);
			$table->text('otherPolicies');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('accommpolicies');
	}

}
