
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFlashdealsVoucher extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('flashdeals', function(Blueprint $table)
		{
			//new columns to be added 
			$table->string('flashDealName')->after('accommID');
			$table->text('summary')->after('roomID');
			$table->text('highlights')->after('summary');
			$table->boolean('breakfastIncluded')->after('highlights');
			$table->decimal('flashDealAmount', 5, 2)->after('originalPrice');
			$table->string('flashDealDuration')->after('flashDealAmount');
			$table->integer('minimumDaysPriorBooking');
			$table->timestamp('redemptionPeriodFrom');
			$table->timestamp('redemptionPeriodTo');
			$table->integer('salesLimit')->after('roomID');
			$table->string('status')->after('roomID');

			//columns to be renamed
			$table->renameColumn('validityPeriodFrom', 'bookingPeriodFrom');
			$table->renameColumn('validityPeriodTo', 'bookingPeriodTo');
			$table->renameColumn('voucherPolicies', 'flashDealPolicies');
			//columns to be removed
			$table->dropColumn('bookingConditions');

		});
	}
	public function down()
	{
		Schema::table('flashdeals', function(Blueprint $table)
		{
			$table->dropColumn('flashDealName');
			$table->dropColumn('summary');
			$table->dropColumn('highlights');
			$table->dropColumn('breakfastIncluded');
			$table->dropColumn('flashDealAmount');
			$table->dropColumn('flashDealDuration');
			$table->dropColumn('minimumDaysPriorBooking');
			$table->dropColumn('redemptionPeriodFrom');
			$table->dropColumn('redemptionPeriodTo');
			$table->dropColumn('salesLimit');
			$table->dropColumn('status');
			$table->renameColumn( 'bookingPeriodFrom' , 'validityPeriodFrom');
			$table->renameColumn('bookingPeriodTo' ,'validityPeriodTo');

			$table->text('bookingConditions');


		});
	}

}