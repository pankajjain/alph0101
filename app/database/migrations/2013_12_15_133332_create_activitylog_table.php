<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitylogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('activitylogs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('userID')->nullable();
			$table->string('message')->nullable();
			$table->string('model',50)->nullable();
			$table->string('action',50)->nullable();
			$table->string('refrenceID',50)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activitylogs');
	}

}
