<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			/*$table->increments('id');
			$table->integer('userID');
			$table->integer('accommID');
			$table->integer('roomID');
			$table->integer('voucherID');
			$table->integer('flashdealID');
			$table->decimal('price');
			$table->string('token');
			$table->string('correlationID');
			$table->string('ack');
			$table->string('build');
			$table->string('transactionID');
			$table->string('transactionType');
			$table->string('paymentType');
			$table->string('paymentStatus');
			$table->string('secureMerchantAccID');
			$table->string('paymentInfoAck');
			$table->timestamps();*/
			
			
			$table->increments('id');
			$table->integer('userID');
			$table->integer('accommID');
			$table->integer('roomID');
			$table->integer('voucherID');
			$table->integer('flashdealID');
			$table->decimal('price');
			$table->string('token');
			
			$table->string('transactionID');
			
			$table->string('paymentStatus');
			
			$table->timestamps();
			
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
