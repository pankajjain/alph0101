<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMainContactMobileToAccomm extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('accommodations', function(Blueprint $table)
		{
			$table->string('mainContactMobile')->after('mainContactEmail');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('accommodations', function(Blueprint $table)
		{
			$table->dropColumn('mainContactMobile');
		});
	}

}
