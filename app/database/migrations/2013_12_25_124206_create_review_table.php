<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reviewsrating', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('accommID');
			$table->integer('rating')->nullable();
			$table->string('negativeReviews')->nullable();
			$table->string('positiveReviews')->nullable();
			$table->integer('clean');
			$table->integer('comfort');
			$table->integer('location');
			$table->integer('staff');
			$table->integer('services');
			$table->integer('valForMoney');
			$table->integer('byUserID');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reviewsrating');
	}

}