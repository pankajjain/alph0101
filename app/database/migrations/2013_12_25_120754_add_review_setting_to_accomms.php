<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReviewSettingToAccomms extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('accommodations', function(Blueprint $table)
		{
			$table->boolean('enableReview')->after('numberBars');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('accommodations', function(Blueprint $table)
		{
			$table->dropColumn('enableReview');
		});
	}

}