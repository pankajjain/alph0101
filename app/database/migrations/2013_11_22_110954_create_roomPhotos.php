<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomPhotos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('roomPhotos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('roomID');
			$table->string('title');
			$table->string('description');
			$table->string('type');
			$table->string('fileName');
			$table->string('originalFileName');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('roomPhotos');
	}

}
