<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlashDealsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('flashdeals', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('accommID');
			$table->integer('roomID');
			$table->integer('voucherID');
			$table->decimal('originalPrice');
			$table->decimal('discountedPrice');
			$table->decimal('discountPercentage');
			$table->decimal('discountAmount');
			$table->text('bookingConditions');
			$table->text('voucherPolicies');
			$table->timestamp('validityPeriodFrom');
			$table->timestamp('validityPeriodTo');
			$table->string('flashdealID');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('flashdeals');
	}

}
