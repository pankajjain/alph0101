<?php


use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateAccommodationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('accommodations', function(Blueprint $table)
        {

            $table->increments('id');
            $table->integer('userID');
            $table->string('name');
            $table->boolean('isVerified')->default(false);
            $table->smallInteger('starRating');
            $table->smallInteger('numberOfRooms')->nullable();
            $table->string('typeID');
            $table->string('country');
            $table->string('province');
            $table->string('city');
            $table->string('streetAddress');
            $table->string('zipCode');
            $table->string('contactNumber');
            $table->string('fax');
            $table->string('website');
            $table->string('mainContactName');
            $table->string('mainContactRole');
            $table->string('mainContactEmail');
            $table->timestamps();

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('accommodations');
	}

}