<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoomSizeRoomViewToRoomsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rooms', function(Blueprint $table)
		{
			
			$table->string('roomSize')->after('maxGuestsAllowed');
			$table->string('view')->after('maxGuestsAllowed');
			$table->boolean('breakfastIncluded')->after('maxGuestsAllowed')->nullable()->default(false);

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rooms', function(Blueprint $table)
		{
			 $table->dropColumn('roomSize');
			 $table->dropColumn('view');
			 $table->dropColumn('breakfastIncluded');
		});
	}

}
