<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFacilityTypeToFacilitiesTable extends Migration {

	public function up()
	{
		Schema::table('facilities', function(Blueprint $table)
		{			
			$table->string('type')->after('title');
		});
		Schema::table('accommFacilities', function(Blueprint $table)
		{			
			$table->string('type')->after('facility');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('facilities', function(Blueprint $table)
		{
			 $table->dropColumn('type');

		});
		Schema::table('accommFacilities', function(Blueprint $table)
		{			
			$table->dropColumn('type');
		});
	}

}