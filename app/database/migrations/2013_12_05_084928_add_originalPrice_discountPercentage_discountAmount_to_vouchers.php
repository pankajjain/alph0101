<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOriginalPriceDiscountPercentageDiscountAmountToVouchers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vouchers', function(Blueprint $table)
		{
			$table->decimal('discountPercentage')->after('discountedPrice');
			$table->decimal('discountAmount')->after('discountedPrice');
			$table->decimal('originalPrice')->after('discountedPrice');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vouchers', function(Blueprint $table)
		{
			$table->dropColumn('discountPercentage');
			$table->dropColumn('discountAmount');
			$table->dropColumn('originalPrice');
		});
	}

}