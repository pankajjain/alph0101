<?php

class AccommodationTypesTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		DB::table('accommodationTypes')->truncate();

		$accomodationtypes = array(
			
			array('title' => 'Apartment'),
			array('title' => 'Bed & Breakfast'),
			array('title' => 'Guest House'),
			array('title' => 'Holiday House'),
			array('title' => 'Hostel'),
			array('title' => 'Hotel'),
			array('title' => 'Motel'),
			array('title' => 'Resort'),
			array('title' => 'Private Villa'),
			array('title' => 'Residence'),
			array('title' => 'Ryokan'),
			array('title' => 'Villa')

			);
		// Uncomment the below to run the seeder
		DB::table('accommodationTypes')->insert($accomodationtypes);
	}

}
