<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		$this->call('FacilitiesTableSeeder');
		$this->call('AccommodationTypesTableSeeder');
		$this->call('CountriesTableSeeder');
                $this->call('UsersSeeder');
		$this->call('ConfigSeeder');
	}

}