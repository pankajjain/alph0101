<?php

class FacilitiesTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		DB::table('facilities')->truncate();

		$accommodationFacilities = array(			
				//List of predefined standard facilities for Accommodations
			array('title' => 'Business Center','type' => 'standard'),
			array('title' => 'Airport Transfer','type' => 'standard'),
			array('title' => '24 Hours Room Service','type' => 'standard'),
			array('title' => 'Baby Sitting','type' => 'standard'),
			array('title' => 'Bicycle Rental','type' => 'standard'),
			array('title' => 'Car Park','type' => 'standard'),
			array('title' => 'Casino','type' => 'standard'),
			array('title' => 'Coffee Shop','type' => 'standard'),
			array('title' => 'Concierge','type' => 'standard'),
			array('title' => 'Disable Facilities','type' => 'standard'),
			array('title' => 'Elevator','type' => 'standard'),
			array('title' => 'Executive Floor','type' => 'standard'),
			array('title' => 'Family Room','type' => 'standard'),
			array('title' => 'Laundry Service/Dry Cleaning','type' => 'standard'),
			array('title' => 'Meeting Facilities','type' => 'standard'),
			array('title' => 'Night Club','type' => 'standard'),
			array('title' => 'Pet Allowed','type' => 'standard'),
			array('title' => 'Poolside Bar','type' => 'standard'),
			array('title' => 'Restaurant','type' => 'standard'),
			array('title' => 'Safetly Deposit Box','type' => 'standard'),
			array('title' => 'Salon','type' => 'standard'),
			array('title' => 'Shop','type' => 'standard'),
			array('title' => 'Shuttle Service','type' => 'standard'),
			array('title' => 'Smoking Area','type' => 'standard'),
			array('title' => 'Tours','type' => 'standard'),
			array('title' => 'Wi-Fi in Public Area','type' => 'standard'),
			array('title' => 'In-room Wi-Fi','type' => 'standard'),
			array('title' => 'Valet Parking','type' => 'standard'),
				//list of sport&recreation facilities for accommodations
			array('title' => 'Fitness Center','type' => 's&r'),
			array('title' => 'Games Room','type' => 's&r'),
			array('title' => 'Garden','type' => 's&r'),
			array('title' => 'Golf Course (On Site)','type' => 's&r'),
			array('title' => 'Golf Course (within 3km)','type' => 's&r'),
			array('title' => 'Hot Spring Bath','type' => 's&r'),
			array('title' => 'Indoor Pool','type' => 's&r'),
			array('title' => 'Jacuzzi','type' => 's&r'),
			array('title' => 'Kids Club','type' => 's&r'),
			array('title' => 'Massage','type' => 's&r'),
			array('title' => 'Outdoor Pool','type' => 's&r'),
			array('title' => 'Pool (kids)','type' => 's&r'),
			array('title' => 'Private Beach','type' => 's&r'),
			array('title' => 'Skiing' ,'type' => 's&r'),
			array('title' => 'Diving' ,'type' => 's&r'),
			array('title' => 'Fishing','type' => 's&r'),
			array('title' => 'Solarium' ,'type'=> 's&r'),
			array('title' => 'Children Playground','type' => 's&r'),
			array('title' => 'Billiards','type' => 's&r'),
			array('title' => 'Sauna' ,'type'=> 's&r'),
			array('title' => 'Spa','type' => 's&r'),
			array('title' => 'Squash Courts' ,'type'=> 's&r'),
			array('title' => 'Tennis Courts' ,'type'=> 's&r'),
			array('title' => 'Water Sports (Motorized)' ,'type' => 's&r'),
			array('title' => 'Water Sports (Non-Motorized)' ,'type'=> 's&r'),
			array('title' => 'Table Tennis' ,'type' => 's&r'),
			array('title' => 'Horse Riding'  ,'type'=> 's&r'),
			array('title' => 'Karaoke' ,'type'=> 's&r'),
			array('title' => 'Steamroom' ,'type'=> 's&r'),				
			);
	$roomFacilities = array(
		array('title' => 'Jacuzzi Bathtub','type' => 'roomFacility'),
		array('title' => 'Private Pool','type' => 'roomFacility'),
		array('title' => 'Balcony/Terrace','type' => 'roomFacility'),
		array('title' => 'Air Conditioning','type' => 'roomFacility'),
		array('title' => 'Bathrobes','type' => 'roomFacility'),
		array('title' => 'Bathtub','type' => 'roomFacility'),
		array('title' => 'Coffee/Tea Maker','type' => 'roomFacility'),
		array('title' => 'Complimentary Bottled Water','type' => 'roomFacility'),
		array('title' => 'Daily Newspaper','type' => 'roomFacility'),
		array('title' => 'Non Smoking Rooms','type' => 'roomFacility'),
		array('title' => 'Desk','type' => 'roomFacility'),
		array('title' => 'Hair Dryer','type' => 'roomFacility'),
		array('title' => 'Ironing Facilities','type' => 'roomFacility'),
		array('title' => 'In room Safe','type' => 'roomFacility'),
		array('title' => 'Television LCD/Plasma screen','type' => 'roomFacility'),
		array('title' => 'Shower','type' => 'roomFacility'),
		array('title' => 'Mini Bar','type' => 'roomFacility'),
		array('title' => 'Satellite/ Cable TV','type' => 'roomFacility'),
		array('title' => 'Free WiFi-Access','type' => 'roomFacility'),
		array('title' => 'WiFi Access (Charges Apply)','type' => 'roomFacility'),
		array('title' => 'Free LAN Access','type' => 'roomFacility'),
		array('title' => 'Seating area','type' => 'roomFacility'),
		array('title' => 'DVD/CD Player','type' => 'roomFacility'),
		array('title' => 'In House Movie','type' => 'roomFacility'),
		array('title' => 'Separate Shower and tub','type' => 'roomFacility'),
		array('title' => 'Kitchenette','type' => 'roomFacility'),
		array('title' => 'Refrigerator','type' => 'roomFacility')
		);

		// Uncomment the below to run the seeder
DB::table('facilities')->insert($accommodationFacilities);
DB::table('facilities')->insert($roomFacilities);
}

}
