<?php

class UsersSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		DB::table('users')->truncate();

		$users = array(

			array(
				'email' => 'user@rq.com',
				'password' => '$2y$10$WLse3qKsE8mo3cvPfa2P1ufjNJTV3NLFS2lXbgfrULL9IAFL6M9f.',
				'typeID' => '1',
				'firstName' => 'User',
				'lastName' => 'RQ',
				'activated' => '1'),

			array(
				'email' => 'admin@rq.com',
				'password' => '$2y$10$WLse3qKsE8mo3cvPfa2P1ufjNJTV3NLFS2lXbgfrULL9IAFL6M9f.',
				'typeID' => '2',
				'firstName' => 'Admin',
				'lastName' => 'RQ',
				'activated' => '1'),

		);

		// Uncomment the below to run the seeder
		DB::table('users')->insert($users);
	}
}