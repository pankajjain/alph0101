<?php

class ConfigSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		DB::table('configs')->truncate();

		$config = array(

			array(
				'id' => 2,
				'key' => 'flashDealStartDay',
				'value' => 'Friday'
				),

			array(
				'id' => 3,
				'key' => 'flashDealStartTime',
				'value' => '1400'
				),

			array(
				'id' => 4,
				'key' => 'flashDealEndDay',
				'value' => 'Friday'
				),

			array(
				'id' => 5 ,
				'key' => 'flashDealEndTime',
				'value' => '1600'
				),

			array(
				'id' => 1,
				'key' => 'CurrencyMarkup',
				'value' => '0.04'
				),

			array(
				'id' => 6,
				'key' => 'AgreeDocVer',
				'value' => '1'
				),
		);

		// Uncomment the below to run the seeder
		DB::table('configs')->insert($config);
	}

}
