<?php 
class Flashdeal extends BaseModel{
	protected $guarded = array();
	protected $softDelete = true;

	public static $rules = array(
		"flashDealName" => 'required',
		"roomID" => "required",
		"summary" => 'required',
		"highlights" => "required",
		"originalPrice" => "required|numeric",
		"discountPercentage" => "required|numeric|max:100",
		"discountedPrice" => "required|numeric",
		"flashDealAmount" => "required",
		"flashDealDuration" => "required",
		"minimumDaysPriorBooking" => "required|numeric",
		"redemptionPeriodTo" => "required",
		"redemptionPeriodFrom" => "required",
		//"bookingPeriodFrom" => "required",
		//"bookingPeriodTo" => "required",
		"flashDealPolicies" => "required",
	);


    public function isTime(){
		
         $day = (date("l") == Configs::where("key","flashDealStartDay")->first()->value);
        $timeS = (date("Gi") > Configs::where("key","flashDealStartTime")->first()->value);
        $timeE = (date("Gi") < Configs::where("key","flashDealEndTime")->first()->value);
		return ($day && $timeS && $timeE);
	}
	
	public function room(){
		return $this->belongsTo('Room','roomID');
	}

	public function accomm(){
		return $this->belongsTo('Accomm','accommID');
	}

	public function photos(){
		//returns all photos belonging to the room behind this voucher
		return RoomPhoto::where('roomID',$this->roomID)->get();
	}

}

 