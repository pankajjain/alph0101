<?php

class RoomFacility extends BaseModel {
	protected $guarded = array();
	protected $table = "roomfacilities";
	public static $rules = array();

	public function room(){
		return $this->belongsTo('Room','roomID');
	}


	public function createdMessage(){
		return 'A  facility(' . ucwords($this->facility) . ') added for "' . ucwords($this->room->name) . '" room';
	}
	public function updatedMessage(){
		return 'The facility(' . ucwords($this->facility) . ') updated for "' . ucwords($this->room->name) . '" room';
	}
	public function deletedMessage(){
		return 'The facility(' . ucwords($this->facility) . ') was deleted from "' . ucwords($this->room->name) . '" room';
	}
}
