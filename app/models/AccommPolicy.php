<?php

class AccommPolicy extends Eloquent {
	protected $guarded = array();
	protected $table = "accommpolicies";
	public static $rules = array();
	protected static $loggable = false;

	public static function getAgeArray($start =1,$end = 100){
		$age = array();
		for($i=$start ; $i <= $end ; $i++){
			$age[$i] = $i;
		}
		return $age;
	}


}
