<?php
class BaseModel extends Eloquent {


	public $errors;
	protected static $loggable = true;

	public function created_at($format=null){
		if(is_null($format)){
			return $this->created_at;
		};

		return date($format,strtotime($this->created_at));
	}

	public function validate($messages = false)
	{
		if($messages === false) 
		{
			$v = Validator::make($this->attributes, static::$rules);

		}else {
			$v = Validator::make($this->attributes, static::$rules,static::$messages);
		}
		if ($v->passes()) return true;


		$this->errors = $v->messages();


		return false;

	}

	public function encryptedKey(){
		return Crypt::encrypt($this->getKey());
	}

	public static function random($take=5){
		return self::orderBy( DB::raw('RAND()'))->take($take)->get();

	}	


	public static function boot()
	{
		parent::boot();
		if(static::$loggable == false){
			return 0;
		}
		self::created(function($model)
		{
			Activity::create(array(
				'userID' => Sentry::getUser()->getKey(),
				'message' => $model->createdMessage(),
				'model' => get_class($model),
				'action' => 'created',
				'refrenceID' => $model->getKey()
				)
			);
		});

		self::updated(function($model)
		{
			Activity::create(array(
				'userID' => Sentry::getUser()->getKey(),
				'message' => $model->updatedMessage(),
				'model' => get_class($model),
				'action' => 'updated',
				'refrenceID' => $model->getKey()
				)
			);
		});

		self::deleted(function($model)
		{
			Activity::create(array(
				'userID' => Sentry::getUser()->getKey(),
				'message' => $model->deletedMessage(),
				'model' => get_class($model),
				'action' => 'deleted',
				'refrenceID' => $model->getKey()
				)
			);
		});

	}


	public function createdMessage(){
		return "A new " . get_class($this) . " created";
	}
	public function updatedMessage(){
		return "A  " . get_class($this) . " updated";
	}
	public function deletedMessage(){
		return "A  " . get_class($this) . " deleted";
	}




}