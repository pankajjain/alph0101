<?php

class Facility extends BaseModel {
	protected $guarded = array();
	protected $table = "facilities";
	public static $rules = array();

	public static function getStandardFacilities(){
		return Facility::where('type','standard')->get();
	}

	public static function getSportAndRecreationFacilities(){
		return Facility::where('type','s&r')->get();
	}

	public static function getRoomFacilities(){
		return Facility::where('type','roomFacility')->get();
	}
}
