<?php

class Accomm extends BaseModel {
	protected $table = "accommodations";
  protected $guarded = array();

  public function rooms(){
    return $this->hasMany('Room','accommID');
  }
  public function flashdeal(){
    return $this->hasMany('Flashdeal','accommID');
  }
  public function photo(){
    return $this->hasMany('AccommPhotos','accommID');
  }

  public static $accommodationMainContactRoles = array(
    'E-commerce'=>'E-commerce',
    'Finanace'=>'Finanace',
    'Owner'=>'Owner',
    'Reservations' => 'Reservations',
    'Revenue'=>'Revenue',
    'Sales'=>'Sales',
    'Other'=>'Other'

    );

  public static $rules = array (
    "name" => "required|unique:accommodations,name",
    "starRating" => "required|max:5|numeric",
    "numberOfRooms" => "required|numeric|min:1",
    "country" => "required",        
    "province" => "required",
    "city" => "required",
    "typeID" => "required",
    "zipCode" => "required|numeric",
    "contactNumber" => "numeric",
    "fax" => "numeric",
   // "website" => "active_url",
    "website" => "url",
    "streetAddress" => "required",        
    "contactNumber" => "required|numeric",
    "mainContactName" => "required",
    "mainContactRole" => "required",
    "mainContactEmail" => "required|email",
    "mainContactMobile" => "numeric",
    );  




public function certificates(){
   return Upload::where('type','VerificationDocument')->where('refrenceID',$this->id)->get();
}

public function getFacilities($type='standard'){
  return AccommFacility::where('accommID',$this->id)->where('type',$type)->orderBy('facility')->get();
}

public function getSrFacilities($type='s&r'){
  return AccommFacility::where('accommID',$this->id)->where('type',$type)->orderBy('facility')->get();
}

/*public function getInternetUses(){
	
  return Accomm::where('id',$this->id)->get();
}
*/
public function getFacilitiesCount($type='standard'){
  return AccommFacility::where('accommID',$this->id)->where('type',$type)->count();
}

public function policies(){
  return $this->hasOne('AccommPolicy','accommID');
}

public function vouchers(){
  return $this->hasMany('Voucher','accommID')->where('redemptionPeriodFrom', '<=', date('Y-m-d H:i:s'))
				->where('redemptionPeriodTo', '>=', date('Y-m-d H:i:s'));
				
}

public function partnerVouchers(){
  return $this->hasMany('Voucher','accommID');
				
}

public function partnerFlashdeals(){
  return $this->hasMany('Flashdeal','accommID');
}

public function flashdeals(){
  return $this->hasMany('Flashdeal','accommID')->where('redemptionPeriodFrom', '<=', date('Y-m-d H:i:s'))
				->where('redemptionPeriodTo', '>=', date('Y-m-d H:i:s'));
}


public function getCountry(){
  return DB::table('countries')->where('iso', $this->country)->pluck('name');
}

public function getVerificationDocuments()
{
  return Upload::where('refrenceID',$this->getKey())->where('type','rqacommcert')->get();
}

public function user()
{
  return $this->belongsTo('User','userID');
}

public function accommType(){
  return $this->belongsTo('AccommType','typeID');
}

/*public function createdMessage(){
  return 'A new accommodation(' . ucwords($this->name) . ') created by "' . ucwords(Sentry::getUser()->getFullName()) ;
}
public function updatedMessage(){
  return 'The accommodation(' . ucwords($this->name) . ')  was updated by "' . ucwords(Sentry::getUser()->getFullName());
}
public function deletedMessage(){
  return 'The accommodation(' . ucwords($this->name) . ')  was deleted by "' . ucwords(Sentry::getUser()->getFullName());
}
*/


public function createdMessage(){
  return 'A new accommodation(' . ucwords($this->name) . ') created.' ;
}
public function updatedMessage(){
  return 'The accommodation(' . ucwords($this->name) . ')  was updated.';
}
public function deletedMessage(){
  return 'The accommodation(' . ucwords($this->name) . ')  was deleted.';
}


}

?>