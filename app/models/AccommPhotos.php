<?php

class AccommPhotos extends BaseModel {
	protected $guarded = array();
	protected $table = "accommphotos";
	public static $rules = array();


	public function getPhotoPath(){
		$filePath = Config::get('roomquickly.acoommodationPhotosDirectory');
		$filePath = $filePath . $this->accommID . '/' . $this->fileName;
		return $filePath;
	}

	public function getThumbPath(){
		$filePath = Config::get('roomquickly.acoommodationPhotosDirectory');
		$filePath = $filePath . $this->accommID . '/thumb/' . $this->fileName;
		return $filePath;
	}

	public function getPhotoURL(){
		// $url = '/accomms/';
		// $url .=  $this->accommID . '/' . $this->fileName;
		$s3 = AWS::get('s3');
		$theURL = $s3->getObjectUrl('rqphoto',$this->fileName);
		return $theURL;

	}
	public function getThumbURL(){
		// $url = '/accomms/';
		// $url .= $this->accommID . '/thumb/' . $this->fileName;
		$s3 = AWS::get('s3');
		$thumb = "268_".$this->fileName;
		$theURL = $s3->getObjectUrl('rqphoto',$thumb);
		return $theURL;
	}

	public function deletePhoto(){
		$this->delete();
	}

	public function createdMessage(){
		return "A new photo added for '" . Accomm::find($this->accommID)->name . "'";
	}
	public function updatedMessage(){
		return "Details of the photo (" . $this->fileName .  ") belonging to "   
		. Accomm::find($this->accommID)->name  . " updated";
	}
	public function deletedMessage(){
		return "Photo (" . $this->fileName .  ") belonging to "   
		. Accomm::find($this->accommID)->name  . " deleted";
	}
}
