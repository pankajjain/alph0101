<?php

class Voucher extends BaseModel {
	protected $guarded = array();
	protected $softDelete = true;
	protected $table = 'vouchers';

	public static $rules = array(
		"voucherName" => 'required',
		"roomID" => "required",
		"summary" => 'required',
		"highlights" => "required",
		"originalPrice" => "required|numeric",
		"discountPercentage" => "required|numeric|max:100",
		"discountedPrice" => "required|numeric",
		"voucherAmount" => "required",
		"voucherDuration" => "required",
		"minimumDaysPriorBooking" => "required|numeric",
		"redemptionPeriodTo" => "required",
		"redemptionPeriodFrom" => "required",
		"voucherPolicies" => "required",



	);

	public function room(){
		return $this->belongsTo('Room','roomID');
	}

	public function accomm(){
		
		return $this->belongsTo('Accomm','accommID');
	}

	public function photos(){
		//returns all photos belonging to the room behind this voucher
		return RoomPhoto::where('roomID',$this->roomID)->get();
	}

	public function discountPercentage(){
		return floor($this->discountPercentage);
	}

	
	public function getStatus(){
		return $this->status == 'saved' ? 'Draft' : 'Published';
	}

	public static function mostPopular($quantity=8){
		return Voucher::get()->take($quantity);
	}

	public function createdMessage(){
		return "A new " . get_class($this) . " created";
	}
	public function updatedMessage(){
		return "A  " . get_class($this) . " updated";
	}
	public function deletedMessage(){
		return "A  " . get_class($this) . " deleted";
	}

}
