<?php

class RoomPhoto extends BaseModel {
	protected $guarded = array();
	protected $table = "roomPhotos";
	public static $rules = array();

	public function getPhotoURL(){
		$s3 = AWS::get('s3');
		$theURL = $s3->getObjectUrl($this->type,$this->fileName);
		return $theURL;

	}

	public function room(){
		return $this->belongsTo('Room','roomID');
	}

	public function deletePhoto(){
		$s3 = AWS::get('s3');
		$s3->deleteObject(array(
		    'Bucket'     => $this->type,
		    'Key'        => $this->fileName,
		));
		$this->delete();
	}

	public function createdMessage(){
		return "A new photo added for '" . ucwords($this->room->name) . "' room";
	}
	public function updatedMessage(){
		return "Photo details updated for '" . ucwords($this->room->name) . "' room";
	}
	public function deletedMessage(){
		return "Photo (" . $this->fileName .  ") belonging to room '"   
		. ucwords($this->room->name)  . "' deleted";
	}

}