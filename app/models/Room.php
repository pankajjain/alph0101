<?php

class Room extends BaseModel {
	protected $guarded = array();
	protected $softDelete = true;

	public static $rules = array(
		'name' => 'required',
		'bedType' => 'required',
		'maxGuestsAllowed' => 'required|numeric|min:1',
		'originalPrice' => 'required|numeric|min:1',
		'roomSize' => 'required|numeric|min:1',
		'roomAvailability' => 'required|numeric|min:1',
		'description' => 'required'
		);

	
	public static $bedTypes = array(
		'1 Single Bed' ,
		'1 Double Bed' ,
		'3 Single Bed' ,
		'1 Queen Bed' ,
		'1 King Bed' , 
		'Bunk Bed',
		'1 Double Bed or 2 Single Beds',
		'2 Single Beds',
		'2 Double Beds',
		'3 Single Beds',
		'2 Queen Beds',
		'2 King Beds',
		'Sofa Bed'
		);


	public static $roomViews = array(
		'Ocean',
		'Pool',
		'City'
		);



	public function photos(){
		return $this->hasMany('RoomPhoto','roomID');
	}



	public function accomm(){
		return $this->belongsTo('Accomm','accommID');
	}

        public function facilities(){
                return $this->hasMany('RoomFacility', 'roomID');
        }

	public static function getBedTypes($associative = false){
		if($associative){
			return array_combine(self::$bedTypes, self::$bedTypes);
		}
		return self::$bedTypes;
	}

	public static function getRoomViews($associative = false){
		if($associative){
			return array_combine(self::$roomViews, self::$roomViews);
		}
		return self::$roomViews;
	}

	public static function published($accommKey){
		return Room::where('accommID', $accommKey)->where('status', '=', 'published')->get();
	}

	public static function deleted($accommKey){
		return Room::where('accommID', $accommKey)->where('status', '=', 'deleted')->get();
	}

	public function createdMessage(){
		return 'A new room(' . ucwords($this->name) . ') created for "' . ucwords($this->accomm->name) . '"';
	}
	public function updatedMessage(){
		return 'A room(' . ucwords($this->name) . ') was updated from "' . ucwords($this->accomm->name) . '"';
	}
	public function deletedMessage(){
		return 'A room(' . ucwords($this->name) . ') was deleted from "' . ucwords($this->accomm->name) . '"';
	}

	public function voucher(){
		return $this->hasMany('Voucher','roomID');
	}
	public function flashdeal(){
		return $this->hasMany('Flashdeal','roomID');
	}

	public function hasActiveVouchers(){
		if($count = Voucher::where('roomID',$this->getKey())->where('status','published')->get()->count()){
			return $count;
		}
		return 0;

	}

	public function hasActiveFlashDeals(){
		if($count =Flashdeal::where('roomID',$this->getKey())->where('status','published')->get()->count()){
			return $count;
		}
		return 0;

	}

	public function numberOfVouchers(){
		if($count = Voucher::where('roomID',$this->getKey())->get()->count()){
			return $count;
		}
		return 0;

	}

	public function numberOfFlashDeals(){
		if($count =Flashdeal::where('roomID',$this->getKey())->get()->count()){
			return $count;
		}
		return 0;

	}

}
