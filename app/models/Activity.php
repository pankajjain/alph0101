<?php 
class Activity extends BaseModel{

	protected $table="activitylogs";
	protected $guarded = array();
	protected static $loggable = false;




	public static function logLogin(){
		$userID = Sentry::getUser()->getKey();
		$activity = 'login';
		Activity::create(array('userID' => $userID,'activity' => $activity));
	}


	public static function logAccommCreate($params){
		$accommID = $params['accommID'];
		$activity = 'NewAccommodation';
		$userID = $params['userID']?: Sentry::getUser()->getKey();
		Activity::create(array(
			'userID' => $userID,
			'activity' => $activity,
			'accommID' => $accommID
			)
		);

	}






}


 ?>