<?php

class AccommFacility extends BaseModel {
	protected $guarded = array();
	protected $table = "accommfacilities";
	public $timestamps = false;
	public static $rules = array();

	public function createdMessage(){
		return "A new facility ('" . $this->facility . "') added for '" . Accomm::find($this->accommID)->name . "'";
	}
	public function updatedMessage(){
		return "A  " . get_class($this) . " updated";
	}
	public function deletedMessage(){
		return "A  facility ('" . $this->facility . "') was removed from '" . Accomm::find($this->accommID)->name . "'";
	}

}
