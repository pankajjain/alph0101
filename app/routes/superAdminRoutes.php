<?php

/*
|--------------------------------------------------------------------------
| Super Admin Routes
|--------------------------------------------------------------------------
|
| Here is where  we register all of our routes related to Admin Panel.
|
*/

Route::group(array('prefix' => 'admin','before' => 'admin'),function(){

	//Index page for super admins
	Route::get('/',array('as' => 'adminIndex','uses' => 'AdminController@index'));

	Route::get('applications',array('as' => 'applications','uses' => 'AdminController@application'));
	Route::get('archived',array('as' => 'archived','uses' => 'AdminController@archived'));
	Route::get('users',array('as' => 'users','uses' => 'AdminController@users'));
	
	Route::get('soldvoucher',array('as' => 'soldvoucher','uses' => 'AdminController@soldvoucher'));
	Route::get('soldflashdeal',array('as' => 'soldflashdeal','uses' => 'AdminController@soldflashdeal'));
	
	Route::get('pages',array('as' => 'pages','uses' => 'PageController@index'));
	Route::get('createPage',array('as' => 'createPage','uses' => 'PageController@create'));
	Route::post('storePage',array('as' => 'storePage','uses' => 'PageController@store'));
	Route::get('edit-page/{id}',array('as' => 'edit-page','uses' => 'PageController@edit'));
	Route::get('delete-page/{id}',array('as' => 'delete-page','uses' => 'PageController@delete'));
	Route::post('update/{id}',array('as' => 'update','uses' => 'PageController@update'));
	

	Route::get('activitylog',array('as' => 'activityLog', 'uses' => 'AdminController@showActivityLog'));
	
	//approve a certain accommodation 
	Route::get('approve/{id}',array('as' => 'approveAccommodation','uses' => 'AdminController@approveAccommodation'));
	Route::get('delete/{id}',array('as' => 'sudeleteAccommodation','uses' => 'AdminController@deleteAccommodation'));
	Route::post('disapprove/{id}',array('as' => 'disapproveAccommodation','uses' => 'AdminController@disapproveAccommodation'));

	//display details of a specific accomodation
	Route::get('viewaccommodation/{accomm}',array('as' => 'viewAccommodation' ,'uses' => 'AdminController@viewAccommodation'));
	
	//settings page for super admins
	Route::get('settings',array( 'as' => 'appsettings' , 'uses' => 'AdminController@showSettings'));
	Route::get('test',array('as' => 'test','uses' => 'AdminController@test'));
	Route::post('uploadAgreement',array('as' => 'uploadVeridoc','uses' => 'AdminController@uploadVeridoc'));
	Route::post('updateCurrencyMarkup',array('as' => 'updateCurrency','uses' => 'AdminController@updateCurrency'));

	Route::get('listing',array( 'as' => 'suReportHotelListing' , 'uses' => 'AdminController@suReportHotelListing'));
	Route::get('getlisting',array( 'as' => 'getsuReportHotelListing' , 'uses' => 'AdminController@getsuReportHotelListing'));
	Route::get('summary',array( 'as' => 'suReportHotelSummary' , 'uses' => 'AdminController@suReportHotelSummary'));
	Route::get('getsummary',array( 'as' => 'getsuReportHotelSummary' , 'uses' => 'AdminController@getsuReportHotelSummary'));
	
	Route::get('getuserreview',array( 'as' => 'userReviewRating' , 'uses' => 'ReviewsRatingController@getUserReviewRatingInAdmin'));

});
