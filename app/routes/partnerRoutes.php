<?php
/*
|--------------------------------------------------------------------------
| Accommodation Partner  Routes
|--------------------------------------------------------------------------
|
| Here is where  we register all of our routes related to Partners Panel
|
*/
//For all routes  with {accomm} parameter we inject the Accommodation to their  view.


Route::group(array('prefix'=>'partners','before' => 'partners|ownershipChecker'), function()
{


	Route::model('accomm', 'Accomm');

	Route::get('/',array('as' => 'partnersIndex','uses' => 'AccommodationsController@index'));
	Route::get('newaccomm', array('as' => 'newAccommodation', 'uses' => 'AccommodationsController@createAccommodation'));
	Route::post('newaccomm', array('as' => 'postNewAccommodation', 'before' => 'csrf' , 'uses' => 'AccommodationsController@storeAccommodation'));
	Route::get('editaccomm/{accomm}',array('as' => 'editAccommodation','uses' => 'AccommodationsController@edit'));
	Route::get('editaccommalt/{accomm}',array('as' => 'editAccommodationWithSideMenu','uses' => 'AccommodationsController@editWithSideMenu'));
	Route::post('editaccomm/{accomm}',array('as' => 'postEditAccommodation','uses' => 'AccommodationsController@update'));
	Route::get('deleteaccomm/{accomm}',array('as' => 'deleteAccommodation' , 'uses' => 'AccommodationsController@delete'));
	Route::post('deleteaccomm/{accommid}',array('as' => 'postDeleteAccommodation','uses' => 'AccommodationsController@destroy'));
	Route::get('removefile/{id}',array('as' => 'removeFile' , 'uses' => 'UploadsController@destroy'));

	Route::get('accomm/{accomm}/activity',array('as' => 'activityLogPartner' , 'uses' => 'AccommodationsController@showActivityLogPartner'));
	
	Route::get('accomm/{accomm}',array('as' => 'accommodationIndex' , 'uses' => 'AccommodationsController@accommodationIndex'));
	Route::get('facilityCount/{accommid}',array('as' => 'getFacilityCount' ,'uses' => 'AccommodationsController@getFacilityCount'));
	Route::get('srfacilityCount/{accommid}',array('as' => 'getsrFacilityCount' ,'uses' => 'AccommodationsController@getsrFacilityCount'));

	Route::get('accomm/{accomm}/xtrainfo',array('as' => 'accommodationXtraInfo' , 'uses' => 'AccommodationsController@accommodationXtraInfo'));
	Route::post('accomm/{accomm}/xtrainfo',array('as' => 'postAccommodationXtraInfo' , 'uses' => 'AccommodationsController@updateAccommodationXtraInfo'));
	
	Route::get('accomm/{accomm}/setting',array('as' => 'accommodationSetting' , 'uses' => 'AccommodationsController@accommodationSetting'));
	Route::post('accomm/{accomm}/setting',array('as' => 'postAccommodationSetting' , 'uses' => 'AccommodationsController@updateAccommodationSetting'));

	Route::get('accomm/{accomm}/map',array('as' => 'accommmodationMap' , 'uses' => 'AccommodationsController@accommodationMap'));
	Route::post('accomm/{accomm}/map',array('as' => 'accommmodationMapStore' , 'uses' => 'AccommodationsController@accommodationMapStore'));

	Route::post('accomm/addfacility',array('as' => 'addFacility' , 'uses' => 'AccommodationsController@addFacility'));
	Route::get('accomm/{accomm}/facilities',array('as' => 'accommodationFacilities' , 'uses' => 'AccommodationsController@accommodationFacilities'));

	Route::get('accomm/{accomm}/sarfacilities',array('as' => 'accommodationSarFacilities' , 'uses' => 'AccommodationsController@accommodationSarFacilities'));

	Route::post('accomm/addfacility',array('as' => 'addFacility' , 'uses' => 'AccommodationsController@addFacility'));
	Route::post('accomm/removefacility',array('as' => 'removeFacility', 'uses' => 'AccommodationsController@removeFacility'));

	Route::get('accomm/{accomm}/policies',array('as' => 'accommodationPolicies' , 'uses' => 'AccommodationsController@accommodationPolicies'));
	Route::post('accomm/{accomm}/policies',array('as' => 'postAccommodationPolicies' , 'uses' => 'AccommodationsController@updateAccommodationPolicies'));

	Route::get('accomm/{accomm}/photos',array('as' => 'accommodationPhotos' , 'uses' => 'AccommodationsController@acccommodationPhotos'));
	Route::post('accomm/{accomm}/photos',array('as' => 'accommPhotoUploadHandler', 'uses' => 'AccommodationsController@storePhotos'));
	Route::post('accomm/{accomm}/photos/{id}',array('as' => 'photoDetailsUpdate', 'uses' => 'AccommodationsController@updatePhotoDetails'));
	Route::post('accomm/{accomm}/removephoto',array('as' => 'accommPhotoRemove', 'uses' => 'AccommodationsController@removePhoto'));

 

	//-----------------------Accomodation Rooms --------------------
	Route::group(array('prefix'=>'accomm/{accomm}/rooms'), function()
	{

		Route::get('/',array('as' => 'roomsIndex', 'uses' => 'RoomsController@index'));
		Route::get('create',array('as' => 'createRoom', 'uses' => 'RoomsController@create'));
		Route::post('create',array('as' => 'PostcreateRoom', 'uses' => 'RoomsController@store'));

		Route::get('edit/{roomID}',array('as' => 'editRoom', 'uses' => 'RoomsController@editRoom'));
		Route::post('edit/{roomID}',array('as' => 'postEditRoom', 'uses' => 'RoomsController@update'));

		Route::get('delete/{roomID}',array('as' => 'removeRoom', 'uses' => 'RoomsController@destroy'));
		Route::post('removephoto/',array('as' => 'removeRoomPhoto' ,'uses' => 'RoomsController@removeRoomPhoto'));

	});
	// -----------------------Accomodation vouchers --------------------
	Route::group(array('prefix'=>'accomm/{accomm}/vouchers'), function()
	{

		Route::get('/',array('as' => 'voucherIndex', 'uses' => 'VouchersController@index'));
		Route::get('create',array('as' => 'createVoucher', 'uses' => 'VouchersController@create'));
		Route::post('create',array('as' => 'PostcreateVoucher', 'uses' => 'VouchersController@store'));
		Route::get('edit/{voucherID}',array('as' => 'editVoucher', 'uses' => 'VouchersController@edit'));
		Route::post('edit/{voucherID}',array('as' => 'updateVoucher', 'uses' => 'VouchersController@update'));
		Route::get('delete/{voucherID}',array('as' => 'removeVoucher', 'uses' => 'VouchersController@destroy'));

	});

	Route::group(array('prefix'=>'accomm/{accomm}/flashdeals'), function()
	{

		Route::get('/',array('as' => 'flashDealIndex', 'uses' => 'FlashDealsController@index'));
		Route::get('create',array('as' => 'createflashDeal','before' => 'oneFlashDealPerAccomm', 'uses' => 'FlashDealsController@create'));
		Route::post('create',array('as' => 'PostcreateflashDeal', 'uses' => 'FlashDealsController@store'));
		Route::get('edit/{flashdealID}',array('as' => 'editFlashdeal', 'uses' => 'FlashDealsController@edit'));
		Route::post('edit/{flashdealID}',array('as' => 'updateFlashdeal', 'uses' => 'FlashDealsController@update'));
		Route::get('delete/{id}',array('as' => 'removeflashDeal', 'uses' => 'FlashDealsController@destroy'));

	});
	
	//-----------------------Accomodation Report --------------------
	Route::group(array('prefix'=>'accomm/{accomm}/partnerreport'), function()
	{

		Route::get('/',array('as' => 'partnerReportIndex', 'uses' => 'PartnerreportController@index'));
		

	});
	
	//-----------------------Accomodation Report --------------------
	Route::group(array('prefix'=>'accomm/{accomm}/reviewsrating'), function()
	{

		Route::get('/',array('as' => 'reviewsRatingIndex', 'uses' => 'ReviewsRatingController@index'));
		
		

	});
	
	//-----------------------Sold Voucher --------------------
	Route::group(array('prefix'=>'accomm/{accomm}/soldvoucher'), function()
	{

		Route::get('/',array('as' => 'soldVoucherIndex', 'uses' => 'VouchersController@soldVoucher'));
		

	});
	
	
	//-----------------------Sold FlashDeal --------------------
	Route::group(array('prefix'=>'accomm/{accomm}/soldflashdeal'), function()
	{

		Route::get('/',array('as' => 'soldFlashDealIndex', 'uses' => 'FlashDealsController@soldFlashDeals'));
		

	});
	
	

	//-----------------------Voucher for flashdeals ajax--------------------
	
	//Route::get('ajaxvoucher/{vID}',array('as' => 'ajaxVoucher', 'uses' => 'FlashDealsController@ajaxVoucher'));
	Route::group(array('prefix'=>'accomm/{accomm}/ajaxvoucher'), function()
	{

		Route::get('/{voucherId}',array('as' => 'ajaxVoucher', 'uses' => 'FlashDealsController@ajaxVoucher'));
		

	});
	
	//-----------------------Validate Voucher --------------------
	Route::group(array('prefix'=>'accomm/{accomm}/validateVoucher'), function()
	{

		Route::any('/',array('as' => 'validateVoucher', 'uses' => 'VouchersController@validateVoucher'));
		Route::post('validVoucher}',array('as' => 'validVoucher', 'uses' => 'VouchersController@validVoucher'));

	});
	
	//-----------------------Validate Flashdeal --------------------
	
	Route::group(array('prefix'=>'accomm/{accomm}/validateFlashDeal'), function()
	{

		Route::any('/',array('as' => 'validateFlashDeal', 'uses' => 'FlashDealsController@validateFlashDeal'));
		Route::post('validFlashDeal}',array('as' => 'validFlashDeal', 'uses' => 'FlashDealsController@validFlashDeal'));

	});	
	
});

