<?php


Route::get('/',array('as' => 'publicIndex','uses' => 'PublicController@index'));

Route::get('lang/{countryCode}',array('as' => 'langSetter' , 'uses' => 'PublicController@languageSetter'));
Route::get('curr/{countryCode}',array('as' => 'currSetter' , 'uses' => 'PublicController@currencySetter'));
Route::get('nearby',array('as' => 'searchNearby' , 'uses' => 'PublicController@searchNearby'));


Route::get('partnersignup',array('as' => 'signupPartners' , 'uses' => 'PublicController@partnerSignUpCreate'));
Route::get('membersignup',array('as' => 'signupMembers' , 'uses' => 'PublicController@memberSignupCreate'));
Route::post('partnersignup',array('as' => 'postSignup' , 'uses' => 'PublicController@SignUpStore'));

Route::get('signin',array('as' => 'signin' , 'uses' => 'PublicController@signin'));
Route::post('signin',array('as' => 'postSignin' , 'uses' => 'AuthController@login'));
Route::post('resetCredential',array('as' => 'postResetCredential' , 'uses' => 'AuthController@resetCredential'));
Route::post('resentValidation',array('as' => 'postResentValidation' , 'uses' => 'AuthController@resentValidation'));
Route::get('signout',array('as' => 'signout', 'uses' => 'AuthController@logout'));

Route::get('changePassword', array('as' => 'changepassword', 'uses' => 'AuthController@changePassword'));
Route::post('changePassword', array('as' => 'postChangepassword', 'uses' => 'AuthController@postChangePassword'));

Route::get('confirmuser/{userID}',array('as' => 'confirmUser','uses' => 'AuthController@confirmUser'));
Route::post('updateUser',array('as' => 'updateUser' ,'uses' => 'PublicController@updateUser'));

Route::post('postReview/{accommID}',array('as' => 'postReview' , 'uses' => 'PublicController@saveReview'));
Route::any('search',array('as' => 'sampleSearch' ,'uses' => 'PublicController@searchHotel'));
Route::any('ajaxSearch',array('as' => 'ajaxSearch' ,'uses' => 'PublicController@ajaxSearch'));
Route::any('homepageSearchBox',array('as' => 'homepageSearchBox' ,'uses' => 'PublicController@homepageSearchBox'));
Route::any('homePageSearch',array('as' => 'homePageSearch' ,'uses' => 'PublicController@homePageSearch'));

Route::any('hotel',array('as' => 'listHotel' ,'uses' => 'PublicController@listHotel'));
Route::any('hotel/{id}',array('as' => 'sampleHotel' ,'uses' => 'PublicController@showHotel'));

Route::any('voucher',array('as' => 'voucherListing' ,'uses' => 'PublicController@voucherListing'));
Route::any('voucher/{id}',array('as' => 'showVoucherDetails' ,'uses' => 'PublicController@showVoucherDetails'));

Route::any('flashdeal',array('as' => 'flashdealListing' ,'uses' => 'PublicController@flashdealListing'));
Route::any('flashdeal/{id}',array('as' => 'showFlashDealDetails' ,'uses' => 'PublicController@showFlashDealDetails'));

Route::get('myaccount',array('as' => 'sampleMyAccount' ,'uses' => 'PublicController@showSampleMyAccount'));


/*Route::get('voucherPurchase/{vID}',array('as' => 'voucherPurchase' ,'uses' => 'PaypalController@voucherPurchase'));
Route::get('completeVoucherPurcahse',array('as' => 'completeVoucherPurcahse' ,'uses' => 'PaypalController@completeVoucherPurcahse'));

Route::get('flashDealPurchase/{flashID}',array('as' => 'flashDealPurchase' ,'uses' => 'PaypalController@flashDealPurchase'));
Route::get('completeFlashDealPurcahse/{flashID}',array('as' => 'completeFlashDealPurcahse' ,'uses' => 'PaypalController@completeFlashDealPurcahse'));*/


Route::get('voucherPurchase/{vID}',array('as' => 'voucherPurchase' ,'uses' => 'PaypalController@voucherPurchase'));
Route::any('completeVoucherPurcahse',array('as' => 'completeVoucherPurcahse' ,'uses' => 'PaymentController@completeVoucherPurcahse'));


Route::any('paymentTransactionUpdate',array('as' => 'paymentTransactionUpdate' ,'uses' => 'PaymentController@paymentTransactionUpdate'));

Route::get('flashDealPurchase/{flashID}',array('as' => 'flashDealPurchase' ,'uses' => 'PaypalController@flashDealPurchase'));
Route::get('completeFlashDealPurcahse/{flashID}',array('as' => 'completeFlashDealPurcahse' ,'uses' => 'PaypalController@completeFlashDealPurcahse'));



Route::get('contact-us',array('as' => 'contact','uses' => 'contactController@index'));
Route::post('contactPost',array('as' => 'contactPost','uses' => 'contactController@contactStore'));

//Route::get('page/{id}',array('as' => 'page','uses' => 'PageController@page'));

Route::get('voucherOrderDetail/{vID}/{oID}',array('voucherOrderDetail' => 'page','uses' => 'publicController@voucherOrderDetal'));
Route::get('flashDealOrderDetail/{flashID}/{oID}',array('as' => 'flashDealOrderDetail','uses' => 'publicController@flashDealOrderDetal'));

//Route::any('voucherOrder',array('as' => 'voucherOrder' ,'uses' => 'PublicController@voucherOrderDetails'));




