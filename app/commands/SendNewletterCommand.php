<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SendNewletterCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'newsletter:send';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
		{
			$accomm = Accomm::
					where('isVerified','=',1)
					->where('created_at', '>',date('Y-m-d').' 00:00:00')
					->get();
			$voucher = Voucher::
					where('redemptionPeriodFrom', '<=', date('Y-m-d H:i:s'))
					->where('redemptionPeriodTo', '>=', date('Y-m-d H:i:s'))
					->where('salesLimit', '>=', 1)
					->where('created_at', '>',date('Y-m-d').' 00:00:00')
					->get();
			$flashDeal = Flashdeal::
					where('redemptionPeriodFrom', '<=', date('Y-m-d H:i:s'))
					->where('redemptionPeriodTo', '>=', date('Y-m-d H:i:s'))
					->where('salesLimit', '>=', 1)
					->where('created_at', '>',date('Y-m-d').' 00:00:00')
					->get();
		   foreach(Newsletter::where('deleted_at', '=', NULL)->get() as $Newsletter)
		   {
				Mail::send('emails.public.newletter', array('accomm'=>$accomm,'voucher' => $voucher,'flashdeal' => $flashDeal), function($message) use ($Newsletter)
				{
					$message->to($Newsletter->email)->subject('Welcome!');
				});
		   }
		}
	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}