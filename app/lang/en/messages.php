<?php

return array(
	'newAccommodationSuccess' => 'You have successfully registered a new accommodation. Your accommodation is awaiting confirmation. We will inform you via email as soon as your accommodation is verified',
	'editAccommodationSuccess' => 'You have successfully updated the accommodation details. The changes made to your accommodation must be verified by administrator again',
	'confirmAccommodationDelete' => "You are about to delete  ':name'. This is an irreversible action and all data related to the accommodation will be permanently removed. Are you sure you want to continue ?",
	'accommodationDeleteSuccess' => 'You have successfully deleted ":name" and all data related to it.',
	);