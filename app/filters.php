<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::guest('signin');
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

Route::filter('ajax', function()
{
    if(!Request::ajax()){
        return "Access Denied";
    }
});

Route::filter('partners',function(){
    if((!Sentry::check()) or (Sentry::getUser()->typeID!=1)){
    	return Redirect::route('signin');
    }
});

Route::filter('admin',function(){
    if((!Sentry::check()) or (Sentry::getUser()->typeID!=2)){
    	return Redirect::route('signin');
    }
});

App::missing(function($exception)
{
    return Response::view('templates.404', array(), 404);
});




Route::filter('ownershipChecker', function($route)
{
    $requestedAccommodation =  $route->getParameter('accomm');
    if($requestedAccommodation){
        $accomm = $route->getParameter('accomm');
        if($accomm->userID != Sentry::getUser()->getKey()){
            App::abort(404, 'Page not found');
        }
    }

    
});


Route::filter('oneFlashDealPerAccomm', function($route)
{
    $requestedAccommodation =  $route->getParameter('accomm');
    if($requestedAccommodation){
        $accomm = $route->getParameter('accomm');
        if($accomm->flashdeal()->count() > 0 ){
            App::abort(404, 'Page not found');
        }
    }

    
});
