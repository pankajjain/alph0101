<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|


Route::get('/full',array('as' => '',function(){
     return View::make('partner.full');
}));



*/



require_once(app_path() . '/routes/commonRoutes.php');

require_once(app_path() . '/routes/superAdminRoutes.php');

require_once(app_path() . '/routes/partnerRoutes.php');

require_once(app_path() . '/routes/publicRoutes.php');

require_once(app_path() . '/routes/mapRoutes.php');

Route::get('page/{id}',array('as' => 'page','uses' => 'PageController@page'));