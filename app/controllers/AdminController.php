<?php 
class AdminController extends BaseController{

	public function index()
	{
		return View::make('admin.index');
	}

	public function application()
	{
		return View::make('admin.accomms.applications');
	}

	public function viewAccommodation($accomm)
	{
		return View::make('admin.accomms.viewAccomm')->with('accomm',$accomm);
	}

	public function approveAccommodation($id){
		// Accommodation approving for super admin. 
		// Partner accommodation will be approved via this method
		$id = Crypt::decrypt($id);
		$accomm = Accomm::find($id);
		$accomm->isVerified = 1;
		$accomm->save();
		return Redirect::back()
		->with('message', ucwords($accomm->name) . " approved!");
	}

	public function deleteAccommodation($id){
		// Change the status of accomodation into deleted
		// Keep the copy of information within database without affected
		$id = Crypt::decrypt($id);
		$accomm = Accomm::find($id);
		$accomm->isVerified = 3;
		$accomm->save();
		return Redirect::back()->with('message', ucwords($accomm->name) . " deleted");
	}

	public function disapproveAccommodation($id){
		// Disapprove the accommodation of partner
		// with reason given.
		$accomm = Accomm::find($id);
		$accomm->isVerified = 2;
		$accomm->remarks = Input::get('remarks');
		$accomm->save();
		
		Mail::send('emails.public.accommStatus', ['status' => "Disapproved", 'remarks' => Input::get('remarks'), 'name'=>$accomm->mainContactName], function($message) use($accomm)
		{
			$message->to($accomm->mainContactEmail, $accomm->mainContactName)->subject('[RoomQuickly]Accommodation Update!');
		});
		return Redirect::back()->with('message', ucwords($accomm->name) . " disapproved");
	}

	public function test(){
		// Test method to login with the registered user
		// Bypass the form and logon as partner directly
		$user = Sentry::findUserByLogin("partnerLogin@email.com");
		Sentry::login($user, false);
		return Redirect::route('partnersIndex');
	}

	public function archived(){
		return View::make('admin.accomms.archived');
	}

	public function users(){
		return View::make('admin.accomms.users');
	}
	
	public function soldvoucher(){
		
		$vouchers = DB::table('orders')
			->join('accommodations', 'orders.accommID', '=', 'accommodations.id')
            ->join('rooms', 'orders.roomID', '=', 'rooms.id')
            ->join('users', 'orders.userID', '=', 'users.id')
			->join('vouchers', 'orders.voucherID', '=', 'vouchers.id')
			->where("orders.voucherID", "<>",  0)
			->select('rooms.name as roomName', 'users.firstName', 'users.lastName', 'users.email' ,'vouchers.voucherName','orders.price','accommodations.name as accommName','orders.paymentStatus','orders.created_at')
			->get();
			
		//echo "<pre>"; print_r($vouchers); die;
		
		return View::make('admin.accomms.soldvoucher',compact('vouchers'));
	}
	
	public function soldflashdeal(){
		
		$flashdeals = DB::table('orders')
			->join('accommodations', 'orders.accommID', '=', 'accommodations.id')
            ->join('rooms', 'orders.roomID', '=', 'rooms.id')
            ->join('users', 'orders.userID', '=', 'users.id')
			->join('flashdeals', 'orders.flashdealID', '=', 'flashdeals.id')
			->where("orders.flashdealID", "<>",  0)
			->select('rooms.name as roomName', 'users.firstName', 'users.lastName', 'users.email' ,'flashdeals.flashDealName','orders.price','accommodations.name as accommName','orders.paymentStatus','orders.created_at')
			->get();
			
		//echo "<pre>"; print_r($vouchers); die;
		
		return View::make('admin.accomms.soldflashdeal',compact('flashdeals'));
	}

	public function showSettings()
	{
		return View::make('admin.settings.index');
	}

	public function uploadVeridoc(){
		// To have multiple version of agreement
		$date = new DateTime();
		$timestamp = $date->getTimestamp();

		$files = Input::file('files');
		if (Input::hasFile('files'))
		{
			// Temporary move to the specific folder
			$filePath = Config::get('roomquickly.agreementLetterDirectory');
			$fileName = "AgreementLetter_".$timestamp.".pdf";
			Input::file('files')->move($filePath, $fileName);
			// Initiate S3 & setting. Upload to S3
			$bucket = "rqadmin";
			$theFile = $filePath.$fileName;
			$s3 = AWS::get('s3');
			$s3->putObject(array(
			    'Bucket'     => $bucket,
			    'Key'        => $fileName,
			    'SourceFile' => $theFile,
			    'ACL'    => 'public-read',
			));
			// Delete Temporary File & Getting URL
			File::delete($theFile);
			$theURL = $s3->getObjectUrl($bucket,$fileName);
			// Save the entire URL to DB
			$docVer = Configs::find(6);
			$docVer->value = $theURL;
			$docVer->save();
			return Redirect::back()->with('message', "File uploaded. ".$theURL);
		} else {
			return Redirect::back()->with('message', "Error: File too large. Please contact system developer for further information.");	
		}
	}

	public function updateCurrency(){
		// Markup the currency of the system
		// Reset the currency rate of Malaysia Ringgit as default
		$markUp = Input::get('markup');
		$markDB = Configs::find(1);
		$markDB->value = $markUp;
		$markDB->save();
		Artisan::call('currency:update');
		$currencyDB = TheCurrency::find(22);
		$currencyDB->value = 1.00000000;
		$currencyDB->save();
		return Redirect::back()->with('message', "Currency Updated.");
	}

	public function showActivityLog(){

		return View::make("admin.accomms.activitylog");
	}

	public function suReportHotelListing(){
		// Listing of hotel
		$statesDB = DB::table('accommodations')->select('province', DB::raw('count(*) as total'))->groupBy('province')->get();
		$states = array();
		foreach($statesDB as $state) {
			$states[$state->province] = $state->province ." (". $state->total.")";
		}

		return View::make('admin.report.glisting')->with('states', $states);
	}

	public function suReportHotelSummary(){
		return View::make('admin.report.gsummary');
	}

	public function getsuReportHotelListing(){
		// Generate the report of accommodation
		// with status, location, and range of registration period
		$hs = Input::get('hotelStatus');
		$a = 0;
		switch($hs){
			case "all":
				$a = 0;
				break;
			case "disapproved":
				$a = 2;
				break;
			case "approved":
				$a = 1;
				break;
			case "deleted":
				$a = 3;
				break;
			}
		$b = Input::get('hotelCountry');
		$c = Input::get('hotelState');
		$d = Input::get('yearRangeStart');
		$e = Input::get('monthRangeStart');
		$f = Input::get('yearRangeEnd');
		$g = Input::get('monthRangeEnd');
		$start = \Carbon\Carbon::createFromDate($d, $e)->startOfMonth();
		$end = \Carbon\Carbon::createFromDate($f, $g)->endOfMonth();

		if($start > $end){
			return Redirect::back()->with('message', "Start date shouldn't later than end date.");
		}
                $query = "";
                $param = array();
                
		$accomms = new Accomm();//::whereBetween('updated_at', [$start, $end]);

                $query .= "updated_at BETWEEN ? AND ? ";
                $param[] = $start;
                $param[] = $end;
                
		if(isset($a)&&$a!=0){
                    $query .= "AND isVerified = ? ";
                    $param[] = $a;
		}

		if(isset($b)&&$b!="all"){
		   $query .= "AND country = ? ";
                    $param[] = $b;
		}

		if(isset($c)&&$c!="all"){
		   $query .= "AND province = ? ";
                    $param[] = $c;
		}
			
		$result = $accomms->whereRaw($query, $param)
                        ->orderBy('created_at','desc')->paginate(5);
		
                
		// echo "<pre>";
//		return var_dump($result->toArray());
		// echo "</pre>";
		// return Redirect::route('displayHotelListing')->with('accomm', $accomms);
		 return View::make('admin.report.printableListing',compact('result', 'hs', 'b', 'c', 'd', 'e', 'f', 'g'));
	}

	public function getsuReportHotelSummary(){
		// Generate the report base on country and registration period only
		$b = Input::get('hotelCountry');
		$d = Input::get('yearRangeStart');
		$e = Input::get('monthRangeStart');
		$f = Input::get('yearRangeEnd');
		$g = Input::get('monthRangeEnd');
		$start = \Carbon\Carbon::createFromDate($d, $e)->startOfMonth();
		$end = \Carbon\Carbon::createFromDate($f, $g)->endOfMonth();
		if($start > $end){
			return Redirect::back()->with('message', "Start date shouldn't later than end date.");
		}

		$accomms = Accomm::where('country', $b)
							->groupBy('province')
							->whereBetween('updated_at', [$start, $end])->orderBy('created_at','desc')->get(array('province'));

		$accommsDB = DB::table('accommodations')
			->where('country', $b)
			->select('province', DB::raw('count(*) as total'))
			->groupBy('province')
			->whereBetween('updated_at', [$start, $end])->orderBy('created_at','desc')->get();

		$statesDB = DB::table('accommodations')->select('province', DB::raw('count(*) as total'))->groupBy('province')->get();
		$accommsV3 = new ArrayObject();
		$x = 0;
		foreach($statesDB as $state) {
			// $states[$state->province] = $state->province ." (". $state->total.")";
			$accommsX[$x] = Accomm::where('country', $b)
							->where('province', $state->province)
							->groupBy('status')
							->orderBy('status', 'DESC')
							->whereBetween('updated_at', [$start, $end])->orderBy('created_at','desc')->get(array(
						    		DB::raw('isVerified as status'),
						    		DB::raw('COUNT(*) as "statusCount"')
						    	));
						    $x += 1;
		}

		// echo "<pre>";
		//var_dump($accommsX);
		// echo "</pre>";




		// $otherAccomms = $accomms->filter(function($accomm)
  //       {
  //           if($accomm->city < 2'kl')
  //           {
  //               return $accomm;
  //           }
  //       });
		
		
		// return Redirect::route('displayHotelListing')->with('accomm', $accomms);
		return View::make('admin.report.printableSummary',compact('accomms', 'b', 'd', 'e', 'f', 'g'));
	}

	
}

?>