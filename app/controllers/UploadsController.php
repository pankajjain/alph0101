<?php

class UploadsController extends \BaseController {

	//this function will remove a previously uploaded file
	public function destroy($id)
	{
		//get the file ID injected into the controller through the route parameter and decrypt it
		$uploadFileID = Crypt::decrypt($id);
		//find its record in the database
		$fileRecord = Upload::find($uploadFileID);
		//remove it from amazon S3
		$s3 = AWS::get('s3');
		$s3->deleteObject(array(
		    'Bucket'     => $fileRecord->type,
		    'Key'        => $fileRecord->filename,
	    ));

		//remove the file record from database

		$fileRecord->removeFile();
		return Redirect::back();
	}

	public function download($id){
		//echo $id; die;
		//get the file ID injected into the controller action through the route parameter and decrypt it
		$id = Crypt::decrypt($id);
		//find its record in the database
		$fileRecord = Upload::find($id);
		//get its path on filesystem (not used since we switched to AS3)
		$tmppath = $fileRecord->filename;

		//get the file record from AS3
		$s3 = AWS::get('s3');
		$download = $s3->getObject(array(
		    'Bucket'     => $fileRecord->type,
		    'Key'        => $fileRecord->filename,
		    'SaveAs'	 => $tmppath

	    ));
		//return the file URL for downloading.

		return Response::download($download['Body']->getUri(), $fileRecord->filename, array('content-type'=>'application/pdf'));
	}

}
