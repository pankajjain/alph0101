<?php 
use Carbon\Carbon;
class VouchersController extends \BaseController {

	//displays the index page for vouchers
	public function index($accomm){
		
		return View::make('partner.accomms.vouchers.index')->with('accomm',$accomm);
	}

	//displays the form for creating a new voucher
	public function create($accomm)
	{
		return View::make('partner.accomms.vouchers.create')->with('accomm',$accomm);

	}


	//stores a new voucher
	public function store($accomm)
	{
		//create a new voucher	
		$voucher = new Voucher();
		//specicfy the accommodation to which the new voucher belongs
		$voucher->accommID = $accomm->getKey();
		//specify the room based on which the voucher is being created
		$voucher->roomID = Input::get('roomID');
		//set the status of the voucher ('published or saved')
		$voucher->status = Input::get('status');
		//populate the voucher the user input data 
		$voucher->voucherName = Input::get('voucherName');
		$voucher->summary = Input::get('summary');
		$voucher->highlights = Input::get('highlights');
		$voucher->breakfastIncluded = Input::has('breakfastIncluded');
		$voucher->voucherAmount = Input::get('voucherAmount');
		$voucher->voucherDuration = Input::get('voucherDuration');
		$voucher->minimumDaysPriorBooking = Input::get('minimumDaysPriorBooking');
		$voucher->redemptionPeriodFrom = date("Y-m-d H:i:s",strtotime(Input::get('redemptionPeriodFrom')));
		$voucher->redemptionPeriodTo = date("Y-m-d H:i:s",strtotime(Input::get('redemptionPeriodTo')));

		//Calculate the sales limit for the voucher
		$startDate = new Carbon(Input::get('redemptionPeriodFrom'));
		$endDate = new Carbon(Input::get('redemptionPeriodTo'));
		$diffInDays =  $endDate->diffInDays($startDate);
		$numberOfRooms = Room::find($voucher->roomID)->roomAvailability;
		$voucherPackageNights = $voucher->voucherDuration - 1;
		$salesLimit = (($numberOfRooms * $diffInDays) / $voucherPackageNights) / 2;
		$voucher->salesLimit =  floor($salesLimit);
		//Calculate and set the discount price and percentage
		if(Input::get('discountedPrice') or Input::get('discountPercentage')){

			$originalPrice = Room::find($voucher->roomID)->originalPrice;
			$discountedPrice = Input::get('discountedPrice');
			$discountPercentage = (($originalPrice - $discountedPrice) * 100) / $originalPrice;
			$discountAmount = $originalPrice - $discountedPrice;

			$voucher->discountPercentage = $discountPercentage;
			$voucher->discountAmount = $discountAmount;
			$voucher->originalPrice = $originalPrice;
			$voucher->discountedPrice = $discountedPrice;
		}
		
		//set the voucher policies
		$voucher->voucherPolicies = Input::get('voucherPolicy');
		//Set a unique name for the voucher.
		//the algorithm for unique names is as follows
		//add capital 'V' character to the begining of room name initials
		//and append  the voucher ID at the end of it.
		$words = explode(" ", Room::find($voucher->roomID)->name);
		$vnum = "V";
		foreach ($words as $w) {
			$vnum .= $w[0];
		}
		if($voucher->validate()){
		$voucher->save();
		$voucher->voucherID = strtoupper($vnum) . $voucher->id;
		//save the new voucher and return a success message to the user
		$voucher->save();
		return Redirect::back()->with('message','You have successfully created a new voucher');
						
		}else{
			return Redirect::back()->withInput()->withErrors($voucher->errors);
			// var_dump($voucher->errors);
		}
	}


	//Displays the form for editing the voucher
	public function edit($accomm,$voucherID)
	{
		//find the voucher to be edited by decrypting it's id
		$voucherID = Crypt::decrypt($voucherID);
		$voucher = Voucher::find($voucherID);
		//Display the form for editing the voucher
		return View::make('partner.accomms.vouchers.edit')->with(
			array(
				'voucher' => $voucher,
				'accomm' => $accomm
				)
			);

	}

	//Updates the specified voucher
	public function update($accomm,$voucherID)
	{
		//find the voucher to be updated by decrypting its id
		$voucher = Voucher::find(Crypt::decrypt($voucherID));
		//update all it's fields based on user input
		$voucher->roomID = Input::get('roomID');
		$voucher->status = Input::get('status');
		$voucher->voucherName = Input::get('voucherName');
		$voucher->summary = Input::get('summary');
		$voucher->highlights = Input::get('highlights');
		$voucher->breakfastIncluded = Input::has('breakfastIncluded');
		$voucher->voucherAmount = Input::get('voucherAmount');
		$voucher->voucherDuration = Input::get('voucherDuration');
		$voucher->minimumDaysPriorBooking = Input::get('minimumDaysPriorBooking');
		$voucher->redemptionPeriodFrom = date("Y-m-d H:i:s",strtotime(Input::get('redemptionPeriodFrom')));
		$voucher->redemptionPeriodTo = date("Y-m-d H:i:s",strtotime(Input::get('redemptionPeriodTo')));
		
		//Calculate the sales limit for the voucher
		$originalPrice = Room::find($voucher->roomID)->originalPrice;
		$discountedPrice = Input::get('discountedPrice');
		$discountPercentage = (($originalPrice - $discountedPrice) * 100) / $originalPrice;
		$discountAmount = $originalPrice - $discountedPrice;
	
		//Calculate and set the discount price and percentage
		$voucher->discountPercentage = $discountPercentage;
		$voucher->discountAmount = $discountAmount;
		$voucher->originalPrice = $originalPrice;
		$voucher->discountedPrice = $discountedPrice;
		
		//Add the voucher policies		
		$voucher->voucherPolicies = Input::get('voucherPolicy');
		
		//Set a unique name for the voucher.
		//the algorithm for unique names is as follows
		//add capital 'V' character to the begining of room name initials
		//and append  the voucher ID at the end of it.
		$words = explode(" ", Room::find($voucher->roomID)->name);
		$vnum = "V";
		foreach ($words as $w) {
			$vnum .= $w[0];
		}
		$voucher->save();
		$voucher->voucherID = strtoupper($vnum) . $voucher->id;
		$voucher->save();
		return Redirect::back()->with('message','You have successfully edited the new voucher');
	}

	//deletes the specified voucher 
	public function destroy($accomm,$voucherID)
	{
		//find the voucher to be deleted by decrypting its id
		$id = Crypt::decrypt($voucherID);
		//remove the voucher and display a success message to the user
		Voucher::destroy($id);
		return Redirect::back()->with('message','The selected voucher is successfully removed');

	}
	
	public function soldVoucher($accomm){
		//echo "<pre>"; print_r($accomm->id); die;
		//$vouchers = DB::table("orders")->where("accommID", "=", $accomm->id)->get();
		
		$vouchers = DB::table('orders')
            ->join('rooms', 'orders.roomID', '=', 'rooms.id')
            ->join('users', 'orders.userID', '=', 'users.id')
			->join('vouchers', 'orders.voucherID', '=', 'vouchers.id')
			->orderBy("orders.created_at","desc")
            ->where("orders.accommID", "=",  $accomm->id)
			->where("orders.voucherID", "<>",  0)
			->select('rooms.name', 'users.firstName', 'users.lastName', 'users.email' ,'vouchers.voucherName','orders.price','orders.paymentStatus','orders.created_at')
			->get();
			
		//echo "<pre>"; print_r($vouchers); die;
		return View::make('partner.accomms.vouchers.soldvoucher', compact('accomm', 'vouchers'));
		//echo "<pre>"; print_r($vouchers); die;
		}
		
		
	public function validateVoucher($accomm){
		
			if(Input::get('submit') == 'Validate'){
					$qrcode = Input::get('qrcode');
					$accommID = Input::get('accommID');
					$details = DB::table('orders')
					->join('vouchers','orders.voucherID','=','vouchers.id')
					->join('users','orders.userID','=','users.id')
					->where('voucherCode','=',$qrcode)->where('voucherStatus','=','new')
					->where('vouchers.redemptionPeriodFrom', '<=', date('Y-m-d H:i:s'))
					->where('vouchers.redemptionPeriodTo', '>=', date('Y-m-d H:i:s'))
					->where('orders.accommID', '=', $accommID)
					->select('vouchers.*','users.firstName', 'users.lastName', 'users.email','orders.voucherCode as vCode','orders.id as ordId')
					->get();
					if(count($details) > 0){
					
					 return View::make('partner.accomms.vouchers.validateVoucher', compact('details','accomm'));
					}else{
							return Redirect::back()->with('errMessage','This voucher code has been expired or not valid, Please try with new voucher code.')->withInput();
						}
				}
				
				if(Input::get('submit') == 'Yes'){
					$orderID = Input::get('orderID');
					$accommID = Input::get('accommID');
					DB::table('orders')
					->where('id', $orderID)
					->where('accommID', $accommID)
					->update(array('voucherStatus' => 'used'));
					
					return Redirect::back()->with('message','You have successfully used this voucher.');
				}
				
				
			return View::make('partner.accomms.vouchers.validateVoucher', compact('accomm'));
		}
	

}

?>