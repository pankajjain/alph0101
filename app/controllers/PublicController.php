<?php 
use Omnipay\Omnipay;

class PublicController extends BaseController {

	public function index(){
		//$ip = '180.188.253.229';
		//$a =  unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
		//echo "<pre>"; print_r($a); die;
		// echo currency(100, Session::get('my.currency', Config::get('app.currency')));die;
		//echo Session::get('my.currency');die;
		//echo $a['geoplugin_currencyCode'];die;
		//echo Currency::format(12.00, $a['geoplugin_currencyCode']);die;
		//echo "pankaj"; die;
		
		if(Sentry::check())
				{
					  
				$userID = Sentry::getUser()->getKey();
				}
               
			
			$orderVoucher = DB::table('orders')
                     ->select(DB::raw('count(*) as ord_count, voucherID'))
                     ->groupBy('voucherID')
					 ->where('voucherID', '<>', 0)
					 ->orderBy('ord_count','desc')
                     ->get();
					 
					
			
			$voucherArr =array();
			for($i=0;$i<count($orderVoucher);$i++){
				$voucherArr[] = $orderVoucher[$i]->voucherID;	
			}
			
			
			$voucher = Voucher::
			    whereIn('id',$voucherArr)
				->where('redemptionPeriodFrom', '<=', date('Y-m-d H:i:s'))
				->where('redemptionPeriodTo', '>=', date('Y-m-d H:i:s'))
				->take(15)
				->get();
                
                /*$fd = new Flashdeal();
				if ($fd->isTime())
                $flashdeal = Flashdeal::take(8)->get();*/
                
                return View::make('public.index', compact('voucher','flashdeal','userID'));
	}


	public function partnerSignUpCreate()
	{
		return View::make('public.signup')->with('userType','1');
	}

	public function memberSignupCreate()
	{
		return View::make('public.signup')->with('userType','3');
	}

	public function signin()
	{
		return View::make('public.signup')->with('userType','3')->with('signIn','true');
	}

	public function SignUpStore()
	{

		$rules = array(
			'firstName' => 'required|alpha',
			'lastName' => 'required',
			'email' => 'required|email|unique:users,email',
			'password' => 'required|min:6',
			'tnc' => 'required'
			);

		$messages = array(
			'firstName.required'    => 'Please enter your name',
			'alpha'    => 'You can only enter alphabetic characters',
			'required' => 'This field is required',
			'email'      => 'Please enter a correct email address',
			'password.min' => 'Password must be at least 6 characters',
			'tnc.required' =>  'you must agree with Terms & Conditions'
			);

		$validator = Validator::make(Input::all(), $rules,$messages);
		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator->messages())->withInput();
		}

		$userDetails = Input::only('firstName','lastName','email','password');
		$userDetails['typeID'] = Crypt::decrypt(Input::get('usertype'));
		$user = Sentry::getUserProvider()->create($userDetails);
		$message = "Your account is created successfully and an email is sent to you containing the confirmation link. Click on the activation link to activate your account.";

		$url = URL::route('confirmUser',array('userID' => Crypt::encrypt($user->getKey())));
		Mail::send('emails.public.confirmSignUp', ['link' => $url,'firstname'=>Input::get('firstName'),'lastname'=>Input::get('lastName'),'email'=>Input::get('email'),'password'=>Input::get('password')], function($message) use($user)
		{
			$message->to($user->email)->subject('[RoomQuickly]Welcome!');
		});

		return Redirect::route('signin')->with('message',$message);

	}
        
        public function saveReview($accommID)
        {
			//echo $accommID; die;
                /*$rules = array();
                
                $validator = Validator::make(Input::all(), $rules);
					if ($validator->fails()) {
						return Redirect::back()->withErrors($validator->messages())->withInput();
					}
                
                $input = Input::all();
                
                ReviewsRating::create($input);
                
                return Redirect::back();*/
									  
				$userID = Sentry::getUser()->getKey();
				
				
				$review = new ReviewsRating();
					$review->accommID       = $accommID;
					$review->rating      = Input::get('rating');
					$review->negativeReviews = Input::get('negativeReviews');
					$review->positiveReviews = Input::get('positiveReviews');
					$review->clean = Input::get('clean');
					$review->comfort = Input::get('comfort');
					$review->location = Input::get('location');
					$review->staff = Input::get('staff');
					$review->services = Input::get('services');
					$review->valForMoney = Input::get('valForMoney');
					$review->byUserID = $userID;
					/*DB::table('users')->insert(
						array('email' => 'john@example.com', 'votes' => 0)
					);*/
					
					$review->save();
					return Redirect::back()->with('message','You have successfully added a review');
					//echo "<pre>"; print_r($review); die;
        }
		
		
        
        public function showVoucher($id)
        {
                $voucher = Voucher::findorFail($id);

                return View::make('public.basic.hotel', compact('voucher'));
        }
        
        public function listVoucher()
        {
                $voucher = Voucher::orderBy('updated_by', 'desc')
                        ->limit(15);
                
                //need to add condition to check for flashdeal period
                $flashdeal = Flashdeal::orderBy('updated_by', 'desc')
                        ->limit(8);
                return date("l");
                return View::make('public.basic.search', compact('voucher','flashdeal'));
        }
		
		 public function voucherListing()
        {
			//$ip = '180.188.253.229';
		//$country =  unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
		//echo $country['geoplugin_countryName']; die;
			   if(Sentry::check())
				{
					  
				$userID = Sentry::getUser()->getKey();
				//$voucher = Voucher::all();
				
				$voucher = Voucher::
				where('redemptionPeriodFrom', '<=', date('Y-m-d H:i:s'))
				->where('redemptionPeriodTo', '>=', date('Y-m-d H:i:s'))
				->get();
				//echo "<pre>"; print_r($voucher);die;
            
                return View::make('public.voucher.list', compact('voucher','userID'));
				}else{
					
				$voucher = Voucher::
				where('redemptionPeriodFrom', '<=', date('Y-m-d H:i:s'))
				->where('redemptionPeriodTo', '>=', date('Y-m-d H:i:s'))
				->get();
               
                return View::make('public.voucher.list', compact('voucher'));
					}
				
			    
        }
		
		
		
		 public function showVoucherDetails($id)
        {
			$voucher = DB::table('vouchers')->where("id", "=", $id)->where('redemptionPeriodFrom', '<=', date('Y-m-d H:i:s'))
				->where('redemptionPeriodTo', '>=', date('Y-m-d H:i:s'))->get();
			
			if(isset($voucher[0]->accommID)){		
			$accommPhoto = DB::table('accommphotos')->where("accommID", "=", $voucher[0]->accommID)->get();	
			}
			
                if(Sentry::check())
				{
				$userID = Sentry::getUser()->getKey();	  
				 return View::make('public.voucher.detail', compact('voucher','accommPhoto','userID'));
				}else{
					
				 return View::make('public.voucher.detail', compact('voucher','accommPhoto'));
					}
					
              
        }
		
		 public function flashdealListing()
        {
			//$voucher = DB::table('vouchers')->get();				
               // $voucher = Voucher::orderBy('updated_by', 'desc')->limit(15);
						//echo "<pre>"; print_r($voucher);die;
                
                //need to add condition to check for flashdeal period
                //$flashdeal = Flashdeal::orderBy('updated_by', 'desc')->limit(8);
               // return date("l");
			   if(Sentry::check())
				{
					  
				$userID = Sentry::getUser()->getKey();
				}
				
			    $flashdeal = FlashDeal::
							where('redemptionPeriodFrom', '<=', date('Y-m-d H:i:s'))
							->where('redemptionPeriodTo', '>=', date('Y-m-d H:i:s'))
							->get();
               
                return View::make('public.flashdeal.list', compact('flashdeal','userID'));
        }
		
		 public function showFlashDealDetails($id)
        {
			$flashdeal = DB::table('flashdeals')->where("id", "=", $id)->where('redemptionPeriodFrom', '<=', date('Y-m-d H:i:s'))
				->where('redemptionPeriodTo', '>=', date('Y-m-d H:i:s'))->get();
				
				if(isset($flashdeal[0]->accommID)){				
               $accommPhoto = DB::table('accommphotos')->where("accommID", "=", $flashdeal[0]->accommID)->get();	
				}
			
                if(Sentry::check())
				{
				$userID = Sentry::getUser()->getKey();	  
				 return View::make('public.flashdeal.detail', compact('flashdeal','accommPhoto','userID'));
				}else{
					
				 return View::make('public.flashdeal.detail', compact('flashdeal','accommPhoto'));
					}
              // return View::make('public.flashdeal.detail', compact('flashdeal'));
        }
        
        public function listHotel()
        {
			
                $accomms = Accomm::orderBy('updated_at', 'desc')->paginate(16);
				/*$accomms = DB::table('accommodations')
							->join('accommphotos','accommphotos.accommID', '=', 'accommodations.id')
							->select('accommphotos.fileName','accommodations.*')->distinct('accommphotos.accommID')
							->orderBy('accommodations.updated_at','desc')
                       		 ->paginate(12);*/
					//echo "<pre>"; print_r($accomms); die;
                return View::make('public.hotel.list', compact('accomms'));
        }
        
        public function showHotel($id)
        {
			
			if(Sentry::check())
				{
					  
				$userID = Sentry::getUser()->getKey();
				$accomvisitor = new AccommVisitor();
				
				
				$query = DB::table("users")
				->where("typeID", "=", '3') // "=" is optional
				->where("id", "=",  $userID) // "=" is optional
				->get();
				
					if(count($query) > 0){
						$check_data = DB::table("accommodationvisitor")
						->where("accommID", "=", $id) // "=" is optional
						->where("userID", "=",  $userID) // "=" is optional
						->get();
						
						if(count($check_data) == 0){
							$accomvisitor->accommID = $id;
							$accomvisitor->userID = $userID;
							$accomvisitor->coun = 1;
							$accomvisitor->save();
				
						}
					}
			}
				
				
                $accomm = Accomm::findorFail($id);
				
							
				
				$review = DB::table('reviewsrating')
                  ->join('users', 'reviewsrating.byUserID', '=', 'users.id')
			       ->where("reviewsrating.accommID", "=",  $id)
			->select('users.firstName', 'users.lastName', 'reviewsrating.rating','reviewsrating.negativeReviews','reviewsrating.positiveReviews','reviewsrating.created_at')
			->get();
				//echo "<pre>"; print_r($review);die;
				
				if(Sentry::check())
				{
               	 return View::make('public.basic.hotel', compact('accomm', 'review','userID'));
				}else{
					 return View::make('public.basic.hotel', compact('accomm', 'review'));	
				}
        }
        
        
		
	public function showSampleSearch(){
		return View::make('public.mockups.sampleSearchResult');
	}
        
        public function showSampleHotel(){
		return View::make('public.hotel.hotel');
	}

	

	public function currencySetter($countryCode){
		if( in_array($countryCode, array('MYR', 'USD', 'GBP')) )
		{
			Session::put('my.currency', $countryCode);
			return Redirect::back();
		} else {
			Session::put('my.currency', 'MYR');
			return Redirect::route('publicIndex')->with('message',"language not available.");
		}
	}

	public function languageSetter($countryCode){
		// Language security check. Prevent session hijack.
		if( in_array($countryCode, array('en', 'my', 'zh-CN', 'zh-HK')) )
		{
			Session::put('my.locale', $countryCode);
			return Redirect::back();
		} else {
			Session::put('my.locale', 'en');
			return Redirect::route('publicIndex')->with('message',"language not available.");
		}
	}

    public function searchNearby(){
        // get the ip of current user and turn it into longitude latitude via geoip
        // get the name of location (state or country)
        // filter the database to get the hotel of state and calculate the nearest distance

        $geotools = new \League\Geotools\Geotools();
        $coordA   = new \League\Geotools\Coordinate\Coordinate(array(48.8234055, 2.3072664));
        $coordB   = new \League\Geotools\Coordinate\Coordinate(array(43.296482, 5.36978));
        $distance = $geotools->distance()->setFrom($coordA)->setTo($coordB);
		
		echo "<pre>"; print_r($distance); die;
        //flat (most performant), haversine or vincenty (most accurate) algorithms
        printf("%s\n",$distance->flat()); // meter
        printf("%s\n",$distance->in('km')->haversine());
        printf("%s\n",$distance->in('km')->vincenty());
        printf("%s\n",$distance->in('km')->flat());
    }
	
	
	public function searchHotel()
        {
			//echo "<pre>"; print_r(Input::all()); die;
                
			$per_page=5;
$page=Input::get('page',1);
                $result = Accomm::select('accommodations.*','accommphotos.fileName')
                        ->leftJoin('rooms', 'accommodations.id', '=', 'rooms.accommid')
                        ->leftJoin('accommphotos', 'accommodations.id', '=', 'accommphotos.accommID')
						->where('accommodations.isVerified','=',1)
                        ->where('accommodations.name', 'LIKE', Input::get('searchText').'%')
						->orWhere('accommodations.city', 'LIKE', Input::get('searchText').'%')
						->orWhere('accommodations.country', 'LIKE', Input::get('searchText').'%')
                        ->groupBy('accommodations.id')
                        ->orderBy('accommodations.updated_at','desc')
                      	->paginate(15);
				
								
				
						
					
			/*$queries = DB::getQueryLog();
			$last_query = end($queries);
			echo "<pre>"; print_r($last_query); die;*/
               // echo "<pre>"; print_r($result); die;
			   //$tmpArr = array('fileName'=>'test.jpg');
			   
			   
               //$result=Paginator::make($tmpArr, count($result), $per_page); 
                return View::make('public.basic.search', compact('result'));
        }
        
		
		public function homePageSearch(){
			
			//echo "<pre>"; print_r(Input::All()); die;
			$result = Accomm::select('accommodations.*','accommphotos.fileName')
                        ->leftJoin('rooms', 'accommodations.id', '=', 'rooms.accommid')
						->leftJoin('vouchers', 'accommodations.id', '=', 'vouchers.accommid')
						->leftJoin('flashdeals', 'accommodations.id', '=', 'flashdeals.accommid')
                        ->leftJoin('accommphotos', 'accommodations.id', '=', 'accommphotos.accommid')
						->where('accommodations.isVerified','=',1)
                        ->where('accommodations.name', 'LIKE', '%'.Input::get('site_search').'%')
						->where('accommodations.hotelDescription', 'LIKE', '%'.Input::get('site_search').'%')
						->orWhere('accommodations.city', 'LIKE', '%'.Input::get('site_search').'%')
						->orWhere('accommodations.country', 'LIKE', '%'.Input::get('site_search').'%')
						->orWhere('vouchers.voucherName', 'LIKE', '%'.Input::get('site_search').'%')
						->orWhere('flashdeals.flashDealName', 'LIKE', '%'.Input::get('site_search').'%')
                        ->groupBy('accommodations.id')
                        ->orderBy('accommodations.updated_at','desc')
                        ->paginate(15);
				
				
			/*if (Request::ajax()) {
			
			//return Response::json("pankaj"); 
           return Response::json($this->ajaxSearch()->render());
			
        }*/
				
					
					/*$queries = DB::getQueryLog();
			$last_query = end($queries);
			echo "<pre>"; print_r($last_query); die;*/	
                
               
                return View::make('public.basic.search', compact('result'));
        
			}
	
	public function ajaxSearch(){
		//echo Input::get('starRate'); die;
		
		$tmpArr = array();
		$per_page=5;
		$page=Input::get('page',1);
		
		
		
		$pri = Input::get('price');
		
		if(!empty($pri)){
		$arrStr = implode(',',Input::get('price'));
		$arrStr = explode(',',$arrStr);
		$price[] = $arrStr[0];
		$price[] = end($arrStr); 
		
		
			if(!empty($arrStr)){
					if($price[1] != 'All'){
					array_push($tmpArr, "(rooms.originalPrice >= ".$price[0]." AND rooms.originalPrice <= ".$price[1].")");
				}else{
					array_push($tmpArr, "rooms.originalPrice >= ".$price[0]);
					}
			}
		}
		
		$accommType = Input::get('accommType');
		if(!empty($accommType)){
			array_push($tmpArr, "accommodations.typeID in('".implode("','", Input::get('accommType'))."')");
		}
		$accommFac = Input::get('accommFac');
		if(!empty($accommFac)){
			array_push($tmpArr, "(accommfac1.facility IN('".implode("','", Input::get('accommFac'))."') AND accommfac1.type='standard')");
		}
		$srFac = Input::get('srFac');
		if(!empty($srFac)){
			array_push($tmpArr, "(accommfac2.facility IN('".implode("','", Input::get('srFac'))."') AND accommfac2.type='s&r')");	
		}
		$roomFac = Input::get('roomFac');
		if(!empty($roomFac)){
			array_push($tmpArr, "roomfacilities.facility IN('".implode("','", Input::get('roomFac'))."')");
		}
		
		$queryStr='select accommodations.*,accommphotos.fileName,rooms.originalPrice, accommfac1.facility, accommfac2.facility, rooms.name as roomName, roomfacilities.facility,reviewsrating.rating from accommodations 
			LEFT JOIN accommfacilities as accommfac1 on(accommfac1.accommID=accommodations.id)
			LEFT JOIN accommfacilities as accommfac2 on(accommfac2.accommID=accommodations.id)
			LEFT JOIN rooms on(rooms.accommID=accommodations.id)
			LEFT JOIN roomfacilities on(roomfacilities.roomID=rooms.id)
			LEFT JOIN accommphotos  on(accommodations.id=accommphotos.accommid)
			LEFT JOIN reviewsrating  on(accommodations.id=reviewsrating.accommid)';
		if(!empty($tmpArr)){
				$queryStr.=' WHERE '.implode(' AND ', $tmpArr);
				
				if(Input::get('starRate') != ''){
					 
						$queryStr.=' AND accommodations.starRating = '.Input::get('starRate');
					
				}
				
				
			 if(Input::get('userRate') != ''){
				 $queryStr.=' AND reviewsrating.rating = '.Input::get('userRate');
				}
		}else{
		
			 if(empty($tmpArr) && Input::get('starRate') != '' && Input::get('order') == ''){
					 
						$queryStr.=' WHERE accommodations.starRating = '.Input::get('starRate');
					
				}
				
				
			 if(empty($tmpArr) && Input::get('userRate') != '' && Input::get('userRateOrder') == ''){
				 if(empty($tmpArr) && Input::get('starRate') != ''){
					$queryStr.=' AND reviewsrating.rating = '.Input::get('userRate');
				 }else{
						$queryStr.=' WHERE reviewsrating.rating = '.Input::get('userRate');
					 }
				}
			
		}
		
		
		$queryStr.=' GROUP BY accommodations.id';
		if(Input::get('order') != ''){
			$queryStr.=' Order by accommodations.starRating '.Input::get('order');
		}
		
		if(Input::get('userRateOrder') != ''){
			$queryStr.=' Order by reviewsrating.rating '.Input::get('userRateOrder');
		}
		
		if(Input::get('priceOrder') != ''){
			$queryStr.=' Order by rooms.originalPrice '.Input::get('priceOrder');
		}
		
		
		$resultCount = DB::select( DB::raw($queryStr));
		$rowCount = count($resultCount);
		
		$queryStr.= ' limit '.$per_page.' offset '.($page-1)*$per_page;
		//echo $queryStr; die;
		
		
		$result = DB::select( DB::raw($queryStr));
		
		
		
		/*$result = Accomm::select('accommodations.*','accommphotos.*')
                        ->leftJoin('rooms', 'accommodations.id', '=', 'rooms.accommid')
                        ->leftJoin('accommphotos', 'accommodations.id', '=', 'accommphotos.accommID')
						->where('accommodations.isVerified','=',1)
                        ->groupBy('accommodations.id')
                        ->orderBy('accommodations.updated_at','desc')
                        ->paginate(5);*/
						
		
		$result = Paginator::make($result, $rowCount, $per_page);
		
				if (Request::ajax()) {
			          return Response::json(View::make('public.basic.ajaxSearchResult', compact('result'))->render());
				}
			
	
		//return View::make('public.basic.ajaxSearchResult', compact('result'),$a,$b,$c,$d,$e,$f,$g);
		return View::make('public.basic.ajaxSearchResult', compact('result'));
		 
		
		
				
				
	}
	
	
	public function showSampleMyAccount(){
		
		if(Sentry::check())
				{
					  
				$userID = Sentry::getUser()->getKey();
				
				$user = new User();
				
				
				/*$query = DB::table("orders")
				
				->where("userID", "=", $userID) // "=" is optional
				->where("voucherID", "<>",  0) // "=" is optional
				->select('voucherID')->distinct()
				->get();*/
				
				
				
				
				$vouchers = DB::table('orders')
            ->join('rooms', 'orders.roomID', '=', 'rooms.id')
           	->join('vouchers', 'orders.voucherID', '=', 'vouchers.id')
			->join('accommodations', 'orders.accommID', '=', 'accommodations.id')
			->orderBy("orders.created_at","desc")
            ->where("orders.userID", "=",   $userID)
			->where("orders.voucherID", "<>",  0)
				->select('orders.id as orderID','rooms.name as roomName','accommodations.name as accommName','vouchers.id as vID','vouchers.voucherName','vouchers.accommID','vouchers.id as vid','orders.price','orders.transactionID as bookingNumber','vouchers.redemptionPeriodFrom','vouchers.redemptionPeriodTo')
			->get();
			
				
				
				
				$review = DB::table('reviewsrating')
                  ->join('users', 'reviewsrating.byUserID', '=', 'users.id')
				  ->join('accommodations', 'accommodations.id', '=', 'reviewsrating.accommID')
			       ->where("reviewsrating.byUserID", "=",  $userID)
			->select( 'accommodations.name','reviewsrating.rating','reviewsrating.negativeReviews','reviewsrating.positiveReviews','reviewsrating.created_at')
			->get();
			
			
			$users = DB::table('users')
                 		->where("users.id", "=",  $userID)
						->get();
			
			
			//echo "<pre>"; print_r($users); die;			
			
					
				return View::make('public.mockups.myaccount', compact('vouchers','review','users','userID'));
			}
			else{
			return Redirect::to('/signin');
			}
		//return View::make('public.mockups.myaccount');
	}
	
	public function updateUser(){
		$userID = Sentry::getUser()->getKey();
			$arr = Input::all();
			//echo $arr['fieldId']." ".$arr['fieldValue'];die;
			DB::table('users')
            ->where('id', $userID)
            ->update(array($arr['fieldId'] => $arr['fieldValue']));
		}
	
	public function homepageSearchBox(){
		
			//echo "<pre>"; print_r(Input::all()); die;
			$cityResult = Accomm::where('city', 'LIKE', Input::get('search').'%')->distinct('city')->get();
			$countryResult = Accomm::where('country', 'LIKE', Input::get('search').'%')->distinct()->get();
			$hotelResult = Accomm::where('name', 'LIKE', Input::get('search').'%')->distinct()->get();
			$searchString = Input::get('search');
			//echo "<pre>"; print_r($hotelResult); die;
			
			 return View::make('public.basic.homepageSearchBox', compact('cityResult','countryResult','hotelResult','searchString'));
		}
		
	public function voucherOrderDetal($vID,$oID){
			$vouchers = Voucher::where('id','=',$vID)->get();
			$ords = DB::table('orders')->where('id','=',$oID)->get();
			$orders = new Orders();
			 $orders->voucherCode = $ords[0]->voucherCode;
			 $user = Sentry::getUser();
			 
			return View::make('public.voucher.voucherOrderDetail', compact('vouchers','orders','user'));
		}
		
	public function flashDealOrderDetal($flashID,$oID){
			$flashdeal = Flashdeal::where('id','=',$flashID)->get();
			$ords = DB::table('orders')->where('id','=',$oID)->get();
			$orders = new Orders();
			 $orders->flashDealCode = $ords[0]->flashDealCode;
			 $user = Sentry::getUser();
			return View::make('public.flashdeal.flashDealOrderDetail', compact('flashdeal','orders','user'));
		}
		
		
	

} ?>