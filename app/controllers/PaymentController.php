<?php
use Omnipay\Omnipay;
//use \Omnipay\Common\GatewayFactory;
class PaymentController extends BaseController {

 private $responsereturn;
	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'PaypalController@showWelcome');
	|
	*/


			
			
	public function completeVoucherPurcahse()
	{
		
		
		
		$userID = Sentry::getUser()->getKey();
		$user = Sentry::getUser();
		
		if(Input::get('RESPONSE') == 00){
			if(Input::get('REF2') == 'Voucher'){
				
				
				$vID = Input::get('REF1');
			$vouchers = Voucher::where('id', '=', $vID )->get();
			
			
			$voucherUser = array();
			$voucherUser = DB::table("users")->where('id', '=', $vouchers[0]->accomm->userID)->get(); 
			$adminUser = array();
			$adminUser = DB::table("users")->where('typeID', '=', 2)->get(); 
					
					
			//$ip =$_SERVER['REMOTE_ADDR'];
			$ip = '31.222.178.197';
			$curr =  unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
			$curPrice = Currency::format($vouchers[0]->discountedPrice, $curr['geoplugin_currencyCode']);
			$tmpCurPrice = str_replace(array('RM',html_entity_decode($curr['geoplugin_currencySymbol'])),"",$curPrice);
			$findme   = 'RM';
			$pos = strpos($curPrice, $findme);
			if ($pos !== false) {
				$curr['geoplugin_currencyCode'] = "RM";			
			} 
			$curPrice = $tmpCurPrice;
			
			
			
			
		
		
					$orders = new Orders();
					
					function randomPrefix($length) 
						{ 
						$random= "";
						srand((double)microtime()*1000000);
						
						$data = "ABCDEFGHIJKLMNOPQRSTUVWXYX"; 
						
						
						
						for($i = 0; $i < $length; $i++) 
						{ 
						$random .= substr($data, (rand()%(strlen($data))), 1); 
						}
						
						return $random; 
						}
						
						$voucherCode = randomPrefix(8); 
					
						$orders->userID = $userID;
						$orders->accommID = $vouchers[0]->accommID;
						$orders->roomID = $vouchers[0]->roomID;
						$orders->voucherID =$vID;
						$orders->voucherCode = $voucherCode.(int)$vouchers[0]->discountPercentage;
						$orders->voucherStatus = 'new';
						$orders->price = $vouchers[0]->discountedPrice;
						$orders->token = Input::get('CHECKSUM');
						
						$orders->transactionID = Input::get('APPCODE');
						$orders->paymentStatus = 'Success';
						
						
					$orders->save();
					
					DB::table('vouchers')->decrement('salesLimit', 1);
					
					
					//USER ORDER MAIL
					Mail::send('emails.public.voucherOrdersDetails',array('invoice'=>str_pad($orders->id, 6, "0", STR_PAD_LEFT),'status'=>$orders->paymentStatus,'vouchers'=>$vouchers,'orders'=>$orders,'user'=>$user), function($message) use($user)
						{
							//$message->to($user->email, $user->getFullName())->subject('[RoomQuickly]Welcome!');
							$message->to($user->email, $user->firstName.' '.$user->lastName)->subject('RoomQuickely Order!');
						});
						
						
						
					//PARTNER ORDER MAIL	
					
					
					
						Mail::send('emails.partner.voucherOrdersDetails',array('invoice'=>str_pad($orders->id, 6, "0", STR_PAD_LEFT),'status'=>$orders->paymentStatus,'vouchers'=>$vouchers,'orders'=>$orders,'user'=>$user,'voucherUser'=>$voucherUser), function($message) use($voucherUser,$adminUser)
						{
							//$message->to($user->email, $user->getFullName())->subject('[RoomQuickly]Welcome!');
							$message->to($voucherUser[0]->email)->cc($adminUser[0]->email)->subject('RoomQuickely Order!');
						});
						
						
					
					//return View::make('public.voucher.voucherOrderDetail', compact('vouchers','orders','user'));
					return Redirect::to('/voucherOrderDetail/'.$vID.'/'.$orders->id)->with('message','Your Order has been successfully done.');
				
					
			
			
			}
			else if(Input::get('REF2') == 'FlashDeal'){
				
				
				$flashID = Input::get('REF1');
			$flashdeals = Flashdeal::where('id', '=', $flashID)->get();
		
			$flashdealsUser = array();
			$flashdealsUser = DB::table("users")->where('id', '=', $flashdeals[0]->accomm->userID)->get(); 
			$adminUser = array();
			$adminUser = DB::table("users")->where('typeID', '=', 2)->get(); 
			
			
			
			//$ip =$_SERVER['REMOTE_ADDR'];
			$ip = '31.222.178.197';
			$curr =  unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
			$curPrice = Currency::format($flashdeals[0]->discountedPrice, $curr['geoplugin_currencyCode']);
			$tmpCurPrice = str_replace(array('RM',html_entity_decode($curr['geoplugin_currencySymbol'])),"",$curPrice);
			$findme   = 'RM';
			$pos = strpos($curPrice, $findme);
			if ($pos !== false) {
				$curr['geoplugin_currencyCode'] = "RM";			
			} 
			$curPrice = $tmpCurPrice;
		
		
		
			
			
					$orders = new Orders();
					
					function randomPrefix($length) 
						{ 
						$random= "";
						srand((double)microtime()*1000000);
						
						$data = "ABCDEFGHIJKLMNOPQRSTUVWXYX"; 
						
						
						
						for($i = 0; $i < $length; $i++) 
						{ 
						$random .= substr($data, (rand()%(strlen($data))), 1); 
						}
						
						return $random; 
						}
						
						$flashDealCode = randomPrefix(8); 
					
						$orders->userID = $userID;
						$orders->accommID = $flashdeals[0]->accommID;
						$orders->roomID = $flashdeals[0]->roomID;
						$orders->flashdealID =$flashID;
						$orders->flashDealCode = $flashDealCode.(int)$flashdeals[0]->discountPercentage;
						$orders->flashDealStatus = 'new';
						$orders->price = $flashdeals[0]->discountedPrice;
						$orders->token = Input::get('CHECKSUM');
						
						$orders->transactionID = Input::get('APPCODE');
						$orders->paymentStatus = 'Success';
						
						
					$orders->save();
					
					DB::table('flashdeals')->decrement('salesLimit', 1);
					
					//USER ORDER MAIL
					Mail::send('emails.public.flashdealOrdersDetails',array('invoice'=>str_pad($orders->id, 6, "0", STR_PAD_LEFT),'status'=>$orders->paymentStatus,'flashdeals'=>$flashdeals,'orders'=>$orders,'user'=>$user), function($message) use($user)
						{
							//$message->to($user->email, $user->getFullName())->subject('[RoomQuickly]Welcome!');
							$message->to($user->email, $user->firstName.' '.$user->lastName)->subject('RoomQuickely Order!');
						});
						
						
						
					//PARTNER ORDER MAIL	
					
					
					
						Mail::send('emails.partner.flashdealOrdersDetails',array('invoice'=>str_pad($orders->id, 6, "0", STR_PAD_LEFT),'status'=>$orders->paymentStatus,'flashdeals'=>$flashdeals,'orders'=>$orders,'user'=>$user,'flashdealsUser'=>$flashdealsUser), function($message) use($flashdealsUser,$adminUser)
						{
							//$message->to($user->email, $user->getFullName())->subject('[RoomQuickly]Welcome!');
							$message->to($flashdealsUser[0]->email)->cc($adminUser[0]->email)->subject('RoomQuickely Order!');
						});
						
						
					
					//return View::make('public.voucher.voucherOrderDetail', compact('vouchers','orders','user'));
					return Redirect::to('/flashDealOrderDetail/'.$flashID.'/'.$orders->id)->with('message','Your Order has been successfully done.');
				
					
			
		
			
			
			}
		}else{
					return Redirect::to('/')->with('errorMsg','Your Order has not been done.');
					//return View::make('public.voucher.voucherOrderDetail', compact('vouchers','orders'));
				}	
		
	}
		
		
		

}