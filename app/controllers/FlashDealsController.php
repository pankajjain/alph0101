<?php 
use Carbon\Carbon;

class FlashDealsController extends BaseController{

	public function index($accomm)
	{
		return View::make('partner.accomms.flashDeals.index')->with('accomm',$accomm);
	}

	public function create($accomm)
	{
		return View::make('partner.accomms.flashDeals.create')->with('accomm',$accomm);
	}

	
	public function edit($accomm,$flashdealID){
		$flashdealID = Crypt::decrypt($flashdealID);
		$flashdeal = Flashdeal::find($flashdealID);
		return View::make('partner.accomms.flashDeals.edit')->with(['accomm'=>$accomm,'flashdeal' => $flashdeal]);
	}


	public function update($accomm,$flashdealID){

//echo Input::get('status'); die;
		$flashdeal = Flashdeal::find(Crypt::decrypt($flashdealID));
		$flashdeal->roomID = Input::get('roomID');
		$flashdeal->voucherID = Input::get('voucherID');
		$flashdeal->status = Input::get('status');
		$flashdeal->flashDealName = Input::get('flashDealName');
		$flashdeal->summary = Input::get('summary');
		$flashdeal->highlights = Input::get('highlights');
		$flashdeal->breakfastIncluded = Input::has('breakfastIncluded');
		$flashdeal->flashDealAmount = Input::get('flashDealAmount');
		$flashdeal->flashDealDuration = Input::get('flashDealDuration');
		$flashdeal->minimumDaysPriorBooking = Input::get('minimumDaysPriorBooking');

		if(Input::get('redemptionPeriodFrom') and Input::get('redemptionPeriodTo')){
			$flashdeal->redemptionPeriodFrom = date("Y-m-d H:i:s",strtotime(Input::get('redemptionPeriodFrom')));
			$flashdeal->redemptionPeriodTo = date("Y-m-d H:i:s",strtotime(Input::get('redemptionPeriodTo')));
		}
		

		$originalPrice = Room::find($flashdeal->roomID)->originalPrice;
		$discountedPrice = Input::get('discountedPrice');
		$discountPercentage = (($originalPrice - $discountedPrice) * 100) / $originalPrice;
		$discountAmount = $originalPrice - $discountedPrice;

		$flashdeal->discountPercentage = $discountPercentage;
		$flashdeal->discountAmount = $discountAmount;
		$flashdeal->originalPrice = $originalPrice;
		$flashdeal->discountedPrice = $discountedPrice;
		
		$flashdeal->flashDealPolicies = Input::get('flashDealPolicies');
		$flashdeal->bookingPeriodFrom = date("Y-m-d H:i:s",strtotime(Input::get('bookingPeriodFrom')));
		$flashdeal->bookingPeriodTo = date("Y-m-d H:i:s",strtotime(Input::get('bookingPeriodTo')));
		

		$words = explode(" ", Room::find($flashdeal->roomID)->name);
		$vnum = "F";
		foreach ($words as $w) {
			$vnum .= $w[0];
		}
		if($flashdeal->validate()){

			$flashdeal->save();
			$flashdeal->flashDealID = strtoupper($vnum) . $flashdeal->id;
			$flashdeal->save();
			return Redirect::back()->with('message','You have successfully edited the new flashDeal');

		}else{
			return Redirect::back()->withInput()->withErrors($flashdeal->errors);
		}
	}




	public function store($accomm)
	{
		
		$flashDeal = new Flashdeal();
		$flashDeal->accommID = $accomm->getKey();
		$flashDeal->roomID = Input::get('roomID');
		$flashDeal->voucherID = Input::get('voucherID');
		$flashDeal->status = Input::get('status');
		$flashDeal->flashDealName = Input::get('flashDealName');
		$flashDeal->summary = Input::get('summary');
		$flashDeal->highlights = Input::get('highlights');
		$flashDeal->breakfastIncluded = Input::has('breakfastIncluded');
		$flashDeal->flashDealAmount = Input::get('flashDealAmount');
		$flashDeal->flashDealDuration = Input::get('flashDealDuration');
		$flashDeal->minimumDaysPriorBooking = Input::get('minimumDaysPriorBooking');
		$flashDeal->redemptionPeriodFrom = date("Y-m-d H:i:s",strtotime(Input::get('redemptionPeriodFrom')));
		$flashDeal->redemptionPeriodTo = date("Y-m-d H:i:s",strtotime(Input::get('redemptionPeriodTo')));

		//salesLimit
		$startDate = new Carbon(Input::get('redemptionPeriodFrom'));
		$endDate = new Carbon(Input::get('redemptionPeriodTo'));
		$diffInDays =  $startDate->diffInDays($endDate);
		$numberOfRooms = Room::find($flashDeal->roomID)->roomAvailability;
		$flashDealPackageNights = $flashDeal->flashDealDuration - 1;
		$salesLimit = (($numberOfRooms * $diffInDays) / $flashDealPackageNights) / 2;
		$flashDeal->salesLimit =  floor($salesLimit);

		if(Input::get('discountPercentage') or Input::get('discountedPrice'))
		{
			$originalPrice = Room::find($flashDeal->roomID)->originalPrice;
			$discountedPrice = Input::get('discountedPrice');
			$discountPercentage = (($originalPrice - $discountedPrice) * 100) / $originalPrice;
			$discountAmount = $originalPrice - $discountedPrice;

			$flashDeal->discountPercentage = $discountPercentage;
			$flashDeal->discountAmount = $discountAmount;
			$flashDeal->originalPrice = $originalPrice;
			$flashDeal->discountedPrice = $discountedPrice;
			
			$flashDeal->flashDealPolicies = Input::get('flashDealPolicies');
			$flashDeal->bookingPeriodFrom = date("Y-m-d H:i:s",strtotime(Input::get('bookingPeriodFrom')));
			$flashDeal->bookingPeriodTo = date("Y-m-d H:i:s",strtotime(Input::get('bookingPeriodTo')));
		}
		

		$words = explode(" ", Room::find($flashDeal->roomID)->name);
		$vnum = "F";
		foreach ($words as $w) {
			$vnum .= $w[0];
		}
		if($flashDeal->validate())
		{
			$flashDeal->save();
			$flashDeal->flashDealID = strtoupper($vnum) . $flashDeal->id;
			$flashDeal->save();
			return Redirect::to('/partners/accomm/'.$flashDeal->accommID.'/flashdeals')->with('message','You have successfully created a new flashDeal');
			//return Redirect::back()->with('message','You have successfully created a new flashDeal');
		}else{
			return Redirect::back()->withInput()->withErrors($flashDeal->errors);

		}
	}

		public function ajaxVoucher($accomm,$voucherId){
		
		
		
		$voucher = Voucher::find($voucherId);
		return View::make('partner.accomms.flashDeals.ajaxvoucher')->with(['accomm'=>$accomm,'voucher' => $voucher]);
		
		} 
		
	public function destroy($accomm,$id)
	{
		$id = Crypt::decrypt($id);
		Flashdeal::destroy($id);
		return Redirect::back()->with('message','The selected flash deal is successfully removed');
	}
	
	public function soldFlashDeals($accomm){
		//echo "<pre>"; print_r($accomm->id); die;
		//$vouchers = DB::table("orders")->where("accommID", "=", $accomm->id)->get();
		
		$flashdeals = DB::table('orders')
            ->join('rooms', 'orders.roomID', '=', 'rooms.id')
            ->join('users', 'orders.userID', '=', 'users.id')
			->join('flashdeals', 'orders.flashdealID', '=', 'flashdeals.id')
			->orderBy("orders.created_at","desc")
            ->where("orders.accommID", "=",  $accomm->id)
			->where("orders.flashdealID", "<>",  0)
			->select('rooms.name', 'users.firstName', 'users.lastName', 'users.email' ,'flashdeals.flashDealName','orders.price','orders.paymentStatus','orders.created_at')
			->get();
			
		//echo "<pre>"; print_r($vouchers); die;
		return View::make('partner.accomms.flashdeals.soldflashdeal', compact('accomm', 'flashdeals'));
		//echo "<pre>"; print_r($vouchers); die;
		}
		
		
		public function validateFlashDeal($accomm){
		
			if(Input::get('submit') == 'Validate'){
					$qrcode = Input::get('qrcode');
					$accommID = Input::get('accommID');
					$details = DB::table('orders')
					->join('flashdeals','orders.flashdealID','=','flashdeals.id')
					->join('users','orders.userID','=','users.id')
					->where('flashDealCode','=',$qrcode)->where('FlashDealStatus','=','new')
					->where('flashdeals.redemptionPeriodFrom', '<=', date('Y-m-d H:i:s'))
					->where('flashdeals.redemptionPeriodTo', '>=', date('Y-m-d H:i:s'))
					->where('orders.accommID', '=', $accommID)
					->select('flashdeals.*','users.firstName', 'users.lastName', 'users.email','orders.flashDealCode as flashCode','orders.id as ordId')
					->get();
					
					
					if(count($details) > 0){
					
					 return View::make('partner.accomms.flashDeals.validateFlashDeal', compact('details','accomm'));
					}else{
							return Redirect::back()->with('errMessage','This flashdeal code has been expired or not valid, Please try with new flashdeal code.')->withInput();
						}
				}
				
				if(Input::get('submit') == 'Yes'){
					$orderID = Input::get('orderID');
					$accommID = Input::get('accommID');
					DB::table('orders')
					->where('id', $orderID)
					->where('accommID', $accommID)
					->update(array('flashDealStatus' => 'used'));
					
					return Redirect::back()->with('message','You have successfully used this flashdeal.');
				}
				
				
			return View::make('partner.accomms.flashDeals.validateFlashDeal', compact('accomm'));
		}
	

}

?> 