<?php

class RoomsController extends BaseController {

	/**
	 * Displays Room index page
	 *
	 * @return Response
	 */
	public function index($accomm)
	{
		return View::make('partner.accomms.rooms.index')->with('accomm',$accomm);
	}

	/**
	 * Displays the form for creating a new room
	 *
	 * @return Response
	 */
	public function create($accomm)
	{
		return View::make('partner.accomms.rooms.create')->with('accomm',$accomm);
	}

	/**
	 * Store a newly created room in storage.
	 *
	 * @return Response
	 */
	public function store($accomm)
	{
		//Get all uploaded files and put them in $files
		$files = Input::file('files');
		//Create a new room and populate its fields with user input
		$newRoom = new Room();
		$newRoom->fill(Input::only('name','description','roomAvailability','bedType','maxGuestsAllowed','originalPrice','view','roomSize'));
		$newRoom->breakfastIncluded = Input::has('breakfastIncluded');
		//specifiy the accommodation the room belongs to
		$newRoom->accommID = $accomm->id;
		$newRoom->status = "published";
		//See if the new room can be validated against the $rules array within Room model
		if($newRoom->validate()){
			//save the new room since it passed the validation
			$newRoom->save();
			// For every photo uoloaded do the following things
			foreach($files as $index => $file) {
				if($file){
					//get the original file name of image on clients machine
					$originalFileName  =  $file->getClientOriginalName() ;
					//get the path where the photo must be saved to from roomquickly config file
					$filePath = Config::get('roomquickly.acoommodationPhotosDirectory');		
					$fileName = $file->getFilename() . '.' . $file->getClientOriginalExtension();
					//move the file to the path we got from the config file
					$file->move($filePath,$fileName);
					// Initiate S3 & setting. Upload to S3
					$bucket = "rqphoto";
					$theFile = $filePath.$fileName;
					$s3 = AWS::get('s3');
					$s3->putObject(array(
					    'Bucket'     => $bucket,
					    'Key'        => $fileName,
					    'SourceFile' => $theFile,
					    'ACL'    => 'public-read',
					));
					// Delete Temporary File & Getting URL
					File::delete($theFile);
					//save the details of the file in the database;
					$roomPhoto = new RoomPhoto();
					$roomPhoto->roomID = $newRoom->getKey();
					$roomPhoto->title = 'untitled';
					$roomPhoto->fileName = $fileName;
					$roomPhoto->type = $bucket;
					$roomPhoto->originalFileName = $originalFileName;
					$roomPhoto->save();

				}

			}

			//--------------------Room Facilities----------------
			//array_filter removes the empty string keys from facilities array - array_filter returns assosiative 
			//array so we pass its result to array_values to convert it into a  normal array
			$facilities = array_values(array_filter(Input::get('facilities'), 'strlen'));
			foreach ($facilities as  $value) {
				RoomFacility::create(array('roomID' => $newRoom->getKey(), 'facility' => $value));
			}

			return Redirect::route('roomsIndex',array('accomm'=>$accomm->id))->with('message','You have successfully added a new room.');

		}else{
			return Redirect::back()->withInput()->withErrors($newRoom->errors);
		}

	}

	/**
	 * Display the specified room
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return View::make('rooms.show');
	}

	/**
	 * Show the form for editing the specified room.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function editRoom($accomm, $roomID)
	{
		//Find the room to be editted by searching its decrypted id
		$id = Crypt::decrypt($roomID);
		$room = Room::find($id);
		//Show the form for editing the specified room
		return View::make('partner.accomms.rooms.edit')->with('room', $room)->with('accomm', $accomm);
	}

	/**
	 * Update the specified room in database.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($accomm, $roomID)
	{
		//Find the room to be updated by its decrypted id
		$id = Crypt::decrypt($roomID);
		//get the list of all uploaded files
		$files = Input::file('files');
		//find the room and fill it up with updated data coming from user input
		$theRoom = Room::find($id);
		$theRoom->fill(Input::only('name','description','roomAvailability','bedType','maxGuestsAllowed','originalPrice','view','roomSize'));
		$theRoom->breakfastIncluded = Input::has('breakfastIncluded');
		//validate the room data against the $rules array within the Room model
		if($theRoom->validate()){
		// save the updated room in database since it passed the validation
		$theRoom->save();
		// go through all the uploaded files to store them
		foreach($files as $index => $file){
			if($file){
				//get the original file name on clients machine
				$originalFileName  =  $file->getClientOriginalName() ;
				//get the path to move the file to from roomquickly configuration file
				$filePath = Config::get('roomquickly.acoommodationPhotosDirectory');					
				$fileName = $file->getFilename() . '.' . $file->getClientOriginalExtension();
				//move the file to the location we fetched from configuration file
				$file->move($filePath,$fileName);
				// Initiate S3 & Setting. Upload to S3
				$bucket = "rqphoto";
				$theFile = $filePath.$fileName;
				$s3 = AWS::get('s3');
				$s3->putObject(array(
				    'Bucket'     => $bucket,
				    'Key'        => $fileName,
				    'SourceFile' => $theFile,
				    'ACL'    => 'public-read',
				));
				// Delete Temporary File & Getting URL
				File::delete($theFile);
				//Store the record of the phot in roomphotos table
				$roomPhoto = new RoomPhoto();
				$roomPhoto->roomID = $theRoom->getKey();
				$roomPhoto->title = 'untitled';
				$roomPhoto->fileName = $fileName;
				$roomPhoto->type = $bucket;
				$roomPhoto->originalFileName = $originalFileName;
				$roomPhoto->save();
			}
		}
		//remove all old facilities from the database
		RoomFacility::where('roomID',$theRoom->getKey())->delete();
		//get and store all selected facilities
		$facilities = array_values(array_filter(Input::get('facilities'), 'strlen'));
		foreach ($facilities as  $value) {
			RoomFacility::create(array('roomID' => $theRoom->getKey(), 'facility' => $value));
		}
		//Redirect the user and inform him that his changes are made to the application.
		return Redirect::back()->with('message', "You have successfully updated room details");
	}else{
		//return $newRoom->errors;
			return Redirect::back()->withInput()->withErrors($theRoom->errors);
	}
	}


	/**
	 * Remove the specified room  from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($accomm,$roomID)
	{
		//Get the room to be deleted by decrypting its id
		$id = Crypt::decrypt($roomID);
		//find the room and delete alls its vouchers and flash deals
		$room = Room::find($id);
		$room->voucher()->delete();
		$room->flashdeal()->delete();
		//finally delete the room itself
		$room->delete();
		

		return Redirect::back()->with('message','The selected room is successfully removed');

	}


	/**
	 * Remove the specified room photo  from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function removeRoomPhoto(){
		//Get the room photo to be deleted by decrypting its id

		$photoID = Crypt::decrypt(Input::get('photoID'));
		$photo = RoomPhoto::find($photoID);
		//delete the  file
		$photo->deletePhoto();
		return $photo->originalFileName;
	}

}
