<?php
class ContactController extends BaseController {

    

    public function index()
    {
        return View::make('common.contactus');
    }

    public function contactStore()
    {
		$name = Input::get('name');
		$email = Input::get('email'); 
		$content = Input::get('content');
       $rules = array(
			'name' => 'required',
			'email' => 'required|email|email',
			'content' => 'required',
			
			);

		$messages = array(
			'name.required'    => 'Please enter your name',
			'required' => 'This field is required',
			'email'      => 'Please enter a correct email address',
			'content.required'    => 'Please enter your Message'
			
			);
			
			
		$adminUser = array();
		$adminUser = DB::table("users")->where('typeID', '=', 2)->get(); 
		

		$validator = Validator::make(Input::all(), $rules,$messages);
		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator->messages())->withInput();
		}
		
		
		Mail::send('emails.public.contact', array('name' => $name,'email' => $email,'content' => $content), function($message) use($email,$adminUser)
		{
			$message->to($adminUser[0]->email)->from($email)->subject('[RoomQuickly]Contact!');
		});
	
	//return Redirect::back()->withErrors($validator->messages())->withInput();
   return Redirect::to('/contact-us')->with('message', 'Your message was successfully sent!');
    }
}
?>