<?php 
use Carbon\Carbon;
class PageController extends \BaseController {

	//displays the index page for vouchers
	public function index(){
		 $page = DB::table('pages')->get();
						
		return View::make('admin.accomms.pages.index', compact('page'));
	}

	//displays the form for creating a new voucher
	public function create()
	{
		
		return View::make('admin.accomms.pages.create');

	}


	//stores a new voucher
	public function store()
	{
		//create a new voucher	
		
		
		$title = Input::get('title');
		$content = Input::get('content');
       $rules = array(
			'title' => 'required',
			'content' => 'required'
			
			);

		$messages = array(
			'title.required'    => 'Please enter page title',
			'required' => 'This field is required',
			'content.required'    => 'Please enter page content'
			
			);
			
	
		

		$validator = Validator::make(Input::all(), $rules,$messages);
		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator->messages())->withInput();
		}
		
		$page = new Page();
				
					$page->title = $title;
					$page->content = $content;
					
					$page->save();
					
		
		
	
	//return Redirect::back()->withErrors($validator->messages())->withInput();
   return Redirect::to('/admin/pages')->with('message', 'You have succesfully added page!');
	}


	//Displays the form for editing the voucher
	public function edit($pageid)
	{
		//find the voucher to be edited by decrypting it's id
		 $page = DB::table('pages')->where('id','=',$pageid)->get();
		
		
		
		//Display the form for editing the voucher
		return View::make('admin.accomms.pages.edit')->with(compact('page'));

	}

	//Updates the specified voucher
	public function update($pageid)
	{
		$title = Input::get('title');
		$content = Input::get('content');
       $rules = array(
			'title' => 'required',
			'content' => 'required'
			
			);

		$messages = array(
			'title.required'    => 'Please enter page title',
			'required' => 'This field is required',
			'content.required'    => 'Please enter page content'
			
			);
			
	
		

		$validator = Validator::make(Input::all(), $rules,$messages);
		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator->messages())->withInput();
		}
		
		$page = Page::find($pageid);

			$page->title = $title;
			$page->content = $content;
				
		
		$page->save();
		return Redirect::to('/admin/pages')->with('message','You have successfully edited page');
	}

	//deletes the specified voucher 
	public function delete($pageid)
	{
		//find the voucher to be deleted by decrypting its id
		
		//remove the voucher and display a success message to the user
		Page::destroy($pageid);
		return Redirect::to('/admin/pages')->with('message','The selected page is successfully removed');

	}
	
	
	public function page($id)
	{
			$page = DB::table('pages')->where('id','=',$id)->get();
			 return View::make('common.page', compact('page'));
	}
	
	
}

?>