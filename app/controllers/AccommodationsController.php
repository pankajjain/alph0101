<?php

class AccommodationsController extends \BaseController {



	/**
	 *
	 * @return Response
	 * This action is responsible for displaing the Index page Partners Panel
	 * */
	public function index()
	{
		
		return View::make('partner.index');
	}

	/**
	 * Show the form for creating a new Accommodation.
	 *
	 * @return Response
	 */
	public function createAccommodation()
	{

		return View::make('partner.accomms.newAccomm');
	}


	/**
	 * Store the new accommodation details
	 */

	public function storeAccommodation()
	{
		//Define a MessageBag to use as error container. we use two different
		//validators and therefore we need a seperate MessageBag to keep error
		//messages of both validators. One validator is used for validating files
		//and one validator is used for validating user input.
		//
		$errorContainer = new Illuminate\Support\MessageBag();
		// Validate uploaded files
		$file = Input::file('files');
		//$uploadRules = array ("files" => "required|mimes:pdf");
		$uploadRules = array ("files" => "required");
		$fileValidator = Validator::make(Input::all(), $uploadRules);
		if( $fileValidator->fails()){
			//Since the validation failed we store it's error messages in $errorContainer
			$errorContainer = $fileValidator->errors();
		}
		// Creating new accommodation starts here
		$newAccomm = new Accomm();
		//fill the new accommodation with all data comning from Input
		$newAccomm->fill(Input::get());
		//set the userID field of the new accommodation to the user id of currently logged in user
		$newAccomm->userID = Sentry::getUser()->getKey();
		// Validated the new accommodation based on the $rules defined within Accomm model 
		if($newAccomm->validate())
		{			
			//Since the validation was successfull we can go ahead and save the new accommodation.
			$newAccomm->save();
			// Now we save the policies of the hotel within accommpolicies table.
			$accommPolicies = new AccommPolicy();
			// indicate the Accommodation that this policy belongs to
			$accommPolicies->accommID = $newAccomm->id;
			// and finally save the new policy.
			$accommPolicies->save();
				if($file){
					//Save the file record into the database and upload it to S3 cloud storage
					$originalFileName  =  $file->getClientOriginalName();
					$filePath = Config::get('roomquickly.acommodationCertificatesDirectory')  . "{$newAccomm->id}/";
					$fileName = $file->getFilename() . '.' . $file->getClientOriginalExtension();				
					$file->move($filePath,$fileName);
					$theFile = $filePath.$fileName;
					$bucket = "rqacommcert";
					$s3 = AWS::get('s3');
					// Upload to S3
					$s3->putObject(array(
					    'Bucket'     => $bucket,
					    'Key'        => $fileName,
					    'SourceFile' => $theFile,
					    'ACL'    => 'public-read',
					));
					// Remove tmp file & Get the URL
					File::delete($theFile);
					$theURL = $s3->getObjectUrl($bucket,$fileName);
					// Save the URL to DB
					$upload = new Upload();
					$upload->type = $bucket;
					$upload->refrenceID = $newAccomm->id;
					$upload->filename = $fileName;
					$upload->originalFileName = $originalFileName;
					$upload->save();

				}
			
			return Redirect::route('partnersIndex')->with('message',Lang::get('messages.newAccommodationSuccess'));
		}
		else
		{
			(!isset($errorContainer)) ? $errorContainer = $newAccomm->errors : $errorContainer->merge($newAccomm->errors->getMessages()); //merge with existing errors if present
			return Redirect::back()->withInput()->withErrors($errorContainer);
		}
	}

	/*
	 * Display the form for editing the accommodation
	 */
	public function edit($accomm)
	{
		Return View::make('partner.accomms.editAccomm')->with('accomm' , $accomm);
	}

	/*
	 * Display the form for editing  the accommodation and signal the view to display the sidebar
	 */
	public function editWithSideMenu($accomm){
		Return View::make('partner.accomms.editAccomm')->with('accomm' , $accomm)->with('showSideMenu',1);

	}

	/* 
	 * Update the accommodation
	 */
	 public function update($accomm){
	 	//Get the uploaded files
		 $file = Input::file('files');
		//$accomm already holds the accomm model so we do not need to fetch it from database
		//so we just clone it to later compare it agains the updated accoom and see if anything
		//is changed.
		 $oldAccomm = clone $accomm;
		//fill the $accomm with user input
		 $accomm->fill(Input::get());
		 // see if the updated accomm is any different from the accomm and also check if there is 
		 // any file uploaded. if both fail simply return to the previous page without changing anything 
		if($oldAccomm == $accomm and !Input::hasFile('files')) {
			return Redirect::route('partnersIndex')->with('message','No changes were made to the accommodation');
		};
		 //Since the accomm data is changed we will mark the accommodation as unverified again
		$accomm->isVerified = false;
		//save the accommodation
		$accomm->save();
		//if there are anyfiles uploaded, save them as well.	
			if($file){
					// File settings & initiation
					$originalFileName  =  $file->getClientOriginalName() ;
					$filePath = Config::get('roomquickly.acommodationCertificatesDirectory')  . "{$accomm->id}/";
					$fileName = $file->getFilename() . '.' . $file->getClientOriginalExtension();					
					$file->move($filePath,$fileName);
					$theFile = $filePath.$fileName;
					$bucket = "rqacommcert";
					$s3 = AWS::get('s3');
					// Upload to S3
					$s3->putObject(array(
					    'Bucket'     => $bucket,
					    'Key'        => $fileName,
					    'SourceFile' => $theFile,
					    'ACL'    => 'public-read',
					));
					// Remove tmp file & Getting URL
					File::delete($theFile);
					$theURL = $s3->getObjectUrl($bucket,$fileName);
					//save the details of the file in the database;
					$upload = new Upload();
					$upload->type = $bucket;
					$upload->refrenceID = $accomm->id;
					$upload->filename = $fileName;
					$upload->originalFileName = $originalFileName;
					$upload->save();
				}
			//return with success message. the success message comes from 'messages' language file

			return Redirect::route('partnersIndex')->with('message',Lang::get('messages.editAccommodationSuccess'));
	}

	/*
	 * Display a page for deleting the accommodation
	 */
	public function delete($accomm)
	{
		return View::make('partner.accomms.deleteAccomm')->with('accomm' , $accomm);
	}
	
	/*
	 * Delete the accommodation
	 * TODO :: remove all vouchers and flash deals belonging to the accommodation
	*/
	public function destroy($id){
		$accommID = Crypt::decrypt(Input::get('accommid'));
		$accomm = Accomm::find($accommID);
		$accomm->delete();
		return Redirect::route('partnersIndex')->with('message' , Lang::get('messages.accommodationDeleteSuccess', array('name' => $accomm->name)));
	}

	/*
	 * Display the index page for $accomm
	 */
	public function accommodationIndex(Accomm $accomm)
	{	
		return View::make('partner.accomms.accomm.accommIndex')->with('accomm',$accomm);
	}

	/*
	 * Display the page for adding/removing facilities to $accomm
	 */
	public function accommodationFacilities($accomm){
		/*echo "<pre>";
		print_r($accomm); die;*/
		return View::make('partner.accomms.accomm.facilities')->with('accomm',$accomm);
	}

	/*
	 * Display the page for adding/removing Sports and Recreation facilities
	*/
	public function accommodationSarFacilities($accomm){
		return View::make('partner.accomms.accomm.sarFacilities')->with('accomm',$accomm);	
	}
	
	/*
	 * Add facilities to an accommodation
	 */
	public function addFacility()
	{
		//Get tht the accommodation  ID and decrypt it
		
		$accommID = Input::get('aid');
		$accommID = Crypt::decrypt($accommID);
		//See if this is a standard facility or Sports and Recreation facility
		$type = Input::get('type');		
		//Store the new facility
		AccommFacility::create(array('accommID'=>$accommID,'facility' => Input::get('facility'),'type' => $type));
		return '1';
	}

	/*
	 * Removes a facility previously given to an accommodation
	 */
	public function removeFacility(){
		//Get tht the accommodation  ID and decrypt it
		$accommID = Input::get('aid');
		$accommID = Crypt::decrypt($accommID);
		$facility = Input::get('facility');
		//find the facility in database and remove it
		$AccommFacility = AccommFacility::where('accommID',$accommID)->where('facility',$facility)->first();
		$AccommFacility->delete();
		return 1;
	}
	/*
	 * Displays the page for specifiying policies of the accommodaton
	 */
	public function accommodationPolicies($accomm)
	{
		//$policies = DB::table('accommpolicies')->where('accommID', $accomm->id)->get();
		$policies = AccommPolicy::where('accommID',$accomm->id)->first();
		//echo "<pre>"; print_r($policies);die;
		return View::make('partner.accomms.accomm.policies')->with('accomm',$accomm)->with('policies', $policies);
	}


	/*
	 * Updates the policies of the accommodation.
	 */
	public function updateAccommodationPolicies($accomm)
	{
		$accommPolicies = $accomm->policies;
		$accommPolicies->fill(Input::all());
		$accommPolicies->childrenStayFree = Input::has('childrenStayFree') ? true : false;
		$accommPolicies->save();
		return Redirect::back()->with('message','You have successfully updated the policies');
	}

	/*
	 * Displays the page for adding/removing accommodation policies.
	 */
	public function acccommodationPhotos($accomm){
		return View::make('partner.accomms.accomm.photos')->with('accomm',$accomm);
	}

	/*
	 * Stores photos uploaded for an accommodaton
	 */
	public function storePhotos($accomm){
		//Get all the uploaded files along with their original name and extension
		$files = Input::file();
		$originalFileName = Input::get('name');
		$clientOriginalExtension = File::extension($originalFileName);
		//for every file uploaded
		foreach ($files as $index => $file) {
			if($file){
			//Get the directory to store the file in from roomquickly configuration file
				$filePath = Config::get('roomquickly.acoommodationPhotosDirectory')  . "{$accomm->id}/";					
				$fileName = $file->getFilename() . '.' . $clientOriginalExtension;
				$fileName_268 = '268_'.$file->getFilename() . '.' . $clientOriginalExtension;
				$theFile = $filePath.$fileName;
				$theFile_268 = $filePath.$fileName_268;

				// Save to photo dir and resize
				$file->move($filePath,$fileName);
				Image::make($theFile)->resize(268, null,true)->save($theFile_268);

				$bucket = "rqphoto";
				$s3 = AWS::get('s3');
				$s3_268 = AWS::get('s3');
				// Upload Normarl Image to S3
				$photoResult = $s3->putObject(array(
				    'Bucket'     => $bucket,
				    'Key'        => $fileName,
				    'SourceFile' => $theFile,
				    'ACL'    => 'public-read',
				));
				// Upload Thumbnail to S3
				$thumbResult = $s3_268->putObject(array(
				    'Bucket'     => $bucket,
				    'Key'        => $fileName_268,
				    'SourceFile' => $theFile_268,
				    'ACL'    => 'public-read',
				));
				// Remove tmp file & Getting URL
				File::delete($theFile);
				File::delete($theFile_268);
				$theURL = $s3->getObjectUrl($bucket,$fileName);
				$theURL_268 = $s3_268->getObjectUrl($bucket,$fileName_268);

				// Save to DB
				$upload = new AccommPhotos();
				$upload->type = $bucket;
				$upload->accommID = $accomm->id;
				$upload->filename = $fileName;
				$upload->originalFileName = $originalFileName;
				$upload->save();
			}
		}
		$response = array ('id' => $upload->id, 'thumbURL' =>  $thumbResult['ObjectURL'] , 'photoURL' =>  $photoResult['ObjectURL']);
		return $response;
	}


	/*
	 * Removes a photo previously uploaded to an accommodation
	 */
	public function removePhoto($accomm){
		//Check if the request is an Ajax request
		if(Request::ajax()){
			//Find the record of the file to be deleted
			$accommPhoto = AccommPhotos::where('id',Input::get('fid'))->get()->first();
			//Find its thumbnail filename
			$thumbFile = "268_".$accommPhoto->fileName;
			//Find the file in Amazon S3 cloud storage
			$s3 = AWS::get('s3');
			$s3->deleteObject(array(
			    'Bucket'     => $accommPhoto->type,
			    'Key'        => $accommPhoto->fileName,
			));
			//Remove the file from S3
			$s3->deleteObject(array(
			    'Bucket'     => $accommPhoto->type,
			    'Key'        => $thumbFile,
			));
			//Remove the file record from database
			$accommPhoto->delete();
		}
	}

	/*
	 * Updates details of a  previously uploaded photo
	 */
	public function updatePhotoDetails($accomm,$photoID){
		//Find the photo whose details must be updated
		$photo = AccommPhotos::find(Input::get('pid'));
		//check that the photo belongs to the accommodation and that the accommodation belongs to the user		
		if(($accomm->id == $photo->accommID) and ($accomm->userID == Sentry::getUser()->getKey())){
			//Turn the input data array into variables
			extract(Input::get());
			//Update the photo
			$photo->title = $title;
			$photo->description = $description;
			$photo->save();
			//Save and return the photo
			return $photo;
		}
		//If the photo does not belong to the accommodation or accommodation does not belong the user
		//we deny the access to the resource
		return 'Access Denied';
	}

	/*
	 * Displays the page for updating the MAP location of the accommodation.
	 */
	public function accommodationMap($accomm)
	{
		return View::make('partner.accomms.accomm.map')->with('accomm',$accomm);
	}

	
	/*
	 * Displays the setting page for the accommodation
	 */
	public function accommodationSetting($accomm){
		return View::make('partner.accomms.accomm.setting')->with('accomm',$accomm);
	}

	
	/*
	 * updates the settings of the  accommodation
	 */
	public function updateAccommodationSetting($accomm){
		$accommSetting = Accomm::find($accomm->id);
		$accommSetting->enableReview = Input::get('enableReview');
		$accommSetting->save();
		return Redirect::back()->with('message','You have successfully updated the setting');

	}

	/*
	 * Displays the page for updating the detailed information of the accommodation
	 */
	public function accommodationXtraInfo($accomm)
	{
		return View::make('partner.accomms.accomm.xtraInfo')->with('accomm',$accomm);
	}

	/*
	 *  updates the detailed information of the accommodation
	 */

	public function updateAccommodationXtraInfo($accomm)
	{
		//Define the rules against which the user input must be validated
		$rules = array(
			'numberBars' => 'numeric',
			'numberRestaurant' => 'numeric',
			'numberRoom' => 'numeric',
			'numberFloor' => 'numeric',
			'roomVoltage' => '',
			'internetUsage' => '',
			'breakfastCharge' => 'numeric',
			'roomService' => '',
			'nonSmokingRoom' => '',
			'elevator' => '',
			'parking' => '',
			'parkingFee' => 'numeric',
			'receptionOpen' => '',
			'checkIn' => '',
			'checkOut' => '',
			'hotelBuilt' => 'numeric',
			'hotelRenovated' => 'numeric',
			'airportTransfer' => '',
			'airportTransferFee' => 'numeric',
			'distanceCity' => 'numeric',
			'distanceAirport' => 'numeric',
			'timeAirport' => 'numeric',
			'hotelDescription' => '',
		);

		//Create a validator object to validate user data agains the $rules array
		$validator = Validator::make(Input::all(), $rules);
		//If validation fails, return to the previous page with errors
		if ($validator->fails()) {
            return Redirect::back()->withErrors($validator->messages())->withInput();
        }

        $acom = Accomm::find($accomm->id);
		$acom->numberBars = Input::get('numberBars');
		$acom->numberRestaurant = Input::get('numberRestaurant');
		$acom->numberRoom = Input::get('numberRoom');
		$acom->numberFloor = Input::get('numberFloor');
		$acom->roomVoltage = Input::get('roomVoltage');
		$acom->internetUsage = Input::get('internetUsage');
		$acom->breakfastCharge = Input::get('breakfastCharge');
		$acom->roomService = Input::get('roomService');
		$acom->nonSmokingRoom = Input::get('nonSmokingRoom');
		$acom->elevator = Input::get('elevator');
		$acom->parking = Input::get('parking');
		$acom->parkingFee = Input::get('parkingFee');
		$acom->receptionOpen = Input::get('receptionOpen');
		$acom->checkIn = Input::get('checkIn');
		$acom->checkOut = Input::get('checkOut');
		$acom->hotelBuilt = Input::get('hotelBuilt');
		$acom->hotelRenovated = Input::get('hotelRenovated');
		$acom->airportTransfer = Input::get('airportTransfer');
		$acom->airportTransferFee = Input::get('airportTransferFee');
		$acom->distanceCity = Input::get('distanceCity');
		$acom->distanceAirport = Input::get('distanceAirport');
		$acom->timeAirport = Input::get('timeAirport');
		$acom->hotelDescription = Input::get('hotelDescription');
		$acom->save();

		$message = "Information saved!";
		return Redirect::back()->with('message', $message);
	}

	//Stores the longtitue and latitude information for a given accommodation
	public function accommodationMapStore($accomm)
	{
		//get the longtitude and latittude values from input and store them
		$accomm->lng = Input::get('lng');
		$accomm->lat = Input::get('lat');
		$accomm->save();

		return Redirect::route('accommmodationMap',array('accomm' => $accomm->getKey()))->with('message',"Map details successfully updated");
	}

	/*
	* Displays the total number of standard facilities for a given accommodation
	*/
	public function getFacilityCount($accommid){
		// return $accomm->getFacilitiesCount("standard");
		echo AccommFacility::where('accommID',$accommid)->where('type',"standard")->count();
	}

	/*
	* Displays the total number of Sports and Recreation facilities for a given accommodation
	*/
	public function getsrFacilityCount($accommid){
		echo AccommFacility::where('accommID',$accommid)->where('type',"s&r")->count();
	}


	/*
	* Displays the page for summerizing all the activities performed on a partner account
	*/

	public function showActivityLogPartner($accommid){
		return View::make('partner.accomms.activityLog.index')->with('accomm',$accommid);
	}


	


}
