<?php
/**
 * This file is a part of a project by ITECHSPARK.
 * User: Saarang
 */
class AuthController extends BaseController {

    //This action is responsible for authenticating and signing users in.	
    public function login()
    {
	    //get users credentials from Input
        $credentials = array(
            'email' => Input::get('email'),
            'password' => Input::get('password'
                ));
	try {
		//using Sentry package, attempt to validate and authenticate the credentials

            $user = Sentry::authenticate($credentials, false);
		
	    //now that the user is authenticated, we redirect him to his dashboard accoring to his usertype
	    //available typeIDs are 1,2 and 3 and are assigned to partners, admins, and member users respectively

            switch($user->typeID){
                case 1 : return Redirect::route('partnersIndex');break;
                case 2 : return Redirect::route('adminIndex');break;
                case 3 : return Redirect::route('publicIndex');break;
            }

	
	//if authentication fails depending on the error we produce a message to
	    //display tot he user.
        } catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
            $errorMessage = 'Login field is required.';

        }
        catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
            $errorMessage = 'Password field is required.';

        }
        catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
            $errorMessage = 'Wrong password, try again.';

        }
        catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            $errorMessage = 'User was not found.';

        }
        catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
            $errorMessage = 'User is not activated.';

        }
        catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e) {
            $errorMessage = 'User is suspended for 15 minutes.';

        }
        catch (Cartalyst\Sentry\Throttling\UserBannedException $e) {
            $errorMessage = 'User is banned.';

        }
	
	//now we redirect the user back to SignIn page and display the error message.
        return Redirect::back()->withErrors(['SentryError' => $errorMessage]);

    }

    //this action is responsible for loging users out
    public function logout(){
	    Sentry::logout();
	    //after the log out, lets redirect the user to the public page.
        return Redirect::route('publicIndex');
    }

    public function resetCredential(){
        try {
            $user = Sentry::findUserByLogin(Input::get('email'));
            // $user->password = "DesiredPassword"; 
            // $user->save();
            $resetCode = $user->getResetPasswordCode();
            $randPass = $this->randomPassword();
             
            if($user->attemptResetPassword($resetCode, $randPass)){
                $message = "Password reset successfully!";
                Mail::send('emails.public.resetPassword', ['password' => $randPass,'name'=>$user->firstName.' '.$user->lastName], function($message) use($user)
                {
                    $message->to($user->email, $user->firstName.' '.$user->lastName)->subject('[RoomQuickly]Password reset successfully!');
                });
                return Redirect::route('signin')->with('message',$message);
            } else {
                $message = "Password reset error!";
                return Redirect::route('signin')->with('errormsg',$message);
            }
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            $message = 'User was not found.';
            return Redirect::route('signin')->with('errormsg',$message);
        }
    }

    public function resentValidation(){
        $user = Sentry::findUserByLogin(Input::get('email'));
        if(!$user->activated){
            $url = URL::route('confirmUser',array('userID' => Crypt::encrypt($user->getKey())));
            Mail::send('emails.public.confirmSignUp', ['link' => $url], function($message) use($user)
            {
                //$message->to($user->email, $user->getFullName())->subject('[RoomQuickly]Welcome!');
				$message->to($user->email)->subject('[RoomQuickly]Welcome!');
            });
            $message = "Verification email resent!";
        } else {
            $message = "Account Verified";
        }
        return Redirect::route('signin')->with('message',$message);
    }

    // Authentication required before they changing password
    public function changePassword(){
        if ( ! Sentry::check()){
            $message = "Please login to continue";
            return Redirect::route('signin')->with('message', $message);
        } else {
            return View::make('public.changePass');
        }
    }

    public function postChangePassword(){
        $rules = array(
            'newpassword' => 'required',
            'cfmnewpassword' => 'required|same:newpassword',
        );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator->messages())->withInput();
        }

        $user = Sentry::findUserByLogin(Sentry::getUser()->email);
        $user->password = Input::get('newpassword');
        $user->save();
        $message = "Password changed!";
        return Redirect::to('changePassword')->with('message', $message);

    }

    public function confirmUser($userID){
        $user = Sentry::findUserById(Crypt::decrypt($userID));
        $user->activated = true;
        $user->save();
        Sentry::login($user, false);
        switch($user->typeID){
            case 1 : return Redirect::route('partnersIndex');break;
            case 2 : return Redirect::route('adminIndex');break;
            case 3 : return Redirect::route('publicIndex');break;
        }
    }

    protected static function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }


}
