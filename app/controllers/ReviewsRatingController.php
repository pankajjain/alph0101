<?php

class ReviewsRatingController extends BaseController {

	/**
	 * Displays Room index page
	 *
	 * @return Response
	 */
	public function index($accomm)
	{
		$review = DB::table('reviewsrating')
                  ->join('users', 'reviewsrating.byUserID', '=', 'users.id')
			       ->where("reviewsrating.accommID", "=",  $accomm->id)
			->select('users.firstName', 'users.lastName', 'reviewsrating.rating','reviewsrating.negativeReviews','reviewsrating.positiveReviews','reviewsrating.created_at')
			->orderBy('created_at','desc')
			->get();
			
			//echo "<pre>"; print_r($review);die; 
		//return View::make('partner.accomms.reviewsrating.index')->with('accomm',$accomm);
		 return View::make('partner.accomms.reviewsrating.index', compact('accomm','review'));
	}
	
	
	
	public function getUserReviewRatingInAdmin(){
		/*$review = DB::table('reviewsrating')
                  ->join('users', 'reviewsrating.byUserID', '=', 'users.id')
				   ->join('accommodations', 'reviewsrating.accommID', '=', 'accommodations.id')
			       ->select('accommodations.id as accommID','accommodations.name as accommName','users.firstName', 'users.lastName', 'reviewsrating.rating','reviewsrating.negativeReviews','reviewsrating.positiveReviews','reviewsrating.created_at')
			->orderBy('created_at','desc')
			->get();*/
			
			
			$review = DB::table('reviewsrating')
			->join('accommodations', 'reviewsrating.accommID', '=', 'accommodations.id')
			->select(DB::raw('accommodations.id as accommID,accommodations.name,count(*) as ratcount , sum(rating) as rat'))->groupBy('accommID')->get();
			
			//echo "<pre>"; print_r($review);die; 
		//return View::make('partner.accomms.reviewsrating.index')->with('accomm',$accomm);
		 return View::make('admin.report.userReviewRating', compact('review'));
	}

	

	/**
	 * Displays the form for creating a new room
	 *
	 * @return Response
	 */
	/*public function create($accomm)
	{
		return View::make('partner.accomms.rooms.create')->with('accomm',$accomm);
	}*/

	
	/**
	 * Display the specified room
	 *
	 * @param  int  $id
	 * @return Response
	 */
	/*public function show($id)
	{
		return View::make('rooms.show');
	}*/

	

	

	


	
}
