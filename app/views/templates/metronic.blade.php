<?php
/*
* 	All Rights Reserved © 2013, iTechSpark Inc.
* 	Source: /Users/spencerlim/Dropbox/Development/Furdino/app/views/templates/metronic.blade.php
*  Author: iTechSpark Inc.
* 	Filename: metronic.blade.php
* 	Timestamp: 29, Sept 2013
* 
* 	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* 	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
* 	FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
* 	COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
* 	IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
* 	CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="The only best deal in town with more than thousand coupon from famous accommodations you knew!" name="description" />
	<meta content="iTechSpark Inc." name="author" />
	<meta name="robots" content="noindex">
	<title>@yield('title') | RoomQuickly</title>
	@include('templates.metronicludes')
</head>
<body class="page-header-fixed">
	@yield('sideMenu')
	@include('templates.metronicNavi')
	<div class="page-container">
	
	@section('sidebar')
		@include('templates.metronicSide')		
	@show	
		<div class="page-content">
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12" style="margin-top:15px;">
						@yield('content')
					</div>
				</div>

			</div> 
		</div>
	</div>
	@include('templates.metronicFoot')
	@include('templates.metronicludes2')
	@yield('inlineJS')
</body>
</html>