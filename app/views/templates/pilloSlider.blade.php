<section class="slider clearfix">
	<div id="sequence">
		<ul>
			<li>
				<div class="info animate-in">
					<h2>Last minute Winter escapes</h2><br />
					<p>January 2013 holidays 40% off! An unique opportunity to realize your dreams</p>
				</div>
				<img class="main-image animate-in" src="{{ URL::to('static/pillo/images/slider/1.jpg') }}" alt="" />
			</li>
			<li>
				<div class="info animate-in">
					<h2>Check out our top weekly deals</h2><br />
					<p>Save Now. Book Later.</p>
				</div>
				<img class="main-image animate-in" src="{{ URL::to('static/pillo/images/slider/2.jpg') }}" alt="" />
			</li>
			<li>
				<div class="info animate-in">
					<h2>Check out last minute flight, hotel &amp; vacation offers!</h2><br />
					<p>Save up to 50%!</p>
				</div>
				<img class="main-image animate-in" src="{{ URL::to('static/pillo/images/slider/6.jpg') }}" alt="" />
			</li>
		</ul>
	</div>
</section>

<!--search-->
<div class="main-search">
	<form id="main-search" method="post" action="search_results.html">
		 
		<div class="forms">
			<!--form hotel-->
			<div class="form" id="form1">
				<!--column-->
				<div class="column where">
					<h4 style="">Where?</h4>
					<div class="f-item">

						<label for="destination1"></label>
						<input type="text" placeholder="City, region, district or specific hotel" id="destination1" name="destination" />
					</div>
				</div>
				<!--//column-->
				
				<!--column-->
				<!-- <div class="column twins">
					<h4><span>02</span> When?</h4>
					<div class="f-item datepicker">
						<label for="datepicker1">Check-in date</label>
						<div class="datepicker-wrap"><input type="text" placeholder="" id="datepicker1" name="datepicker1" /></div>
					</div>
					<div class="f-item datepicker">
						<label for="datepicker2">Check-out date</label>
						<div class="datepicker-wrap"><input type="text" placeholder="" id="datepicker2" name="datepicker2" /></div>
					</div>
				</div> -->
			</div>	
			<!--//form hotel-->
		</div>
		<input type="submit" value="Proceed to results" class="search-submit" id="search-submit" />
	</form>
</div>
<!--//search-->