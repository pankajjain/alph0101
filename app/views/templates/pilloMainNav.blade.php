<!--main navigation-->
<nav class="main-nav" role="navigation" id="nav">
    <ul class="wrap">
        <li><a href="{{URL::to('/')}}" title="Home">Home</a></li>
        <li><a href="searchNearby.php" title="About Us">About Us</a></li>
        <li><a href="{{URL::to('/hotel')}}" title="Hotel Listing"> Hotels</a></li>
        
        <li><a href="{{URL::to('/voucher')}}" title="Featured Hotels">Vouchers</a></li>
        <li><a href="{{URL::to('/flashdeal')}}" title="Featured Hotels">FlashDeals</a></li>
        
        <li><a href="{{URL::to('/contact-us')}}" title="Contact Us">Contact Us</a></li>
       
    </ul>
</nav>
<!--//main navigation-->