<!--ribbon-->

<script>
$(document).ready(function(){
	$('.ribbon li').hide();
	$('.ribbon li.active').show();
	$('.ribbon li a').click(function() {
		$('.ribbon li').hide();
		if ($(this).parent().parent().hasClass('open'))
			$(this).parent().parent().removeClass('open');
		else {
			$('.ribbon ul').removeClass('open');
			$(this).parent().parent().addClass('open');
		}
		$(this).parent().siblings().each(function() {
			$(this).removeClass('active');
		});
		$(this).parent().attr('class', 'active'); 
		$('.ribbon li.active').show();
		$('.ribbon ul.open li').show();
		return true;
	});
	})
</script>
<div class="ribbon">
    <nav>

        <ul class="profile-nav">
            <li class="active" style="display: block;"><a href="#" title="My Account">My Account</a></li>
            @if(Sentry::check())
                <li style="display: none;"><a href="{{URL::route('sampleMyAccount')}}" title="Manage Account">Account</a></li>
                <li style="display: none;"><a href="{{URL::route('changepassword')}}" title="Change Password">Password</a></li>
                <li class="last" style="display: none;"><a href="{{URL::route('signout')}}" title="Sign Out">Sign Out</a></li>
            @else 
                <li style="display: none;"><a href="{{URL::route('signin')}}" title="Sign in">Sign in</a></li>
                <li class="last" style="display: none;"><a href="{{URL::route('signupMembers')}}" title="Sign Up">Sign Up</a></li>
            @endif
        </ul>

       <?php /*?> <ul class="lang-nav">
            <li class="active" style="display: block;"><a href="#" title="Language">Language</a></li>
            <li style="display: none;"><a href="{{ route('langSetter', 'en') }}" title="English">English</a></li>
            <li style="display: none;"><a href="{{ route('langSetter', 'my') }}" title="Malay">Malay</a></li>
            <li style="display: none;"><a href="{{ route('langSetter', 'zh-CN') }}" title="简体中文">简体中文</a></li>
            <li class="last" style="display: none;"><a href="{{ route('langSetter', 'zh-HK') }}" title="繁體中文">繁體中文</a></li>
        </ul>
        
        <ul class="currency-nav">
            <li class="active" style="display: block;"><a href="#" title="Currency">Currency</a></li>
            <li style="display: block;"><a href="{{ route('currSetter', 'MYR') }}" title="RM Ringgit">RM Ringgit</a></li>
            <li style="display: block;"><a href="{{ route('currSetter', 'USD') }}" title="$US Dollar">$US Dollar</a></li>
            <li class="last" style="display: block;"><a href="{{ route('currSetter', 'GBP') }}" title="£ Pound">£ Pound</a></li>
        </ul><?php */?>
    </nav>
</div>
<!--//ribbon-->