<!--footer-->
<div class="clearclear"></div>
    <footer>
        <div class="wrap clearfix">
            <!--column-->
            <article class="one-fourth">
                <h3>Room Quickly</h3>
                <p>1400 Pennsylvania Ave. Washington, DC</p>
                <p><em>P:</em> 24/7 customer support: 1-555-555-5555</p>
                <p><em>E:</em> <a href="#" title="booking@mail.com">booking@mail.com</a></p>
            </article>
            <!--//column-->
            
            <!--column-->
            <article class="one-fourth">
                <h3>Customer support</h3>
                <ul>
                    <li><a href="{{URL('/page/2')}}" title="Faq">Faq</a></li>
                    <li><a href="{{URL('/page/4')}}" title="How do I make a reservation?">How do I make a reservation?</a></li>
                    <li><a href="{{URL('/page/5')}}" title="Payment options">Payment options</a></li>
                    <li><a href="{{URL('/page/6')}}" title="Booking tips">Booking tips</a></li>
                </ul>
            </article>
            <!--//column-->
            
            <!--column-->
            <article class="one-fourth">
                <h3>Follow us</h3>
                <ul class="social">
                    <li class="facebook"><a href="#" title="facebook">facebook</a></li>
                    <li class="youtube"><a href="#" title="youtube">youtube</a></li>
                    <li class="rss"><a href="#" title="rss">rss</a></li>
                    <li class="linkedin"><a href="#" title="linkedin">linkedin</a></li>
                    <li class="googleplus"><a href="#" title="googleplus">googleplus</a></li>
                    <li class="twitter"><a href="#" title="twitter">twitter</a></li>
                    <li class="vimeo"><a href="#" title="vimeo">vimeo</a></li>
                    <li class="pinterest"><a href="#" title="pinterest">pinterest</a></li>
                </ul>
            </article>
            <!--//column-->
            
            <!--column-->
            <style>
			.footer_form label{display:none;}
            .footer_form input[type="email"]{
				border: 1px solid #CCCCCC;
				border-radius: 6px;
				color: #999999;
				font: 1.2em 'OpenSansRegular';
				padding: 6px 4%;
				width: 67%;	
				}
			footer input[type="submit"] {
				border-radius: 0 6px 6px 0;
				height: 31px;
				padding: 0;
				position: absolute;
				right: 0;
				top: 0;
				width: 70px;
				background: linear-gradient(to bottom, #FDD22B 0%, #EFBA01 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
				color:#fff;
				font:11px/30px 'OpenSansBold';
				text-shadow:0 -1px 0 rgba(0, 0, 0, 0.2);
				text-transform:uppercase;
				text-align:center

			}
			footer input[type="submit"]:hover {
				 background: linear-gradient(to bottom, #EFBA01 0%, #FDD22B 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
				text-shadow: 0 1px 0 rgba(0, 0, 0, 0.2);
			}
			footer .footer_form form{ position:relative}
			footer .alert{ position:absolute; top:50px}
            </style>
            <article class="one-fourth last footer_form">
                <h3>Don’t miss our exclusive offers</h3>
                
                @include('laravel-newsletter-signup::signup')
                <?php /*?>@include('laravel-newsletter-signup::unsubscribe')<?php */?>
                <?php /*?><form id="newsletter" action="newsletter.php" method="post">
                    <fieldset>
                        <input type="email" id="newsletter_signup" name="newsletter_signup" placeholder="Enter your email here" />
                        <input type="submit" id="newsletter_submit" name="newsletter_submit" value="Signup" class="gradient-button" />
                    </fieldset>
                </form><?php */?>
            </article>
            <!--//column-->
            
            <section class="bottom">
                <p class="copy">Copyright {{ date('Y') }} RoomQuickly ltd. All rights reserved</p>
                <nav>
                    <ul>
                        <li><a href="{{URL('/page/3')}}" title="About us">About us</a></li>
                        <li><a href="{{URL::to('/contact-us')}}" title="Contact">Contact</a></li>
                        <li><a href="{{URL::route('signupPartners')}}" title="Partners">Partners</a></li>
                        <li><a href="{{URL('/page/7')}}" title="Customer service">Customer service</a></li>
                        <li><a href="{{URL('/page/2')}}" title="FAQ">FAQ</a></li>
                        <li><a href="{{URL('/page/8')}}" title="Careers">Careers</a></li>
                        <li><a href="{{URL('/page/9')}}" title="Terms & Conditions">Terms &amp; Conditions</a></li>
                        <li><a href="{{URL('/page/10')}}" title="Privacy statement">Privacy statement</a></li>
                    </ul>
                </nav>
            </section>
        </div>
    </footer>
    <!--//footer-->