<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<meta content="width=device-width, initial-scale=1.0" name="viewport" />
		<meta content="The only best deal in town with more than thousand coupon from famous accommodations you knew!" name="description" />
		<meta content="iTechSpark Inc." name="author" />
		<title>@yield('title') | RoomQuickly</title>
		{{ HTML::style('static/metronic/plugins/bootstrap/css/bootstrap.css') }}
		{{ HTML::style('static/metronic/plugins/bootstrap/css/bootstrap-responsive.min.css') }}
		{{ HTML::style('static/metronic/plugins/font-awesome/css/font-awesome.min.css') }}
		{{ HTML::style('static/metronic/css/style-metro.css') }}
		{{ HTML::style('static/metronic/css/style2.css') }}
		{{ HTML::style('static/metronic/css/style-responsive.css') }}
		{{ HTML::style('static/metronic/css/themes/default.css') }}
		{{ HTML::style('static/metronic/plugins/uniform/css/uniform.default.css') }}
		{{ HTML::style('static/metronic/plugins/select2/select2_metro.css') }}
		{{ HTML::style('static/metronic/css/pages/login-soft.css') }}
		<link rel="shortcut icon" href="favicon.ico" />
	</head>
	<body @yield('bodyClass')>

		<div class="logo">
			<img src="/static/metronic/img/logo.png" alt="" /> 
		</div>
		<div class="content">
			@yield('content')
		</div>
		<div class="copyright">
			{{ date('Y') }} &copy; RoomQuickly.
		</div>
		{{ HTML::script('static/metronic/plugins/jquery-1.10.1.min.js') }}
		{{ HTML::script('static/metronic/plugins/jquery-migrate-1.2.1.min.js') }}
		<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
		{{ HTML::script('static/metronic/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js') }}
		{{ HTML::script('static/metronic/plugins/bootstrap/js/bootstrap.min.js') }}
		{{ HTML::script('static/metronic/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js') }}
		<!--[if lt IE 9]>
		<script src="static/metronic/plugins/excanvas.min.js"></script>
		<script src="static/metronic/plugins/respond.min.js"></script>  
		<![endif]-->  
		{{ HTML::script('static/metronic/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}
		{{ HTML::script('static/metronic/plugins/jquery.blockui.min.js') }}
		{{ HTML::script('static/metronic/plugins/jquery.cookie.min.js') }}
		{{ HTML::script('static/metronic/plugins/uniform/jquery.uniform.min.js') }}
		<!-- PAGE LEVEL PLUGINS -->
		{{ HTML::script('static/metronic/plugins/jquery-validation/jquery.validate.min.js') }}
		{{ HTML::script('static/metronic/plugins/backstretch/jquery.backstretch.min.js') }}
		{{ HTML::script('static/metronic/plugins/select2/select2.min.js') }}
		<!-- PAGE LEVEL SCRIPTS -->
		{{ HTML::script('static/metronic/scripts/app.js') }}
		{{ HTML::script('static/metronic/scripts/login-soft.js') }} 
		<script>
			jQuery(document).ready(function() {
				App.init();
			    Login.init();
			});
		</script>
		@yield('inlineJS')
	</body>
</html>