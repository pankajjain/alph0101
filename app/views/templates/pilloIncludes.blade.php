<link rel="stylesheet" href="{{ URL::to('static/pillo/css/style.css') }}" type="text/css" media="screen,projection,print" />
<link rel="stylesheet" href="{{ URL::to('static/pillo/css/theme-yellow.css') }}" type="text/css" media="screen,projection,print" />
<link rel="stylesheet" href="{{ URL::to('static/pillo/css/prettyPhoto.css') }}" type="text/css" media="screen" />
<link rel="stylesheet" href="{{ URL::to('static/pillo/css/custom.css') }}" type="text/css" media="screen,projection,print" />
<link rel="shortcut icon" href="{{ URL::to('static/pillo/images/favicon.ico') }}" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>

@yield('pageAssets')

