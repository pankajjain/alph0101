{{ HTML::script('static/metronic/plugins/jquery-1.10.1.min.js') }}
{{ HTML::script('static/metronic/plugins/jquery-migrate-1.2.1.min.js') }}
<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
{{ HTML::script('static/metronic/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js') }}
{{ HTML::script('static/metronic/plugins/bootstrap/js/bootstrap.min.js') }}
{{ HTML::script('static/metronic/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js') }}
<!--[if lt IE 9]>
<script src="static/metronic/plugins/excanvas.min.js"></script>
<script src="static/metronic/plugins/respond.min.js"></script>  
<![endif]-->  
{{ HTML::script('static/metronic/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}
{{ HTML::script('static/metronic/plugins/jquery.blockui.min.js') }}
{{ HTML::script('static/metronic/plugins/jquery.cookie.min.js') }}
{{ HTML::script('static/metronic/plugins/uniform/jquery.uniform.min.js') }}
<!-- PAGE LEVEL PLUGINS -->

{{ HTML::script('static/metronic/plugins/flot/jquery.flot.js') }}
{{ HTML::script('static/metronic/plugins/flot/jquery.flot.resize.js') }}
{{ HTML::script('static/metronic/plugins/jquery.pulsate.min.js') }}
{{ HTML::script('static/metronic/plugins/bootstrap-daterangepicker/date.js') }}
{{ HTML::script('static/metronic/plugins/bootstrap-daterangepicker/daterangepicker.js') }}
{{ HTML::script('static/metronic/plugins/gritter/js/jquery.gritter.js') }}
{{ HTML::script('static/metronic/plugins/fullcalendar/fullcalendar/fullcalendar.min.js') }}
{{ HTML::script('static/metronic/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js') }}
{{ HTML::script('static/metronic/plugins/jquery.sparkline.min.js') }}
<!-- PAGE LEVEL SCRIPTS -->
{{ HTML::script('static/metronic/scripts/app.js') }}
{{ HTML::script('static/metronic/scripts/ui-jqueryui.js') }}
{{ HTML::script('static/metronic/scripts/index.js') }}
{{ HTML::script('static/metronic/scripts/tasks.js') }}
@yield('pageAssets2')

<script>
	jQuery(document).ready(function() {    
	   App.init(); // initlayout and core plugins
	   Index.init();
	   UIJQueryUI.init();
	  // Index.initJQVMAP(); // init index page's custom scripts
	   Index.initCalendar(); // init index page's custom scripts
	   Index.initCharts(); // init index page's custom scripts
	   // Index.initChat();
	   Index.initMiniCharts();
	   Index.initDashboardDaterange();
	   Index.initIntro();
	   Tasks.initDashboardWidget();
	   
	});
</script>
