<div class="footer">
	<div class="footer-inner">
		{{ date('Y') }} &copy; RoomQuickly by iTechSpark Inc. 
	</div>
	<div class="footer-tools">
		<span class="go-top">
		<i class="icon-angle-up"></i>
		</span>
	</div>
</div>