<html>
<head>
	<meta charset="UTF-8">
	<title>404 - The requested page was not found </title>
	<link rel="stylesheet" href="/static/metronic/plugins/bootstrap/css/bootstrap.min.css">
</head>
<body style="background-color:#f9f9ef;height: 100%;">
	<div class="box" style="width:75%;margin:auto;margin-top:20%;float:left;padding-left:10%">
		<div class="alert alert-danger">
			<h3>Oops..the requested resource does not exist on this server.</h3>
			<p>We are sorry, the page you requested cannot be found.<br>
			The URL may be misspelled or the page you're looking for is no longer available.</p>
			<br/>
		</div>
	</div>
	<div style="float:right; margin-right: 2%;margin-top: 40px;">
		<img src="/static/pillo/css/404.png" alt="">
		</div>
</body>
</html>