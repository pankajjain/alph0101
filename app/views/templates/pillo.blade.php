<!DOCTYPE html>
<!--[if IE 7 ]>    <html class="ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 	 ]>    <html class="ie" lang="en"> <![endif]-->
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="HandheldFriendly" content="True">
	<meta name="robots" content="noindex">
	<title> @yield('title') | RoomQuickly </title>
	@include('templates.pilloIncludes')
</head>
<body @yield('bodyyield')>
	<header>
		<div class="wrap clearfix">
			<h1 class="logo"><a href="{{ URL::to('/')}}" title="RoomQuickly"><img src="{{ URL::to('static/pillo/images/txt/logo.png') }}" alt="Room Quickly" /></a></h1>
			@include('templates.pilloRibbon')
			<div class="search">
				<form id="search-form" method="post"  action="{{URL::route('homePageSearch')}}">
					<input type="search" placeholder="Search entire site here" name="site_search" id="site_search" /> 
					<input type="submit" id="submit-site-search" value="submit-site-search" name="submit-site-search"/>
				</form>
			</div>
			<div class="contact">
				<span>24/7 Support number</span>
				<span class="number">1- 555 - 555 - 555</span>
			</div>
		</div>
		@include('templates.pilloMainNav')
	</header>
	
	@yield('content')
	@include('templates.pilloFooter')
	@yield('thescripts')

	<script>
		// Initiate selectnav function
		selectnav();
		@yield('inlineJS')
	</script>
</body>
</html>