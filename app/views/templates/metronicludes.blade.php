{{ HTML::style('static/metronic/plugins/bootstrap/css/bootstrap.css') }}
{{ HTML::style('static/metronic/plugins/bootstrap/css/bootstrap-responsive.min.css') }}
{{ HTML::style('static/metronic/plugins/font-awesome/css/font-awesome.min.css') }}
{{ HTML::style('static/metronic/css/style-metro.css') }}
{{ HTML::style('static/metronic/css/style2.css') }}
{{ HTML::style('static/metronic/css/custom.css') }}
{{ HTML::style('static/metronic/css/style-responsive.css') }}
{{ HTML::style('static/metronic/css/themes/default.css') }}
{{ HTML::style('static/metronic/plugins/uniform/css/uniform.default.css') }}

<!-- PAGE LEVEL ASSET -->
{{ HTML::style('static/metronic/plugins/gritter/css/jquery.gritter.css') }}
{{ HTML::style('static/metronic/plugins/bootstrap-daterangepicker/daterangepicker.css') }}
{{ HTML::style('static/metronic/plugins/bootstrap-daterangepicker/datepicker.css') }}
{{ HTML::style('static/metronic/plugins/fullcalendar/fullcalendar/fullcalendar.css') }}
{{ HTML::style('static/metronic/plugins/jqvmap/jqvmap/jqvmap.css', array('media'=>'screen')) }}
{{ HTML::style('static/metronic/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css', array('media'=>'screen')) }}
{{ HTML::style('static/metronic/css/pages/tasks.css', array('media'=>'screen')) }}
{{ HTML::style('static/metronic/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css', array('media'=>'screen')) }}

@yield('pageAssets')
<link rel="shortcut icon" href="favicon.ico" />
