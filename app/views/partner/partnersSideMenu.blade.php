<div class="page-sidebar nav-collapse collapse"> 
	<ul class="page-sidebar-menu">
		<li>
			<div class="sidebar-toggler hidden-phone"></div>
		</li>
		<li>
			<br>
		</li>
		<li class="start">
			<a href="{{URL::route('partnersIndex')}}">
				<i class="icon-chevron-sign-left"></i> 
				<span class="title">Back</span>
				<span class="selected"></span>
			</a>
		</li>
		<li {{Request::is('partners/accomm/' . $accomm->getKey())? 'class="active"':''}}>
			<a href="{{URL::route('accommodationIndex',array('accomm' => $accomm->id))}}">
				<i class="icon-home"></i> 
				<span class="title">Dashboard</span>
				<span class="selected"></span>
			</a>
		</li>
		<li 
		{{Request::is('partners/editaccomm/*')? 'class="active"':''}}
		{{Request::is('partners/accomm/*/xtrainfo')? 'class="active"':''}}
		{{Request::is('partners/accomm/*/map')? 'class="active"':''}}
		{{Request::is('partners/accomm/*/*facilities')? 'class="active"':''}}
		{{Request::is('partners/accomm/*/photos')? 'class="active"':''}}
		{{Request::is('partners/accomm/*/policies')? 'class="active"':''}}
		{{Request::is('partners/accomm/*/setting')? 'class="active"':''}}
		{{Request::is('partners/deleteaccomm/*')? 'class="active"':''}}
		>
		<a href="javascript:;">
			<i class="icon-building"></i> 
			<span class="title">{{ucwords($accomm->name)}}</span>
			<span class="arrow "></span>
		</a>
		<ul class="sub-menu">
			<li {{Request::is('partners/editaccomm/*')? 'class="active"':''}}>
				<a href="{{URL::route('editAccommodationWithSideMenu',array('accomm' => $accomm->id))}}">Basic Info</a>
			</li>
			<li {{Request::is('partners/accomm/*/xtrainfo')? 'class="active"':''}}>
				<a href="{{URL::route('accommodationXtraInfo',array('accomm' => $accomm->id))}}">Detailed Information</a>
			</li>
			<li {{Request::is('partners/accomm/*/map')? 'class="active"':''}}>
				<a href="{{URL::route('accommmodationMap',array('accomm' => $accomm->id))}}">Maps</a>
			</li>
			<li {{Request::is('partners/accomm/*/facilities')? 'class="active"':''}}>
				<a href="{{URL::route('accommodationFacilities',array('accomm' => $accomm->id))}}">Facilities</a>
			</li>
			<li {{Request::is('partners/accomm/*/sarfacilities')? 'class="active"':''}}>
				<a href="{{URL::route('accommodationSarFacilities',array('accomm' => $accomm->id))}}">S&R Facilities</a>
			</li>
			<li {{Request::is('partners/accomm/*/photos')? 'class="active"':''}}>
				<a href="{{URL::route('accommodationPhotos',array('accomm' => $accomm->id))}}">Photos</a>
			</li>
			<li {{Request::is('partners/accomm/*/policies')? 'class="active"':''}}>
				<a href="{{URL::route('accommodationPolicies',array('accomm' => $accomm->id))}}">Policies</a>
			</li>
			<li {{Request::is('partners/accomm/*/setting')? 'class="active"':''}}>
				<a href="{{URL::route('accommodationSetting',array('accomm' => $accomm->id))}}">Settings</a>
			</li>
			<li {{Request::is('partners/accomm/*/activity')? 'class="active"':''}}>
				<a href="{{URL::route('activityLogPartner',array('accomm' => $accomm->id))}}">Activity Log</a>
			</li>
			<!-- <li {{Request::is('partners/deleteaccomm/*')? 'class="active"':''}}>
				<a href="{{URL::route('deleteAccommodation',array('accomm' => $accomm->id))}}">Delete</a>
			</li> -->
		</ul>
	</li>
	<li {{Request::is('partners/accomm/*/rooms')? 'class="active"':''}}>
		<a href="{{URL::route('roomsIndex',array('accomm' => $accomm->id))}}">
			<i class="icon-suitcase"></i> 
			<span class="title">Rooms</span>
			<span class="arrow "></span>
		</a>
	</li>
	<li {{Request::is('partners/accomm/*/vouchers')? 'class="active"':''}}>
		<a href="{{URL::route('voucherIndex',array('accomm' => $accomm->getKey()))}}">
			<i class="icon-certificate"></i> 
			<span class="title">Vouchers</span>
			<span class="arrow "></span>
		</a>
	</li >
	<li {{Request::is('partners/accomm/*/flashdeals')? 'class="active"':''}}>
		<a href="{{URL::route('flashDealIndex',array('accomm' => $accomm->getKey()))}}">
			<i class="icon-certificate"></i> 
			<span class="title">Flash Deals</span>
			<span class="arrow "></span>
		</a>
	</li>
    
    <li {{Request::is('partners/accomm/*/soldvoucher')? 'class="active"':''}}>
		<a href="{{URL::route('soldVoucherIndex',array('accomm' => $accomm->getKey()))}}">
			<i class="icon-certificate"></i> 
			<span class="title">Sold Voucher</span>
			<span class="arrow "></span>
		</a>
	</li>
    <li {{Request::is('partners/accomm/*/soldflashdeal')? 'class="active"':''}}>
		<a href="{{URL::route('soldFlashDealIndex',array('accomm' => $accomm->getKey()))}}">
			<i class="icon-certificate"></i> 
			<span class="title">Sold Flash Deals</span>
			<span class="arrow "></span>
		</a>
	</li>
    
    <li {{Request::is('partners/accomm/*/partnerreport')? 'class="active"':''}}>
		<a href="{{URL::route('partnerReportIndex',array('accomm' => $accomm->getKey()))}}">
			<i class="icon-certificate"></i> 
			<span class="title">Hotel Partner Report</span>
			<span class="arrow "></span>
		</a>
	</li>
    
     <li {{Request::is('partners/accomm/*/reviewsrating')? 'class="active"':''}}>
		<a href="{{URL::route('reviewsRatingIndex',array('accomm' => $accomm->getKey()))}}">
			<i class="icon-certificate"></i> 
			<span class="title">User Reviews & Rating</span>
			<span class="arrow "></span>
		</a>
	</li>
    
    <li {{Request::is('partners/accomm/*/validateVoucher')? 'class="active"':''}}>
		<a href="{{URL::route('validateVoucher',array('accomm' => $accomm->getKey()))}}">
			<i class="icon-certificate"></i> 
			<span class="title">Validate Voucher</span>
			<span class="arrow "></span>
		</a>
	</li>
    
    <li {{Request::is('partners/accomm/*/validateFlashDeal')? 'class="active"':''}}>
		<a href="{{URL::route('validateFlashDeal',array('accomm' => $accomm->getKey()))}}">
			<i class="icon-certificate"></i> 
			<span class="title">Validate FlashDeal</span>
			<span class="arrow "></span>
		</a>
	</li>
    
	<li >
		<a href="javascript:;">
			<i class="icon-bullhorn"></i> 
			<span class="title">24/7 Supports</span>
			<span class="arrow "></span>
		</a>
		<ul class="sub-menu">
			<li>
				<a href="http://support.roomquickly.com/" target="new">Support Ticket Center</a>
			</li>
			<li>
				<a href="http://support.roomquickly.com/open.php" target="new">Open New Ticket</a>
			</li>
			<li>
				<a href="http://support.roomquickly.com/open.php" target="new">Issue Reporting</a>
			</li>
			<li>
				<a href="http://support.roomquickly.com/view.php" target="new">Check Ticket Status</a>
			</li>
			<li >
				<a href="http://support.roomquickly.com/kb/index.php" target="new">FAQ & Knowledge Base</a>
			</li>
		</ul>
	</li>
	<li>
	</ul>
</div>