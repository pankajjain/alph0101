@extends('templates.metronic')

@section('title')
Accommodation Manager
@stop

@section('sidebar')
@stop
<!-- 
Please check out the template metronic. 
This section main for page level styles	
Include here if it's not in the metronicludes.blade.php
-->
@section('pageAssets')
<style>
.page-content {margin-left:0px!important;}
td.pos-nag{ margin:auto}
.pos-nag span{ float:left;;;  margin-right:5px;text-align:center; padding:3px 15px}
span.positive{ background:#cbffcb; color:#309664; border-radius:4px 0 0 4px !important;}
span.negative{ background:#ffdddd; color:#ce281c; border-radius:0 4px 4px 0 !important;}
</style>
@stop

<!-- 
Please check out the template metronic. 
This section main for page level scripts	
Include here if it's not in the metronicludes2.blade.php
-->
@section('pageAsset2')
@stop

@section('content')
  @if(Session::has('message'))
    <div class="alert alert-success"> {{Session::get('message')}}</div>
  @endif

<?php $veriPending = 0; ?>

<a href="{{URL::route('newAccommodation')}}" class="btn">Add a new accommodation</a>
<br>
<table class="table table-condensed">
<thead>
	<tr>
		<th>Name</th>
		<th>Status</th>
		<th>Hotel Star</th>
        <th>USer Rating</th>
		<th>Rooms</th>
		<th>Street Address</th>
		<th>Created</th>
		<th>Action</th>
		<!-- <th>Preview</th> -->
	</tr>
</thead>
@if(Accomm::where('userID',Sentry::getUser()->getKey())->get()->count() < 1)
<tr>
	<td>You have not yet created any accommodation.</td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
@else
@foreach(Accomm::where('userID',Sentry::getUser()->getKey())->get() as $accomm)
	@if($accomm->isVerified === '0')
		<?php $veriPending += 1 ?>
	@endif
<tr>
	<?php
	$status = "Error";
	switch($accomm->isVerified){
			case 0:
				$status = "Pending";
				break;
			case 1:
				$status = "Approved";
				break;
			case 2:
				$status = "Disapproved";
				break;
			default:
				$status = "Error";
				break;
			}
			?>

	<td>
		@if($status=="Approved") <a href="{{URL::route('accommodationIndex' , array('accomm' => $accomm->id))}}" class="accomnamelink">@endif{{ucwords($accomm->name)}}</a>
	</td>
	<td>{{$status}}</td>
	<td>{{ $accomm->starRating}}</td>
    <td class="pos-nag">
    
   
   
 
   
   @if(DB::table('reviewsrating')->where('accommID',$accomm->id)->count('rating') > 0 && DB::table('reviewsrating')->where('accommID',$accomm->id)->sum('rating') > 0)
    	{{  DB::table('reviewsrating')->where('accommID',$accomm->id)->sum('rating')/DB::table('reviewsrating')->where('accommID',$accomm->id)->count('rating') }}
     @else
        {{0}}
     @endif
     
   
   
   
     <?php /*?> <div>
    	<span class="positive">+{{  DB::table('reviewsrating')->where('accommID',$accomm->id)->where('positiveReviews', '<>', '')->count('negativeReviews') }}</span>
        <span class="negative">-{{  DB::table('reviewsrating')->where('accommID',$accomm->id)->where('negativeReviews', '<>', '')->count('negativeReviews') }}</span>
      </div><?php */?>
    </td>
	<td>{{ $accomm->numberOfRooms }}</td>
	<td>
		{{ $accomm->streetAddress }}<br>
		{{ $accomm->city.", ".$accomm->zip." ".$accomm->country}}<br>
	</td>
	<td>{{ date_format($accomm->created_at, 'Y-m-d') }}</td>
	<td>
		@if($status=="Approved")<a href="{{URL::route('editAccommodation',array('accomm' => $accomm->id))}}" class="btn blue">Edit</a>@endif
		<!-- <a href="{{URL::route('deleteAccommodation',array('accomm' => $accomm->id))}}" class="btn">Delete</a> -->
	</td>
	<!-- <td> -->
		<?php //$pho = AccommPhotos::where('accommID',$accomm->id)->get()->first(); ?>
		<!-- @if(isset($pho)) -->
			<!-- <img src="{{ $pho->getThumbURL() }}" style="float: left; margin: 5px; width: 100%;max-width: 200px;"/> -->
		<!-- @endif -->
	<!-- </td> -->

</tr>
{{-- print_r($accomm) --}}
@endforeach
@endif
</table>

@if($veriPending > 0)
	<div class="alert alert-info">Verification process may take 24 - 48 hours to update the status.</div>
@endif
@stop