@extends('templates.metronic')

@section('title')
Room Listing
@stop

@section('sidebar')
@include('partner.partnersSideMenu')
@stop

@section('pageAssets')

@stop


@section('pageAsset2')
@stop

@section('content')

@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
		
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon icon-reorder"></i> Accommodation Partner Report
				</div>
			</div>
			<div class="portlet-body">
				
					
				<table class="table table-striped table-bordered ">
					<tbody>
						
                       
						@foreach(DB::table("accommodationvisitor")->where('accommID',$accomm->getKey())->get() as $accommData)
                       
                            @foreach(DB::table("users")->where('id',$accommData->userID)->get() as $user)
                            
                            	<tr>
							<td>
									{{$user->email}}
							</td>
							<td>
									{{ucwords($user->firstName)}} 
									{{ucwords($user->lastName)}}
							</td>
                           
							
						</tr>
                            @endforeach	
						
						@endforeach	
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop

@section('inlineJS')

@stop