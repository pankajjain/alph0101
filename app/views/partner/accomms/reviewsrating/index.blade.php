@extends('templates.metronic')

@section('title')
Room Listing
@stop

@section('sidebar')
@include('partner.partnersSideMenu')
@stop

@section('pageAssets')

@stop


@section('pageAsset2')
<a onClick="window.print()" class="btn blue">Print</a>
@stop


@section('topNavs')

<a onClick="window.print()" class="btn blue">Print</a>
@stop

@section('content')



@if(Session::has('message'))

<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif




<style>

.pos-nag{ margin:auto;float:right}
.pos-nag span{ float:left;;;  margin-right:5px;text-align:center; padding:3px 15px}
span.positive{ background:#cbffcb; color:#309664; border-radius:4px 0 0 4px !important;}
span.negative{ background:#ffdddd; color:#ce281c; border-radius:0 4px 4px 0 !important;}
</style>

<?php //echo "<pre>"; print_r($review); ?>
<div class="row-fluid">
	<div class="span12">
		
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon icon-reorder"></i> User Reviews & Rating
				</div>
			</div>
			<div class="portlet-body">
				
				<p></p>
				<table class="table table-striped table-bordered ">
					<tbody>
						<tr>
                        	<th>Name</th>
                            <th>Rating</th>
                            <th>Positive review <div class="pos-nag">
            <span class="positive">+{{  DB::table('reviewsrating')->where('accommID',$accomm->id)->where('positiveReviews', '<>', '')->count('negativeReviews') }}</span>
            
      </div></th>
                            <th>Negative review <div class="pos-nag">
            
            <span class="negative">-{{  DB::table('reviewsrating')->where('accommID',$accomm->id)->where('negativeReviews', '<>', '')->count('negativeReviews') }}</span>
      </div></th>
                            <th>Created On</th>
                        </tr>
                       </thead>
                      <tbody> 
						@foreach($review as $rev)
                       
                            <tr>
                            <td>{{$rev->firstName}} {{$rev->lastName}}</td>
                            <td>{{$rev->rating}}</td>
                            <td>{{$rev->positiveReviews}}</td>
                            <td>{{$rev->negativeReviews}}</td>
                            <td>{{$rev->created_at}}</td>
							</tr>
						@endforeach	
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop

@section('inlineJS')

@stop