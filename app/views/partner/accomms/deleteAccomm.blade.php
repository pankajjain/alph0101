@extends('templates.metronic')

@section('title')
Delete accommodation
@stop

@section('sidebar')
<style>
.page-content {margin-left: 0px!important;}
</style>
@stop

@section('pageAssets')
<link rel="stylesheet" href="/static/metronic/plugins/rateit/rateit.css">
<script src="/static/metronic/plugins/rateit/jquery.rateit.min.js"></script>
@stop


@section('content')
<div class="page-container">
	@if(Session::has('message'))
	<div class="alert alert-success"> {{Session::get('message')}}</div>
	@endif
	<div class="alert alert-block alert-danger fade in" style="border-left: 5px solid #B94A4F;">
		<button type="button" class="close" data-dismiss="alert"></button>
		<h4 class="alert-heading">Warning</h4>
		<p>
			{{Lang::get('messages.confirmAccommodationDelete',['name' => ucwords($accomm->name)])}}
		</p>
		<p>
			{{Form::open()}}{{Form::hidden('accommid',Crypt::encrypt($accomm->id))}} {{Form::submit('Delete',array('class' => 'btn red'))}} <a class="btn blue" href="{{URL::previous()}}">Cancel</a>{{Form::close()}}
		</p>
	</div>
</div>
@stop