@extends('templates.metronic')

@section('title')
Hotel Applications
@stop

@section('sidebar')
@include('partner.partnersSideMenu')
@stop

@section('pageAssets')

@stop


@section('pageAsset2')
@stop

@section('content')
@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title">User Activity Log</h3>
		<hr>
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon icon-reorder"></i> Latest activities
				</div>
			</div>
			<div class="portlet-body">
				<table class="table">
					<thead>
						<tr>
							<th>User</th>
							<th>Operation</th>
							<th>Time</th>
						</tr>
						<?php $logs = Activity::where('userID',Sentry::getUser()->getKey())->orderBy('created_at','desc')->paginate(15); ?>	
						@foreach($logs as $log)	
                       <?php  $user = Sentry::where('id', '=', $log->userID)->get()?>
						<tr>
							<td>{{$user[0]->firstName}} {{$user[0]->lastName}}</td>
							<td>{{$log->message}}</td>
							<td>{{date('D-M-Y H:i:s',strtotime($log->created_at))}}</td>
						</tr>
						@endforeach
					</thead>
				</table>
				<div style="text-align:center">
					{{$logs->links()}}
				</div>

			</div>
		</div>
	</div>
</div>
@stop