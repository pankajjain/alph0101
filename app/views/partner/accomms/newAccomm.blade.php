@extends('templates.metronic')

@section('title')
New accommodation
@stop

@section('sidebar')

@stop

@section('pageAssets')
<style>
.page-content {margin-left: 0px!important;}
</style>

@stop

@section('pageAssets2')
<link rel="stylesheet" href="/static/metronic/plugins/rateit/rateit.css">
<script src="/static/metronic/plugins/rateit/jquery.rateit.min.js"> </script>
{{ HTML::script('static/metronic/scripts/countries.js') }}
@stop


@section('content')
<a href="{{URL::Route('partnersIndex')}}" class="btn default red-stripe btn-lg pull-left">
  <i class="icon-mail-reply"></i> Back to accommodation listing 
</a> <br>
<div class="page-container">
  {{Form::open(array('class'=>'form-horizontal','route'=>'postNewAccommodation','files'=>true))}}
  <div class="tabbable tabs-left" id="myTabs">
    <ul class="nav nav-tabs">
       <li class="active"><a href="#tab_6_1" data-toggle="tab">Basic Information</a></li>
       <li class=""><a href="#tab_6_2" data-toggle="tab">Advance Information</a></li>
       <li class=""><a href="#tab_6_3" data-toggle="tab">Contacts Details</a></li>
    </ul>
    <div class="tab-content">
       <div class="tab-pane active" id="tab_6_1">
          <div class="container">
            @if($errors->count() > 0)
             <div class="alert alert-danger">
               Ouch, you missed something!<br/>Please fill up the required information.
             </div><br/>
            @endif
            <?php $submitButtonText = "Register"; ?>
            <div class="control-group">
              {{Form::label('name','Accommodation Name*',array('class'=>'control-label'))}}
              <div class="controls">
                {{Form::text('name',null,array('placeholder'=>'Accommodation Name',"data-provide"=>"typeahead",'autocomplete'=>"off"))}}
                @if($errors->has('name'))
                <div class="formErrors">
                  <ul>
                    @foreach($errors->get('name') as $message)
                    <li>
                      {{$message}}
                    </li>
                    @endforeach
                  </ul>
                </div>
                @endif
              </div>
            </div>
            <div class="control-group">
              {{Form::label('type','Accommodation Type*',array('class'=>'control-label'))}}
              <div class="controls">
                {{Form::select('typeID',AccommType::lists('title','id'), 6)}}
                @if($errors->has('type'))
                <div class="formErrors">
                  <ul>
                    @foreach($errors->get('name') as $message)
                    <li>
                      {{$message}}
                    </li>
                    @endforeach
                  </ul>
                </div>
                @endif
              </div>
            </div>
            <div class="control-group">
              {{Form::label("starRating",'Star Rating',array('class'=>'control-label'))}}
              <div class="controls">
                {{Form::hidden("starRating","0")}}
                <div class="rateit" style="margin-top:8px;" data-rateit-step="1" data-rateit-backingfld="#starRating"  data-rateit-resetable="false">
                </div>            
                @if($errors->has('starRating'))
                <div class="formErrors">
                  <ul>
                    @foreach($errors->get('starRating') as $message)
                    <li>
                      {{$message}}
                    </li>
                    @endforeach
                  </ul>
                </div>
                @endif
              </div>
            </div>
            <div class="control-group">
              {{Form::label("numberOfRooms",'Number of rooms*',array('class'=>'control-label'))}}
              <div class="controls">
                {{Form::text('numberOfRooms',null,array('placeholder'=>'Number of rooms'))}}

                @if($errors->has('numberOfRooms'))
                <div class="formErrors">
                  <ul>
                    @foreach($errors->get('numberOfRooms') as $message)
                    <li>
                      {{$message}}
                    </li>
                    @endforeach
                  </ul>
                </div>
                @endif
              </div>
            </div>
            
            
              <div class="control-group">
						{{Form::label("hotelDescription",'Hotel Description',array('class'=>'control-label'))}}*
						<div class="controls">
							{{Form::textarea("hotelDescription",null,array("placeholder"=>"Hotel Description","rows"=>"4"))}}

							@if($errors->has('hotelDescription'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('hotelDescription') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>
            
            
            
            <!--<div class="control-group">
			{{Form::label('language','Language *',array('class'=>'control-label'))}}
			<div class="controls">
				
				<select name="language" id="language">
					
					<option value="0" >Select Language</option>
                    <option value="" >English</option>
                    <option value="" >Malay</option>
                    <option value="" >简体中文</option>
                    <option value="" >繁體中文</option>
					
				</select>
				@if($errors->has('roomID'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('language') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>
            <div class="control-group">
              {{Form::label("description",'Accommodation Description*',array('class'=>'control-label'))}}
              <div class="controls">
                
				 {{Form::textarea('description',null,array('placeholder' => 'Accommodation Description','rows' => '3' , 'style' => 'width:27%'))}}
                @if($errors->has('description'))
                <div class="formErrors">
                  <ul>
                    @foreach($errors->get('description') as $message)
                    <li>
                      {{$message}}
                    </li>
                    @endforeach
                  </ul>
                </div>
                @endif
              </div>
            </div>-->
            
            
            <a href="#" id="TabTwo">
              <button type="button" class="btn btn-info">Next</button>
            </a>
          </div>
       </div>
       <div class="tab-pane fade" id="tab_6_2">
          <div class="container">
            <div class="control-group">
              {{Form::label('country','Country',array('class'=>'control-label'))}}
              <div class="controls">
                {{Form::select('country')}}
                <!-- DB::table('countries')->lists('nicename', 'iso') -->
                @if($errors->has('country'))
                <div class="formErrors">
                  <ul>
                    @foreach($errors->get('country') as $message)
                    <li>
                      {{$message}}
                    </li>
                    @endforeach
                  </ul>
                </div>
                @endif
              </div>
            </div>
            
            <div class="control-group">
              {{Form::label("province",'State/Province*',array('class'=>'control-label'))}}
              <div class="controls">
                <!-- Form::text('province',null,array('placeholder'=>'State/Province')) -->
                {{Form::select('province')}}
                @if($errors->has('province'))
                <div class="formErrors">
                  <ul>
                    @foreach($errors->get('province') as $message)
                    <li>
                      {{$message}}
                    </li>
                    @endforeach
                  </ul>
                </div>
                @endif
              </div>
            </div>
            <div class="control-group">
             {{Form::label('city','City*',array('class'=>'control-label'))}}
             <div class="controls">
               {{Form::text('city',null,array('placeholder'=>'City'))}}
               @if($errors->has('city'))
               <div class="formErrors">
                 <ul>
                   @foreach($errors->get('city') as $message)
                   <li>
                     {{$message}}
                   </li>
                   @endforeach
                 </ul>
               </div>
               @endif
             </div>
            </div>
            <div class="control-group">
              {{Form::label("streetAddress",'Street Address*',array('class'=>'control-label'))}}
              <div class="controls">
                {{Form::textarea("streetAddress",null,array("placeholder"=>"Street Address","rows"=>"4"))}}
                @if($errors->has('streetAddress'))
                <div class="formErrors">
                  <ul>
                    @foreach($errors->get('streetAddress') as $message)
                    <li>
                      {{$message}}
                    </li>
                    @endforeach
                  </ul>
                </div>
                @endif
              </div>
            </div>
            <div class="control-group">
             {{Form::label('zipCode','Zip Code*',array('class'=>'control-label'))}}
             <div class="controls">
               {{Form::text('zipCode',null,array('placeholder'=>'Zip Code'))}}
               @if($errors->has('zipCode'))
               <div class="formErrors">
                 <ul>
                   @foreach($errors->get('zipCode') as $message)
                   <li>
                     {{$message}}
                   </li>
                   @endforeach
                 </ul>
               </div>
               @endif
             </div>
            </div>
            <div class="control-group">
              {{Form::label('contactNumber','Hotel Main Phone*',array('class'=>'control-label'))}}
              <div class="controls">
                {{Form::text('contactNumber',null,array('placeholder'=>'Hotel Main Phone Number'))}}
                @if($errors->has('contactNumber'))
                <div class="formErrors">
                  <ul>
                    @foreach($errors->get('contactNumber') as $message)
                    <li>
                      {{$message}}
                    </li>
                    @endforeach
                  </ul>
                </div>
                @endif
              </div>
            </div>
            <div class="control-group">
             {{Form::label('fax','Fax',array('class'=>'control-label'))}}
             <div class="controls">
               {{Form::text('fax',null,array('placeholder'=>'Fax'))}}
               @if($errors->has('fax'))
               <div class="formErrors">
                 <ul>
                   @foreach($errors->get('fax') as $message)
                   <li>
                     {{$message}}
                   </li>
                   @endforeach
                 </ul>
               </div>
               @endif
             </div>
            </div>
            <div class="control-group">
             {{Form::label('website','Website',array('class'=>'control-label'))}}
             <div class="controls">
               {{Form::text('website',null,array('placeholder'=>'Website'))}} Ex. http://www.google.com
               @if($errors->has('website'))
               <div class="formErrors">
                 <ul>
                   @foreach($errors->get('website') as $message)
                   <li>
                     {{$message}}
                   </li>
                   @endforeach
                 </ul>
               </div>
               @endif
             </div>
            </div>

            <a href="#" id="TabOne">
              <button type="button" class="btn btn-info">Back</button>
            </a>
            <a href="#" id="TabThree">
              <button type="button" class="btn btn-info">Next</button>
            </a>
          </div>
       </div>
       <div class="tab-pane fade" id="tab_6_3">
          <div class="container">
            <div class="control-group">
              {{Form::label('mainContactName','Main Contact Name*',array('class'=>'control-label'))}}
              <div class="controls">
               {{Form::text('mainContactName',null,array('placeholder'=>'Main Contact Name'))}}
               @if($errors->has('mainContactName'))
               <div class="formErrors">
                 <ul>
                   @foreach($errors->get('mainContactName') as $message)
                   <li>
                     {{$message}}
                   </li>
                   @endforeach
                 </ul>
               </div>
               @endif
              </div>
            </div>
            <div class="control-group">
              {{Form::label('mainContactRole','Role*',array('class'=>'control-label'))}}
              <div class="controls">
                {{Form::select('mainContactRole',Accomm::$accommodationMainContactRoles,'Owner')}}
                @if($errors->has('mainContactRole'))
                <div class="formErrors">
                  <ul>
                    @foreach($errors->get('mainContactRole') as $message)
                    <li>
                      {{$message}}
                    </li>
                    @endforeach
                  </ul>
                </div>
                @endif
              </div>
            </div>
            <div class="control-group">
              {{Form::label('mainContactEmail','Email Address*',array('class'=>'control-label'))}}
              <div class="controls">
                {{Form::text('mainContactEmail',null,array('placeholder'=>'Email'))}}
                @if($errors->has('mainContactEmail'))
                <div class="formErrors">
                  <ul>
                    @foreach($errors->get('mainContactEmail') as $message)
                    <li>
                      {{$message}}
                    </li>
                    @endforeach
                  </ul>
                </div>
                @endif
              </div>
            </div>
            <div class="control-group">
              {{Form::label('mainContactMobile','Mobile Number',array('class'=>'control-label'))}}
              <div class="controls">
                {{Form::text('mainContactMobile',null,array('placeholder'=>'Mobile Number'))}}
                @if($errors->has('mainContactMobile'))
                <div class="formErrors">
                  <ul>
                    @foreach($errors->get('mainContactMobile') as $message)
                    <li>
                      {{$message}}
                    </li>
                    @endforeach
                  </ul>
                </div>
                @endif
              </div>
            </div>
            <div class="control-group">
              {{Form::label('files','Agreement Letter',array('class'=>'control-label'))}}
              <div class="controls">
               {{ Form::file('files',['class' => 'fileupload'])}}    
               @if($errors->has('files'))
               <div class="formErrors">
                <ul>
                  @foreach($errors->get('files') as $message)
                  <li>
                    {{$message}}
                  </li>
                  @endforeach
                </ul>
              </div>        
              @endif

              <?php
              $Ver = Configs::find(6);
              $Ver = $Ver->value;
              ?>
              <a href="{{$Ver}}" target="_blank">Download current agreement letter</a> Please upload in pdf format.
              <p class="help-block">Please download, sign, and upload the agreement letter.</p>
              </div>
            </div>

            
            <input type="reset" value="Reset " class="btn "/>
            <a href="#" id="TabTwoTwo">
              <button type="button" class="btn btn-info">Back</button>
            </a>
            {{form::submit($submitButtonText,array('class'=>'btn btn-primary'))}}
          </div>
       </div>
    </div>
  </div>
{{Form::close()}}
</div> 

@stop

@section('inlineJS')
<script>
// $(document).ready(function(){
//   $(document).on('change','.fileupload',function(){
//     if($(this).data('exists') != 'true'){
//       $(this).clone().insertAfter($(this));
//       $(this).data('exists','true');
//     }
//   });
// });

$('#TabOne').click(function(){
    $('#myTabs li:eq(0) a').tab('show');
  });

$('#TabTwo').click(function(){
    $('#myTabs li:eq(1) a').tab('show');
  });

$('#TabTwoTwo').click(function(){
    $('#myTabs li:eq(1) a').tab('show');
  });

$('#TabThree').click(function(){
    $('#myTabs li:eq(2) a').tab('show');
  });

populateCountries("country", "province");
</script>
@stop