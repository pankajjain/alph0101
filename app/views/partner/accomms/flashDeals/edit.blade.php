@extends('templates.metronic')

@section('title')
Edit FlashDeal
@stop

@section('sidebar')
@include('partner.partnersSideMenu')
@stop

@section('pageAssets')

@stop


@section('pageAsset2')
@stop

@section('content')

@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif

<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title">Edit Flash Deal</h3>	
		{{Form::model($flashdeal,array('class' => 'form-horizontal'))}}
		{{Form::hidden('status','saved')}}

		<div class="control-group">
			{{Form::label('flashDealName','Flash Deal Name *',array('class'=>'control-label'))}}
			<div class="controls">
				{{Form::text('flashDealName',null,array('placeholder' => 'Flash Deal Name'))}}
			</div>
		</div>

		<div class="control-group">
			{{Form::label('roomID','Room *',array('class'=>'control-label'))}}
			<div class="controls">
				<?php $rooms= $accomm->rooms; ?>
				<select name="roomID" id="roomID">
					@foreach($rooms as $room)
					<option value="{{$room->id}}" data-originalPrice="{{$room->originalPrice}}" @if($room->id == $flashdeal->roomID) {{'selected="selected"'}} @endif>{{$room->name}}</option>
					@endforeach
				</select>
				@if($errors->has('roomID'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('roomID') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>
		
         <div class="control-group">
			{{Form::label('voucherID','Voucher ',array('class'=>'control-label'))}}
			<div class="controls">
				<?php $vouchers= $accomm->vouchers; ?>
				<select name="voucherID" id="voucherID">
					@foreach($vouchers as $voucher)
					<option value="{{$voucher->id}}" data-originalPrice="{{$voucher->originalPrice}}" @if($voucher->id == $flashdeal->voucherID) {{'selected="selected"'}} @endif >{{$voucher->voucherName}}</option>
					@endforeach
				</select>
				<?php /*?>@if($errors->has('voucherID'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('voucherID') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif<?php */?>
			</div>
		</div>

        
		<div class="control-group">
			{{Form::label('summary','Summary *',array('class'=>'control-label'))}}
			<div class="controls">
				{{Form::textarea('summary',null,array('placeholder' => 'Summary','rows' => '5' , 'style' => 'width:85%'))}}
			</div>
		</div>

		<div class="control-group">
			{{Form::label('highlights','Hightlights *',array('class'=>'control-label'))}}
			<div class="controls">
				{{Form::textarea('highlights',null,array('placeholder' => 'Hightlights','rows' => '5' , 'style' => 'width:85%'))}}
			</div>
		</div>

		<div class="control-group">
			<div class="controls">
				{{ Form::checkbox('breakfastIncluded', '1');}} Breakfast Included
			</div>
		</div>

		<div class="control-group">
			{{Form::label('','Original Price',array('class'=>'control-label'))}}
			<div class="controls">
				<input type='text' value="{{$flashdeal->room->originalPrice}}" id="originalPrice" disabled="true" />
			</div>
		</div>

		<div class="control-group">
			{{Form::label('discountPercentage','Discount Percentage *',array('class'=>'control-label'))}}
			<div class="controls">
				{{Form::text('discountPercentage',null,array('onkeypress'=>'return isNumberKey(event)', 'onkeyup'=>'isNumberKeyWithPer(this.value)','placeholder' => 'Discount Percentage'))}}
			</div>
		</div>

		<div class="control-group">
			{{Form::label('discountedPrice','Discounted Price *',array('class'=>'control-label'))}}
			<div class="controls">
				{{Form::text('discountedPrice',null,array('onkeypress'=>'return isNumberKey(event)','placeholder' => 'Discounted Price'))}}
			</div>
		</div>

		<div class="control-group">
			{{Form::label('flashDealAmount','Flash Deal  Amount *',array('class'=>'control-label'))}}
			<div class="controls">
				{{Form::text('flashDealAmount',null,array('onkeypress'=>'return isNumberKey(event)','placeholder' => 'Flash Deal  Amount'))}}
			</div>
		</div>

		<div class="control-group">
			{{Form::label('flashDealDuration','Flash Deal  Duration *',array('class'=>'control-label'))}}
			<div class="controls">
				<?php /*?>{{Form::select('flashDealDuration',range(2,10),array('placeholder' => 'Flash Deal  Duration'))}}<?php */?>
                <select name="flashDealDuration" id="flashDealDuration" style="width:50px">
                
                @for($i=2;$i<=10;$i++)
                	<option  value="{{$i}}" @if($flashdeal->flashDealDuration == $i) {{'selected="selected"'}} @endif>{{$i}}</option>
                @endfor
                	
                </select>
			</div>
		</div>

		

		<div class="control-group">
			{{Form::label('minimumDaysPriorBooking','Minimum days prior booking *',array('class'=>'control-label'))}}
			<div class="controls">
				{{Form::text('minimumDaysPriorBooking',null,array('onkeypress'=>'return isNumberKey(event)','placeholder' => 'Minimum days prior booking'))}}
			</div>
		</div>

		<?php /*?><div class="control-group">
			{{Form::label('bookingPeriodFrom','Booking Period *',array('class'=>'control-label'))}}
			<div class="controls">
				{{Form::text('bookingPeriodFrom',null,array('placeholder' => 'From','class' => 'datepicker input-small','id'=>'bookingPeriodFrom'))}}
				{{Form::text('bookingPeriodTo',null,array('placeholder' => 'From','class' => 'datepicker input-small', 'id'=>'bookingPeriodTo'))}}
				

			</div>
		</div><?php */?>
		
		<div class="control-group">
			{{Form::label('redemptionPeriodFrom','Redemption Period *',array('class'=>'control-label'))}}
			<div class="controls">
				{{Form::text('redemptionPeriodFrom',null,array('placeholder' => 'From','class' => 'datepicker input-small', 'id' => 'redemptionPeriodFrom'))}}
				{{Form::text('redemptionPeriodTo',null,array('placeholder' => 'From','class' => 'datepicker input-small', 'id' => 'redemptionPeriodTo'))}}
				

			</div>
		</div>


		<div class="control-group">
			{{Form::label('flashDealPolicy','Flash Deal  Policies *',array('class'=>'control-label'))}}
			<div class="controls">
				{{Form::textarea('flashDealPolicies',null,array('placeholder' => 'Flash Deal Policies','rows' => '5' , 'style' => 'width:85%'))}}
			</div>
		</div>
		
		<a  class="btn btn-primary pull-right" id="saveButton">Save</a>
		<a  class="btn btn-primary pull-right" id="publishButton">Save and Publish</a> 
		{{Form::close()}}


	</div>

</div>



@stop

@section('inlineJS')

<script>
$(document).ready(function(){
	$('#voucherID').change(function(){
		//alert($('#voucherID').val());
		
		if($('#voucherID').val() == ''){
				 $('#flashDealName').val('');
					
					 $('#summary').val('');
					 $('#highlights').val('');
					  $(".checker span").removeClass('checked');
						$('input:checkbox[name=breakfastIncluded]').attr('checked',false);
					 
					 $('#discountedPrice').val('');
					 $('#flashDealAmount').val('');
					 $('#flashDealDuration').val('2');
					 $('#discountPercentage').val('');
					 $('#flashDealPolicies').val('');
					 $('#minimumDaysPriorBooking').val('');
					 $('#redemptionPeriodFrom').val('');
					 $('#redemptionPeriodTo').val('');
					
					 return false;
			}
			
			
		var accommID = <?php echo $accomm->id; ?>;
			$.ajax({
				 type: "GET",
				 url: "{{URL('/partners/accomm/"+ accommID +"/ajaxvoucher')}}/"+$('#voucherID').val(),
				 
				 }).success(function( result ) {
					 var flash = jQuery.parseJSON(result);
					 //alert(flash.voucherName);
					 $('#flashDealName').val(flash.voucherName);
					 $('#roomID').val(flash.roomID);
					 $('#summary').val(flash.summary);
					 $('#highlights').val(flash.highlights);
					 if(flash.breakfastIncluded == 1){
						 $(".checker span").addClass('checked');
						$('input:checkbox[name=breakfastIncluded]').attr('checked',true);
					 }else if(flash.breakfastIncluded != 1){
						 $(".checker span").removeClass('checked');
						$('input:checkbox[name=breakfastIncluded]').attr('checked',false);
					 }
					 $('#discountedPrice').val(flash.discountedPrice);
					 $('#flashDealAmount').val(flash.voucherAmount);
					 $('#flashDealDuration').val(flash.voucherDuration);
					 $('#discountPercentage').val(flash.discountPercentage);
					 $('#flashDealPolicies').val(flash.voucherPolicies);
					 $('#minimumDaysPriorBooking').val(flash.minimumDaysPriorBooking);
					 $('#redemptionPeriodFrom').val(flash.redemptionPeriodFrom);
					 $('#redemptionPeriodTo').val(flash.redemptionPeriodTo);
					 $('#originalPrice').val(flash.originalPrice);
					
			});
		});
	});
</script>
<script>
function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : event.keyCode;
          if (charCode != 46 && charCode > 31  && (charCode < 48 || charCode > 57))
		  	{
             	alert("Please enter numeric value.");
			 return false;
			 }

          return true;
       }
	   
function isNumberKeyWithPer(v){
	if(v > 100 || v < 1){
			alert("Please enter numeric value between 1 to 100.");
			$('#discountPercentage').val('');
			 return false;
		}
		 return true;
	}
</script>
<script>
	$(document).ready(function(){
		$('.datepicker').datepicker();

		$('#roomID').change(function(){
			$('#originalPrice').val($('#roomID').find(':selected').data('originalprice'));
			if($('#discountPercentage').val() != ''){
					originalprice = $('#originalPrice').val();
					$('#discountedPrice').val(discountCalculator(originalprice,$('#discountPercentage').val()));
				}
		});

		discountCalculator = function(originalprice,discountPercentage){
			return (originalprice - (originalprice / 100) * discountPercentage);
		}

		percentCalculator = function(originalPrice,discountedPrice){
			return ((originalPrice - discountedPrice) * 100) / originalPrice;
		}

		$('#discountPercentage').change(function(){
			originalprice = $('#originalPrice').val();
			$('#discountedPrice').val(discountCalculator(originalprice,$(this).val()));
		});

		$('#discountedPrice').change(function(){
			originalprice = $('#originalPrice').val();
			var per = percentCalculator(originalprice,$(this).val());
			if(per > 100 || per < 1){
				alert("Please enter sufficient amount.");
				$('#discountedPrice').val(0);
				$('#discountPercentage').val(0);
				return false;
			}else{
			$('#discountPercentage').val(percentCalculator(originalprice,$(this).val()));
			}

		});

		$('#saveButton').click(function(){
			form = $(this).parents('form');
			if($('#bookingPeriodFrom').val() > $('#bookingPeriodTo').val()){
					alert("Please enter correct booking period date;");
					return false;
				}
				
			if($('#redemptionPeriodFrom').val() > $('#redemptionPeriodTo').val()){
					alert("Please enter correct redemption period date;");
					return false;
				}
			form.submit();
		});

		$('#publishButton').click(function(){
			if($('#bookingPeriodFrom').val() > $('#bookingPeriodTo').val()){
					alert("Please enter correct booking period date;");
					return false;
				}
				
			if($('#redemptionPeriodFrom').val() > $('#redemptionPeriodTo').val()){
					alert("Please enter correct redemption period date;");
					return false;
				}
			form = $(this).parents('form');
			form.find('input[name=status]').val('published');
			form.submit();
		});
	});

</script>
@stop 