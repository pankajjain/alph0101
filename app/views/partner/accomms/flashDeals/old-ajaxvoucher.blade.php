<?php
 //echo "<pre>"; print_r($voucher);die;
//echo $voucher->roomID;die;
//echo "<pre>"; print_r($voucher->room->originalPrice); die;
?>
<div class="span12">
		<h3 class="page-title">New Flash Deal</h3>	
		{{Form::open(array('class' => 'form-horizontal'))}}
		{{Form::hidden('status','saved')}}

		<div class="control-group">
			{{Form::label('flashDealName','Flash Deal Name *',array('class'=>'control-label'))}}
			<div class="controls">
				<?php /*?>{{Form::text('flashDealName',null,array('placeholder' => 'Flash Deal Name'))}}<?php */?>
                <input type="text" id="flashDealName" value="{{$voucher->voucherName}}" name="flashDealName" placeholder="Flash Deal Name">
				@if($errors->has('flashDealName'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('flashDealName') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>

		<div class="control-group">
			{{Form::label('roomID','Room *',array('class'=>'control-label'))}}
			<div class="controls">
				<?php $rooms= $accomm->rooms; ?>
				<select name="roomID" id="roomID">
					@foreach($rooms as $room)
					<option value="{{$room->id}}" @if($room->id == $voucher->roomID) {{'selected="selected"'}}@endif data-originalPrice="{{$room->originalPrice}}">{{$room->name}}</option>
					@endforeach
				</select>
                
                <?php /*?><select id="roomID" name="roomID">
										<option data-originalprice="266.00" value="17">my room</option>
										<option data-originalprice="1000.00" value="18">Test room</option>
									</select><?php */?>
				@if($errors->has('roomID'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('roomID') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>
        
        <div class="control-group">
			{{Form::label('voucherID','Voucher *',array('class'=>'control-label'))}}
			<div class="controls">
				<?php $vouchers= $accomm->vouchers; ?>
				<select name="voucherID" id="voucherID">
					@foreach($vouchers as $voucher)
					<option value="{{$voucher->id}}" data-originalPrice="{{$voucher->originalPrice}}">{{$voucher->voucherName}}</option>
					@endforeach
				</select>
                
                <?php /*?><select id="voucherID" name="voucherID">
										<option data-originalprice="266.00" value="1">my voucher</option>
										<option data-originalprice="266.00" value="2">tttttt</option>
										<option data-originalprice="1000.00" value="3">pankaj test voucher</option>
									</select><?php */?>
                                    
				@if($errors->has('voucherID'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('voucherID') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>

		<div class="control-group">
			{{Form::label('summary','Summary *',array('class'=>'control-label'))}}
			<div class="controls">
				<?php /*?>{{Form::textarea('summary',null,array('placeholder' => 'Summary','rows' => '5' , 'style' => 'width:85%'))}}<?php */?>
                <textarea id="summary" cols="50" name="summary" style="width:85%" rows="5" placeholder="Summary">{{$voucher->summary}}</textarea>
				@if($errors->has('summary'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('summary') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>

		<div class="control-group">
			{{Form::label('highlights','Hightlights *',array('class'=>'control-label'))}}
			<div class="controls">
				<?php /*?>{{Form::textarea('highlights',null,array('placeholder' => 'Hightlights','rows' => '5' , 'style' => 'width:85%'))}}<?php */?>
                <textarea id="highlights" cols="50" name="highlights" style="width:85%" rows="5" placeholder="Hightlights">{{$voucher->highlights}}</textarea>
				@if($errors->has('highlights'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('highlights') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>

		<div class="control-group">
			<div class="controls">
				<?php /*?>{{ Form::checkbox('breakfastIncluded', '1');}}<?php */?>
                <input type="checkbox" value="1" name="breakfastIncluded" @if($voucher->breakfastIncluded == 1) {{'checked="checked"'}}@endif> Breakfast Included
				@if($errors->has('breakfastIncluded'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('breakfastIncluded') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>

		<div class="control-group">
			{{Form::label('','Original Price',array('class'=>'control-label'))}}
			<div class="controls">
				<input type='text' value="{{$voucher->room->originalPrice}}" id="originalPrice" disabled="true" />
			@if($errors->has('originalPrice'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('originalPrice') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>

		<div class="control-group">
			{{Form::label('discountPercentage','Discount Percentage *',array('class'=>'control-label'))}}
			<div class="controls">
				<?php /*?>{{Form::text('discountPercentage',null,array('placeholder' => 'Discount Percentage'))}}<?php */?>
                <input type="text" id="discountPercentage" value="{{$voucher->discountPercentage}}" name="discountPercentage" placeholder="Discount Percentage">
				@if($errors->has('discountPercentage'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('discountPercentage') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>

		<div class="control-group">
			{{Form::label('discountedPrice','Discounted Price *',array('class'=>'control-label'))}}
			<div class="controls">
				<?php /*?>{{Form::text('discountedPrice',null,array('placeholder' => 'Discounted Price'))}}<?php */?>
                <input type="text" id="discountedPrice" value="{{$voucher->discountedPrice}}" name="discountedPrice" placeholder="Discounted Price">
				@if($errors->has('discountedPrice'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('discountedPrice') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>

		<div class="control-group">
			{{Form::label('flashDealAmount','Flash Deal  Amount *',array('class'=>'control-label'))}}
			<div class="controls">
				<?php /*?>{{Form::text('flashDealAmount',null,array('placeholder' => 'Flash Deal  Amount'))}}<?php */?>
                <input type="text" id="flashDealAmount" value="{{$voucher->voucherAmount}}" name="flashDealAmount" placeholder="Flash Deal  Amount">
				@if($errors->has('flashDealAmount'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('flashDealAmount') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>

		<div class="control-group">
			{{Form::label('flashDealDuration','Flash Deal  Duration *',array('class'=>'control-label'))}}
			<div class="controls">
				<?php /*?>{{Form::select('flashDealDuration',range(2,10),array('placeholder' => 'Flash Deal Duration'))}}<?php */?>
                
                <select name="flashDealDuration" id="flashDealDuration">
                
                @for($i=2;$i<=10;$i++)
                	<option  value="{{$i}}"  @if($voucher->voucherDuration == $i) {{'selected="selected"'}}@endif >{{$i}}</option>
                @endfor
                	
                </select>
                
				@if($errors->has('flashDealDuration'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('flashDealDuration') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>

		

		<div class="control-group">
			{{Form::label('minimumDaysPriorBooking','Minimum days prior booking *',array('class'=>'control-label'))}}
			<div class="controls">
				<?php /*?>{{Form::text('minimumDaysPriorBooking',null,array('placeholder' => 'Minimum days prior booking'))}}<?php */?>
                <input type="text" id="minimumDaysPriorBooking" value="{{$voucher->minimumDaysPriorBooking}}" name="minimumDaysPriorBooking" placeholder="Minimum days prior booking">
                
				@if($errors->has('minimumDaysPriorBooking'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('minimumDaysPriorBooking') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>




		<?php /*?><div class="control-group">
			{{Form::label('bookingPeriodFrom','Booking Period *',array('class'=>'control-label'))}}
			<div class="controls">
				{{Form::text('bookingPeriodFrom',null,array('placeholder' => 'From','class' => 'datepicker input-small'))}}
				{{Form::text('bookingPeriodTo',null,array('placeholder' => 'To','class' => 'datepicker input-small'))}}
                
               
				@if($errors->has('bookingPeriodFrom'))
				
				<div class="formErrors">
					<ul>
						@foreach($errors->get('bookingPeriodFrom') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
			@if($errors->has('bookingPeriodTo'))
			<div class="formErrors">
				<ul>
					@foreach($errors->get('bookingPeriodTo') as $message)
					<li>
						{{$message}}
					</li>
					@endforeach
				</ul>
			</div>
			@endif				

		</div><?php */?>
        
        
		
		<div class="control-group">
			{{Form::label('redemptionPeriodFrom','Redemption Period *',array('class'=>'control-label'))}}
			<div class="controls">
				<input class=" input-small datepicker" type="text" placeholder="From" value="{{$voucher->redemptionPeriodFrom}}"  name="redemptionPeriodFrom"/>
				<input class=" input-small datepicker" type="text" placeholder="To" value="{{$voucher->redemptionPeriodTo}}"  name="redemptionPeriodTo"/>
				@if($errors->has('redemptionPeriodFrom'))
				
				<div class="formErrors">
					<ul>
						@foreach($errors->get('redemptionPeriodFrom') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
			@if($errors->has('redemptionPeriodTo'))
			<div class="formErrors">
				<ul>
					@foreach($errors->get('redemptionPeriodTo') as $message)
					<li>
						{{$message}}
					</li>
					@endforeach
				</ul>
			</div>
			@endif				

		</div>



		
        
        

		<div class="control-group">
			{{Form::label('flashDealPolicy','Flash Deal  Policies *',array('class'=>'control-label'))}}
			<div class="controls">
				<?php /*?>{{Form::textarea('flashDealPolicies',null,array('placeholder' => 'Flash Deal Policies','rows' => '5' , 'style' => 'width:85%'))}}<?php */?>
                
                <textarea cols="50" name="flashDealPolicies" style="width:85%" rows="5" placeholder="Flash Deal Policies">{{$voucher->voucherPolicies}}</textarea>
				@if($errors->has('flashDealPolicies'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('flashDealPolicies') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>
		
		<a  class="btn btn-primary pull-right" id="saveButton">Save</a>
		<a  class="btn btn-primary pull-right" id="publishButton">Save and Publish</a> 
		{{Form::close()}}


	</div>