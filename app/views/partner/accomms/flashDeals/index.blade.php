@extends('templates.metronic')

@section('title')
FlashDeal Listing
@stop

@section('sidebar')
@include('partner.partnersSideMenu')
@stop

@section('pageAssets')

@stop


@section('pageAsset2')
@stop

@section('content')

@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title">Flash Deals</h3>
		<?php $numberOfFlashDeals = $accomm->flashdeal()->count(); ?>
	
		@if(count($accomm->rooms) > 0)
		<a href="{{URL::route('createflashDeal',array('accomm'=>$accomm->getKey()))}}" class="btn addFlashDeal" data-flashdealcount="{{$numberOfFlashDeals}}">Add Flash Deal</a>
		@else 
		<a href="#" class="btn">Add voucher disabled because to no room registered</a>
		@endif
		<table class="table table-condensed">
			<thead>
				<tr>
					<th>Flash deal  room</th>
					<th>Original room price</th>
					<th>Discounted price</th>
					<th>Discount percentage</th>
					<th>Valid till</th>
				</tr>
			</thead>
			@if(!$accomm->partnerFlashdeals()->count())
			<tr>
				<td>You have not added any flash deals  yet.</td>
				<td></td>
				<td></td>
				<td></td>
				<th></th>
			</tr>
			@else
			@foreach($accomm->partnerFlashdeals()->get() as $flashdeal)
			<tr>
				<td>{{ucwords(Room::find($flashdeal->roomID)->name)}}</td>
				<td>{{$flashdeal->originalPrice}}</td>
				<td>{{$flashdeal->discountedPrice}}</td>
				<td>{{ceil($flashdeal->discountPercentage)}}%</td>
				<td>{{date('d M Y',strtotime($flashdeal->redemptionPeriodTo))}}</td>
				<td>
					<a href="{{URL::route('editFlashdeal',array('flashdealID' => $flashdeal->encryptedKey(),'accomm' => $accomm->id))}}" class = "btn">Edit</a>		
                    								
					<a onclick="deleteFlashDeal(this.id)" id="{{URL::route('removeflashDeal',array('id' => $flashdeal->encryptedKey(),'accomm' => $accomm->id))}}" class="btn">Delete</a>										
				</td>
			</tr>
			@endforeach
			@endif
		</table>


	</div>

</div>



@stop

@section('inlineJS')
<script>
function deleteFlashDeal(url){
	
	var r = confirm("Are you sure, you want to delete flash deal?");
	
		if (r == true)
		  {
		 	window.location=url;
		  }
		else
		  {
		  	return false;
		  }
	}
</script>
<script>
$(document).ready(function(){
	$('.addFlashDeal').click(function(event){
		if($(this).data('flashdealcount') > 0){
			alert('You can have only one Flash Deal per accommodation');
			event.preventDefault();
		}
	})
});

</script>
@stop