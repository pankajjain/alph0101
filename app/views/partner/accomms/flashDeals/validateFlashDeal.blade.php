@extends('templates.metronic')

@section('title')
New Voucher
@stop

@section('sidebar')
@include('partner.partnersSideMenu')
@stop

@section('pageAssets')

@stop


@section('pageAsset2')
@stop

@section('content')


<div class="row-fluid">
	<div class="span12">
    @if(Session::has('errMessage'))
        <div class="alert alert-success" style="color:#fff;background:#FFBABA"> {{Session::get('errMessage')}}</div>
     @endif
     
       @if(Session::has('message'))
        <div class="alert alert-success" > {{Session::get('message')}}</div>
     @endif
		<h3 >Validate FlashDeal</h3>	
       
		@if(!isset($details))
        	
      {{Form::open(array('class' => 'form-horizontal'))}}
		
		<div class="control-group">
			{{Form::label('qrcode','Qrcode *',array('class'=>'control-label'))}}
			<div class="controls" style="float:left">
				{{Form::text('qrcode',null,array('placeholder' => 'Qr Code'))}}
				@if($errors->has('qrcode'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('flashDealName') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif 
                <div style="float:left">
                <input type="hidden" name="accommID" value="{{$accomm->id}}" />
                <input type="submit" class="btn btn-primary pull-right" style="width:100px;background:#006dcc;color:#fff;margin-top:10px" name="submit" value="Validate" />
                </div>
			</div>
		</div>
	{{Form::close()}}
    @else
    	<div class="alert alert-success"> This flashdeal is valid.</div>
        <p style="font-weight:bold;font-size:14px;">FlashDeal deatils</p>
       <div>
       		<div style="float:left;width:100%">
            	<div style="float:left;width:20%">FlashDeal Name </div>
                <div style="float:left;width:40%">: {{$details[0]->flashDealName}}</div>
            </div>
            <div style="float:left;width:100%">
            	<div style="float:left;width:20%">FlashDeal Code </div>
                <div style="float:left;width:40%"> : {{$details[0]->flashCode}}</div>
            </div>
            <div style="float:left;width:100%">
            	<div style="float:left;width:20%">FlashDeal Price</div>
                <div style="float:left;width:40%"> : {{$details[0]->originalPrice}}</div>
            </div>
            
            <div style="float:left;width:100%">
            	<div style="float:left;width:20%">Discount Percentage</div>
                <div style="float:left;width:40%"> : {{(int)$details[0]->discountPercentage}}</div>
            </div>
            
            <div style="float:left;width:100%">
            	<div style="float:left;width:20%">Discounted Price : </div>
                <div style="float:left;width:40%"> : {{$details[0]->discountedPrice}}</div>
            </div>
            
            <div style="float:left;width:100%">
            	<div style="float:left;width:20%">FlashDeal Duration</div>
                <div style="float:left;width:40%"> : {{$details[0]->flashDealDuration}}</div>
            </div>
            
            <div style="float:left;width:100%">
            	<div style="float:left;width:20%">From</div>
                <div style="float:left;width:40%"> : {{$details[0]->redemptionPeriodFrom}}</div>
            </div>
            
             <div style="float:left;width:100%">
            	<div style="float:left;width:20%">To</div>
                <div style="float:left;width:40%"> : {{$details[0]->redemptionPeriodTo}}</div>
            </div>
            
            <div style="float:left;width:100%">
            	<div style="float:left;width:20%">User Name</div>
                <div style="float:left;width:40%"> : {{$details[0]->firstName.' '.$details[0]->lastName}}</div>
            </div>
            
            <div style="float:left;width:100%">
            	<div style="float:left;width:20%">User Email</div>
                <div style="float:left;width:40%"> : {{$details[0]->email}}</div>
            </div>
            
            <div style="float:left;width:100%">
            	<p style="color:#090">Are you want to use this FlashDeal code.</p>
            </div>
             <div style="float:left;width:25%">
              {{Form::open(array('class' => 'form-horizontal'))}}
                 <input type="hidden" name="accommID" value="{{$accomm->id}}" />
                 <input type="hidden" name="orderID" value="{{$details[0]->ordId}}" />
                <input type="submit" class="btn btn-primary pull-right" style="width:70px;background:#006dcc;color:#fff;float:left" name="submit" value="Yes" /> 
                 <a href="{{URL::route('validateFlashDeal',array('accomm' => $accomm->getKey()))}}" class="btn btn-primary">No</a>
            {{Form::close()}}
            
          
            </div>
            
       </div>
    @endif
		
	</div>

</div>



@stop

@section('inlineJS')



@stop