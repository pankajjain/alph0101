@extends('templates.metronic')

@section('title')
Room Listing
@stop

@section('sidebar')
@include('partner.partnersSideMenu')
@stop

@section('pageAssets')

@stop


@section('pageAsset2')
@stop

@section('content')

@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title">Sold Flash Deals</h3>
		<div class="portlet box grey">
			<div class="portlet-title">
				<div class="caption"><i class="icon icon-reorder"></i>{{ucwords($accomm->name)}}'s Sold Flash Deals</div>
				<div class="actions">
					<?php /*?><a href="{{URL::route('createRoom',array('accomm' => $accomm->id))}}" class="btn blue">
					<i class="icon icon-share"></i>Add a New Room</a><?php */?>
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-condensed">
					<thead>
						<tr>
							<th>Flash Deal Name</th>
							<th>Room</th>
							<th>Buyer Name</th>
                            <th>Buyer Email</th>							
							<th>Amount</th>
							<th>Status</th>
							<th>Created_at</th>
						</tr>
					</thead>
                    
                   @foreach($flashdeals as $flashdeal)
                    <tr>
                   		<td>{{$flashdeal->flashDealName}}</td>
                        <td>{{$flashdeal->name}}</td>
                        <td>{{$flashdeal->firstName}}&nbsp;{{$flashdeal->lastName}}</td>
                        <td>{{$flashdeal->email}}</td>
                         <td>{{$flashdeal->price}}</td>
                        <td>{{$flashdeal->paymentStatus}}</td>
                        <td>{{$flashdeal->created_at}}</td>
                     </tr>
                   @endforeach    
                        
                        
					</table>
				</div>
			</div>
		</div>
	</div>

	@stop
