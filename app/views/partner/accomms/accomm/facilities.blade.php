@extends('templates.metronic')

@section('title')
{{ucwords($accomm->name)}}'s Facility
@stop

@section('sidebar')
@include('partner.partnersSideMenu')
@stop

@section('pageAssets2')
<link rel="stylesheet" href="/static/metronic/plugins/taghandler/css/jquery.taghandler.css">
<script src="/static/metronic/plugins/taghandler/js/jquery.taghandler.js" ></script>
<link rel="stylesheet" href="/static/metronic/plugins/jquery-ui/jquery-ui-1.10.3.custom.css">
@stop

@section('content')
	<h3 class="page-title">
		{{ucwords($accomm->name)}}'s Facility <span id="mycount" class="badge badge-important"> {{ $accomm->getFacilitiesCount('standard') }} </span><br/><small>Click on the box to get started! Adding and removing facilities for your hotel.</small>
	</h3>
	@if(Session::has('message'))
	<div class="alert alert-success"> {{Session::get('message')}}</div>
	@endif
	<ul id="facilities"></ul>
@stop
<?php //$ab = Facility::getRoomFacilities(); echo "<pre>"; print_r($ab); die;?>
@section('inlineJS')
<script>
	jQuery(document).ready(function() {
		addFacility = function(title){
			accommid = "{{Crypt::encrypt($accomm->id)}}";
			URL = "{{URL::route('addFacility')}}"
			$.post(URL,{aid:accommid,facility:title,type:'standard'});
		}
		removeFacility = function(title){
			accommid = "{{Crypt::encrypt($accomm->id)}}";
			URL = "{{URL::route('removeFacility')}}"
			$.post(URL,{aid:accommid,facility:title});	
		}

		$("#facilities").tagHandler({			
			assignedTags : [ @foreach($accomm->getFacilities('standard') as $fac) '{{$fac->facility}}' , @endforeach],
			availableTags: [ @foreach(Facility::getRoomFacilities() as $fac) '{{$fac->title}}' , @endforeach],
			autocomplete: true,
			afterAdd:function(tag) { addFacility(tag); },
			afterDelete:function(tag) { removeFacility(tag); }
		});
	});

function getCount(){
    $.ajax({
      type: "GET",
      url: "{{ URL::route('getFacilityCount', $accomm->id) }}"
    })
    .done(function( data ) {
		$('#mycount').html(data);
		setTimeout(function(){getCount()}, 1000);
    });
}

getCount();
</script>
@stop