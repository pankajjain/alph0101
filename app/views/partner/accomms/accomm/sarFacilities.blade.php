@extends('templates.metronic')

@section('title')
{{ucwords($accomm->name)}}'s Sport and Recreation Facility
@stop

@section('sidebar')
@include('partner.partnersSideMenu')
@stop

@section('pageAssets2')
<link rel="stylesheet" href="/static/metronic/plugins/taghandler/css/jquery.taghandler.css">
<script src="/static/metronic/plugins/taghandler/js/jquery.taghandler.js" ></script>
<link rel="stylesheet" href="/static/metronic/plugins/jquery-ui/jquery-ui-1.10.3.custom.css">
@stop

@section('content')
	<h3 class="page-title">
		{{ucwords($accomm->name)}}'s Sport & Creation Facility <span id="mycount" class="badge badge-important"> {{ $accomm->getFacilitiesCount('s&r') }} </span><br/><small>Click on the box to get started! Adding and removing facilities for your hotel.</small>
	</h3>
	@if(Session::has('message'))
	<div class="alert alert-success"> {{Session::get('message')}}</div>
	@endif
	<ul id="facilities"></ul>
@stop

@section('inlineJS')
<script>
	jQuery(document).ready(function() {
		addFacility = function(title){
			accommid = "{{Crypt::encrypt($accomm->id)}}";
			URL = "{{URL::route('addFacility')}}"
			$.post(URL,{aid:accommid,facility:title,type:'s&r'});
		}
		removeFacility = function(title){
			accommid = "{{Crypt::encrypt($accomm->id)}}";
			URL = "{{URL::route('removeFacility')}}"
			$.post(URL,{aid:accommid,facility:title});	
		}

		$("#facilities").tagHandler({			
			assignedTags : [ @foreach($accomm->getFacilities('s&r') as $fac) '{{$fac->facility}}' , @endforeach],
			availableTags: [ @foreach(Facility::getSportAndRecreationFacilities() as $fac) '{{$fac->title}}' , @endforeach],
			autocomplete: true,
			afterAdd:function(tag) { addFacility(tag); },
			afterDelete:function(tag) { removeFacility(tag); }
		});

	});

function getCount(){
    $.ajax({
      type: "GET",
      url: "{{ URL::route('getsrFacilityCount', $accomm->id) }}"
    })
    .done(function( data ) {
		$('#mycount').html(data);
		setTimeout(function(){getCount()}, 1000);
    });
}

getCount();
</script>
@stop