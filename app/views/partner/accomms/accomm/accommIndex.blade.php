@extends('templates.metronic')

@section('title')
{{$accomm->name}}'s Panel
@stop

@section('sidebar')
@include('partner.partnersSideMenu')
@stop

@section('content')
@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<h3 class="page-title">
	{{$accomm->name}}
</h3>
<div class="full">
	<h4 class="block">{{$accomm->name}} Quick Stats </h4>
	<p class="well">
		Location :
		<code>
			{{$accomm->streetAddress}} ,
			{{$accomm->city . ', ' . $accomm->province . ', ' . $accomm->country}}
		</code>
		<br><br>
		Facilities : 
			@foreach($accomm->getFacilities() as $index=> $facility)
			@if($index>0) , @endif
			<code>{{$facility->facility}}</code>
			@endforeach
		<br><br>		
		Types of rooms : <code>{{$accomm->rooms()->count()}}</code>
		<br><br>
		Types of vouchers : <code>{{$accomm->vouchers()->count()}}</code>
		<br>

	</p>
</div>
@stop