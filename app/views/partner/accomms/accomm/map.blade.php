@extends('templates.metronic')

@section('title')
{{ucwords($accomm->name)}} Location
@stop

@section('sidebar')
@include('partner.partnersSideMenu')
@stop

@section('pageAssets')
<script type="text/javascript"
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnm6ykLCH25kF6s7kl_CJ4Red1CcrtrhI&sensor=false">
</script>
@stop
@section('content')

@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif

<div>
	<div class="row-fluid">
		<div class="span12">
			{{Form::open(array('class' => 'form-horizontal'))}}
			<h3>{{ucwords($accomm->name)}} Location</h3>
			<div class="alert ">Enter your accommodation name to search or click the map to update new location.</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span3">
			<div class="control-group">
				{{Form::label("name",'Enter accommodation name',array('class'=>'control-label'))}}
				<div class="controls">
					{{Form::text('name',null,array('placeholder' => 'Search the hotel name'))}}

					@if($errors->has('name'))
					<div class="formErrors">
						<ul>
							@foreach($errors->get('name') as $message)
							<li>
								{{$message}}
							</li>
							@endforeach
						</ul>
					</div>
					@endif
				</div>
			</div>			
			<div class="control-group">
				{{Form::label("lat",'Latitude*',array('class'=>'control-label'))}}
				<div class="controls">
					{{Form::text('lat',$accomm->lat,array('placeholder'=>'0','class' => ''))}}

					@if($errors->has('province'))
					<div class="formErrors">
						<ul>
							@foreach($errors->get('lat') as $message)
							<li>
								{{$message}}
							</li>
							@endforeach
						</ul>
					</div>
					@endif
				</div>
			</div>

			<div class="control-group">
				{{Form::label("lng",'Longitude*',array('class'=>'control-label'))}}
				<div class="controls">
					{{Form::text('lng',$accomm->lng,array('placeholder'=>'0','class' => ''))}}

					@if($errors->has('lng'))
					<div class="formErrors">
						<ul>
							@foreach($errors->get('lng') as $message)
							<li>
								{{$message}}
							</li>
							@endforeach
						</ul>
					</div>
					@endif
				</div>
			</div>
			{{Form::submit('Update',array('class' => 'btn btn-primary'))}}
		</div>
		<div class="span9 well">
			<div id="mapWrapper" style=" height: 650px;margin: 0px; padding: 0px;width:100%;float: right;">
				
			</div>
		</div>
	</div>
	{{Form::close()}}
</div>

@stop

@section('inlineJS')
<script type="text/javascript">

$(document).ready(function() {
	autocomplete =  $("#name").typeahead({source:window.autoCompleteData,
		updater: function(item) {
			window.ds.forEach(function(entry){
				if (entry.description == item) {

					url = '/gmap/map/details/' + entry.reference;
					$.getJSON(url,function(data){
						console.log(data);
						loc = data.result.geometry.location;
						$('input[name=lat]').val(loc.lat);
						$('input[name=lng]').val(loc.lng);
						document.map.setCenter(new google.maps.LatLng(loc.lat, loc.lng));
						document.map.setZoom(17);
						document.marker = new google.maps.Marker({
							position: new google.maps.LatLng(loc.lat, loc.lng),
							map: document.map,
							title: data.result.name,
							animation: google.maps.Animation.DROP
						});
					});
				}
			});
			return item;
		}});
	window.autoCompleteData = [];
	$('#name').on('keyup',function(){

		country = 'my';
		query = $("#name").val();

		window.autoCompleteData = [];

		url = '/gmap/map/' + query + '-' + country;
		url = url.replace(/\s+/g, '+').toLowerCase();

		$.getJSON(url,function(data){
			window.ds = data.predictions;
			$.each(data.predictions, function (i, entry) {
				window.autoCompleteData.push(entry.description);

			});
			autocomplete.data('typeahead').source =window.autoCompleteData;
		});

	});

});


function requestDetails(ref){

}

// gmap API
// convert address into LongLat
// https://developers.google.com/maps/documentation/javascript/geocoding
function initialize() {
	@if(!empty($accomm->lat))
	var myLatlng = new google.maps.LatLng({{$accomm->lat}},{{$accomm->lng}});
	defaultZoom = 17;
	@else
	var myLatlng = new google.maps.LatLng(-25.363882,131.044922);
	defaultZoom = 8;

	@endif
	
	var mapOptions = {
		
		center: myLatlng,
		zoom: defaultZoom,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	document.map = new google.maps.Map(document.getElementById("mapWrapper"), mapOptions);
	
	google.maps.event.addListener(document.map, 'click', function(event) {
		document.marker.setPosition(event.latLng);
		console.log(event);
		loc = document.marker.getPosition();
		$('input[name=lat]').val(event.latLng.lat());
		$('input[name=lng]').val(event.latLng.lng());

	});

	@if(!empty($accomm->lat))
	document.marker = new google.maps.Marker({
		position: new google.maps.LatLng({{$accomm->lat}}, {{$accomm->lng}}),
		map: document.map,
		title: '{{$accomm->name}}',
		animation: google.maps.Animation.DROP
	});
	@endif
}

google.maps.event.addDomListener(window, 'load', initialize);
@if(empty($accomm->lat))
	//--------------------GEO CODER-----------------

	geocoder = new google.maps.Geocoder();
	geocoder.geocode( { 'address': '{{$accomm->streetAddress}}'}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      console.log(results[0]);
      document.map.setCenter(results[0].geometry.location);
      document.map.setZoom(17);
      $('input[name=lat]').val(results[0].geometry.location.lat());
	  $('input[name=lng]').val(results[0].geometry.location.lng());
      var marker = new google.maps.Marker({
          map: document.map,
          position: results[0].geometry.location
      });
      }});

	//----------------------------------------------
@endif	
</script>
@stop