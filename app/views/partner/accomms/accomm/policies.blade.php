@extends('templates.metronic')

@section('title')
{{ucwords($accomm->name)}}'s Policy
@stop

@section('sidebar')
@include('partner.partnersSideMenu')
@stop

@section('content')

@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif


<div>
	{{Form::open(array('class' => 'form-horizontal'))}}
	<h3>{{ucwords($accomm->name)}} policies</h3>
	<br>
	<br>
	

	<div class="control-group">
		{{Form::label('infantAgeUntil','Infant Age Until',array('class'=>'control-label'))}}
		<div class="controls">

			{{Form::select('infantAgeUntil',AccommPolicy::getAgeArray(1,4),$policies->infantAgeUntil)}} &nbsp; Years Old
			@if($errors->has('infantAgeUntil'))
			<div class="formErrors">
				<ul>
					@foreach($errors->get('infantAgeUntil') as $message)
					<li>
						{{$message}}
					</li>
					@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>



	<div class="control-group">
		{{Form::label('childrenAgeFrom',"Children Age From $policies->infantAgeUntil Until ",array('class'=>'control-label'))}}
		<div class="controls">

			{{Form::select('childrenAgeFrom',AccommPolicy::getAgeArray(4,16),$policies->childrenAgeFrom,array('class' => 'input-small'))}} &nbsp; Years Old
			@if($errors->has('childrenAgeFrom'))
			<div class="formErrors">
				<ul>
					@foreach($errors->get('childrenAgeFrom') as $message)
					<li>
						{{$message}}
					</li>
					@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>	



	<div class="control-group">
		{{Form::label('minimumGuestAgeForStay',"Minimum Guest Age For Stay ",array('class'=>'control-label'))}}
		<div class="controls">

			{{Form::select('minimumGuestAgeForStay',AccommPolicy::getAgeArray(0),$policies->minimumGuestAgeForStay,array('class' => 'input-small'))}} &nbsp; Years Old
			@if($errors->has('minimumGuestAgeForStay'))
			<div class="formErrors">
				<ul>
					@foreach($errors->get('minimumGuestAgeForStay') as $message)
					<li>
						{{$message}}
					</li>
					@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>	


	<div class="control-group">
		{{Form::label('extraBedRequiredFrom',"Extra Bed Required From ",array('class'=>'control-label'))}}
		<div class="controls">

			{{Form::select('extraBedRequiredFrom',AccommPolicy::getAgeArray(0),$policies->extraBedRequiredFrom,array('class' => 'input-small'))}} &nbsp; Years Old
			@if($errors->has('extraBedRequiredFrom'))
			<div class="formErrors">
				<ul>
					@foreach($errors->get('extraBedRequiredFrom') as $message)
					<li>
						{{$message}}
					</li>
					@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>	


	<div class="control-group">
		{{Form::label('childrenStayFree',"Children can stay free",array('class'=>'control-label'))}}
		<div class="controls">

			{{Form::checkbox('childrenStayFree','childrenStayFree',$policies->childrenStayFree)}} 
			@if($errors->has('childrenStayFree'))
			<div class="formErrors">
				<ul>
					@foreach($errors->get('childrenStayFree') as $message)
					<li>
						{{$message}}
					</li>
					@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>	


	<div class="control-group">
		{{Form::label('otherPolicies',"Other Policies",array('class'=>'control-label'))}}
		<div class="controls">

			{{Form::textarea('otherPolicies',$policies->otherPolicies,array('rows' => '3' , 'style' => 'width:60%'))}} 
			@if($errors->has('otherPolicies'))
			<div class="formErrors">
				<ul>
					@foreach($errors->get('otherPolicies') as $message)
					<li>
						{{$message}}
					</li>
					@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>
	
	{{Form::submit('Update',array('class' => 'btn btn-primary'))}}
	{{Form::close()}}

</div>

@stop

@section('inlineJS')

@stop