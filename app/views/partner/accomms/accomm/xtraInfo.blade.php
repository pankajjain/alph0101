@extends('templates.metronic')

@section('title')
{{ucwords($accomm->name)}}'s Detailed Information
@stop

@section('sidebar')
@include('partner.partnersSideMenu')

@stop

@section('pageAssets2')
<script src="/static/metronic/plugins/timepicker/js/bootstrap-timepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="/static/metronic/plugins/timepicker/css/bootstrap-timepicker.min.css">
@stop

@section('content')
<h3 class="page-title">
	{{ucwords($accomm->name)}} <br/>
</h3>
@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
		<div class="portlet box yellow ">
			<div class="portlet-title">
				<div class="caption"><i class="icon-reorder"></i>Detailed Information</div>
				<div class="tools">
					<a href="javascript:;" class="collapse"></a>
				</div>
			</div>
			<div class="portlet-body form">
				<!-- BEGIN FORM-->
				{{Form::model($accomm,array('class' => 'form-horizontal form-bordered form-row-stripped'))}}
					<div class="control-group">
						{{Form::label('numberBars',"Bars",array('class'=>'control-label'))}}
						<div class="controls">
							{{Form::text('numberBars',null,array('placeholder' => 'Number of Bars', 'class' => 'm-wrap span12'))}}
							@if($errors->has('numberBars'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('numberBars') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>
					<div class="control-group">
						{{Form::label('numberRestaurant',"Restaurants",array('class'=>'control-label'))}}
						<div class="controls">
							{{Form::text('numberRestaurant',null,array('placeholder' => 'Number of Restaurants', 'class' => 'm-wrap span12'))}}
							@if($errors->has('numberRestaurant'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('numberRestaurant') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>
					<div class="control-group">
						{{Form::label('numberRoom',"Rooms",array('class'=>'control-label'))}}
						<div class="controls">
							{{Form::text('numberRoom',null,array('placeholder' => 'Number of Rooms', 'class' => 'm-wrap span12'))}}
							@if($errors->has('numberRoom'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('numberRoom') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>
					<div class="control-group">
						{{Form::label('numberFloor',"Floors",array('class'=>'control-label'))}}
						<div class="controls">
							{{Form::text('numberFloor',null,array('placeholder' => 'Number of Floors', 'class' => 'm-wrap span12'))}}
							@if($errors->has('numberFloor'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('numberFloor') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="control-group">
						{{Form::label('roomVoltage',"Room Voltage",array('class'=>'control-label'))}}
						<div class="controls">
							{{Form::text('roomVoltage',null,array('placeholder' => '12V', 'class' => 'm-wrap span12'))}}
							@if($errors->has('roomVoltage'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('roomVoltage') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="control-group">
						{{Form::label('internetUsage',"Internet Usage Fees",array('class'=>'control-label'))}}
						<div class="controls">
							{{Form::text('internetUsage',null,array('placeholder' => 'RM 10/hour', 'class' => 'm-wrap span12'))}}
							@if($errors->has('internetUsage'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('internetUsage') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="control-group">
						{{Form::label('breakfastCharge',"Breakfast Charge",array('class'=>'control-label'))}}
						<div class="controls">
							{{Form::text('breakfastCharge',null,array('placeholder' => 'RM 30/person', 'class' => 'm-wrap span12'))}}
							@if($errors->has('breakfastCharge'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('breakfastCharge') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="control-group">
						{{Form::label('roomService',"Room Service",array('class'=>'control-label'))}}
						<div class="controls">
							
							
							<label class="radio">
								{{ Form::radio('roomService','yes',array('class'=>'radio'))}}
								YES
							</label>
							<label class="radio">
								{{ Form::radio('roomService','no',array('class'=>'radio'))}}
								NO
							</label>
							<label class="radio">
								{{ Form::radio('roomService','24hours',array('class'=>'radio'))}}
								24 Hours
							</label>
							@if($errors->has('roomService'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('roomService') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="control-group">
						{{Form::label('nonSmokingRoom',"Non-Smoking Room/Floor",array('class'=>'control-label'))}}
						<div class="controls">
							<label class="radio">
								{{ Form::radio('nonSmokingRoom','yes',array('class'=>'radio'))}}
								YES
							</label>
							<label class="radio">
								{{ Form::radio('nonSmokingRoom','no',array('class'=>'radio'))}}
								NO
							</label>
							@if($errors->has('nonSmokingRoom'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('nonSmokingRoom') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="control-group">
						{{Form::label('elevator',"Elevator",array('class'=>'control-label'))}}
						<div class="controls">
							<label class="radio">
								{{ Form::radio('elevator','yes',array('class'=>'radio'))}}
								YES
							</label>
							<label class="radio">
								{{ Form::radio('elevator','no',array('class'=>'radio'))}}
								NO
							</label>
							@if($errors->has('elevator'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('elevator') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="control-group">
						{{Form::label('parking',"Parking",array('class'=>'control-label'))}}
						<div class="controls">
							<label class="radio">
								{{ Form::radio('parking','yes',array('class'=>'radio'))}}
								YES
							</label>
							<label class="radio">
								{{ Form::radio('parking','no',array('class'=>'radio'))}}
								NO
							</label>
							@if($errors->has('parking'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('parking') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="control-group">
						{{Form::label('parkingFee',"Parking Fee",array('class'=>'control-label'))}}
						<div class="controls">
							{{Form::text('parkingFee',null,array('placeholder' => 'RM 10/night', 'class' => 'm-wrap span12'))}}
							@if($errors->has('parkingFee'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('parkingFee') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="control-group">
						{{Form::label('receptionOpen',"Reception Open",array('class'=>'control-label'))}}
						<div class="controls bootstrap-timepicker">
							{{Form::text('receptionOpen',null,array('placeholder' => '9AM', 'class' => 'm-wrap span12'))}}
							@if($errors->has('receptionOpen'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('receptionOpen') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="control-group">
						{{Form::label('checkIn',"Earliest Check-in",array('class'=>'control-label'))}}
						<div class="controls">
							{{Form::text('checkIn',null,array('placeholder' => '9AM', 'class' => 'm-wrap span12'))}}
							@if($errors->has('checkIn'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('checkIn') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="control-group">
						{{Form::label('checkOut',"Check-out",array('class'=>'control-label'))}}
						<div class="controls">
							{{Form::text('checkOut',null,array('placeholder' => '12PM', 'class' => 'm-wrap span12'))}}
							@if($errors->has('checkOut'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('checkOut') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="control-group">
						{{Form::label('hotelBuilt',"Year Hotel Built",array('class'=>'control-label'))}}
						<div class="controls">
							{{Form::text('hotelBuilt',null,array('placeholder' => '2012', 'class' => 'm-wrap span12'))}}
							@if($errors->has('hotelBuilt'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('hotelBuilt') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="control-group">
						{{Form::label('hotelRenovated',"Year Hotel Last Renovated",array('class'=>'control-label'))}}
						<div class="controls">
							{{Form::text('hotelRenovated',null,array('placeholder' => '2013', 'class' => 'm-wrap span12'))}}
							@if($errors->has('hotelRenovated'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('hotelRenovated') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="control-group">
						{{Form::label('airportTransfer',"Airport Transfer",array('class'=>'control-label'))}}
						<div class="controls">
							<label class="radio">
								{{ Form::radio('airportTransfer','yes',array('class'=>'radio'))}}
								YES
							</label>
							<label class="radio">
								{{ Form::radio('airportTransfer','no',array('class'=>'radio'))}}
								NO
							</label>
							@if($errors->has('airportTransfer'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('airportTransfer') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="control-group">
						{{Form::label('airportTransferFee',"Airport Transfer Fee",array('class'=>'control-label'))}}
						<div class="controls">
							{{Form::text('airportTransferFee',null,array('placeholder' => 'RM 200', 'class' => 'm-wrap span12'))}}
							@if($errors->has('airportTransferFee'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('airportTransferFee') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="control-group">
						{{Form::label('distanceCity',"Distance from City Center (KM)",array('class'=>'control-label'))}}
						<div class="controls">
							{{Form::text('distanceCity',null,array('placeholder' => '20', 'class' => 'm-wrap span12'))}}
							@if($errors->has('distanceCity'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('distanceCity') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="control-group">
						{{Form::label('distanceAirport',"Distance to Airport (KM)",array('class'=>'control-label'))}}
						<div class="controls">
							{{Form::text('distanceAirport',null,array('placeholder' => '60', 'class' => 'm-wrap span12'))}}
							@if($errors->has('distanceAirport'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('distanceAirport') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="control-group">
						{{Form::label('timeAirport',"Time to Airport (Minutes)",array('class'=>'control-label'))}}
						<div class="controls">
							{{Form::text('timeAirport',null,array('placeholder' => '30', 'class' => 'm-wrap span12'))}}
							@if($errors->has('timeAirport'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('timeAirport') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>



					<div class="control-group">
						{{Form::label('hotelDescription',"Hotel Description",array('class'=>'control-label'))}}
						<div class="controls">
							{{Form::textarea('hotelDescription',null,array('placeholder' => 'Hotel Description','rows' => '5' , 'style' => 'width:95%'))}}
							@if($errors->has('hotelDescription'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('hotelDescription') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="form-actions">
						<button type="submit" class="btn yellow"><i class="icon-ok"></i> Update</button>
						<button type="cancel" class="btn "> Cancel</button>
					</div>
				{{Form::close()}}
				<!-- END FORM-->  
			</div>
		</div>
	</div>
</div>
@stop

@section('inlineJS')
<script>
   $(document).ready(function(){
	 $('#receptionOpen, #checkOut, #checkIn').timepicker({
                minuteStep: 5,
                showInputs: false,
                disableFocus: true
            });
	//$('#hotelBuilt, #hotelRenovated').datepicker();
   })
</script>
@stop