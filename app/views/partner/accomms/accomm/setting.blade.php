@extends('templates.metronic')

@section('title')
{{ucwords($accomm->name)}}'s Setting
@stop

@section('sidebar')
@include('partner.partnersSideMenu')
@stop

@section('pageAssets2')
@stop

@section('content')
<h3 class="page-title">
	{{ucwords($accomm->name)}} <br/>
</h3>
@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
		<div class="portlet box yellow ">
			<div class="portlet-title">
				<div class="caption"><i class="icon-reorder"></i>Accommodation Setting</div>
				<div class="tools">
					<a href="javascript:;" class="collapse"></a>
				</div>
			</div>
			<div class="portlet-body form">
				<!-- BEGIN FORM-->
				{{Form::model($accomm,array('class' => 'form-horizontal form-bordered form-row-stripped'))}}
					
					<div class="control-group">
						{{Form::label('enableReview',"Reviews & Rating",array('class'=>'control-label'))}}
						<div class="controls">
							<label class="radio">
								{{ Form::radio('enableReview','1',array('class'=>'radio'))}}
								Enable
							</label>
							<label class="radio">
								{{ Form::radio('enableReview','0',array('class'=>'radio'))}}
								Disable
							</label>
							<p>Warning: User inputs will not be able to revise or moderate.</p>
							@if($errors->has('enableReview'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('enableReview') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="form-actions">
						<button type="submit" class="btn yellow"><i class="icon-ok"></i> Update</button>
						<button type="cancel" class="btn "> Cancel</button>
					</div>
				{{Form::close()}}
				<!-- END FORM-->  
			</div>
		</div>
	</div>
</div>
@stop

@section('inlineJS')
@stop