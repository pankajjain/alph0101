@extends('templates.metronic')

@section('title')
{{ucwords($accomm->name)}}'s Photo
@stop

@section('sidebar')
@include('partner.partnersSideMenu')
@stop

@section('pageAssets2')
<link rel="stylesheet" href="/static/metronic/plugins/toastr/toastr.css">
<script src="/static/metronic/plugins/plupload/js/plupload.full.min.js" > </script>
<script src="/static/metronic/plugins/plupload/js/jquery.plupload.queue/jquery.plupload.queue.min.js" ></script>
<script src="/static/metronic/scripts/tmpl.min.js" ></script>
<script src="/static/metronic/plugins/toastr/toastr.min.js"></script>
@stop

@section('content')
@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
		<div class="portlet box yellow">
			<div class="portlet-title line">
				<div class="caption"><i class="fa fa-comments"></i>Photos</div>
				<div class="actions">
					<a class="btn blue" data-toggle="modal" href="#basic">َUpload</a>
				</div>
			</div>
			<div class="portlet-body ">
				<div id="imageHolder">
					<?php $index=1; ?>
					@foreach(AccommPhotos::where('accommID',$accomm->id)->get() as  $photo)
						@if($index==1)
						<div class="row-fluid" style="margin-top:10px;">
						@endif
							<div class="span4">	
								<div class="thumbnail" style="height:332px;">
									<div style="min-height:170px;text-align:center;">
										<img src="{{$photo->getThumbURL()}}" style="max-height:170px;">
									</div>	
									<div class="caption" style="border-top:1px dotted rgba(44,44,44,.1);">
										<input type="text" class="input-block-level" placeholder="Enter image title" value="{{$photo->title}}">
										<textarea class="input-block-level" placeholder="Enter Description for the image">{{$photo->description}}</textarea>
										<p style="text-align:right;">
											<button class="btn mini red delete" data-id="{{$photo->id}}"><i class="icon-trash"></i> Delete Item</button>
											<button class="btn mini blue update" data-id="{{$photo->id}}">Update Details</button>
										</p>
									</div>
								</div>
							</div>
						@if($index==3)
						</div>
						<?php $index=0; ?>
						@endif
						<?php $index++; ?>

					@endforeach
					@if($index < 3) </div> @endif
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Upload Images</h4>
				</div>
				<div class="modal-body">
					<div class="dd" id="nestable_list_1">
						<div id="container">
							<div id="filelist"></div>
							<br />
							<a id="pickfiles" href="#"  class="btn ">Select files</a>
							<a id="uploadfiles" href="#" class="btn green">Start Upload</a>
						</div></div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn blue" data-dismiss="modal">Done Uploading</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
	</div>
</div>

<script type="text/x-tmpl" id="photosTmp">
	<div class="span4" >	
		<div class="thumbnail" style="height:332px;">
			<div style="min-height:170px;text-align:center;">
				<img src="{%=o.imgSource%}" style="max-height:170px;"/>
			</div>	
			<div class="caption" style="border-top:1px dotted rgba(44,44,44,.1);">
				<input type="text" class="input-block-level" placeholder="Enter image title" />
				<textarea class="input-block-level" placeholder="Enter Description for the image"></textarea>
				<p style="text-align:right;">
					<button class="btn mini red delete" data-id="{%=o.id %}" ><i class="icon-trash"></i> Delete Item</button>
					<button class="btn mini blue update" data-id="{%=o.id %}" >Update Details</button>
				</p>
			</div>
		</div>
	</div>
</script>
@stop

@section('inlineJS')
<script>
	$(document).ready(function(){
		
		window.i = 0;

		$(document).on('click','.update',function(){
			url = '{{URL::route("accommPhotoUploadHandler",array("accomm" => $accomm->id))}}';
			photoID = $(this).data('id');
			title = $(this).parent().parent().find('input').val();
			description = $(this).parent().parent().find('textarea').val();
			url = url + '/' + photoID;
			$.post(url,{pid : photoID , "title" : title, "description" : description})
			.success(function(){toastr.success('Photo details updated successfully', title)});
			
		});

		$(document).on('click','.delete',function(){
			url = '{{URL::route("accommPhotoUploadHandler",array("accomm" => $accomm->id))}}';
			fid = $(this).data('id');
			removePhoto(fid);
			$(this).parents('.span4:eq(0)').fadeOut().remove();
			toastr.error('The selected photo is successfully removed');
			
		});		

		$(document).on('click','.close',function(){
			fileLI = $(this).parents('div:eq(0)');
			fid = fileLI.data('fid');			
			removePhoto(fid);
			$(fileLI).hide('slow');		
		});

		removePhoto = function(fid){
			URL = '{{URL::route("accommPhotoRemove",array("accomm" => $accomm->id))}}';
			$.post(URL,{"fid" : fid});	
		}

		var uploader = new plupload.Uploader({
			runtimes : 'gears,html5,flash,silverlight,browserplus',
			browse_button : 'pickfiles',
			container : 'container',
			max_file_size : '10mb',
			url : '{{URL::route("accommPhotoUploadHandler",array("accomm" => $accomm->id))}}',
			flash_swf_url : '/plupload/js/plupload.flash.swf',
			silverlight_xap_url : '/plupload/js/plupload.silverlight.xap',
			filters : [
			{title : "Image files", extensions : "jpg,jpeg,gif,png"},
			{title : "Zip files", extensions : "zip"}
			],
		});

		uploader.bind('Init', function(up, params) {
			// console.log(params.runtime);
			//$('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
		});

		$('#uploadfiles').click(function(e) {
			uploader.start();
			e.preventDefault();
		});

		uploader.init();

		uploader.bind('FilesAdded', function(up, files) {
			$.each(files, function(i, file) {
				// console.log(file);
				$('#filelist').append(
					'<div id="' + file.id + '" data-filename="' + file.name + '">' +
					file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
					'</div>');
			});

		up.refresh(); // Reposition Flash/Silverlight
	});

		uploader.bind('UploadProgress', function(up, file) {
			// console.log(file);
			$('#' + file.id + " b").html('<span class="alert alert-success">' + file.percent + "% <span>");
		});

		uploader.bind('Error', function(up, err) {
			console.log(err);
			$('#filelist').append("<div>Error: " + err.code +
				", Message: " + err.message +
				(err.file ? ", File: " + err.file.name : "") +
				"</div>"
				);

		up.refresh(); // Reposition Flash/Silverlight
	});

		uploader.bind('FileUploaded', function(up, file,r) {
			response = JSON.parse(r.response);
			console.log(response.thumbURL);
			fileURL = response.thumbURL;
			data = { 'id' : response.id, "title": "Untitled Image" , "description" : 'No Description' , "imgSource" : fileURL , "i" : window.i }
			$("#imageHolder .row-fluid:last-child").append(tmpl("photosTmp", data));
			window.i = window.i + 1;
			if(window.i == 3){
				$("#imageHolder").append('<div class="row-fluid" style="margin-top:10px;"></div>')
				window.i = 0;
			}
			$('#' + file.id).addClass('alert alert-success');
			$('#' + file.id).attr('data-fid',response.id);
			$('#' + file.id + " b").html("<span class='pull-right'>100%" + '<button type="button" class="close" ></button>' + "</span>");
		});

	});
</script>
@stop