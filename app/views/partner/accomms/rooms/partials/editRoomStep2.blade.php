    <div class="tabbable-custom ">
      <ul class="nav nav-tabs ">
        <li class="active"><a href="#tab_5_1" data-toggle="tab">Upload New Photos</a></li>
        <li class=""><a href="#tab_5_2" data-toggle="tab">Previously Uploaded photos</a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_5_1">
          <div>Please upload photos belonging to the this room</div>
          <div class="control-group">
            {{Form::label('files','Room photos',array('class'=>'control-label'))}}
            <div class="controls">
             {{ Form::file('files[]',['class' => 'fileupload'])}}    
             @if($errors->has('files[]'))
             <div class="formErrors">
              <ul>
                @foreach($errors->get('files[]') as $message)
                <li>
                  {{$message}}
                </li>
                @endforeach
              </ul>
            </div>        
            @endif
            <p class="help-block">You can upload multiple image files. please note that maximum file size for each file is 2MB.</p>
          </div>
        </div>          
      </div>
      <div class="tab-pane" id="tab_5_2">
        <table class="table">
          <thead>
            <tr>
              <th>Name of the image</th>
              <th>Date added</th>
              <th></th>
            </tr>
          </thead>
          @foreach($room->photos as $photo)
          <tr>
            <td>{{$photo->originalFileName}}</td>
            <td>{{$photo->created_at}}</td>
            <td> 
            
              <a href="{{$photo->getPhotoURL()}}" class="thumbnail"  target="blank">
               <img src="{{$photo->getPhotoURL()}}" style="max-width:150px;">
             </a>
             <a  class="btn red btn-block removePhoto" data-photoid="{{$photo->encryptedKey()}}">Remove Photo</a>

           </td>
         </tr>
         @endforeach
       </table>
     </div>
   </div>
 </div>




