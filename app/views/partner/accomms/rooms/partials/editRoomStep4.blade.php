<div id="policyHolder">
  
</div>


<script type="text/x-tmpl" id="policy">
<div class="control-group">
  {{Form::label('policyTitles[]','Title',array('class'=>'control-label'))}}
  <div class="controls">
    {{Form::text('policyTitles[]',null,array('placeholder'=>'Title','autocomplete'=>"off"))}}
  </div>
</div>

<div class="control-group">
  {{Form::label('description[]','Description',array('class'=>'control-label'))}}
  <div class="controls">
    {{Form::textarea('description[]',null,array('placeholder'=>'Room Description','rows' => '3'))}}
  </div>
</div>
</script>

