<div class="control-group">
  {{Form::label('name','Room Name *',array('class'=>'control-label'))}}
  <div class="controls">
    {{Form::text('name',null,array('placeholder'=>'Room Name','autocomplete'=>"off"))}}
    @if($errors->has('name'))
    <div class="formErrors">
      <ul>
        @foreach($errors->get('name') as $message)
        <li>
          {{$message}}
        </li>
        @endforeach
      </ul>
    </div>
    @endif
  </div>
</div>

<div class="control-group">
  {{Form::label('description','Room Description *',array('class'=>'control-label'))}}
  <div class="controls">
    {{Form::textarea('description',null,array('placeholder'=>'Room Description','style' => 'width:85%','rows' => 4))}}
    @if($errors->has('description'))
    <div class="formErrors">
      <ul>
        @foreach($errors->get('description') as $message)
        <li>
          {{$message}}
        </li>
        @endforeach
      </ul>
    </div>
    @endif
  </div>
</div>

<div class="control-group">
  {{Form::label('roomAvailability ','Room Availability*',array('class'=>'control-label'))}}
  <div class="controls">
    {{Form::text('roomAvailability',null,array('placeholder'=>'Total number of rooms'))}}
    @if($errors->has('roomAvailability'))
    <div class="formErrors">
      <ul>
        @foreach($errors->get('roomAvailability') as $message)
        <li>
          {{$message}}
        </li>
        @endforeach
      </ul>
    </div>
    @endif
  </div>
</div>

<div class="control-group">
  {{Form::label('roomSize ','Room Size (sf) *',array('class'=>'control-label'))}}
  <div class="controls">
    {{Form::text('roomSize',null,array('placeholder'=>'Room Size'))}}
    @if($errors->has('roomSize'))
    <div class="formErrors">
      <ul>
        @foreach($errors->get('roomSize') as $message)
        <li>
          {{$message}}
        </li>
        @endforeach
      </ul>
    </div>
    @endif
  </div>
</div>

<div class="control-group">
  {{Form::label('view','View *',array('class'=>'control-label'))}}
  <div class="controls">
    

    {{Form::select('view',Room::getRoomViews(true))}}
    @if($errors->has('view'))
    <div class="formErrors">
      <ul>
        @foreach($errors->get('view') as $message)
        <li>
          {{$message}}
        </li>
        @endforeach
      </ul>
    </div>
    @endif
  </div>
</div>

<div class="control-group">
  {{Form::label('bedType','Bed Type *',array('class'=>'control-label'))}}
  <div class="controls">
    
  
    {{Form::select('bedType',Room::getBedTypes(true))}}
    @if($errors->has('bedType'))
    <div class="formErrors">
      <ul>
        @foreach($errors->get('bedType') as $message)
        <li>
          {{$message}}
        </li>
        @endforeach
      </ul>
    </div>
    @endif
  </div>
</div>


<div class="control-group">
  {{Form::label('maxGuestsAllowed ','Maximum number of guests allowed *',array('class'=>'control-label'))}}
  <div class="controls">
    {{Form::text('maxGuestsAllowed',null,array('placeholder'=>'Maximum number of guests'))}}
    @if($errors->has('maxGuestsAllowed'))
    <div class="formErrors">
      <ul>
        @foreach($errors->get('maxGuestsAllowed') as $message)
        <li>
          {{$message}}
        </li>
        @endforeach
      </ul>
    </div>
    @endif
  </div>
</div>

<div class="control-group">
  {{Form::label('originalPrice','Original Price *',array('class'=>'control-label'))}}
  <div class="controls">
    {{Form::text('originalPrice',null,array('placeholder'=>'Original Price'))}}
    @if($errors->has('originalPrice'))
    <div class="formErrors">
      <ul>
        @foreach($errors->get('originalPrice') as $message)
        <li>
          {{$message}}
        </li>
        @endforeach
      </ul>
    </div>
    @endif
  </div>
</div>

<div class="control-group">
  <div class="controls">
  {{ Form::checkbox('breakfastIncluded', '1');}} Breakfast Included
  </div>
</div>