<div>Please upload photos belonging to the this room</div>
<div class="control-group">
{{Form::label('files','Room photos',array('class'=>'control-label'))}}
  <div class="controls">
   {{ Form::file('files[]',['class' => 'fileupload'])}}    
   @if($errors->has('files[]'))
   <div class="formErrors">
    <ul>
      @foreach($errors->get('files[]') as $message)
      <li>
        {{$message}}
      </li>
      @endforeach
    </ul>
  </div>        
  @endif
  <p class="help-block">You can upload multiple image files. please note that maximum file size for each file is 2MB.</p>
</div>
</div>