@extends('templates.metronic')

@section('title')
Edit Room 
@stop

@section('sidebar')
@include('partner.partnersSideMenu')
@stop

@section('pageAssets2')
<link rel="stylesheet" href="/static/metronic/plugins/taghandler/css/jquery.taghandler.css">
<script src="/static/metronic/plugins/taghandler/js/jquery.taghandler.js" ></script>
<link rel="stylesheet" href="/static/metronic/plugins/jquery-ui/jquery-ui-1.10.3.custom.css">
<script src="/static/metronic/scripts/tmpl.min.js" ></script>
@stop

@section('content')
@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title">Rooms</h3>
    {{Form::model($room,array('files'=>true ,'class' => 'form-horizontal'))}}
    <div class="portlet box grey" id="form_wizard_1">
      <div class="portlet-title">
       <div class="caption">
        <i class="fa fa-reorder"></i> Add room - <span class="step-title">Step 1 of 3</span>
      </div>

    </div>
    <div class="portlet-body form">

      <div class="form-wizard">
       <div class="form-body">

        <div class="navbar steps">
          <div class="navbar-inner">
            <ul class="row-fluid nav nav-pills steps">
              <li class="span4 active">
                <a href="#tab1" data-toggle="tab" class="step active">
                  <span class="number">1</span>
                  <span class="desc"><i class="icon-ok"></i> Room details</span>   
                </a>
              </li>
              <li class="span4">
                <a href="#tab2" data-toggle="tab" class="step">
                  <span class="number">2</span>
                  <span class="desc"><i class="icon-ok"></i> Photos</span>   
                </a>
              </li>
              <li class="span4">
                <a href="#tab3" data-toggle="tab" class="step">
                  <span class="number">3</span>
                  <span class="desc"><i class="icon-ok"></i> Facilities</span>   
                </a>
              </li>
            </ul>
          </div>
        </div>

        <div id="bar" class="progress progress-success progress-striped">
                      <div class="bar" style="width: 33%;"></div>
                    </div>

        <div class="tab-content">	
         <div class="tab-pane active" id="tab1"> 
          @include('partner.accomms.rooms.partials.step1')
        </div>
        <div class="tab-pane" id="tab2"> 
          @include('partner.accomms.rooms.partials.editRoomStep2')
        </div>
        <div class="tab-pane" id="tab3"> 
          @include('partner.accomms.rooms.partials.editRoomStep3')
        </div>
      </div>

    </div>

    </div>
    <div class="form-actions fluid">
      <div class="row">
       <div class="col-md-12">
        <div class="col-md-offset-3 col-md-9">
         <a href="javascript:;" class="btn blue button-previous disabled" >
           <i class="m-icon-swapleft"></i> Back 
         </a>
         <a href="javascript:;" class="btn blue button-next">
           Continue <i class="m-icon-swapright m-icon-white"></i>
         </a>
         {{Form::submit('Submit',array('class' => 'btn green button-submit','style' => 'display:none;'))}}
         {{Form::close()}}                  
       </div>
     </div>
   </div>
 </div>
</div>
</form>
</div>
</div>


</div>
</div>

@stop

@section('inlineJS')
<style>
.tab-content{padding: 40px;}
</style>

<script>
  $(document).ready(function() {
    document.addeddPhotos = {{$room->photos()->count() + 1}};
    console.log(tmpl('#policy'),{});
    Wizard = {currentStep : 1,totalSteps : 3,Facilities : ['tester']};
 //===========================================================

// Continue Button Event Handler
$('.button-next').click(function(){
  Wizard.currentStep = parseInt($('.steps li.active span.number').text());
  if(Wizard.currentStep <= Wizard.totalSteps) {
    Wizard.currentStep++;
    nextTab = 'a[href="#tab' + Wizard.currentStep + '"]' ;
  }

  var $percentWidth = Wizard.currentStep * 33.34;
    $('#form_wizard_1').find('.bar').css({
        width: $percentWidth + '%'
    });

  $(nextTab).tab('show');
  $('.button-previous').removeClass('disabled');
  if(Wizard.currentStep == Wizard.totalSteps){
    $('.button-next').hide();
    $('input[type=submit]').show();
  }



});
//Back button event handler
$('.button-previous').click(function(){
  Wizard.currentStep = parseInt($('.steps li.active span.number').text());
  if(Wizard.currentStep <= Wizard.totalSteps) {
    Wizard.currentStep--;
    nextTab = 'a[href="#tab' + Wizard.currentStep + '"]' ;
  }

  var $percentWidth = Wizard.currentStep * 33.34;
    $('#form_wizard_1').find('.bar').css({
        width: $percentWidth + '%'
    });

  $(nextTab).tab('show');
  $('.button-next').show();
  $('input[type=submit]').hide();
  if(Wizard.currentStep == 1){
    $('.button-previous').addClass('disabled');
  }
})

//Facilities
//============================================================
addFacility = function(title){
  accommid = "{{Crypt::encrypt($accomm->id)}}";
  Wizard.Facilities.push(title);
  $('input#facilities:eq(0)').clone().insertAfter('input#facilities:eq(0)').val(title);
}
removeFacility = function(title){
  itemIndex = Wizard.Facilities.indexOf(title);
  Wizard.Facilities.splice(itemIndex,1); 
   $('input#facilities[value="' + title + '"]').remove();
}

$("#facilitiesDiv").tagHandler({     
  assignedTags : [ @foreach(RoomFacility::where('roomID',$room->id)->get() as $fac) '{{$fac->facility}}' , @endforeach],
  availableTags: [ @foreach(Facility::getRoomFacilities() as $fac) '{{$fac->title}}' , @endforeach],
  autocomplete: true,
  afterAdd:function(tag) { addFacility(tag); },
  afterDelete:function(tag) { removeFacility(tag); }
});
//============================================================
$(document).on('change','.fileupload',function(){
  if($(this).data('exists') != 'true'){
    if(document.addeddPhotos < 5) {
    $(this).clone().insertAfter($(this));
    $(this).data('exists','true');
    document.addeddPhotos++;
  }
  }
});

$(".removePhoto").click(function(){
  url = '{{URL::route("removeRoomPhoto",array("accomm" => $room->accommID))}}';
 $.post(url,{photoID : $(this).data('photoid')}).done(function(r){
  $('td:contains("' + r + '")').parent().fadeOut();
  document.addeddPhotos--;
 });
});


});
</script>
@stop