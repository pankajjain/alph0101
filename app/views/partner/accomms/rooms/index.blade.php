@extends('templates.metronic')

@section('title')
Room Listing
@stop

@section('sidebar')
@include('partner.partnersSideMenu')
@stop

@section('pageAssets')

@stop


@section('pageAsset2')
@stop

@section('content')

@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title">Rooms</h3>
		<div class="portlet box grey">
			<div class="portlet-title">
				<div class="caption"><i class="icon icon-reorder"></i>{{ucwords($accomm->name)}}'s Rooms</div>
				<div class="actions">
					<a href="{{URL::route('createRoom',array('accomm' => $accomm->id))}}" class="btn blue">
					<i class="icon icon-share"></i>Add a New Room</a>
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-condensed">
					<thead>
						<tr>
							<th>Room Name</th>
							<th>Number of vouchers</th>
							<th>Number of flash deals</th>							
							<th>Bed Type</th>
							<th>Max Guests</th>
							<th>Price</th>
							<th>Action</th>
						</tr>
					</thead>
                    @if($accomm->rooms()->count() > 0)
					@foreach(Room::published($accomm->getKey()) as $room)
					<tr>
						<td><a href="{{URL::route('editRoom',array('roomID'=>$room->encryptedKey(),'accomm' => $accomm->id))}}" class="accomnamelink">{{$room->name}}</a></td>
						<th>{{$room->numberOfVouchers()}}</th>
						<th>{{$room->numberOfFlashDeals()}}</th>
						<td>{{$room->bedType}}</td>
						<td>{{$room->maxGuestsAllowed}}</td>
						<td>{{$room->originalPrice}}</td>
						<td>
							<a href="{{URL::route('editRoom',array('roomID' => $room->encryptedKey(),'accomm' => $accomm->id))}}" class="btn blue">Edit</a>
							@if((!$room->hasActiveVouchers()) and (!$room->hasActiveFlashDeals()))
							<a href="{{URL::route('removeRoom',array('roomID' => $room->encryptedKey(),'accomm' => $accomm->id))}}" class="btn">Delete</a>
							@endif
						</td>
					</tr>
					@endforeach
                    @else
						<tr>
							<td>You have not yet created a any room for '{{ucwords($accomm->name)}}'</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<th></th>
						</tr>

						@endif
					</table>
				</div>
			</div>
		</div>
	</div>

	@stop
