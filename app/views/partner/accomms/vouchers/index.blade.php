@extends('templates.metronic')

@section('title')
Voucher Listing
@stop

@section('sidebar')
@include('partner.partnersSideMenu')
@stop

@section('pageAssets')

@stop


@section('pageAsset2')
@stop

@section('content')

@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title">Vouchers</h3>
		
		@if(count($accomm->rooms) > 0)
		<a href="{{URL::route('createVoucher',array('accomm'=>$accomm->getKey()))}}" class="btn red">Add voucher</a>
		@else 
		<a href="#" class="btn">Add voucher disabled because no room added yet</a>
		@endif
		<table class="table table-condensed">
			<thead>
				<tr>
					<th>Voucher room</th>
					<th>Status</th>
					<th>Voucher amount</th>
					<th>Original room price</th>
					<th>Discounted price</th>
					<th>Discount percentage</th>
					<th>Redemption period</th>
					
					<th></th>
				</tr>
			</thead>
			@if(!$accomm->partnerVouchers()->count())
			<tr>
				<td>You have not added any voucher for your rooms yet.</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			@else
			@foreach($accomm->partnerVouchers()->get() as $voucher)
			<tr>
				<td>{{ucwords(Room::find($voucher->roomID)->name)}}</td>
				<th>{{$voucher->getStatus()}}</th>
				<th>{{$voucher->voucherAmount}}</th>
				<td>{{$voucher->originalPrice}}</td>
				<td>{{$voucher->discountedPrice}}</td>
				<td>{{ceil($voucher->discountPercentage)}}%</td>
				<th>{{date('Y m d',strtotime($voucher->redemptionPeriodFrom))}} - {{date('Y m d',strtotime($voucher->redemptionPeriodTo)) }}</th>
				
				<td>
					@if($voucher->status != 'published')
					<a href="{{URL::route('editVoucher',array('voucherID' => $voucher->encryptedKey(),'accomm' => $accomm->id))}}" class = "btn">Edit</a>					
					@endif
					<a onclick="deleteVoucher(this.id)" id="{{URL::route('removeVoucher',array('voucherID' => $voucher->encryptedKey(),'accomm' => $accomm->id))}}" class="btn">Delete</a>
				</td>
			</tr>
			@endforeach
			@endif
		</table>


	</div>
</div>



@stop

@section('inlineJS')
<script>
function deleteVoucher(url){
	
	var r = confirm("Are you sure, you want to delete voucher?");
	
		if (r == true)
		  {
		 	window.location=url;
		  }
		else
		  {
		  	return false;
		  }
	}
</script>
@stop