@extends('templates.metronic')

@section('title')
New Voucher
@stop

@section('sidebar')
@include('partner.partnersSideMenu')
@stop

@section('pageAssets')

@stop


@section('pageAsset2')
@stop

@section('content')

@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title">New Voucher</h3>	
		{{Form::open(array('class' => 'form-horizontal'))}}
		{{Form::hidden('status','saved')}}

		<div class="control-group">
			{{Form::label('voucherName','Voucher Name *',array('class'=>'control-label'))}}
			<div class="controls">
				{{Form::text('voucherName',null,array('placeholder' => 'Voucher Name'))}}
				@if($errors->has('voucherName'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('voucherName') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>

		<div class="control-group">
			{{Form::label('roomID','Room *',array('class'=>'control-label'))}}
			<div class="controls">
				<?php $rooms= $accomm->rooms; ?>
				<select name="roomID" id="roomID">
					@foreach($rooms as $room)
					<option value="{{$room->id}}" data-originalPrice="{{$room->originalPrice}}">{{$room->name}}</option>
					@endforeach
				</select>
				@if($errors->has('roomID'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('roomID') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>

		<div class="control-group">
			{{Form::label('summary','Summary *',array('class'=>'control-label'))}}
			<div class="controls">
				{{Form::textarea('summary',null,array('placeholder' => 'Summary','rows' => '5' , 'style' => 'width:85%'))}}
				@if($errors->has('summary'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('summary') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>

		<div class="control-group">
			{{Form::label('highlights','Hightlights *',array('class'=>'control-label'))}}
			<div class="controls">
				{{Form::textarea('highlights',null,array('placeholder' => 'Hightlights','rows' => '5' , 'style' => 'width:85%'))}}
				@if($errors->has('highlights'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('highlights') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>

		<div class="control-group">
			<div class="controls">
				{{ Form::checkbox('breakfastIncluded', '1');}} Breakfast Included
				@if($errors->has('breakfastIncluded'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('breakfastIncluded') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>

		<div class="control-group">
			{{Form::label('','Original Price',array('class'=>'control-label'))}}
			<div class="controls">
				<input type='text' value="{{$rooms[0]->originalPrice}}" id="originalPrice" disabled="true" />
				@if($errors->has('originalPrice'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('originalPrice') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>

		<div class="control-group">
			{{Form::label('discountPercentage','Discount Percentage *',array('class'=>'control-label'))}}
			<div class="controls">
				{{Form::text('discountPercentage',null,array('onkeypress'=>'return isNumberKey(event)','onkeyup'=>'isNumberKeyWithPer(this.value)','placeholder' => 'Discount Percentage'))}}
				@if($errors->has('discountPercentage'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('discountPercentage') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>

		<div class="control-group">
			{{Form::label('discountedPrice','Discounted Price *',array('class'=>'control-label'))}}
			<div class="controls">
				{{Form::text('discountedPrice',null,array('onkeypress'=>'return isNumberKey(event)','placeholder' => 'Discounted Price'))}}
				@if($errors->has('discountPrice'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('discountPrice') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>

		<div class="control-group">
			{{Form::label('voucherAmount','Voucher Amount *',array('class'=>'control-label'))}}
			<div class="controls">
				{{Form::text('voucherAmount',null,array('onkeypress'=>'return isNumberKey(event)','placeholder' => 'Voucher Amount'))}}
				@if($errors->has('voucherAmount'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('voucherAmount') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>

		<div class="control-group">
			{{Form::label('voucherDuration','Voucher Duration *',array('class'=>'control-label'))}}
			<div class="controls">
				<?php /*?>{{Form::select('voucherDuration',range(2,10),array('placeholder' => 'Voucher Duration'))}}<?php */?>
                <select name="voucherDuration" id="voucherDuration" style="width:50px">
                  @for($i=2;$i<=10;$i++)
                	<option  value="{{$i}}" >{{$i}}</option>
                @endfor
                </select>
                
				@if($errors->has('voucherDuration'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('voucherDuration') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>

		

		<div class="control-group">
			{{Form::label('minimumDaysPriorBooking','Minimum days prior booking *',array('class'=>'control-label'))}}
			<div class="controls">
				{{Form::text('minimumDaysPriorBooking',null,array('onkeypress'=>'return isNumberKey(event)','placeholder' => 'Minimum days prior booking'))}}
				@if($errors->has('minimumDaysPriorBooking'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('minimumDaysPriorBooking') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>


		
		<div class="control-group">
			{{Form::label('redemptionPeriodFrom','Redemption Period *',array('class'=>'control-label'))}}
			<div class="controls">
				<input class=" input-small datepicker" type="text" placeholder="From" name="redemptionPeriodFrom"   id="redemptionPeriodFrom"/>
				<input class=" input-small datepicker" type="text" placeholder="To"  name="redemptionPeriodTo"  id="redemptionPeriodTo"/>
				@if($errors->has('redemptionPeriodFrom'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('redemptionPeriodFrom') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
			@if($errors->has('redemptionPeriodTo'))
			<div class="formErrors">
				<ul>
					@foreach($errors->get('redemptionPeriodTo') as $message)
					<li>
						{{$message}}
					</li>
					@endforeach
				</ul>
			</div>
			@endif
		</div>


		<div class="control-group">
			{{Form::label('voucherPolicy','Voucher Policies *',array('class'=>'control-label'))}}
			<div class="controls">
				{{Form::textarea('voucherPolicy',null,array('placeholder' => 'Voucher Policies','rows' => '5' , 'style' => 'width:85%'))}}
				@if($errors->has('voucherPolicy'))
				<div class="formErrors">
					<ul>
						@foreach($errors->get('voucherPolicy') as $message)
						<li>
							{{$message}}
						</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>
		
		<a  class="btn btn-primary pull-right" id="saveButton">Save</a>
		<a  class="btn btn-primary pull-right" id="publishButton">Save and Publish</a> 
		{{Form::close()}}


	</div>

</div>



@stop

@section('inlineJS')
<script>
function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : event.keyCode;
          if (charCode != 46 && charCode > 31  && (charCode < 48 || charCode > 57))
		  	{
             	alert("Please enter numeric value.");
			 return false;
			 }

          return true;
       }
	   
function isNumberKeyWithPer(v){
	if(v > 100 || v < 1){
			alert("Please enter numeric value between 1 to 100.");
			$('#discountPercentage').val('');
			 return false;
		}
		 return true;
	}
</script>

<script>
	$(document).ready(function(){
		$('.datepicker').datepicker();

		$('#roomID').change(function(){
			$('#originalPrice').val($('#roomID').find(':selected').data('originalprice'));
			if($('#discountPercentage').val() != ''){
					originalprice = $('#originalPrice').val();
					$('#discountedPrice').val(discountCalculator(originalprice,$('#discountPercentage').val()));
				}
		});

		discountCalculator = function(originalprice,discountPercentage){
			return (originalprice - (originalprice / 100) * discountPercentage);
		}

		percentCalculator = function(originalPrice,discountedPrice){
			return ((originalPrice - discountedPrice) * 100) / originalPrice;
		}

		$('#discountPercentage').change(function(){
			originalprice = $('#originalPrice').val();
			$('#discountedPrice').val(discountCalculator(originalprice,$(this).val()));
		});

		$('#discountedPrice').change(function(){
			originalprice = $('#originalPrice').val();
			var per = percentCalculator(originalprice,$(this).val());
			if(per > 100 || per < 1){
				alert("Please enter sufficient amount.");
				$('#discountedPrice').val(0);
				$('#discountPercentage').val(0);
				return false;
			}else{
			$('#discountPercentage').val(percentCalculator(originalprice,$(this).val()));
			}

		});

		$('#saveButton').click(function(){
			
			form = $(this).parents('form');
							
			if($('#redemptionPeriodFrom').val() > $('#redemptionPeriodTo').val()){
					alert("Please enter correct redemption period date;");
					return false;
				}
			form.submit();
		});

		$('#publishButton').click(function(){
			
			if($('#redemptionPeriodFrom').val() > $('#redemptionPeriodTo').val()){
					alert("Please enter correct redemption period date;");
					return false;
				}
			form = $(this).parents('form');
			form.find('input[name=status]').val('published');
			form.submit();
		});
	});

</script>
@stop