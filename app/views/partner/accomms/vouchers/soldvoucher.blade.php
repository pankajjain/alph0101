@extends('templates.metronic')

@section('title')
Room Listing
@stop

@section('sidebar')
@include('partner.partnersSideMenu')
@stop

@section('pageAssets')

@stop


@section('pageAsset2')
@stop

@section('content')

@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title">Sold Vouchers</h3>
		<div class="portlet box grey">
			<div class="portlet-title">
				<div class="caption"><i class="icon icon-reorder"></i>{{ucwords($accomm->name)}}'s Sold Vouchers</div>
				<div class="actions">
					<?php /*?><a href="{{URL::route('createRoom',array('accomm' => $accomm->id))}}" class="btn blue">
					<i class="icon icon-share"></i>Add a New Room</a><?php */?>
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-condensed">
					<thead>
						<tr>
							<th>Voucher Name</th>
							<th>Room</th>
							<th>Buyer Name</th>
                            <th>Buyer Email</th>							
							<th>Amount</th>
							<th>Status</th>
							<th>Created_at</th>
						</tr>
					</thead>
                    
                   @foreach($vouchers as $voucher)
                    <tr>
                   		<td>{{$voucher->voucherName}}</td>
                        <td>{{$voucher->name}}</td>
                        <td>{{$voucher->firstName}}&nbsp;{{$voucher->lastName}}</td>
                        <td>{{$voucher->email}}</td>
                         <td>{{$voucher->price}}</td>
                        <td>{{$voucher->paymentStatus}}</td>
                        <td>{{$voucher->created_at}}</td>
                     </tr>
                   @endforeach    
                        
                        
					</table>
				</div>
			</div>
		</div>
	</div>

	@stop
