@extends('templates.pillo')

@section('title')
Your Travel Partner
@stop

@section('pageAssets')
<script src="/static/pillo/js/countdown.js"></script>
<link rel="stylesheet" href="/static/metronic/plugins/rateit/rateit.css">
<script src="/static/metronic/plugins/rateit/jquery.rateit.min.js"> </script>
@stop
@section('content')
<div class="main" role="main">		
	<div class="wrap clearfix">

		<div class="content clearfix">

			<section class="three-fourth">
				
					<h1>My Account</h1>
					<p> {{Lang::get('reminders.user')}} </p>
					<p>@currency(99.00, Session::get('my.currency', Config::get('app.currency')))</p>

					
					<!--inner navigation-->
					<nav class="inner-nav">
						<ul>
							<li class="active"><a href="#MyBookings" title="My Bookings">My Vouchers</a></li>
							<li class=""><a href="#MyReviews" title="My Reviews">My Reviews</a></li>
							<li class=""><a href="#MyReviews" title="My Reviews">Referral</a></li>
							<li class=""><a href="#MySettings" title="Settings">Settings</a></li>
						</ul>
					</nav>
					<!--//inner navigation-->
					
					<!--My Bookings-->
					<section id="MyBookings" class="tab-content" style="display: block;">
						<!--booking-->
                        @if(!empty($vouchers))
						@foreach($vouchers as $voucher)
						<article class="bookings">
							<h1><a href="#">{{$voucher->accommName}}</a></h1>
							<div class="b-info">
								<table>
									<tbody><tr>
										<th>Booking number</th>
										<td>#{{str_pad($voucher->orderID, 6, "0", STR_PAD_LEFT)}}</td>
									</tr>
									<tr>
										<th>Room</th>
										<td>{{$voucher->roomName}}</td>
									</tr>
									<tr>
										<th>Check-in Date</th>
										<td>{{$voucher->redemptionPeriodFrom}}</td>
									</tr>
									<tr>
										<th>Check-out Date</th>
										<td>{{$voucher->redemptionPeriodTo}}</td>
									</tr>
									<tr>
										<th>Total Price:</th>
										<td>
                                        <strong> 
                                        <?php
									$ip = $_SERVER['REMOTE_ADDR'];
									$curr =  unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
									echo Currency::format($voucher->price, $curr['geoplugin_currencyCode']);
								?></strong>
                                        </td>
									</tr>
								</tbody></table>
							</div>
							
							<div class="actions">
								<a href="{{URL('/voucherOrderDetail/'.$voucher->vid.'/'.$voucher->orderID)}}" class="gradient-button">View Voucher</a>
								
						
							</div>
						</article>		
						@endforeach
                        @else
                        <p style="color:#F00;text-align:center">You have not any voucher.</p>
                        @endif
					</section>
					<!--//My Bookings-->
					
					
					<!--MyReviews-->
					<section id="MyReviews" class="tab-content" style="display: block;">
                    @if(!empty($review))
						@foreach($review as $rev)
						<article class="myreviews">
							<h1>Your review of {{$rev->name}}</h1>
							<div class="score">
								<span class="achieved">{{ round(DB::table('reviewsrating')->where('byUserID',$userID)->sum('rating')/sizeof($review))}} </span>
								<span> / 5</span>
							</div>
							<div class="reviews">
								<div class="pro"><p>{{$rev->positiveReviews}}</p></div>
								<div class="con"><p>{{$rev->negativeReviews}}</p></div>
							</div>
						</article>
						@endforeach
                        @else
                        <p style="color:#F00;text-align:center">You have not any reviews.</p>
                        @endif
						
					</section>
					<!--//MyReviews-->
					
					<!--MySettings-->
                   
					<section id="MySettings" class="tab-content" style="display: block;">
						<article class="mysettings">
							<h1>Personal details</h1>
							<table>
								<tbody><tr>
									<th>First name:</th>
									<td>{{$users[0]->firstName}}
										<!--edit fields-->
										<div class="edit_field" id="field1" style="display: block;">
											<label for="new_name">Your new name:</label>
											<input type="text" id="firstName" name="firstName">
											<input type="submit" value="save" class="gradient-button" id="submit1" onclick="javascript:updateUserValue('firstName')">
											<a href="#">Cancel</a>
										</div>
										<!--//edit fields-->
									</td>
									<td><a href="javascript:void(0);" onclick="showHide('field1')" class="gradient-button edit">Edit</a></td>
								</tr>
								<tr>
									<th>Last name:</th>
									<td>{{$users[0]->lastName}}
										<!--edit fields-->
										<div class="edit_field" id="field2" style="display: block;">
											<label for="new_last_name">Your new last name:</label>
											<input type="text" id="lastName" name="lastName">
											<input type="submit" value="save" class="gradient-button" id="submit2" onclick="javascript:updateUserValue('lastName')">
											<a href="#">Cancel</a>
										</div>
										<!--//edit fields-->
									</td>
									<td><a href="javascript:void(0);" onclick="showHide('field2')" class="gradient-button edit">Edit</a></td>
								</tr>
								<tr>
									<th>E-mail address: </th>
									<td>{{$users[0]->email}}
										<!--edit fields-->
										<div class="edit_field" id="field3" style="display: block;">
											<label for="new_email">Your new email:</label>
											<input type="text" id="new_email">
											<input type="submit" value="save" class="gradient-button" id="submit3">
											<a href="#">Cancel</a>
										</div>
										<!--//edit fields-->
									</td>
									<td><a href="javascript:void(0);" onclick="showHide('field3')" class="gradient-button edit">Edit</a></td>
								</tr>
								<tr>
									<th>Password: </th>
									<td>*********
										<!--edit fields-->
										<div class="edit_field" id="field4" style="display: block;">
											<label for="new_password">Your new password:</label>
											<input type="password" id="password" name="password">
											<input type="submit" value="save" class="gradient-button" id="submit4" onclick="javascript:updateUserValue('password')">
											<a href="#">Cancel</a>
										</div>
										<!--//edit fields-->
									</td>
									<td><a href="javascript:void(0);" onclick="showHide('field4')" class="gradient-button edit">Edit</a></td>
								</tr>
								<tr>
									<th>Street Address and number:</th>
									<td>
                                    
                                    {{$users[0]->address}}
										<!--edit fields-->
										<div class="edit_field" id="field5" style="display: block;">
											<label for="new_address">Your new address:</label>
											<input type="text" id="address" name="address">
											<input type="submit" value="save" class="gradient-button" id="submit5" onclick="javascript:updateUserValue('address')")>
											<a href="#">Cancel</a>
										</div>
										<!--//edit fields-->
									</td>
									<td><a href="javascript:void(0);" onclick="showHide('field5')" class="gradient-button edit">Edit</a></td>
								</tr>
								
								<tr>
									<th>Town / City: </th>
									<td>{{$users[0]->city}}
										<!--edit fields-->
										<div class="edit_field" id="field6" style="display: block;">
											<label for="new_city">Your new city:</label>
											<input type="text" id="city" name="city">
											<input type="submit" value="save" class="gradient-button" id="submit6" onclick="javascript:updateUserValue('city')">
											<a href="#">Cancel</a>
										</div>
										<!--//edit fields-->
									</td>
									<td><a href="javascript:void(0);" onclick="showHide('field6')" class="gradient-button edit">Edit</a></td>
								</tr>
								
								<tr>
									<th>ZIP code:</th>
									<td>{{$users[0]->zipcode}}
										<!--edit fields-->
										<div class="edit_field" id="field7" style="display: block;">
											<label for="new_zip">Your new ZIP code:</label>
											<input type="text" id="zipcode" name="zipcode">
											<input type="submit" value="save" class="gradient-button" id="submit7" onclick="javascript:updateUserValue('zipcode')">
											<a href="#">Cancel</a>
										</div>
										<!--//edit fields-->
									</td>
									<td><a href="javascript:void(0);" onclick="showHide('field7')" class="gradient-button edit">Edit</a></td>
								</tr>
								
								<tr>
									<th>Country:</th>
									<td> 
										<!--edit fields-->
										<div class="edit_field" id="field8" style="display: block;">
											<label for="new_country">Your new country:</label>
                                           <select id="country" name="country" style="width: 220px;background-color: #ffffff;border: 1px solid #cccccc;height: 30px;line-height: 30px;">
                                            @foreach(DB::table("countries")->get() as $country)
                                            <option value="{{$country->iso}}" @if($users[0]->country == $country->iso) {{'selected="selected"'}} @endif>{{$country->nicename}}</option>
                                            @endforeach
											</select>
                                            
											<input type="submit" value="save" class="gradient-button" id="submit8" onclick="javascript:updateUserValue('country')">
											<a href="#">Cancel</a>
										</div>
										<!--//edit fields-->
									</td>
									<td><a href="javascript:void(0);" onclick="showHide('field8')" class="gradient-button edit">Edit</a></td>
								</tr>
							</tbody></table>

						</article>
					</section>
					<!--//MySettings-->
					
				</section>
				<aside class="right-sidebar">

					<!--Need Help Booking?-->
					<article class="default clearfix">
						<h2>Need Help Booking?</h2>
						<p>Call our customer services team on the number below to speak to one of our advisors who will help you with all of your holiday needs.</p>
						<p class="number">1- 555 - 555 - 555</p>
					</article>
					<!--//Need Help Booking?-->
					
					<!--Why Book with us?-->
					<article class="default clearfix">
						<h2>Why Book with us?</h2>
						<h3>Low rates</h3>
						<p>Get the best rates, or get a refund.<br>No booking fees. Save money!</p>
						<h3>Largest Selection</h3>
						<p>140,000+ hotels worldwide<br>130+ airlines<br>Over 3 million guest reviews</p>
						<h3>We’re Always Here</h3>
						<p>Call or email us, anytime<br>Get 24-hour support before, during, and after your trip</p>
					</article>
					<!--//Why Book with us?-->
					
				</aside>
			
		</div>
	</div>
</div>
<script>

$(document).ready(function(){
		$('#field1,#field2,#field3,#field4,#field5,#field6,#field7,#field8').hide();
	});
	
	function updateUserValue(fieldId){
		
		//alert($("#"+fieldId).val());
		
		var postForm = { //Fetch form data
					fieldId  : fieldId,
					fieldValue  : $("#"+fieldId).val()
					
					};


		$.ajax({
                        type: 'POST',
                        url: "{{URL('/updateUser')}}",
                       	data: postForm,
						success: function (data) {
                               
							  alert(fieldId +" Updated Successfully");
							   //$('.deals').html(data);
                               
                        }
				});
	}
	
	function showHide(divId){
		$('#'+divId).toggle();
		}
</script>
@stop
