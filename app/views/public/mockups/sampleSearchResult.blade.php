@extends('templates.pillo')

@section('title')
Your Travel Partner
@stop

@section('pageAssets')
<script src="/static/pillo/js/countdown.js"></script>
<link rel="stylesheet" href="/static/metronic/plugins/rateit/rateit.css">
<script src="/static/metronic/plugins/rateit/jquery.rateit.min.js"> </script>
@stop
@section('content')
<div class="main" role="main">		
	<div class="wrap clearfix">
		<!--main content-->
		<div class="content clearfix">

			
			<!--sidebar-->
			<aside class="left-sidebar">
				<article class="refine-search-results">
					<h2>Refine search results</h2>
					<dl>
						<!--Price (per night)-->
						<dt class="active">Price (per night)</dt>
						<dd style="display: block; height: auto;">
							<div class="checkbox">
								<div class="checker" id="uniform-ch1"><span><input type="checkbox" id="ch1" name="price" style="opacity: 0;"></span></div>
								<label for="ch1">0 - 49 $</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch2"><span><input type="checkbox" id="ch2" name="price" style="opacity: 0;"></span></div>
								<label for="ch2">50 - 99 $</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch3"><span><input type="checkbox" id="ch3" name="price" style="opacity: 0;"></span></div>
								<label for="ch3">100 -149 $</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch4"><span><input type="checkbox" id="ch4" name="price" style="opacity: 0;"></span></div>
								<label for="ch4">150 - 199 $</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch5"><span><input type="checkbox" id="ch5" name="price" style="opacity: 0;"></span></div>
								<label for="ch5">200 $ +</label>
							</div>
						</dd>
						<!--//Price (per night)-->

						<!--Accommodation type-->
						<dt class="active">Accommodation type</dt>
						<dd style="display: block; height: auto;">
							<div class="checkbox">
								<div class="checker" id="uniform-ch6"><span><input type="checkbox" id="ch6" name="accommodation" style="opacity: 0;"></span></div>
								<label for="ch6">Hotel</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch7"><span><input type="checkbox" id="ch7" name="accommodation" style="opacity: 0;"></span></div>
								<label for="ch7">Hostel</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch8"><span><input type="checkbox" id="ch8" name="accommodation" style="opacity: 0;"></span></div>
								<label for="ch8">Apart Hotel</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch9"><span><input type="checkbox" id="ch9" name="accommodation" style="opacity: 0;"></span></div>
								<label for="ch9">Guest House</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch10"><span><input type="checkbox" id="ch10" name="accommodation" style="opacity: 0;"></span></div>
								<label for="ch10">Apartment</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch11"><span><input type="checkbox" id="ch11" name="accommodation" style="opacity: 0;"></span></div>
								<label for="ch11">Bed &amp; Breakfast</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch12"><span><input type="checkbox" id="ch12" name="accommodation" style="opacity: 0;"></span></div>
								<label for="ch12">Residence</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch13"><span><input type="checkbox" id="ch13" name="accommodation" style="opacity: 0;"></span></div>
								<label for="ch13">Farm stay</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch14"><span><input type="checkbox" id="ch14" name="accommodation" style="opacity: 0;"></span></div>
								<label for="ch14">All-inclusive resort</label>
							</div>
						</dd>
						<!--//Accommodation type-->

						<!--Star rating-->
						<dt class="active">Star rating</dt>
						<dd style="display: block; height: auto;">
							<span class="stars-info">3 or more</span>
							
							{{Form::hidden("starRating","0")}}
								<div class="rateit" style="margin-top:8px;" data-rateit-step="1" data-rateit-backingfld="#starRating"  data-rateit-resetable="false">
								</div>   
							
						</dd>
						<!--//Star rating-->

						<!--User rating-->
						<dt class="active">User rating</dt>
						<dd style="display: block; height: auto;">
							<div id="slider" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header ui-slider-range-min" style="width: 10%;"></div><a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 10%;"></a></div>
							<span class="min">0</span><span class="max">10</span>
						</dd>
						<!--//User rating-->

						<!--Hotel facilities-->
						<dt class="active">Hotel facilities</dt>
						<dd style="display: block; height: auto;">
							<div class="checkbox">
								<div class="checker" id="uniform-ch15"><span><input type="checkbox" id="ch15" name="facilities" style="opacity: 0;"></span></div>
								<label for="ch15">Wi-Fi</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch16"><span><input type="checkbox" id="ch16" name="facilities" style="opacity: 0;"></span></div>
								<label for="ch16">Parking</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch17"><span><input type="checkbox" id="ch17" name="facilities" style="opacity: 0;"></span></div>
								<label for="ch17">Airport Shuttle</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch18"><span><input type="checkbox" id="ch18" name="facilities" style="opacity: 0;"></span></div>
								<label for="ch18">Meeting /&nbsp;Banquet Facilities</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch19"><span><input type="checkbox" id="ch19" name="facilities" style="opacity: 0;"></span></div>
								<label for="ch19">Swimming pool</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch20"><span><input type="checkbox" id="ch20" name="facilities" style="opacity: 0;"></span></div>
								<label for="ch20">Restaurant</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch21"><span><input type="checkbox" id="ch21" name="facilities" style="opacity: 0;"></span></div>
								<label for="ch21">Fitness Centre</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch22"><span><input type="checkbox" id="ch22" name="facilities" style="opacity: 0;"></span></div>
								<label for="ch22">SPA &amp; Wellness&nbsp;Centre</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch23"><span><input type="checkbox" id="ch23" name="facilities" style="opacity: 0;"></span></div>
								<label for="ch23">Pets allowed</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch24"><span><input type="checkbox" id="ch24" name="facilities" style="opacity: 0;"></span></div>
								<label for="ch24">Lift</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch25"><span><input type="checkbox" id="ch25" name="facilities" style="opacity: 0;"></span></div>
								<label for="ch25">Air condition</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch26"><span><input type="checkbox" id="ch26" name="facilities" style="opacity: 0;"></span></div>
								<label for="ch26">Family rooms</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch27"><span><input type="checkbox" id="ch27" name="facilities" style="opacity: 0;"></span></div>
								<label for="ch27">Non - smoking rooms</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch28"><span><input type="checkbox" id="ch28" name="facilities" style="opacity: 0;"></span></div>
								<label for="ch28">Rooms/facilities for disabled guests</label>
							</div>
						</dd>
						<!--//Hotel facilities-->

						<!--Room facilites-->
						<dt class="active">Room facilites</dt>
						<dd style="display: block; height: auto;">
							<div class="checkbox">
								<div class="checker" id="uniform-ch29"><span><input type="checkbox" id="ch29" name="room-facilities" style="opacity: 0;"></span></div>
								<label for="ch29">Bathroom</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch30"><span><input type="checkbox" id="ch30" name="room-facilities" style="opacity: 0;"></span></div>
								<label for="ch30">Cable TV</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch31"><span><input type="checkbox" id="ch31" name="room-facilities" style="opacity: 0;"></span></div>
								<label for="ch31">Air conditioning</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch32"><span><input type="checkbox" id="ch32" name="room-facilities" style="opacity: 0;"></span></div>
								<label for="ch32">Mini bar</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch33"><span><input type="checkbox" id="ch33" name="room-facilities" style="opacity: 0;"></span></div>
								<label for="ch33">Wi - Fi</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch34"><span><input type="checkbox" id="ch34" name="room-facilities" style="opacity: 0;"></span></div>
								<label for="ch34">Wheelchair - friendly room</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch35"><span><input type="checkbox" id="ch35" name="room-facilities" style="opacity: 0;"></span></div>
								<label for="ch35">Pay TV</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch36"><span><input type="checkbox" id="ch36" name="room-facilities" style="opacity: 0;"></span></div>
								<label for="ch36">Desk</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch37"><span><input type="checkbox" id="ch37" name="room-facilities" style="opacity: 0;"></span></div>
								<label for="ch37">Room safe</label>
							</div>
						</dd>
						<!--//Room facilites-->

						<!--Meal Board-->
						<dt class="active">Meal Board</dt>
						<dd style="display: block; height: auto;">
							<div class="checkbox">
								<div class="checker" id="uniform-ch38"><span><input type="checkbox" id="ch38" name="meal" style="opacity: 0;"></span></div>
								<label for="ch38">No meal</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch39"><span><input type="checkbox" id="ch39" name="meal" style="opacity: 0;"></span></div>
								<label for="ch39">Bed &amp; Breakfast</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch40"><span><input type="checkbox" id="ch40" name="meal" style="opacity: 0;"></span></div>
								<label for="ch40">Half board</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch41"><span><input type="checkbox" id="ch41" name="meal" style="opacity: 0;"></span></div>
								<label for="ch41">Full board</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch42"><span><input type="checkbox" id="ch42" name="meal" style="opacity: 0;"></span></div>
								<label for="ch42">All - inclusive</label>
							</div>
						</dd>
						<!--//Meal Board-->

						<!--Accessibility options-->
						<dt class="active">Accessibility options</dt>
						<dd style="display: block; height: auto;">
							<div class="checkbox">
								<div class="checker" id="uniform-ch43"><span><input type="checkbox" id="ch43" name="accessibility" style="opacity: 0;"></span></div>
								<label for="ch43">Accessible bathroom</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch44"><span><input type="checkbox" id="ch44" name="accessibility" style="opacity: 0;"></span></div>
								<label for="ch44">Accessible path of travel</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch45"><span><input type="checkbox" id="ch45" name="accessibility" style="opacity: 0;"></span></div>
								<label for="ch45">Handicapped parking</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch46"><span><input type="checkbox" id="ch46" name="accessibility" style="opacity: 0;"></span></div>
								<label for="ch46">In-room accessibility</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch47"><span><input type="checkbox" id="ch47" name="accessibility" style="opacity: 0;"></span></div>
								<label for="ch47">Roll-in shower</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch48"><span><input type="checkbox" id="ch48" name="accessibility" style="opacity: 0;"></span></div>
								<label for="ch48">Accessibility equipment for the deaf</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch49"><span><input type="checkbox" id="ch49" name="accessibility" style="opacity: 0;"></span></div>
								<label for="ch49">Braille or raised signage</label>
							</div>
						</dd>
						<!--//Accessibility options-->

						<!--Hotel theme-->
						<dt class="active">Hotel theme</dt>
						<dd style="display: block; height: auto;">
							<div class="checkbox">
								<div class="checker" id="uniform-ch50"><span><input type="checkbox" id="ch50" name="theme" style="opacity: 0;"></span></div>
								<label for="ch50">Family Fun</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch51"><span><input type="checkbox" id="ch51" name="theme" style="opacity: 0;"></span></div>
								<label for="ch51">Adventure</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch52"><span><input type="checkbox" id="ch52" name="theme" style="opacity: 0;"></span></div>
								<label for="ch52">Beach &amp; Sun</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch53"><span><input type="checkbox" id="ch53" name="theme" style="opacity: 0;"></span></div>
								<label for="ch53">Casinos</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch54"><span><input type="checkbox" id="ch54" name="theme" style="opacity: 0;"></span></div>
								<label for="ch54">History &amp; Culture</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch55"><span><input type="checkbox" id="ch55" name="theme" style="opacity: 0;"></span></div>
								<label for="ch55">Clubbing</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch56"><span><input type="checkbox" id="ch56" name="theme" style="opacity: 0;"></span></div>
								<label for="ch56">Romance</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch57"><span><input type="checkbox" id="ch57" name="theme" style="opacity: 0;"></span></div>
								<label for="ch57">Shopping</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch58"><span><input type="checkbox" id="ch58" name="theme" style="opacity: 0;"></span></div>
								<label for="ch58">Skiing</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch59"><span><input type="checkbox" id="ch59" name="theme" style="opacity: 0;"></span></div>
								<label for="ch59">Wellness</label>
							</div>
							<div class="checkbox">
								<div class="checker" id="uniform-ch60"><span><input type="checkbox" id="ch60" name="theme" style="opacity: 0;"></span></div>
								<label for="ch60">Eco &amp; Nature</label>
							</div>
						</dd>
						<!--//Hotel theme-->

	
					</dl>
				</article>
			</aside>
			<!--//sidebar-->
			
			<!--three-fourth content-->
			<section class="three-fourth">
				<div class="sort-by">
					<h3>Sort by</h3>
					<ul class="sort">
						<li>Popularity <a href="#" title="ascending" class="ascending">ascending</a><a href="#" title="descending" class="descending">descending</a></li>
						<li>Price <a href="#" title="ascending" class="ascending">ascending</a><a href="#" title="descending" class="descending">descending</a></li>
						<li>Stars <a href="#" title="ascending" class="ascending">ascending</a><a href="#" title="descending" class="descending">descending</a></li>
						<li>Rating <a href="#" title="ascending" class="ascending">ascending</a><a href="#" title="descending" class="descending">descending</a></li>
					</ul>

					<ul class="view-type">
						<li class="grid-view active"><a href="#" title="grid view">grid view</a></li>
						<li class="list-view"><a href="#" title="list view">list view</a></li>
						<li class="location-view"><a href="#" title="location view">location view</a></li>
					</ul>
				</div>

				<div class="deals clearfix">
					@for($k = 0; $k < 15; $k++)
					<article class="one-fourth promo">
						<div class="ribbon-small">- {{rand(30,89)}}%</div>
						<figure><a href="hotel.php" title="">
							<img 
							src="/static/metronic/img/sampleImages/{{rand(1,3)}}.jpg" 
							alt="" width="270" height="152" /></a></figure>
							<div class="details">
								<h1>Voucher Name

									<span class="stars">
										@for($i = 0; $i < rand(2,5); $i++)
										<img src="/static/pillo/images/ico/star.png" alt="">
										@endfor
									</span>
								</h1>
								<span class="address">Kuala Lumpur</span>
								<span class="rating"> 9 /10</span>
								<span class="price">Lowest Price <em>RM {{rand(200,500)}}</em> </span>

								<div class="description">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente dolorum perferendis non culpa error! <a href="hotel.php">More info</a></p>
								</div>
								<a href="hotel.php" title="Book now" class="gradient-button yellow">Purchase</a>
							</div>
						</article>
						@endfor		

						<!--bottom navigation-->
						<div class="bottom-nav">
							<!--back up button-->
							<a href="#" class="scroll-to-top" title="Back up">Back up</a> 
							<!--//back up button-->

							<!--pager-->
							<div class="pager">
								<span class="first"><a href="#">First page</a></span>
								<span><a href="#">&lt;</a></span>
								<span class="current">1</span>
								<span><a href="#">2</a></span>
								<span><a href="#">3</a></span>
								<span><a href="#">4</a></span>
								<span><a href="#">5</a></span>
								<span><a href="#">6</a></span>
								<span><a href="#">7</a></span>
								<span><a href="#">8</a></span>
								<span><a href="#">&gt;</a></span>
								<span class="last"><a href="#">Last page</a></span>
							</div>
							<!--//pager-->
						</div>
						<!--//bottom navigation-->
					</div>
				</section>
				<!--//three-fourth content-->
			</div>
			<!--//main content-->
		</div>
	</div>
	@stop