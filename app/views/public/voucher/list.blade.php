@extends('templates.pillo')

@section('title')
Your Travel Partner
@stop

@section('pageAssets')
<script src="/static/pillo/js/countdown.js"></script>
<link rel="stylesheet" href="/static/metronic/plugins/rateit/rateit.css">
<script src="/static/metronic/plugins/rateit/jquery.rateit.min.js"> </script>
@stop
@section('content')
<div class="main" role="main">		
	<div class="wrap clearfix">

		<div class="content clearfix">
 
<section class="full">
			<h1>All Vouchers</h1>
			<div class="deals clearfix">
						<!--booking-->
                        
						@foreach($voucher as $v)
					
						<article class="one-fourth promo">
					<div class="ribbon-small">-{{(int)$v->discountPercentage}}%</div>
					@if(is_object($v->room->photos->first()))
						<figure><a href="{{URL('/voucher/'.$v->id)}}" title="">
                      
                        @if(HTML::image("https://rqphoto.s3-ap-southeast-1.amazonaws.com/$v->room->photos->first()->fileName"))
							<img 
							src="https://rqphoto.s3-ap-southeast-1.amazonaws.com/{{$v->room->photos->first()->fileName}}" 
							alt="" width="270" height="152" />
                         @else
                         	<img 
							src="http://dummyimage.com/600x400/d6d2d6/000&text=No+Image" 
							alt="" width="270" height="152" />
                         @endif
                            </a></figure>
                     @else
                     		<figure><a href="{{URL('/voucher/'.$v->id)}}" title="">
							<img 
							src="http://dummyimage.com/270x152/d6d2d6/000&text=No+Image" 
							alt="" width="270" height="152" /></a></figure>
                     @endif
							<div class="details">
								<h1>   @if(strlen($v->voucherName) > 20)
                                 {{substr($v->voucherName,0,20)}}...
                                 @else
                                  {{$v->voucherName}}
                                 @endif
								<br />
								<span class="starOuter">
									<span class="stars">
                                    
										@for($i = 0; $i < $v->accomm->starRating; $i++)
										<img src="/static/pillo/images/ico/star.png" alt="">
										@endfor
									</span>
                                   
                                </span>
								</h1>
                                <?php //echo $v->room->photos->first()->fileName;?>
								<span class="address"> @if(strlen($v->accomm->streetAddress) > 25)
                                 {{substr($v->accomm->streetAddress,0,25)}}...
                                 @else
                                  {{$v->accomm->streetAddress}}
                                 @endif<br />  {{$v->accomm->city}} , {{$v->accomm->country}}</span>
								<span class="rating"> 9 /10</span>
                                
								<span class="price">Lowest Price <em><?php /*?>RM {{$v->discountedPrice}}<?php */?>
                               <?php /*?> @currency((int)$v->discountedPrice, Session::get('my.currency', Config::get('app.currency')))<?php */?>
                                <?php
									$ip = $_SERVER['REMOTE_ADDR'];
									//$ip = '27.111.208.0';
									$curr =  unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
									echo $curPrice = Currency::format($v->discountedPrice, $curr['geoplugin_currencyCode']);
									
									$tmpCurPrice = str_replace(array('RM',html_entity_decode($curr['geoplugin_currencySymbol'])),"",$curPrice);
									$findme   = 'RM';
									$pos = strpos($curPrice, $findme);
									if ($pos !== false) {
										$curr['geoplugin_currencyCode'] = "RM";			
									} 
									 $curPrice = $tmpCurPrice;
								?>
                                </em> </span>
                                
                               	<div class="description">
									<p><a href="{{URL('/voucher/'.$v->id)}}">More info</a></p>
								</div>
                                @if(isset($userID) )
                                    @if($v->salesLimit > 0)
                                    
                                     <?php
                                            $ordId = DB::table('orders')->orderBy('id','desc')->take(1)->get();
											
											$invoiceID = "#".str_pad($ordId[0]->id+1, 6, "0", STR_PAD_LEFT);
                                           
										   
										   if($curr['geoplugin_currencyCode'] == 'SGD')
											{
											$MERCHANTID="377";
                                            $CHECKSUMADDON = "wQA6kXpKsjin";
											}else if($curr['geoplugin_currencyCode'] == 'THB')
											{
											$MERCHANTID="380";
                                            $CHECKSUMADDON = "fYuynbFCtRo2";
											}else if($curr['geoplugin_currencyCode'] == 'IDR')
											{
											$MERCHANTID="379";
                                            $CHECKSUMADDON = "KiJica8E15MV";
											}else if($curr['geoplugin_currencyCode'] == 'MYR')
											{
											$MERCHANTID="378";
                                            $CHECKSUMADDON = "LZ7Koth5UEXo";
											}else if($curr['geoplugin_currencyCode'] == 'USD')
											{
											$MERCHANTID="374";
                                            $CHECKSUMADDON = "xWGRjBwKj5tw";
											}else{
												$MERCHANTID="378";
                                           	 $CHECKSUMADDON = "LZ7Koth5UEXo";
												}
                                            
                                            
                                            $URL = 'http://demo2.2c2p.com/2c2pfrontend/Paymentv2/payment.aspx';
                                            $VERSION="5.0";
                                            $PRODUCTINFO= $v->voucherName; 
                                            $INVOICENO=$invoiceID;
                                            $REF1=$v->id;
                                            $REF2="Voucher";
                                            $REF3="";
                                            $AMOUNT= $curPrice;
                                            $PROMOTION="";
                                            $CUSTEMAIL="";
                                            $PAYCURRENCY="";
                                            $PAYCATEGORYID=""; 
											
                                            
                                            
                                            $toHash = $VERSION.$MERCHANTID.$PRODUCTINFO.$INVOICENO.$REF1.$REF2.$REF3.$AMOUNT.$PROMOTION.$CUSTEMAIL.$PAYCURRENCY.$PAYCATEGORYID.$CHECKSUMADDON;
                                            //HASH MD5
                                            $CHECKSUM=md5($toHash); 
                                            //ADD PADDING
                                            $maxPADDING = 40-strlen($CHECKSUM); 
                                            for($i=0;$i<$maxPADDING;$i++){ 
                                                $CHECKSUM = "X".$CHECKSUM;
                                            } 
                                            extract($_POST);
                                            
                                            //set POST variables
                                            $fields = array(
                                                            'VERSION'=>$VERSION,
                                                            'MERCHANTID'=>$MERCHANTID,
                                                            'PRODUCTINFO'=>$PRODUCTINFO,
                                                            'INVOICENO'=>$INVOICENO,
                                                            'REF1'=>$REF1,
                                                            'REF2'=>$REF2,
                                                            'REF3'=>$REF3,
                                                            'AMOUNT'=>$AMOUNT,
                                                            'PROMOTION'=>$PROMOTION,
                                                            'CUSTEMAIL'=>$CUSTEMAIL,
                                                            'PAYCURRENCY'=>$PAYCURRENCY,
                                                            'PAYCATEGORYID'=>$PAYCATEGORYID, 
                                                            'CHECKSUM'=>$CHECKSUM
                                                            ); 
                                            
                                            ?>
                                    
                                    
                                    <form name="requestForm" action="<?php echo $URL ?>" method="post">
										<?php
                                        foreach ($fields as $k => $w) {
                                            //making the HTML hidden field for post data
                                        ?>
                                            <input type="hidden" name="<?php echo $k; ?>" value="<?php echo $w ?>"/>
                                        <?php
                                        }
                                        ?>
                                        <input type="submit" name="submit" value="Purchase" class="gradient-button yellow">
                                        
                                        </form>
                                        
                                      <?php /*?> <a href="{{URL('/voucherPurchase/'.$v->id)}}" title="Book now" class="gradient-button yellow">Purchase</a><?php */?>
                                     @else
                                     	<a  title="Book now" class="gradient-button yellow">Sold Out</a>
                                     @endif
                                @else
                                <a href="{{URL('/signin/')}}" title="Book now" class="gradient-button yellow">Purchase</a>
                                @endif
                                
                                
							</div>
						</article>   
                                             
						@endforeach
                         
                        </div>
					</section>
 </div></div></div>
@stop