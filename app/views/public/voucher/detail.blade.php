@extends('templates.pillo')

@section('title')
Your Travel Partner
@stop

@section('pageAssets')
<!--<script src="/static/pillo/js/countdown.js"></script>-->
<link rel="stylesheet" href="/static/metronic/plugins/rateit/rateit.css">
<link rel="stylesheet" href="/static/metronic/css/jquery.countdown.css">
<script type="text/javascript" src="{{ URL::to('static/pillo/js/sequence.jquery-min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('static/pillo/js/sequence.js') }}"></script>
<script src="/static/metronic/plugins/rateit/jquery.rateit.min.js"> </script>
<script src="/static/metronic/scripts/jquery.plugin.js"></script>
<script src="/static/metronic/scripts/jquery.countdown.js"></script>



@stop
@section('content')
<div class="main" role="main">		
	
   <?php //echo "<pre>"; print_r($voucher); die;?>
		<div class="content clearfix">
 
<?php /*?><section id="MyBookings" class="tab-content" style="display: block;width:100%">
						<!--booking-->
						@foreach($voucher as $vouch)
						<article class="bookings">
							<!--<h1><a href="#">Best ipsum hotel</a></h1>-->
							<div class="b-info" style="width:100%">
								<table>
									<tbody>
                                        <tr>
                                            <th>Vocher Name</th>
                                            <td>{{$vouch->voucherName}}</td>
                                        </tr>
                                        <tr>
                                            <th>Hotel</th>
                                            <td>
                                            @foreach(DB::table("accommodations")->where('id',$vouch->accommID)->get() as $accom)
                                            	{{$accom->name}}
                                            @endforeach
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Room</th>
                                            <td>
                                            @foreach(DB::table("rooms")->where('id',$vouch->roomID)->get() as $room)
                                            	{{$room->name}}
                                            @endforeach
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Voucher Summary</th>
                                            <td>{{$vouch->summary}}</td>
                                        </tr>
                                        <tr>
                                            <th>Voucher Hightlights</th>
                                            <td>{{$vouch->highlights}}</td>
                                        </tr>
                                        <tr>
                                            <th>Breakfast Included</th>
                                            <td>
                                            @if($vouch->breakfastIncluded == 1)
                                            {{ 'Yes' }}
                                            @else
                                            {{ 'No'}}
                                            @endif</td>
                                        </tr>
                                        <tr>
                                            <th> Original Price	</th>
                                            <td>
                                             @currency((int)$vouch->originalPrice, Session::get('my.currency', Config::get('app.currency')))
                                            </td>
                                        </tr>
                                         
                                        <tr>
                                            <th>Discount Percentage</th>
                                            <td>{{$vouch->discountPercentage}}</td>
                                        </tr>
                                        <tr>
                                            <th>Discounted Price</th>
                                            <td>
                                            @currency((int)$vouch->discountAmount, Session::get('my.currency', Config::get('app.currency')))
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Voucher Amount:</th>
                                            <td><strong>
                                            @currency((int)$vouch->voucherAmount, Session::get('my.currency', Config::get('app.currency')))
                                            </strong></td>
                                        </tr>
                                        <tr>
                                            <th>Voucher Duration</th>
                                            <td>{{$vouch->voucherDuration}}</td>
                                        </tr>
                                        <tr>
                                            <th>Voucher Minimum days prior booking</th>
                                            <td>{{$vouch->minimumDaysPriorBooking}}</td>
                                        </tr>
                                        <tr>
                                            <th>Redemption Period</th>
                                            <td>{{$vouch->redemptionPeriodFrom}} To {{$vouch->redemptionPeriodTo}}</td>
                                        </tr>
                                        <tr>
                                            <th>Voucher Policies </th>
                                            <td>{{$vouch->voucherPolicies}}</td>
                                        </tr>
                                        
									</tbody>
                                 </table>
							</div>
							
							
						</<article>		
						@endforeach
					</section><?php */?>
                    
      <?php /*?> <td>{{$vouch->redemptionPeriodFrom}} To {{$vouch->redemptionPeriodTo}}</td>  <?php */?>            
   <div class="voucher_page">
   @foreach($voucher as $vouch)
   

   
   <?php  $dateValue = $vouch->redemptionPeriodTo; 
 $time=strtotime($dateValue);
 $month=date("m",$time);
 $year=date("Y",$time);
 $day=date("d",$time);?>
   <script>
$(function () {
	var austDay = new Date();
	austDay = new Date({{$year}}, {{$month-1}}, {{$day}});
	$('#defaultCountdown').countdown({until: austDay});
	
});
</script>
	<!--<div class="slider"><img src="/static/pillo/images/sliderimg.jpg" /></div>-->
    
    <section class="slider clearfix" style="margin-top: 0px;">
	<div id="sequence" class="innerSlider">
		<ul>
        @if(!empty($accommPhoto))
			 @foreach ($accommPhoto as $photo)
           
                <li>
                @if(HTML::image("https://rqphoto.s3-ap-southeast-1.amazonaws.com/$photo->fileName"))
                    <img class="main-image animate-in" src="https://rqphoto.s3-ap-southeast-1.amazonaws.com/{{$photo->fileName}}" alt="" width="850" height="531" /></li>
                @else
                    <img src="http://dummyimage.com/850x531/d6d2d6/000&text=No+Image" alt="" width="850" height="531"/>
                @endif
        	@endforeach
        @else
        	<img src="http://dummyimage.com/850x531/d6d2d6/000&text=No+Image" alt="" width="850" height="531"/>
        @endif
			
		</ul>
	</div>
</section>
    
    <div class="boucherdiv">
    	 <div class="price"> From <span><?php /*?>@currency((int)$vouch->discountedPrice, Session::get('my.currency', Config::get('app.currency')))<?php */?>
         	<?php
									$ip = $_SERVER['REMOTE_ADDR'];
									//$ip = '27.111.208.0';
									$curr =  unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
									echo $curPrice = Currency::format($vouch->discountedPrice, $curr['geoplugin_currencyCode']);
									
									
									$tmpCurPrice = str_replace(array('RM',html_entity_decode($curr['geoplugin_currencySymbol'])),"",$curPrice);
									$findme   = 'RM';
									$pos = strpos($curPrice, $findme);
									if ($pos !== false) {
										$curr['geoplugin_currencyCode'] = "RM";			
									} 
									 $curPrice = $tmpCurPrice;
								?>
         </span> 
         <?php /*?>@currency((int)$vouch->originalPrice, Session::get('my.currency', Config::get('app.currency')))<?php */?>
         <?php
									$ip = $_SERVER['REMOTE_ADDR'];
									$curr =  unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
									echo Currency::format($vouch->originalPrice, $curr['geoplugin_currencyCode']);
								?>
         </div>
         <div class="counter">
         	Time left to purchase
               
                
                <div id="defaultCountdown"></div>
            
         </div>
         <div class="savingdiv">
         	<div class="bought">Bought <span>{{ DB::table('orders')->where('voucherID',$vouch->id)->count('voucherID') }}</span></div>
            <div class="save">Save up to <span>{{(int)$vouch->discountPercentage}}%</span></div>
         </div>
         <div class="buybox">
         					 @if(isset($userID) )
                                   		 @if($vouch->salesLimit > 0)
                                         
                                          <?php
                                            $ordId = DB::table('orders')->orderBy('id','desc')->take(1)->get();
											
											$invoiceID = "#".str_pad($ordId[0]->id+1, 6, "0", STR_PAD_LEFT);
                                            
											if($curr['geoplugin_currencyCode'] == 'SGD')
											{
											$MERCHANTID="377";
                                            $CHECKSUMADDON = "wQA6kXpKsjin";
											}else if($curr['geoplugin_currencyCode'] == 'THB')
											{
											$MERCHANTID="380";
                                            $CHECKSUMADDON = "fYuynbFCtRo2";
											}else if($curr['geoplugin_currencyCode'] == 'IDR')
											{
											$MERCHANTID="379";
                                            $CHECKSUMADDON = "KiJica8E15MV";
											}else if($curr['geoplugin_currencyCode'] == 'MYR')
											{
											$MERCHANTID="378";
                                            $CHECKSUMADDON = "LZ7Koth5UEXo";
											}else if($curr['geoplugin_currencyCode'] == 'USD')
											{
											$MERCHANTID="374";
                                            $CHECKSUMADDON = "xWGRjBwKj5tw";
											}else{
												$MERCHANTID="378";
                                           	 $CHECKSUMADDON = "LZ7Koth5UEXo";
												}
                                            
                                            
                                            $URL = 'http://demo2.2c2p.com/2c2pfrontend/Paymentv2/payment.aspx';
                                            $VERSION="5.0";
                                            $PRODUCTINFO= $vouch->voucherName; 
                                            $INVOICENO=$invoiceID;
                                            $REF1=$vouch->id;
                                            $REF2="Voucher";
                                            $REF3="";
                                            $AMOUNT= $curPrice;
                                            $PROMOTION="";
                                            $CUSTEMAIL="";
                                            $PAYCURRENCY="";
                                            $PAYCATEGORYID=""; 
											
                                            
                                            
                                            $toHash = $VERSION.$MERCHANTID.$PRODUCTINFO.$INVOICENO.$REF1.$REF2.$REF3.$AMOUNT.$PROMOTION.$CUSTEMAIL.$PAYCURRENCY.$PAYCATEGORYID.$CHECKSUMADDON;
                                            //HASH MD5
                                            $CHECKSUM=md5($toHash); 
                                            //ADD PADDING
                                            $maxPADDING = 40-strlen($CHECKSUM); 
                                            for($i=0;$i<$maxPADDING;$i++){ 
                                                $CHECKSUM = "X".$CHECKSUM;
                                            } 
                                            extract($_POST);
                                            
                                            //set POST variables
                                            $fields = array(
                                                            'VERSION'=>$VERSION,
                                                            'MERCHANTID'=>$MERCHANTID,
                                                            'PRODUCTINFO'=>$PRODUCTINFO,
                                                            'INVOICENO'=>$INVOICENO,
                                                            'REF1'=>$REF1,
                                                            'REF2'=>$REF2,
                                                            'REF3'=>$REF3,
                                                            'AMOUNT'=>$AMOUNT,
                                                            'PROMOTION'=>$PROMOTION,
                                                            'CUSTEMAIL'=>$CUSTEMAIL,
                                                            'PAYCURRENCY'=>$PAYCURRENCY,
                                                            'PAYCATEGORYID'=>$PAYCATEGORYID, 
                                                            'CHECKSUM'=>$CHECKSUM
                                                            ); 
                                            
                                            ?>
                                    
                                    
                                    <form name="requestForm" action="<?php echo $URL ?>" method="post">
										<?php
                                        foreach ($fields as $k => $w) {
                                            //making the HTML hidden field for post data
                                        ?>
                                            <input type="hidden" name="<?php echo $k; ?>" value="<?php echo $w ?>"/>
                                        <?php
                                        }
                                        ?>
                                        <input type="submit" name="submit" value="Purchase" class="gradient-button yellow">
                                        
                                        </form>
                                        
                                    	<?php /*?><a href="{{URL('/voucherPurchase/'.$vouch->id)}}"  title="Purchase">Purchase</a><?php */?>
									@else
                                     	<a  >Sold Out</a>
                                     @endif
                                    @else
                                    	<a href="{{URL('/signin/')}}">Purchase</a>
                                    @endif
         
         </div>
    </div>
    <div class="shadow_voucher"><img src="/static/pillo/images/voucher_shdw.png" /></div>
    @endforeach
</div>



 </div></div>
@stop