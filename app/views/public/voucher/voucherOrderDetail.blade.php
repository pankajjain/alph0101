@extends('templates.pillo')

@section('title')
Your Travel Partner
@stop

@section('pageAssets')
<!--<script src="/static/pillo/js/countdown.js"></script>-->
<link rel="stylesheet" href="/static/metronic/plugins/rateit/rateit.css">
<link rel="stylesheet" href="/static/metronic/css/jquery.countdown.css">
<script type="text/javascript" src="{{ URL::to('static/pillo/js/sequence.jquery-min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('static/pillo/js/sequence.js') }}"></script>
<script src="/static/metronic/plugins/rateit/jquery.rateit.min.js"> </script>
<script src="/static/metronic/scripts/jquery.plugin.js"></script>
<script src="/static/metronic/scripts/jquery.countdown.js"></script>



@stop
@section('content')
<div style="background: url('/static/pillo/images/bgr/body.jpg') repeat scroll 0 0 #E9E6E0;display: inline-block; min-height: 980px; padding: 0 0 75px;
    width: 100%;" role="main">		
	
   <?php //echo "<pre>"; print_r($vouchers); die;?>
		<div style=" padding: 160px 0 0; width: 100%;">
 

   <div  style="clear: both;margin: 0 auto;width: 900px;">
   @foreach($vouchers as $vouch)

   

     
    <div  style="background: none repeat scroll 0 0 #C7C7C7;border-radius: 0 0 4px 4px;box-shadow: 0 3px 4px #CCCCCC;clear: both;min-height: 212px; ">
        <div style="float:left; width:50%; border-right: medium double #888888;min-height:212px;"> 
         	<div style="font-size:16px;font-weight:bold;text-align:center">Hotel</div>
            <div style="font-size:12px;float:left;margin:10px 0 0 10px">NAme : {{$vouch->accomm->name}}</div>
           <div  class="stars" style="float:left;width:100%;margin:10px 0 0 10px">
                                   <span style=" float: left;"> Rating : </span>
										@for($i = 0; $i < $vouch->accomm->starRating; $i++)
										<img style="float:left" src="/static/pillo/images/ico/star.png" alt="">
										@endfor
									</div>
            <div style="float:left;font-size:12px;margin:10px 0 0 10px">
            Address : 
            @if(strlen($vouch->accomm->streetAddress) > 25)
                                 {{substr($vouch->accomm->streetAddress,0,25)}}...
                                 @else
                                  {{$vouch->accomm->streetAddress}}
                                 @endif<br />  {{$vouch->accomm->city}} , {{$vouch->accomm->country}}</div>
            
         </div>
         <div style="float:left; width:49%;min-height:212px;">
             <div style="font-size:16px;font-weight:bold;text-align:center">Voucher</div>
               <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:100%">Code : {{$orders->voucherCode}}</div>
               <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:100%">Name : {{$vouch->voucherName}}</div>
               <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:100%">Voucher Ammount : {{$vouch->originalPrice}}</div>
               <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:100%">Discount Percentage : {{(int)$vouch->discountPercentage}}</div>
               <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:100%">Discounted Price : {{$vouch->discountedPrice}}</div>
               <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:100%">Duration : {{$vouch->voucherDuration}} Days</div>
                 <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:100%">From : {{$vouch->redemptionPeriodFrom}} To {{$vouch->redemptionPeriodTo}}</div>
                
         	
         </div>
      </div> 
      
      <div  style="background: none repeat scroll 0 0 #C7C7C7;border-radius: 0 0 4px 4px;box-shadow: 0 3px 4px #CCCCCC;clear: both;min-height: 180px; ">
         <div style="float:left; width:50%;border-right: medium double #888888;min-height:180px;">
         	<div style="font-size:16px;font-weight:bold;text-align:center">Room</div>
           <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:100%">Name : {{$vouch->room->name}}</div>
           <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:100%">Bed : {{$vouch->room->bedType}}</div>
           <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:100%">Size : {{$vouch->room->roomSize}} Sqft.</div>
        
        
         </div>
         
         <div style="float:left; width:49%;min-height:180px;">
         	<div style="font-size:16px;font-weight:bold;text-align:center">Codes</div>
            <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:60%">QRCODE : 
           <?php
 $qrsize = '150x150';
             $qrstring = urlencode($orders->voucherCode.",".$user->firstName." ".$user->lastName.",".$vouch->voucherName.",".(int)$vouch->discountPercentage.",".$vouch->redemptionPeriodFrom.",".$vouch->redemptionPeriodTo);
             $qrencoding = "UTF-8";
			 ?>
      <img src="https://chart.googleapis.com/chart?chs=<?php echo $qrsize; ?>&cht=qr&chl=<?php echo $qrstring; ?>&choe=<?php echo $qrencoding; ?>" width="100" />
    </div>
        
            
                <div style="font-size:12px;float:left;text-align:left;margin: 117px 0 0 10px;width:25%">
                
                <a class="gradient-button yellow" title="Book now" onclick="window.print()" >Print</a>
            </div>
        
        
        
        </div>
       </div> 
    
    </div>
    @endforeach
     
</div>


 </div></div>
@stop