@extends('templates.metronicFull')

@section('title')
Change Password
@stop

@section('bodyClass')
class="login"
@stop

@section('content')
<a href="{{URL::Route('publicIndex')}}" class="btn default btn-lg"><i class="icon-mail-reply"></i> Back to Home</a>
@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
@if(Session::has('errormsg'))
<div class="alert alert-error"> {{Session::get('errormsg')}}</div>
@endif
<form class="form-vertical changePassword-form" action="{{URL::route('postChangepassword')}}" method="post">
	<h3 class="form-title">Change Your Password</h3>
	<div class="alert alert-error hide">
		<button class="close" data-dismiss="alert"></button>
		<span>Fill up information to change password.</span>
	</div>
	<div class="control-group">
		<label class="control-label visible-ie8 visible-ie9">New Password</label>
		<div class="controls">
			<div class="input-icon left">
				<i class="icon-lock"></i>
				{{Form::password('newpassword',array('class' => 'm-wrap placeholder-no-fix','autocomplete' => 'off' , 'placeholder' => 'New password'))}}
			</div>
			@if($errors->has('newpassword'))
			<div class="formErrors">
				<ul>
					@foreach($errors->get('newpassword') as $message)
					<li>
						{{$message}}
					</li>
					@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>
	<div class="control-group">
		<label class="control-label visible-ie8 visible-ie9">Confirm New Password</label>
		<div class="controls">
			<div class="input-icon left">
				<i class="icon-key"></i>
				{{Form::password('cfmnewpassword',array('class' => 'm-wrap placeholder-no-fix','autocomplete' => 'off' , 'placeholder' => 'Confirm new password'))}}
			</div>
			@if($errors->has('cfmnewpassword'))
			<div class="formErrors">
				<ul>
					@foreach($errors->get('cfmnewpassword') as $message)
					<li>
						{{$message}}
					</li>
					@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>
	@if($errors->has('SentryError'))
	<div class="formErrors">
		<ul>
			@foreach($errors->get('SentryError') as $message)
			<li>
				{{$message}}
			</li>
			@endforeach
		</ul>
	</div>
	@endif
	<div class="form-actions">
		<button type="submit" class="btn blue pull-right">
			Secured Password Update <i class="m-icon-swapright m-icon-white"></i>
		</button>           
	</div>
</form>
@stop