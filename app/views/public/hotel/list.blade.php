@extends('templates.pillo')

@section('title')
Your Travel Partner
@stop

@section('pageAssets')

<script type="text/javascript" src="{{ URL::to('static/pillo/js/css3-mediaqueries.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('static/pillo/js/jquery.uniform.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('static/pillo/js/jquery.raty.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('static/pillo/js/jquery.prettyPhoto.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('static/pillo/js/selectnav.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('static/pillo/js/scripts.js') }}"></script>

<script type="text/javascript">

$(window).load(function () {
	var maxHeight = 0;

	$(".three-fourth .one-fourth").each(function(){
		if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
	});
	$(".three-fourth .one-fourth").height(maxHeight);	
});	

</script>
@stop
<?php //echo "<pre>"; print_r($accomms); die;?>
@section('content')	
	<div class="wrap clearfix">
		<!--main content-->
		<section class="full" style="margin-top:140px;">
			<h1>Most popular Hotels</h1>
			<div id="hotelsContainer" class="deals clearfix">
				@foreach($accomms as $entry)
				<article class="one-fourth">
					<!-- <div class="ribbon-small">- {{rand(30,89)}}%</div> -->
                    <?php //echo $entry->photo->first()->fileName; die; ?>
					<figure>
						@if(!empty($entry->photo->first()->fileName))
                       
											
							 @if(HTML::image("https://rqphoto.s3-ap-southeast-1.amazonaws.com/268_$entry->photo->first()->fileName"))
                                <img alt width="270" height="152" src="https://rqphoto.s3-ap-southeast-1.amazonaws.com/268_{{ $entry->photo->first()->fileName }}"/>
                            @else
                              <img alt width="270" height="152" src="http://dummyimage.com/270x152/d6d2d6/000&text=No+Image"/>
                            @endif
						@else
							<img alt width="270" height="152" src="http://dummyimage.com/270x152/d6d2d6/000&text=No+Image"/>
						@endif
					</figure>
					<div class="details">
						<div class="maininfo">
							<h1>
                            @if(strlen($entry->name) > 20)
                            	{{substr($entry->name,0,20)}}...
                            @else
                            	{{$entry->name}}
                            @endif
                            </h1>
                            <span class="starOuter">
							<span class="stars">
								@for($i = 0; $i < $entry->starRating; $i++)
								<img src="/static/pillo/images/ico/star.png" alt="">
								@endfor

							    <span class="rating">{{$entry->starRating}}</span>
							</span>
                            </span>
							<span class="address">{{$entry->city}}, {{$entry->province}}</span>
						</div>
						<span class="price" style="text-align: justify;min-height:110px">
                        @if($entry->hotelDescription!=NULL)
                            @if(strlen($entry->hotelDescription) > 150)
                                {{substr($entry->hotelDescription,0,150)}} ...
                             @else
                             	{{$entry->hotelDescription}}
                             @endif 
                        @else 
                        	Overlooking the Aqueduct and Nature Park, Lorem Ipsum Hotel is situated 5 minutes’ walk from London’s Zoo logical Gardens and a metro station. Click for more info. 
                        @endif</span>
						<a href="{{URL('/hotel/'.$entry->id)}}" title="Book now" class="gradient-button yellow">Browse</a>
					</div>
				</article>
				@endforeach

			</div>
			{{ $accomms->links() }}
		</section>
	</div>
	

@stop

@section('inlineJS')
@stop