@extends('templates.pillo')

@section('title')
Your Travel Partner
@stop

@section('pageAssets')
<script type="text/javascript" src="{{ URL::to('static/pillo/js/css3-mediaqueries.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('static/pillo/js/sequence.jquery-min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('static/pillo/js/jquery.uniform.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('static/pillo/js/jquery.prettyPhoto.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('static/pillo/js/sequence.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('static/pillo/js/selectnav.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('static/pillo/js/scripts.js') }}"></script>
<script src="/static/pillo/js/countdown.js"></script>
<script type="text/javascript">	
		$(document).ready(function(){
			$(".form").hide();
			$(".form:first").show();
			$(".f-item:first").addClass("active");
			$(".f-item:first span").addClass("checked");
		});
</script>

<script type="text/javascript">
$(function(){
$(".search").keyup(function() 
{ 
var searchid = $(this).val();
var dataString = 'search='+ searchid;
if(searchid!='')
{
	$.ajax({
	type: "POST",
	url: "{{URL('/homepageSearchBox')}}",
	data: dataString,
	cache: false,
	success: function(html)
	{
		//alert(html);
	$("#result").html(html).show();
	}
	});
}return false;    
});

jQuery("#result").live("click",function(e){ 
	var $clicked = $(e.target);
		//alert($clicked);
	var $name = $clicked.find('.name').html();
	
	var decoded = $("<div/>").html($name).text();
	//alert(decoded);
	$('#searchid').val(decoded);
});
jQuery(document).live("click", function(e) { 
	var $clicked = $(e.target);
	if (! $clicked.hasClass("search")){
	jQuery("#result").fadeOut(); 
	}
});
$('#searchid').click(function(){
	jQuery("#result").fadeIn();
});
});
</script>

<style>

#searchid
	{
		width:500px;
		border:solid 1px #000;
		padding:10px;
		font-size:14px;
		margin-left:195px;
	}
	#result
	{
		position:absolute;
		width:500px;
		padding:10px;
		display:none;
		margin-left: 195px;
    	margin-top: 63px;
		border-top:0px;
		overflow:hidden;
		border:1px #CCC solid;
		background-color: white;
		z-index: 9999;
	}
	.show
	{
		padding:10px; 
		border-bottom:1px #999 dashed;
		font-size:15px; 
		height:15px;
	}
	.show:hover
	{
		background:#4c66a4;
		color:#FFF;
		cursor:pointer;
	}
	.rightSpan{
		float:right;
		margin-top:-15px
		}
</style>

@stop

@section('content')
<section class="slider clearfix">
	<div id="sequence">
		<ul>
			<li>
				<div class="info animate-in">
					<h2>Last minute Winter escapes</h2><br />
					<p>January 2014 holidays 40% off! An unique opportunity to realize your dreams</p>
				</div>
				<img class="main-image animate-in" src="/static/pillo/images/slider/1.jpg" alt="" />
			</li>
			<li>
				<div class="info animate-in">
					<h2>Check out our top weekly deals</h2><br />
					<p>Save Now. Book Later.</p>
				</div>
				<img class="main-image animate-in" src="/static/pillo/images/slider/2.jpg" alt="" />
			</li>
			<li>
				<div class="info animate-in">
					<h2>Check out last minute flight, hotel &amp; vacation offers!</h2><br />
					<p>Save up to 50%!</p>
				</div>
				<img class="main-image animate-in" src="/static/pillo/images/slider/6.jpg" alt="" />
			</li>
		</ul>
	</div>
</section>
<!--//slider-->

<!--search-->

 @if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
 @if(Session::has('errorMsg'))
<div class="alert alert-success" style="color:#F00"> {{Session::get('errorMsg')}}</div>
@endif

<div class="main-search">
	<form id="main-search" method="post" action="{{URL::route('sampleSearch')}}">

		<div class="forms">
			<!--form hotel-->
			<div class="form" id="form1">
				<!--column-->
				<div class="column where">
					<h4 style="">Where?</h4>
                    
					<div class="f-item">

						<label for="destination1"></label>
						<!--<input type="text" placeholder="City, region, district or specific hotel" id="destination1" name="destination" />-->
                        
                      
                           <input type="text" class="search" name="searchText" id="searchid"  placeholder="Search for people" />
<div id="result"></div>
                            
                            
					</div>
				</div>

			</div>	
			<!--//form hotel-->
		</div>
		
		<input type="submit" value="Proceed to results" class="search-submit" id="search-submit" />
	</form>
</div>
<!--//search-->
<?php /*?>@if(Session::has('message'))
	<div class="alert alert-info"> {{Session::get('message')}}</div>
@endif<?php */?>
<div style="min-height:150px;background-color:white;margin-top:45px;
padding:60px;background-image:url('/static/pillo/images/clock.jpg');
background-repeat:no-repeat;
background-position:right center;
background-size:25%; ">

<h2>FLASH DEALS STARTING IN</h2>
<h4 id="holder"></h4>
</div>
<!--main-->
<div class="main" role="main">		
	<div class="wrap clearfix">
		<!--deals-->
		<section class="full">
			<h1>Most Popular Vouchers</h1>
			<div class="deals clearfix">
           
                                    @if (isset($flashdeal))
                                        @foreach($flashdeal as $v)
                            <article class="one-fourth promo">
                            <div class="ribbon-small">-{{(int)$v->discountPercentage}}%</div>
                             <?php //echo "<pre>"; print_r($v->room->photos);die;?>
                                 @if(is_object($v->room->photos->first()))
                                <figure><a href="{{URL('/flashdeal/'.$v->id)}}" title="">
                                
                                    @if(HTML::image("https://rqphoto.s3-ap-southeast-1.amazonaws.com/$v->room->photos->first()->fileName"))
                                        <img 
                                        src="https://rqphoto.s3-ap-southeast-1.amazonaws.com/{{$v->room->photos->first()->fileName}}" 
                                        alt="" width="270" height="152" />
                                     @else
                                     <img 
                                        src="http://dummyimage.com/270x152/d6d2d6/000&text=No+Image" 
                                        alt="" width="270" height="152" />
                                     @endif
                                 </a></figure>
                             @else
                                    <figure><a href="{{URL('/flashdeal/'.$v->id)}}" title="">
                                    <img src="http://dummyimage.com/270x152/d6d2d6/000&text=No+Image" 
                                        alt="" width="270" height="152" />
                                        
                                        </a></figure>
                             @endif
                                    <div class="details">
                                        <h1> {{$v->flashDealName}}
        
                                            <span class="stars">
                                                @for($i = 0; $i < $v->accomm->starRating; $i++)
                                                <img src="/static/pillo/images/ico/star.png" alt="">
                                                @endfor
                                            </span>
                                        </h1>
                                        <?php //echo $v->room->photos->first()->fileName;?>
                                        <span class="address">{{$v->accomm->streetAddress}}<br />  {{$v->accomm->city}} , {{$v->accomm->country}}</span>
                                        <span class="rating"> 9 /10</span>
                                        <span class="price">Lowest Price <em><?php /*?>RM {{$v->discountedPrice}}<?php */?>
                                        @currency((int)$v->discountedPrice, Session::get('my.currency', Config::get('app.currency')))
                                        </em> </span>
                                        <div class="description">
                                            <p><a href="{{URL('/voucher/'.$v->id)}}">More info</a></p>
                                        </div>
                                        @if(isset($userID) )
                                            @if($v->salesLimit > 0)
                                               <a href="{{URL('/purchase/'.$v->id)}}" title="Book now" class="gradient-button yellow">Purchase</a>
                                             @else
                                                <a  title="Book now" class="gradient-button yellow">Sold Out</a>
                                             @endif
                                        @else
                                        <a href="{{URL('/signin/')}}" title="Book now" class="gradient-button yellow">Purchase</a>
                                        @endif
                                    </div>
                                </article>
                                        @endforeach
                                    @endif
                                    
					@foreach($voucher as $v)
                  <?php //echo "<pre>"; print_r($v->accomm); ?>
					<article class="one-fourth promo">
					<div class="ribbon-small">-{{(int)$v->discountPercentage}}%</div>
						 @if(is_object($v->room->photos->first()))
						<figure><a href="{{URL('/voucher/'.$v->id)}}" title="">
							
                            @if(HTML::image("https://rqphoto.s3-ap-southeast-1.amazonaws.com/$v->room->photos->first()->fileName"))
                            <img 
							src="https://rqphoto.s3-ap-southeast-1.amazonaws.com/{{$v->room->photos->first()->fileName}}" 
							alt="" width="270" height="152" />
                            @else
                            	 <img src="http://dummyimage.com/270x152/d6d2d6/000&text=No+Image" alt="" width="270" height="152" />
                            @endif
                            
                            </a></figure>
                     @else
                     		<figure><a href="{{URL('/voucher/'.$v->id)}}" title="">
							 <img src="http://dummyimage.com/270x152/d6d2d6/000&text=No+Image" alt="" width="270" height="152" />
                             </a></figure>
                     @endif
                     
							<div class="details">
                            
								<h1>
                                @if(strlen($v->voucherName) > 20)
                                 {{substr($v->voucherName,0,20)}}...
                                 @else
                                  {{$v->voucherName}}
                                 @endif
								<br />
                                <span class="starOuter">
									<span class="stars">
										@for($i = 0; $i < $v->accomm->starRating; $i++)
										<img src="/static/pillo/images/ico/star.png" alt="">
										@endfor
									</span>
                                 </span>
								</h1>
                                <?php //echo $v->room->photos->first()->fileName;?>
								<span class="address"> @if(strlen($v->accomm->streetAddress) > 25)
                                 {{substr($v->accomm->streetAddress,0,25)}}...
                                 @else
                                  {{$v->accomm->streetAddress}}
                                 @endif<br />  {{$v->accomm->city}} , {{$v->accomm->country}}</span>
								<span class="rating"> 9 /10</span>
								<span class="price">Lowest Price <em><?php /*?>
                                @currency((int)$v->discountedPrice, Session::get('my.currency', Config::get('app.currency')))<?php */?>
                                <?php
									//$ip = '27.111.208.0';
									$ip = $_SERVER['REMOTE_ADDR'];
									$curr =  unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
									echo $curPrice = Currency::format($v->discountedPrice, $curr['geoplugin_currencyCode']);
									
									
									$tmpCurPrice = str_replace(array('RM',html_entity_decode($curr['geoplugin_currencySymbol'])),"",$curPrice);
									$findme   = 'RM';
									$pos = strpos($curPrice, $findme);
									if ($pos !== false) {
										$curr['geoplugin_currencyCode'] = "RM";			
									} 
									 $curPrice = $tmpCurPrice;
			
									
								?>
                                
                                </em> </span>
                               	<div class="description">
									<p><a href="{{URL('/voucher/'.$v->id)}}">More info</a></p>
								</div>
                                @if(isset($userID) )
                                    @if($v->salesLimit > 0)
                                    
											 <?php
											 
											 
											 
                                            $ordId = DB::table('orders')->orderBy('id','desc')->take(1)->get();
											$invoiceID = "#".str_pad($ordId[0]->id+1, 6, "0", STR_PAD_LEFT);
                                            
											if($curr['geoplugin_currencyCode'] == 'SGD')
											{
											$MERCHANTID="377";
                                            $CHECKSUMADDON = "wQA6kXpKsjin";
											}else if($curr['geoplugin_currencyCode'] == 'THB')
											{
											$MERCHANTID="380";
                                            $CHECKSUMADDON = "fYuynbFCtRo2";
											}else if($curr['geoplugin_currencyCode'] == 'IDR')
											{
											$MERCHANTID="379";
                                            $CHECKSUMADDON = "KiJica8E15MV";
											}else if($curr['geoplugin_currencyCode'] == 'MYR')
											{
											$MERCHANTID="378";
                                            $CHECKSUMADDON = "LZ7Koth5UEXo";
											}else if($curr['geoplugin_currencyCode'] == 'USD')
											{
											$MERCHANTID="374";
                                            $CHECKSUMADDON = "xWGRjBwKj5tw";
											}else{
												$MERCHANTID="378";
                                           	 $CHECKSUMADDON = "LZ7Koth5UEXo";
												}
                                            
                                            
                                            $URL = 'http://demo2.2c2p.com/2c2pfrontend/Paymentv2/payment.aspx';
                                            $VERSION="5.0";
                                            $PRODUCTINFO= $v->voucherName; 
                                            $INVOICENO=$invoiceID;
                                            $REF1=$v->id;
                                            $REF2="Voucher";
                                            $REF3="";
                                            $AMOUNT= $curPrice;
                                            $PROMOTION="";
                                            $CUSTEMAIL="";
                                            $PAYCURRENCY="";
                                            $PAYCATEGORYID=""; 
											
                                            
                                            
                                            $toHash = $VERSION.$MERCHANTID.$PRODUCTINFO.$INVOICENO.$REF1.$REF2.$REF3.$AMOUNT.$PROMOTION.$CUSTEMAIL.$PAYCURRENCY.$PAYCATEGORYID.$CHECKSUMADDON;
                                            //HASH MD5
                                            $CHECKSUM=md5($toHash); 
                                            //ADD PADDING
                                            $maxPADDING = 40-strlen($CHECKSUM); 
                                            for($i=0;$i<$maxPADDING;$i++){ 
                                                $CHECKSUM = "X".$CHECKSUM;
                                            } 
                                            extract($_POST);
                                            
                                            //set POST variables
                                            $fields = array(
                                                            'VERSION'=>$VERSION,
                                                            'MERCHANTID'=>$MERCHANTID,
                                                            'PRODUCTINFO'=>$PRODUCTINFO,
                                                            'INVOICENO'=>$INVOICENO,
                                                            'REF1'=>$REF1,
                                                            'REF2'=>$REF2,
                                                            'REF3'=>$REF3,
                                                            'AMOUNT'=>$AMOUNT,
                                                            'PROMOTION'=>$PROMOTION,
                                                            'CUSTEMAIL'=>$CUSTEMAIL,
                                                            'PAYCURRENCY'=>$PAYCURRENCY,
                                                            'PAYCATEGORYID'=>$PAYCATEGORYID, 
                                                            'CHECKSUM'=>$CHECKSUM
                                                            ); 
                                            
                                            ?>
                                    
                                    
                                    <form name="requestForm" action="<?php echo $URL ?>" method="post">
										<?php
                                        foreach ($fields as $k => $w) {
                                            //making the HTML hidden field for post data
                                        ?>
                                            <input type="hidden" name="<?php echo $k; ?>" value="<?php echo $w ?>"/>
                                        <?php
                                        }
                                        ?>
                                        <input type="submit" name="submit" value="Purchase" class="gradient-button yellow">
                                        
                                        </form>
                                    
                                       <?php /*?><a href="{{URL('/voucherPurchase/'.$v->id)}}" title="Book now" class="gradient-button yellow">Purchase</a><?php */?>
                                     @else
                                     	<a  title="Book now" class="gradient-button yellow">Sold Out</a>
                                     @endif
                                @else
                                <a href="{{URL('/signin/')}}" title="Book now" class="gradient-button yellow">Purchase</a>
                                @endif
							</div>
						</article>
                                        @endforeach					
					</div>
				</section>	
				<!--//deals-->
				<hr/>

				<!--info boxes-->
				<?php /*?><section class="boxes clearfix">
					<!--column-->
					<h1>Why Pillo Pillo?</h1>
					<article class="one-fourth">
						<h2>Handpicked Hotels</h2>
						<p>All Room Quickly Hotels fulfil strict selection criteria. Each hotel is chosen individually and inclusion cannot be bought. </p>
					</article>
					<!--//column-->

					<!--column-->
					<article class="one-fourth">
						<h2>Detailed Descriptions</h2>
						<p>To give you an accurate impression of the hotel, we endeavor to publish transparent, balanced and precise hotel descriptions. </p>
					</article>
					<!--//column-->

					<!--column-->
					<article class="one-fourth">
						<h2>Exclusive Knowledge</h2>
						<p>We’ve done our research. Our scouts are always busy finding out more about our hotels, the surroundings and activities on offer nearby.</p>
					</article>
					<!--//column-->

					<!--column-->
					<article class="one-fourth last">
						<h2>Passionate Service</h2>
						<p>Room Quicklys’s team will cater to your special requests. We offer expert and passionate advice for finding the right hotel. </p>
					</article>
					<!--//column-->

					<!--column-->
					<article class="one-fourth">
						<h2>Best Price Guarantee</h2>
						<p>We offer the best hotels at the best prices. If you find the same room category on the same dates cheaper elsewhere, we will refund the difference. Guaranteed, and quickly. </p>
					</article>
					<!--//column-->

					<!--column-->
					<article class="one-fourth">
						<h2>Secure Booking</h2>
						<p>Room Quickly reservation system is secure and your credit card and personal information is encrypted.<br />We work to high standards and guarantee your privacy. </p>
					</article>
					<!--//column-->

					<!--column-->
					<article class="one-fourth">
						<h2>Benefits for Hoteliers</h2>
						<p>We provide a cost-effective model, a network of over 5000 partners and a personalised account management service to help you optimise your revenue.</p>
					</article>
					<!--//column-->

					<!--column-->
					<article class="one-fourth last">
						<h2>Any Questions?</h2>
						<p>Call us on <em>1-555-555-555</em> for individual, tailored advice for your perfect stay or <a href="contact.html" title="Contact">send us a message</a> with your hotel booking query.<br /><br /></p>
					</article>
					<!--//column-->
				</section><?php */?>
				<!--//info boxes-->
			</div>
		</div>
		@stop

		@section('inlineJS')
		<script>
			selectnav();
			$(document).ready(function(){

				var timerId =
				countdown(
					new Date('31Dec2014'),
					function(ts) {
						console.log(ts);
						document.getElementById('holder').innerHTML = ts.toHTML("strong");				
					},
					countdown.DAYS|countdown.HOURS|countdown.MINUTES|countdown.SECONDS);
			});
		</script>
		@stop
