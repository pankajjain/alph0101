@extends('templates.metronicFull')

@section('title')
Access Manager
@stop

@section('bodyClass')
class="login"
@stop

@section('content')
@include('public.partials.termsAndConditions')
@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
@if(Session::has('errormsg'))
<div class="alert alert-error"> {{Session::get('errormsg')}}</div>
@endif
<!-- BEGIN LOGIN FORM -->
<form class="form-vertical login-form" action="{{URL::route('postSignin')}}" method="post">
	<h3 class="form-title">Login to your account</h3>
	<div class="alert alert-error hide">
		<button class="close" data-dismiss="alert"></button>
		<span>Enter your email and password.</span>
	</div>
	<div class="control-group">
		<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
		<label class="control-label visible-ie8 visible-ie9">Email</label>
		<div class="controls">
			<div class="input-icon left">
				<i class="icon-envelope"></i>
				{{Form::text('email',null,array('class' => 'm-wrap placeholder-no-fix','autocomplete' => 'off' , 'placeholder' => 'Email'))}}
				
			</div>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label visible-ie8 visible-ie9">Password</label>
		<div class="controls">
			<div class="input-icon left">
				<i class="icon-lock"></i>

				{{Form::password('password',array('class' => 'm-wrap placeholder-no-fix','autocomplete' => 'off' , 'placeholder' => 'Password'))}}

			</div>
		</div>
	</div>
	@if($errors->has('SentryError'))
	<div class="formErrors">
		<ul>
			@foreach($errors->get('SentryError') as $message)
			<li>
				{{$message}}
			</li>
			@endforeach
		</ul>
	</div>
	@endif
	<div class="form-actions">
		<label class="checkbox">
			<input type="checkbox" name="remember" value="1"/> Remember me
		</label>
		<button type="submit" class="btn blue pull-right">
			Login <i class="m-icon-swapright m-icon-white"></i>
		</button>            
	</div>

	<div class="create-account">
		<p>
			Don't have an account yet ?&nbsp; 
			<a href="javascript:;" id="register-btn" >Create an account</a> <br>
			Forgot your password ?&nbsp; 
			<a href="javascript:;" id="forget-password" >Reset your credential</a> <br>
			Not activated ?&nbsp; 
			<a href="javascript:;" id="resent-form">Resent verification email</a>
		</p>
	</div>
</form>
<!-- END LOGIN FORM --> 

<!-- BEGIN FORGOT PASSWORD FORM -->
<form class="form-vertical forget-form" action="{{URL::route('postResetCredential')}}" method="post">
	<h3 >Forget Password ?</h3>
	<p>Please enter your e-mail address.</p>
	<div class="control-group">
		<div class="controls">
			<div class="input-icon left">
				<i class="icon-envelope"></i>
				{{Form::text('email',null,array('class' => 'm-wrap placeholder-no-fix','autocomplete' => 'off' , 'placeholder' => 'Email'))}}
				
			</div>
		</div>
	</div>
	<div class="form-actions">
		<button type="button" id="back-btn" class="btn">
			<i class="m-icon-swapleft"></i> Back
		</button>
		<button type="submit" class="btn blue pull-right">
			Submit <i class="m-icon-swapright m-icon-white"></i>
		</button>            
	</div>
</form>
<!-- END FORGOT PASSWORD FORM -->

<!-- BEGIN RESENT VERIFICATION FORM -->
<form class="form-vertical resent-verification hide" action="{{URL::route('postResentValidation')}}" method="post">
	<h3 >Resent Verification Email</h3>
	<p>Please enter your e-mail address.</p>
	<div class="control-group">
		<div class="controls">
			<div class="input-icon left">
				<i class="icon-envelope"></i>
				{{Form::text('email',null,array('class' => 'm-wrap placeholder-no-fix','autocomplete' => 'off' , 'placeholder' => 'Email'))}}
				
			</div>
		</div>
	</div>
	<div class="form-actions">
		<button type="button" id="resent-back-btn" class="btn">
			<i class="m-icon-swapleft"></i> Back
		</button>
		<button type="submit" class="btn blue pull-right">
			Submit <i class="m-icon-swapright m-icon-white"></i>
		</button>            
	</div>
</form>
<!-- END RESENT PASSWORD FORM -->

<!-- BEGIN REGISTRATION FORM -->
<form class="form-vertical register-form" action="{{URL::route('postSignup')}}" method="post">
	<input type="hidden" value="{{Crypt::encrypt($userType)}}" name="usertype">
	<h3 >Sign Up</h3>
	<p>Enter your personal details below:</p>
	<div class="control-group @if($errors->has('firstName')) error @endif" >
		<label class="control-label visible-ie8 visible-ie9">First Name</label>
		<div class="controls">
			<div class="input-icon left">
				<i class="icon-font"></i>
				
				{{Form::text('firstName',null,array('class' => 'm-wrap placeholder-no-fix','autocomplete' => 'off' ,'required', 'placeholder' => 'First Name'))}}
			</div>
			@if($errors->has('firstName'))			
			@foreach($errors->get('firstName') as $message)
			<div class="help-inline">
				{{$message}}
			</div>
			@endforeach
			@endif
		</div>
	</div>
	<div class="control-group @if($errors->has('lastName')) error @endif">
		<label class="control-label visible-ie8 visible-ie9">Last Name</label>
		<div class="controls">
			<div class="input-icon left">
				<i class="icon-font"></i>				
				{{Form::text('lastName',null,array('class' => 'm-wrap placeholder-no-fix','autocomplete' => 'off' , 'placeholder' => 'Last Name'))}}

			</div>
			@if($errors->has('lastName'))			
			@foreach($errors->get('lastName') as $message)
			<div class="help-inline">
				{{$message}}
			</div>
			@endforeach
			@endif
		</div>
	</div>
	<div class="control-group @if($errors->has('email')) error @endif">
		<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
		<label class="control-label visible-ie8 visible-ie9">Email</label>
		<div class="controls">
			<div class="input-icon left">
				<i class="icon-envelope"></i>
				
				{{Form::text('email',null,array('class' => 'm-wrap placeholder-no-fix','autocomplete' => 'off' , 'placeholder' => 'Email'))}}

			</div>
			@if($errors->has('email'))			
			@foreach($errors->get('email') as $message)
			<div class="help-inline">
				{{$message}}
			</div>
			@endforeach
			@endif
		</div>
	</div>
	<div class="control-group @if($errors->has('password')) error @endif">
		<label class="control-label visible-ie8 visible-ie9">Password</label>
		<div class="controls">
			<div class="input-icon left">
				<i class="icon-lock"></i>
				{{Form::password('password',array('class' => 'm-wrap placeholder-no-fix','autocomplete' => 'off' ,'id' => 'register_password' ,'placeholder' => 'Password'))}}
				
			</div>
			@if($errors->has('password'))			
			@foreach($errors->get('password') as $message)
			<div class="help-inline">
				{{$message}}
			</div>
			@endforeach
			@endif
		</div>
	</div>
	<div class="control-group @if($errors->has('rpassword')) error @endif">
		<label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
		<div class="controls">
			<div class="input-icon left">
				<i class="icon-ok"></i>

				{{Form::password('rpassword',array('class' => 'm-wrap placeholder-no-fix','autocomplete' => 'off', 'placeholder' => 'Re-type Your Password'))}}				
			</div>
		</div>
	</div>
	<div class="control-group">
		<div class="controls">
			<label class="checkbox">
				<input type="checkbox" name="tnc"/> I agree to the <a href="#TaCModal" data-toggle="modal">Terms of Service</a> and <a href="#">Privacy Policy</a>
			</label>  
			<div id="register_tnc_error"></div>
		</div>
	</div>
	<div class="form-actions">
		<button id="register-back-btn" type="button" class="btn">
			<i class="m-icon-swapleft"></i> Back to Login</button>
			<button type="submit" id="register-submit-btn" class="btn green pull-right">
				Sign Up <i class="m-icon-swapright m-icon-white"></i>
			</button>            
		</div>
	</form>

	@stop

	@section('inlineJS')
	<script>
		$(document).ready(function(){
			
			@if(!isset($signIn)) 
			$('#register-btn').trigger('click');
			@endif ;
		});
	</script>
	@stop