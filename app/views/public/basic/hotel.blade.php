@extends('templates.pillo')

@section('title')
Hotel
@stop

@section('bodyyield')
@stop

@section('thescripts')
<script type="text/javascript" src="{{ URL::to('static/pillo/js/sequence.jquery-min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('static/pillo/js/sequence.js') }}"></script>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
$(document).ready(function(){
	initialize();
	});
	function initialize() {
		
		var secheltLoc = new google.maps.LatLng({{$accomm->lat}},{{$accomm->lng}});

		var myMapOptions = {
			 zoom: 15
			,center: secheltLoc
			,mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var theMap = new google.maps.Map(document.getElementById("map_canvas"), myMapOptions);


		var marker = new google.maps.Marker({
			map: theMap,
			draggable: true,
			position: new google.maps.LatLng({{$accomm->lat}},{{$accomm->lng}}),
			visible: true
		});

		var boxText = document.createElement("div");
		boxText.innerHTML = "<strong>Book Your Travel</strong><br>1400 Pennsylvania Ave,<br>Washington DC";

		var myOptions = {
			 content: boxText
			,disableAutoPan: false
			,maxWidth: 0
			,pixelOffset: new google.maps.Size(-140, 0)
			,zIndex: null
			,closeBoxURL: ""
			,infoBoxClearance: new google.maps.Size(1, 1)
			,isHidden: false
			,pane: "floatPane"
			,enableEventPropagation: false
		};

		google.maps.event.addListener(marker, "click", function (e) {
			ib.open(theMap, this);
		});

		var ib = new InfoBox(myOptions);
		ib.open(theMap, marker);
	}
</script>
@stop

@section('content')
<?php //echo "<pre>"; print_r($review); die;?>
  
<div class="main" role="main">		
	<div class="wrap clearfix">
		<!--main content-->
		<div class="content clearfix">

			<!--hotel three-fourth content-->
			<section class="three-fourth">
				<!--gallery-->
				<!--<section class="gallery" id="">-->
                
                
                
<section class="slider clearfix" style="margin-top: 0px;">
	<div id="sequence" class="innerSlider">
		<ul>
     
         @if(count($accomm->photo) > 0)
                 
			 @foreach ($accomm->photo as $photo)
       		<li>
            @if(HTML::image("https://rqphoto.s3-ap-southeast-1.amazonaws.com/$photo->fileName"))
            <img class="main-image animate-in" src="https://rqphoto.s3-ap-southeast-1.amazonaws.com/{{$photo->fileName}}" alt="" width="850" height="531" /></li>
            @else
            <img src="http://dummyimage.com/850x531/d6d2d6/000&text=No+Image" alt="" width="850" height="531"/>
            @endif
        @endforeach
        @else
        	 <img src="http://dummyimage.com/850x531/d6d2d6/000&text=No+Image" alt="" width="850" height="531"/>
        @endif
        
        <?php /*?>@foreach ($accomm->photo as $photo)
       		<li><img class="main-image animate-in" src="https://rqphoto.s3-ap-southeast-1.amazonaws.com/{{$photo->fileName}}" alt="" width="850" height="531" /></li>
        @endforeach<?php */?>
			
		</ul>
	</div>
</section>
             
   @if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
 @if(Session::has('errorMsg'))
<div class="alert alert-success" style="color:#F00"> {{Session::get('errorMsg')}}</div>
@endif


                
                
				<!--//gallery-->
			
				<!--inner navigation-->
				<nav class="inner-nav">
					<ul>
						<li class="availability"><a href="#availability" title="Availability">Room Vouchers</a></li>
						<li class="description"><a href="#description" title="Description">Description</a></li>
						<li class="facilities"><a href="#facilities" title="Facilities">Facilities</a></li>
						<li class="location"><a href="#location" title="Location">Location</a></li>
						<!-- <li class="reviews"><a href="#reviews" title="Reviews">Reviews</a></li>
						<li class="things-to-do"><a href="#things-to-do" title="Things to do">Things to do</a></li> -->
					</ul>
				</nav>
				<!--//inner navigation-->
				
				<!--availability-->
				<section id="availability" class="tab-content">
					<article>
						<!-- <h1>Availability</h1>
						<div class="text-wrap">
							<a href="#" class="gradient-button right" title="Change dates">Change dates</a>
							<p>Available rooms from <span class="date">Thurs 29 Nov 2012</span> to <span class="date">Fri 30 Nov 2012</span>.</p>
						</div> -->
						
						<h1>Room Vouchers</h1>
						<ul class="room-types">
							<!--room-->
                   <!-- <li>
								<figure class="left"><img src="/static/pillo/images/slider/img.jpg" alt="" width="270" height="152" /><a href="/static/pillo/images/slider/img2.jpg" class="image-overlay" rel="prettyPhoto[gallery1]"></a></figure>
								<div class="meta">
									<h2>Superior Double Room</h2>
									<p>Prices are per room<br />20 % VAT Included in price</p>
									<p>Non-refundable<br />Full English breakfast $ 24.80 </p>
									<a href="javascript:void(0)" title="more info" class="more-info">+ more info</a>
								</div>
								<div class="room-information">
									<div class="row">
										<span class="first">Max:</span>
										<span class="second"><img src="/static/pillo/images/ico/person.png" alt="" /><img src="/static/pillo/images/ico/person.png" alt="" /></span>
									</div>
									<div class="row">
										<span class="first">Price:</span>
										<span class="second">$ 55</span>
									</div>
									<div class="row">
										<span class="first">Rooms:</span>
										<span class="second">01</span>
									</div>
									<a href="purchase.php" class="gradient-button" title="Book">Book</a>
								</div>
								<div class="more-information">
									<p>Stylish and individually designed room featuring a satellite TV, mini bar and a 24-hour room service menu.</p>
									<p><strong>Room Facilities:</strong> Safety Deposit Box, Air Conditioning, Desk, Ironing Facilities, Seating Area, Heating, Shower, Bath, Hairdryer, Toilet, Bathroom, Pay-per-view Channels, TV, Telephone</p>
									<p><strong>Bed Size(s):</strong> 1 Double </p>
									<p><strong>Room Size:</strong>  16 square metres</p>
								</div>
							</li>
							
							<li>
								<figure class="left"><img src="/static/pillo/images/slider/img.jpg" alt="" width="270" height="152" /><a href="/static/pillo/images/slider/img.jpg" class="image-overlay" rel="prettyPhoto[gallery1]"></a></figure>
								<div class="meta">
									<h2>Deluxe Single Room</h2>
									<p>Prices are per room<br />20 % VAT Included in price</p>
									<p>Non-refundable<br />Full English breakfast $ 24.80 </p>
									<a href="javascript:void(0)" title="more info" class="more-info">+ more info</a>
								</div>
								<div class="room-information">
									<div class="row">
										<span class="first">Max:</span>
										<span class="second"><img src="/static/pillo/images/ico/person.png" alt="" /></span>
									</div>
									<div class="row">
										<span class="first">Price:</span>
										<span class="second">$ 55</span>
									</div>
									<div class="row">
										<span class="first">Rooms:</span>
										<span class="second">01</span>
									</div>
									<a href="purchase.php" class="gradient-button" title="Book">Book</a>
								</div>
								<div class="more-information">
									<p>Stylish and individually designed room featuring a satellite TV, mini bar and a 24-hour room service menu.</p>
									<p><strong>Room Facilities:</strong> Safety Deposit Box, Air Conditioning, Desk, Ironing Facilities, Seating Area, Heating, Shower, Bath, Hairdryer, Toilet, Bathroom, Pay-per-view Channels, TV, Telephone</p>
									<p><strong>Bed Size(s):</strong> 1 Double </p>
									<p><strong>Room Size:</strong>  16 square metres</p>
								</div>
							</li>
							
							<li>
								<figure class="left"><img src="/static/pillo/images/slider/img.jpg" alt="" width="270" height="152" /><a href="/static/pillo/images/slider/img.jpg" class="image-overlay" rel="prettyPhoto[gallery1]"></a></figure>
								<div class="meta">
									<h2>Standard Family Room</h2>
									<p>Prices are per room<br />20 % VAT Included in price</p>
									<p>Non-refundable<br />Full English breakfast $ 24.80 </p>
									<a href="javascript:void(0)" title="more info" class="more-info">+ more info</a>
								</div>
								<div class="room-information">
									<div class="row">
										<span class="first">Max:</span>
										<span class="second"><img src="/static/pillo/images/ico/person.png" alt="" /><img src="/static/pillo/images/ico/person.png" alt="" /><img src="/static/pillo/images/ico/person.png" alt="" /></span>
									</div>
									<div class="row">
										<span class="first">Price:</span>
										<span class="second">$ 55</span>
									</div>
									<div class="row">
										<span class="first">Rooms:</span>
										<span class="second">01</span>
									</div>
									<a href="purchase.php" class="gradient-button" title="Book">Book</a>
								</div>
								<div class="more-information">
									<p>Stylish and individually designed room featuring a satellite TV, mini bar and a 24-hour room service menu.</p>
									<p><strong>Room Facilities:</strong> Safety Deposit Box, Air Conditioning, Desk, Ironing Facilities, Seating Area, Heating, Shower, Bath, Hairdryer, Toilet, Bathroom, Pay-per-view Channels, TV, Telephone</p>
									<p><strong>Bed Size(s):</strong> 1 Double </p>
									<p><strong>Room Size:</strong>  16 square metres</p>
								</div>
							</li>
							
							<li>
								<figure class="left"><img src="/static/pillo/images/slider/img.jpg" alt="" width="270" height="152" /><a href="/static/pillo/images/slider/img.jpg" class="image-overlay" rel="prettyPhoto[gallery1]"></a></figure>
								<div class="meta">
									<h2>Superior Double Room</h2>
									<p>Prices are per room<br />20 % VAT Included in price</p>
									<p>Non-refundable<br />Full English breakfast $ 24.80 </p>
									<a href="javascript:void(0)" title="more info" class="more-info">+ more info</a>
								</div>
								<div class="room-information">
									<div class="row">
										<span class="first">Max:</span>
										<span class="second"><img src="/static/pillo/images/ico/person.png" alt="" /><img src="/static/pillo/images/ico/person.png" alt="" /></span>
									</div>
									<div class="row">
										<span class="first">Price:</span>
										<span class="second">$ 55</span>
									</div>
									<div class="row">
										<span class="first">Rooms:</span>
										<span class="second">01</span>
									</div>
									<a href="purchase.php" class="gradient-button" title="Book">Book</a>
								</div>
								<div class="more-information">
									<p>Stylish and individually designed room featuring a satellite TV, mini bar and a 24-hour room service menu.</p>
									<p><strong>Room Facilities:</strong> Safety Deposit Box, Air Conditioning, Desk, Ironing Facilities, Seating Area, Heating, Shower, Bath, Hairdryer, Toilet, Bathroom, Pay-per-view Channels, TV, Telephone</p>
									<p><strong>Bed Size(s):</strong> 1 Double </p>
									<p><strong>Room Size:</strong>  16 square metres</p>
								</div>
							</li>
							<!--//room-->
                                                        @foreach ($accomm->vouchers as $voucher)
                                                            <li>
								<?php /*?><figure class="left"><img src="{{$voucher->room->photo->first()->fileName}}" alt="" width="270" height="152" /><a href="/static/pillo/images/slider/img.jpg" class="image-overlay" rel="prettyPhoto[gallery1]"></a></figure><?php */?>
                                @if(is_object($voucher->room->photos->first()))
                                
                                <figure class="left">
                                @if(HTML::image("https://rqphoto.s3-ap-southeast-1.amazonaws.com/$voucher->room->photos->first()->fileName"))
                                <img src="https://rqphoto.s3-ap-southeast-1.amazonaws.com/{{$voucher->room->photos->first()->fileName}}" alt="" width="270" height="152" />
                                <a href="https://rqphoto.s3-ap-southeast-1.amazonaws.com/{{$voucher->room->photos->first()->fileName}}" class="image-overlay" rel="prettyPhoto[gallery1]"></a>
                                @else
                                	<img src="http://dummyimage.com/270x152/d6d2d6/000&text=No+Image" alt="" width="270" height="152" />
                                <a href="http://dummyimage.com/270x152/d6d2d6/000&text=No+Image" class="image-overlay" rel="prettyPhoto[gallery1]"></a>
                                @endif
                                
                                </figure>
                                
                                @else
                                <figure class="left">
                               <img src="http://dummyimage.com/270x152/d6d2d6/000&text=No+Image" alt="" width="270" height="152" />
                                <a href="http://dummyimage.com/270x152/d6d2d6/000&text=No+Image" class="image-overlay" rel="prettyPhoto[gallery1]"></a>
                                @endif
                                
								<div class="meta">
									<h2>{{$voucher->voucherName}}</h2>
									<p>Prices are per room<br />20 % VAT Included in price</p>
									<p>Non-refundable<br />Full English breakfast <?php /*?>@currency(24.80, Session::get('my.currency', Config::get('app.currency')))<?php */?>
                                    <?php
									$ip = $_SERVER['REMOTE_ADDR'];
									$curr =  unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
									echo Currency::format(24.80, $curr['geoplugin_currencyCode']);
								?>
                                     </p>
									<a href="javascript:void(0)" title="more info" class="more-info">+ more info</a>
								</div>
								<div class="room-information">
									<div class="row">
										<span class="first">Max:</span>
										<span class="second">
                                                                                    <img src="/static/pillo/images/ico/person.png" alt="" />
                                                                                    <img src="/static/pillo/images/ico/person.png" alt="" />
                                                                                </span>
									</div>
									<div class="row">
										<span class="first">Price:</span>
										<span class="second">
                                       <?php /*?> @currency((int)$voucher->discountedPrice, Session::get('my.currency', Config::get('app.currency')))<?php */?>
                                        <?php
									$ip = $_SERVER['REMOTE_ADDR'];
									//$ip = '27.111.208.0';
									$curr =  unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
									echo $curPrice = Currency::format($voucher->discountedPrice, $curr['geoplugin_currencyCode']);
									
									$tmpCurPrice = str_replace(array('RM',html_entity_decode($curr['geoplugin_currencySymbol'])),"",$curPrice);
									$findme   = 'RM';
									$pos = strpos($curPrice, $findme);
									if ($pos !== false) {
										$curr['geoplugin_currencyCode'] = "RM";			
									} 
									 $curPrice = $tmpCurPrice;
								?>
                                        </span>
									</div>
                                   
									<div class="row">
										<span class="first">Rooms:</span>
										<!--<span class="second">{{$voucher->salesLimit}}</span>-->
                                        <span class="second">{{$voucher->room->roomAvailability}}</span>
									</div>
                                     @if(isset($userID) )
                                   		 @if($voucher->salesLimit > 0)
                                         
                                          <?php
                                            $ordId = DB::table('orders')->orderBy('id','desc')->take(1)->get();
											
											$invoiceID = "#".str_pad($ordId[0]->id+1, 6, "0", STR_PAD_LEFT);
                                            
											
											if($curr['geoplugin_currencyCode'] == 'SGD')
											{
											$MERCHANTID="377";
                                            $CHECKSUMADDON = "wQA6kXpKsjin";
											}else if($curr['geoplugin_currencyCode'] == 'THB')
											{
											$MERCHANTID="380";
                                            $CHECKSUMADDON = "fYuynbFCtRo2";
											}else if($curr['geoplugin_currencyCode'] == 'IDR')
											{
											$MERCHANTID="379";
                                            $CHECKSUMADDON = "KiJica8E15MV";
											}else if($curr['geoplugin_currencyCode'] == 'MYR')
											{
											$MERCHANTID="378";
                                            $CHECKSUMADDON = "LZ7Koth5UEXo";
											}else if($curr['geoplugin_currencyCode'] == 'USD')
											{
											$MERCHANTID="374";
                                            $CHECKSUMADDON = "xWGRjBwKj5tw";
											}else{
												$MERCHANTID="378";
                                           	 $CHECKSUMADDON = "LZ7Koth5UEXo";
												}
                                            
                                            
                                            $URL = 'http://demo2.2c2p.com/2c2pfrontend/Paymentv2/payment.aspx';
                                            $VERSION="5.0";
                                            $PRODUCTINFO= $voucher->voucherName; 
                                            $INVOICENO=$invoiceID;
                                            $REF1=$voucher->id;
                                            $REF2="Voucher";
                                            $REF3="";
                                            $AMOUNT= $curPrice;
                                            $PROMOTION="";
                                            $CUSTEMAIL="";
                                            $PAYCURRENCY="";
                                            $PAYCATEGORYID=""; 
											
                                            
                                            
                                            $toHash = $VERSION.$MERCHANTID.$PRODUCTINFO.$INVOICENO.$REF1.$REF2.$REF3.$AMOUNT.$PROMOTION.$CUSTEMAIL.$PAYCURRENCY.$PAYCATEGORYID.$CHECKSUMADDON;
                                            //HASH MD5
                                            $CHECKSUM=md5($toHash); 
                                            //ADD PADDING
                                            $maxPADDING = 40-strlen($CHECKSUM); 
                                            for($i=0;$i<$maxPADDING;$i++){ 
                                                $CHECKSUM = "X".$CHECKSUM;
                                            } 
                                            extract($_POST);
                                            
                                            //set POST variables
                                            $fields = array(
                                                            'VERSION'=>$VERSION,
                                                            'MERCHANTID'=>$MERCHANTID,
                                                            'PRODUCTINFO'=>$PRODUCTINFO,
                                                            'INVOICENO'=>$INVOICENO,
                                                            'REF1'=>$REF1,
                                                            'REF2'=>$REF2,
                                                            'REF3'=>$REF3,
                                                            'AMOUNT'=>$AMOUNT,
                                                            'PROMOTION'=>$PROMOTION,
                                                            'CUSTEMAIL'=>$CUSTEMAIL,
                                                            'PAYCURRENCY'=>$PAYCURRENCY,
                                                            'PAYCATEGORYID'=>$PAYCATEGORYID, 
                                                            'CHECKSUM'=>$CHECKSUM
                                                            ); 
                                            
                                            ?>
                                    
                                    
                                    <form name="requestForm" action="<?php echo $URL ?>" method="post">
										<?php
                                        foreach ($fields as $k => $w) {
                                            //making the HTML hidden field for post data
                                        ?>
                                            <input type="hidden" name="<?php echo $k; ?>" value="<?php echo $w ?>"/>
                                        <?php
                                        }
                                        ?>
                                        <input type="submit" name="submit" value="Book" class="gradient-button">
                                        
                                        </form>
                                    	<?php /*?><a href="{{URL('/voucherPurchase/'.$voucher->id)}}" class="gradient-button" title="Book">Book</a><?php */?>
									@else
                                     	<a  title="Book now" class="gradient-button">Sold Out</a>
                                     @endif
                                    @else
                                    	<a href="{{URL('/signin/')}}" title="Book now" class="gradient-button yellow">Purchase</a>
                                    @endif
								</div>
								<div class="more-information">
									<p>{{$voucher->room->description}}</p>
									<p><strong>Room Facilities:</strong> 
                                                                            @foreach ($voucher->room->facilities as $fac)
                                                                            {{$fac->facility}},
                                                                            @endforeach
                                                                        </p>
									<p><strong>Bed Size(s):</strong> {{$voucher->room->bedType}} </p>
									<p><strong>Room Size:</strong> {{$voucher->room->roomSize}} </p>
								</div>
                                                            </li>
                                                        @endforeach
						</ul>
					</article>
				</section>
				<!--//availability-->
				
				<!--description-->
                
				<section id="description" class="tab-content">
					<article>
						<h1>General</h1>
						<div class="text-wrap">	
							<p>{{$accomm->hotelDescription}}</p>
						</div>
						
						<h1>Check-in</h1>
						<div class="text-wrap">	
							<p>{{$accomm->checkIn}}</p>
						</div>
						
						<h1>Check-out</h1>
						<div class="text-wrap">	
							<p>{{$accomm->checkOut}}</p>
						</div>
						
						<h1>Cancellation / Prepayment</h1>
						<div class="text-wrap">	
							<p>Cancellation and prepayment policies vary according to room type. Please check the <a href="#">room conditions</a> when selecting your room. </p>
						</div>
						
						<h1>Children and extra beds</h1>
						<div class="text-wrap">	
							<p><strong>{{$accomm->policies->childrenStayFree}}</strong> All children under 8 years stay free of charge when using existing beds.<br /><strong>Free!</strong> All children under 2 years stay free of charge for children’s cots/cribs.<br /><br />All older children or adults are charged <?php /*?>@currency((int)40, Session::get('my.currency', Config::get('app.currency')))<?php */?> 
                            <?php
									$ip = $_SERVER['REMOTE_ADDR'];
									$curr =  unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
									echo Currency::format(40, $curr['geoplugin_currencyCode']);
								?>
                                per person per night for extra beds.<br />The maximum number of extra beds/children’s cots permitted in a room is 1.<br />Any type of extra bed or child’s cot/crib is upon request and needs to be confirmed by management.<br />Supplements are not calculated automatically in the total costs and will have to be paid for separately when checking out.</p>
						</div>
						
						<h1>Pets</h1>
						<div class="text-wrap">	
							<p>Pets are allowed. Charges may be applicable.</p>
						</div>
						
						<h1>Accepted credit cards</h1>
						<div class="text-wrap">	
							<p>American Express, Visa, Euro/Mastercard, Diners Club<br />The hotel reserves the right to pre-authorise credit cards prior to arrival.</p>
						</div>
					</article>
				</section>
				<!--//description-->
				
				<!--facilities-->
				<section id="facilities" class="tab-content">
					<article>
						<h1>Facilities</h1>
						<div class="text-wrap">	
							<ul class="three-col">
                                                            @foreach ($accomm->getFacilities() as $fac)
                                                            <li>{{$fac->facility}}</li>
                                                            @endforeach
								
							</ul>
						</div>
						
						<h1>Activities</h1>
						<div class="text-wrap">	
							<!--<p>Tennis court, Sauna, Fitness centre, Massage </p>-->
                            <ul class="three-col">
                            
                                @foreach ($accomm->getSrFacilities() as $srfac)
                                <li>{{$srfac->facility}}</li>
                                @endforeach
								
							</ul>
						</div>
						
						<h1>Internet</h1>
						<div class="text-wrap">	
                       
                            @if($accomm->internetUsage)
                                 <p>If you use WiFi, then charge will be <strong><?php /*?>@currency($accomm->internetUsage, Session::get('my.currency', Config::get('app.currency')))<?php */?>
                                 <?php
									$ip = $_SERVER['REMOTE_ADDR'];
									$curr =  unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
									echo Currency::format($accomm->internetUsage, $curr['geoplugin_currencyCode']);
								?>
                                 </strong>.</p>
                             @else
                                <p><strong>Free!</strong> WiFi is available in all areas and is free of charge. </p>
                               @endif
                               
						</div>
						
						<h1>Parking</h1>
						<div class="text-wrap">	
                         @if($accomm->parkingFee != '' &&  $accomm->parking == 'yes')
                         
                             <p>Private parking is possible at a location nearby (reservation is not needed) and costs  <p>If you use WiFi, then charge will be <strong><?php /*?>@currency($accomm->parkingFee, Session::get('my.currency', Config::get('app.currency')))<?php */?>
                             <?php
									$ip = $_SERVER['REMOTE_ADDR'];
									$curr =  unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
									echo Currency::format($accomm->parkingFee, $curr['geoplugin_currencyCode']);
								?>
                             </strong>.</p> per day.</p>
                         @else
                         	<p>Private parking is possible at a location nearby (reservation is not needed) and costs <?php /*?>@currency(28.80, Session::get('my.currency', Config::get('app.currency')))<?php */?> 
                             <?php
									$ip = $_SERVER['REMOTE_ADDR'];
									$curr =  unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
									echo Currency::format(28.80, $curr['geoplugin_currencyCode']);
								?>
                            per day.</p>
                           @endif
                           
							
						</div>
					</article>
				</section>
				<!--//facilities-->
				
				<!--location-->
				<section id="location" class="tab-content">
					<article>
						<!--map-->
							<div class="gmap" id="map_canvas"></div>
						<!--//map-->
					</article>
				</section>
				<!--//location-->
				
				<!--reviews-->
				 <section id="reviews" class="tab-content">
                 @if(count($review) > 0)
					<article>
						<h1>Hotel Score and Score Breakdown</h1>
						<div class="score">
                     
							<span class="achieved">{{ round(DB::table('reviewsrating')->where('accommID',$accomm->id)->sum('rating')/sizeof($review))}}  </span>
							<span> / 5</span>
                           
							<p class="info">Based on {{ count($review)}} reviews</p>
							<p class="disclaimer">Guest reviews are written by our customers <strong>after their stay</strong> at Hotel {{$accomm->name}}.</p>
						</div>
						
						<dl class="chart">
							<dt>Clean</dt>
							<dd><span id="data-one" style="width:{{ round(DB::table('reviewsrating')->where('accommID',$accomm->id)->sum('clean')/sizeof($review))}}0%;">{{ round(DB::table('reviewsrating')->where('accommID',$accomm->id)->sum('clean')/sizeof($review))}}&nbsp;&nbsp;&nbsp;</span></dd>
							<dt>Comfort</dt>
							<dd><span id="data-two" style="width:{{ round(DB::table('reviewsrating')->where('accommID',$accomm->id)->sum('comfort')/sizeof($review))}}0%;">{{ round(DB::table('reviewsrating')->where('accommID',$accomm->id)->sum('comfort')/sizeof($review))}}&nbsp;&nbsp;&nbsp;</span></dd>
							<dt>Location</dt>
							<dd><span id="data-three" style="width:{{ round(DB::table('reviewsrating')->where('accommID',$accomm->id)->sum('location')/sizeof($review))}}0%;">{{ round(DB::table('reviewsrating')->where('accommID',$accomm->id)->sum('location')/sizeof($review))}}&nbsp;&nbsp;&nbsp;</span></dd>
							<dt>Staff</dt>
							<dd><span id="data-four" style="width:{{ round(DB::table('reviewsrating')->where('accommID',$accomm->id)->sum('staff')/sizeof($review))}}0%;">{{ round(DB::table('reviewsrating')->where('accommID',$accomm->id)->sum('staff')/sizeof($review))}}&nbsp;&nbsp;&nbsp;</span></dd>
							<dt>Services</dt>
							<dd><span id="data-five" style="width:{{ round(DB::table('reviewsrating')->where('accommID',$accomm->id)->sum('services')/sizeof($review))}}0%;">{{ round(DB::table('reviewsrating')->where('accommID',$accomm->id)->sum('services')/sizeof($review))}}&nbsp;&nbsp;&nbsp;</span></dd>
							<dt>Value for money</dt>
							<dd><span id="data-six" style="width:{{ round(DB::table('reviewsrating')->where('accommID',$accomm->id)->sum('valForMoney')/sizeof($review))}}0%;">{{ round(DB::table('reviewsrating')->where('accommID',$accomm->id)->sum('valForMoney')/sizeof($review))}}&nbsp;&nbsp;&nbsp;</span></dd>
						</dl>
					</article>
					 @endif
                    
					<article>
                  
                    
					<h1 style="width:50%;float:left;"><a id="guestReviews" style="cursor:pointer">Guest reviews</a></h1>
                   @if(isset($userID))
                  <?php 
				  $userReview = DB::table('reviewsrating')->where('accommID',$accomm->id)->where('byUserID',$userID)->count('rating'); ?>
                        @if(isset($userID) && $userID != '' && $userReview < 1)
                        <h1 style="width:50%;float:right;text-align: right;"><a id="addreview"  style="cursor:pointer">Add reviews</a></h1>
                      
                    	<div class="reviewForm" style="display:none">
                        <?php /*?><form name="postReview" method="post" action="{{URL('/postReview')}}"><?php */?>
                        {{Form::open(array('action' => array('PublicController@saveReview', $accomm->id), 'name' => 'postReview'))}}
                        
                        	<div style="width:100%;float:left">
                            	<div style="width:10%;float:left">Rating : </div>
                            
                            	<div style="width:80%;float:left">
                               
                                	<span ><input type="radio" name="rating" value="1" />Poor</span>
                                    <span style="margin-left:10px;"><input type="radio" name="rating" value="2" />satisfactory</span>
                                    <span style="margin-left:10px;"><input type="radio" name="rating" value="3" />Good</span>
                                    <span style="margin-left:10px;"><input type="radio" name="rating" value="4" />Very Good</span>
                                    <span style="margin-left:10px;"><input type="radio" name="rating" value="5" />Excellent</span>
                                </div>
                                
                               
                                
                             </div>
                             
							<div style="width:100%;float:left;margin-top:15px">
                            	<div style="width:10%;float:left">Reviews : </div>
                            
                            	<div style="width:40%;float:left">
                               
                                	<textarea name="positiveReviews" id="positiveReviews" rows="2" cols="10" placeholder="Enter your positive review."></textarea>
                                </div>
                                
                                <div style="width:40%;float:left;margin-left:10px" >
                                	<div style="background-image: url('../images/themes/yellow/ico/minus.png');"></div>
                                    <textarea name="negativeReviews" id="negativeReviews" rows="2" cols="10" placeholder="Enter your negative review."></textarea>
                                </div>
                                
                             </div>
                             
                             <div style="width:100%;float:left;margin-top:15px;">
                            	<div style="width:10%;float:left">Hotel Score :</div>
                            
                            	<div style="width:90%;float:left">
                               
                                	<span >
                                    	<select name="clean">
                                        	<option value="0">Clean</option>
                                            @for($i=1;$i<=10;$i++)
                                            	<option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                     </span>
                                    <span style="margin-left:10px;">
                                    <select name="comfort">
                                        	<option value="0">Comfort</option>
                                            @for($j=1;$j<=10;$j++)
                                            	<option value="{{$j}}">{{$j}}</option>
                                            @endfor
                                        </select>
                                    </span>
                                    <span style="margin-left:10px;">
                                    <select name="location">
                                        	<option value="0">Location</option>
                                            @for($k=1;$k<=10;$k++)
                                            	<option value="{{$k}}">{{$k}}</option>
                                            @endfor
                                        </select>
                                    </span>
                                    <span style="margin-left:10px;">
                                    <select name="staff">
                                        	<option value="0">Staff</option>
                                            @for($m=1;$m<=10;$m++)
                                            	<option value="{{$m}}">{{$m}}</option>
                                            @endfor
                                        </select>
                                    
                                    </span>
                                    <span style="margin-left:10px;">
                                    <select name="services">
                                        	<option value="0">Services</option>
                                            @for($n=1;$n<=10;$n++)
                                            	<option value="{{$n}}">{{$n}}</option>
                                            @endfor
                                        </select>
                                    </span>
                                    
                                    <span style="margin-left:10px;">
                                    <select name="valForMoney">
                                        	<option value="0">Value For Money</option>
                                            @for($p=1;$p<=10;$p++)
                                            	<option value="{{$p}}">{{$p}}</option>
                                            @endfor
                                        </select>
                                    </span>
                                </div>
                                
                               
                                
                             </div>
                            
                            
                           <div style="width:60%;float:right;margin-top:10px">
                            	<a title="Book" class="gradient-button" style="cursor:pointer" id="saveReviewButton">Save</a>
                            </div>
                           {{Form::close()}}
                        </div>
                        
                         @endif
                      @endif
                         <div class="showreviews">
                         <ul class="reviews">
                         
                         @foreach($review as $rev)
							<li>
								<figure class="left"><img src="/static/pillo/images/uploads/avatar.jpg" alt="avatar" /></figure>
								<address>
                                <span>{{$rev->firstName}} , {{$rev->lastName}}</span>
                               <?php /*?> <br />Solo Traveller<br />Norway<br />22/06/2012<?php */?>
                                </address>
                                @if($rev->positiveReviews)
									<div class="pro"><p>{{$rev->positiveReviews}}</p></div>.
                                @endif
                                @if($rev->negativeReviews != '')
									<div class="con"><p>{{$rev->negativeReviews}}</p></div>
                                @endif
							</li>
                           @endforeach
                            
							
						</ul>
                        
                        @if(sizeof($review) < 1)
                        	<p style="color:#F00"> There are no any reviews for this hotel.</p>
                         @endif
                       </div>
                        
					</article>
				</section> 
				<!--//reviews-->
				
				<!--things to do-->
				<!-- <section id="things-to-do" class="tab-content">
					<article>
						<h1>London</h1>
						<figure class="left_pic"><img src="/static/pillo/images/uploads/img.jpg" alt="Things to do - London general" /></figure>
						<p class="teaser">London is a diverse and exciting city with some of the best sights and attractions in the world. </p>
						<p>See London from above on the London Eye; meet a celebrity at Madame Tussauds; examine some of the world’s most precious treasures at the British Museum or come face-to-face with the dinosaurs at the Natural History Museum.</p>
						
						<h1>Sports and nature</h1>
						<figure class="left_pic"><img src="/static/pillo/images/uploads/img.jpg" alt="Things to do - London Sports and nature" /></figure>
						<p class="teaser">London is one of the greenest capitals in the world, with plenty of green and open spaces. There are more than 3000 open spaces.</p>
						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.<br />Ut wisi enim ad minim veniam, quis nostrud exerci. </p>
						
						<h1>Nightlife</h1>
						<figure class="left_pic"><img src="/static/pillo/images/uploads/img.jpg" alt="Things to do - London Nightlife" /></figure>
						<p class="teaser">Looking for nightclubs in London? Take a look at our guide to London clubs. Browse for club ideas, regular club nights and one-off events. </p>
						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.<br />Ut wisi enim ad minim veniam, quis nostrud exerci. </p>
						
						<h1>Culture and history</h1>
						<figure class="left_pic"><img src="/static/pillo/images/uploads/img.jpg" alt="Things to do - London general" /></figure>
						<p class="teaser">For a display of British pomp and ceremony, watch the Changing the Guard ceremony outside Buckingham Palace.</p>
						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.<br />Ut wisi enim ad minim veniam, quis nostrud exerci. </p>
						<hr />
						<a href="#" class="gradient-button right" title="Read more">Read more</a>
					</article>
				</section> -->
				<!--//things to do-->
			</section>
			<!--//hotel content-->
			
			<!--sidebar-->
			<aside class="right-sidebar">
				<!--hotel details-->
				<article class="hotel-details clearfix">
					<h1> 
                     @if(strlen($accomm->name) > 20)
                                 {{substr($accomm->name,0,20)}}...
                                 @else
                                  {{$accomm->name}}
                                 @endif
						<!--<span class="stars">
							<img src="/static/pillo/images/ico/star.png" alt="" />
							<img src="/static/pillo/images/ico/star.png" alt="" />
							<img src="/static/pillo/images/ico/star.png" alt="" />
							<img src="/static/pillo/images/ico/star.png" alt="" />
						</span>-->
                        
                        <span class="starOuter" style="width:100%;float:left">
									<span class="stars">
										@for($i = 0; $i < $accomm->starRating; $i++)
										<img src="/static/pillo/images/ico/star.png" alt="">
										@endfor
									</span>
                                 </span>
					</h1>
					<span class="address">
                    @if(strlen($accomm->streetAddress) > 25)
                                 {{substr($accomm->streetAddress,0,25)}}...
                                 @else
                                  {{$accomm->streetAddress}}
                                 @endif<br />  {{$accomm->city}} , {{$accomm->country}}
                                 
                   </span>
					<span class="rating"> 
                    @if(DB::table('reviewsrating')->where('accommID',$accomm->id)->sum('rating') > 0)
                    {{ round(DB::table('reviewsrating')->where('accommID',$accomm->id)->sum('rating')/sizeof($review))}} /5
                    @else
                    0/5
                    @endif
                    
                    </span>
                    
					<div class="description">
						<p>@if(strlen($accomm->hotelDescription) > 150)
                                 {{substr($accomm->hotelDescription,0,150)}}...
                                 @else
                                  {{$accomm->hotelDescription}}
                                 @endif</p>
					</div>
				</article>
                
				<!--//hotel details
				
				testimonials
				 <article class="testimonials clearfix">
					<blockquote>Loved the staff and the location was just amazing... Perfect!” </blockquote>
					<span class="name">- Jane Doe, Solo Traveller</span>
				</article> 
				testimonials
				
				Popular hotels in the area
				 <article class="default clearfix">
					<h2>Popular hotels in the area</h2>
					<ul class="popular-hotels">
						<li>
							<a href="#">
								<h3>Plaza Resort Hotel &amp; SPA
									<span class="stars">
										<img src="/static/pillo/images/ico/star.png" alt="" />
										<img src="/static/pillo/images/ico/star.png" alt="" />
										<img src="/static/pillo/images/ico/star.png" alt="" />
										<img src="/static/pillo/images/ico/star.png" alt="" />
									</span>
								</h3>
								<p>From <span class="price">$ 100 <small>/ per night</small></span></p>
								<span class="rating"> 8 /10</span>
							</a>
						</li>
						<li>
							<a href="#">
								<h3>Lorem Ipsum Inn
									<span class="stars">
										<img src="/static/pillo/images/ico/star.png" alt="" />
										<img src="/static/pillo/images/ico/star.png" alt="" />
										<img src="/static/pillo/images/ico/star.png" alt="" />
										<img src="/static/pillo/images/ico/star.png" alt="" />
										<img src="/static/pillo/images/ico/star.png" alt="" />
									</span>
								</h3>
								<p>From <span class="price">$ 110 <small>/ per night</small></span></p>
								<span class="rating"> 7 /10</span>
							</a>
						</li>
						<li>
							<a href="#">
								<h3>Best Eastern London
									<span class="stars">
										<img src="/static/pillo/images/ico/star.png" alt="" />
										<img src="/static/pillo/images/ico/star.png" alt="" />
										<img src="/static/pillo/images/ico/star.png" alt="" />
										<img src="/static/pillo/images/ico/star.png" alt="" />
										<img src="/static/pillo/images/ico/star.png" alt="" />
									</span>
								</h3>
								<p>From <span class="price">$ 125 <small>/ per night</small></span></p>
								<span class="rating"> 8 /10</span>
							</a>
						</li>
						<li>
							<a href="#">
								<h3>Plaza Resort Hotel &amp; SPA
									<span class="stars">
										<img src="/static/pillo/images/ico/star.png" alt="" />
										<img src="/static/pillo/images/ico/star.png" alt="" />
										<img src="/static/pillo/images/ico/star.png" alt="" />
										<img src="/static/pillo/images/ico/star.png" alt="" />
									</span>
								</h3>
								<p>From <span class="price">$ 100 <small>/ per night</small></span></p>
								<span class="rating"> 8 /10</span>
							</a>
						</li>
					</ul>
					<a href="#" title="Show all" class="show-all">Show all</a>
				</article> 
				Popular hotels in the area
				
				Deal of the day
				 <article class="default clearfix">
					<h2>Deal of the day</h2>
					<div class="deal-of-the-day">
						<a href="hotel.html">
							<figure><img src="/static/pillo/images/slider/img.jpg" alt="" width="230" height="130" /></figure>
							<h3>Plaza Resort Hotel &amp; SPA
								<span class="stars">
									<img src="/static/pillo/images/ico/star.png" alt="" />
									<img src="/static/pillo/images/ico/star.png" alt="" />
									<img src="/static/pillo/images/ico/star.png" alt="" />
									<img src="/static/pillo/images/ico/star.png" alt="" />
								</span>
							</h3>
							<p>From <span class="price">$ 100 <small>/ per night</small></span></p>
							<span class="rating"> 8 /10</span>
						</a>
					</div>
				</article> 
				Deal of the day-->
			</aside>
			<!--//sidebar-->
		</div>
		<!--//main content-->
	</div>
</div>
<script>
$(document).ready(function(){
	$('#addreview').click(function(){
			$('.reviewForm').css('display','block');
			$('.showreviews').css('display','none');
		});
		
	$('#guestReviews').click(function(){
			$('.showreviews').css('display','block');
			$('.reviewForm').css('display','none');
		});
	});
	
</script>
<script>
	$('#saveReviewButton').click(function(){
		
		
        if ($('input[name=rating]:checked').length<=0) {
           alert("Please choose any rate."); 
		   return false;
        }else if ($("#positiveReviews").val() == "" && $("#negativeReviews").val() == "") {
           alert("Please choose any rate."); 
			return false;
		}else{
		   
		//form = $(this).parents('form');
		document.postReview.submit();
		}
	});
</script>
@stop