@extends('templates.pillo')

@section('title')
About Us
@stop

@section('bodyyield')
@stop

@section('thescripts')

@stop

@section('content')
<div class="main" role="main">		
	<div class="wrap clearfix">
		<div class="content clearfix">
			<h2>About Us</h2>
			<section class="full">
				<div class="map-wrap">
					<h1>Pillo Pillo</h1>
					<figure class="left_pic"><img src="/static/pillo/images/uploads/img.jpg" width="200" alt="Things to do - London general" /></figure>
					<p class="teaser">London is a diverse and exciting city with some of the best sights and attractions in the world. </p>
					<p>See London from above on the London Eye; meet a celebrity at Madame Tussauds; examine some of the world’s most precious treasures at the British Museum or come face-to-face with the dinosaurs at the Natural History Museum.</p>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.<br />Ut wisi enim ad minim veniam, quis nostrud exerci. </p>
							<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. </p>
							<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
				</div>
			</section>
		</div>
	</div>
</div>
@stop