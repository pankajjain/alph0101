<?php /*?><script type="text/javascript" src="{{ URL::to('static/pillo/js/scripts.js') }}"></script><?php */?>
					<?php $m=1 ?>
					@foreach($result as $entry)                    
                    <?php $customClass = ($m%3==0)?'last':'';  ?>
					<article class="one-fourth promo <?php echo $customClass; ?>">
					
						<figure>
							@if($entry->fileName!=NULL)
                            
                            	@if(HTML::image("https://rqphoto.s3-ap-southeast-1.amazonaws.com/268_$entry->fileName"))
                                <img alt width="270" height="152" src="https://rqphoto.s3-ap-southeast-1.amazonaws.com/268_{{ $entry->fileName }}"/>
                                @else
                                  <img alt width="270" height="152" src="http://dummyimage.com/270x152/d6d2d6/000&text=No+Image"/>
                                @endif
                            
								
							@else
								 <img alt width="270" height="152" src="http://dummyimage.com/270x152/d6d2d6/000&text=No+Image"/>
							@endif
						</figure>
                       
						<div class="details">
							<h1>@if(strlen($entry->name) > 20)
                            	{{substr($entry->name,0,20)}}...
                            @else
                            	{{$entry->name}}
                            @endif
                            <br />
                             <span class="starOuter">
								<span class="stars">
									@for($i = 0; $i < $entry->starRating; $i++)
									<img src="/static/pillo/images/ico/star.png" alt="">
									@endfor
								</span>
                                </span>
							</h1>
                            
							<span class="address">{{$entry->city}}, {{$entry->province}}</span>
							<span class="rating">{{$entry->starRating}}</span>
							<span class="price" style="text-align: justify;min-height:110px">
                            @if($entry->hotelDescription!=NULL)
                            @if(strlen($entry->hotelDescription) > 150)
                                {{substr($entry->hotelDescription,0,150)}} ...
                             @else
                             	{{$entry->hotelDescription}}
                             @endif 
                        @else 
                        	Overlooking the Aqueduct and Nature Park, Lorem Ipsum Hotel is situated 5 minutes' walk from London's Zoo logical Gardens and a metro station. Click for more info. 
                        @endif
                            </span>
							<a href="{{URL('/hotel/'.$entry->id)}}" title="Book now" class="gradient-button yellow">Browse</a>
						</div>
					</article>
                    <?php  $m++;  ?>
					@endforeach		

					<!--bottom navigation-->
					<div class="bottom-nav">
						<!--back up button-->
						<?php /*?><a href="#" class="scroll-to-top" title="Back up">Back up</a> <?php */?>
						<!--//back up button-->

						<!--pager-->
						<?php /*?><div class="pager">
							<span class="first"><a href="#">First page</a></span>
							<span><a href="#">&lt;</a></span>
							<span class="current">1</span>
							<span><a href="#">2</a></span>
							<span><a href="#">3</a></span>
							<span><a href="#">4</a></span>
							<span><a href="#">5</a></span>
							<span><a href="#">6</a></span>
							<span><a href="#">7</a></span>
							<span><a href="#">8</a></span>
							<span><a href="#">&gt;</a></span>
							<span class="last"><a href="#">Last page</a></span>
						</div><?php */?>
                        <div class="paging2">{{$result->appends(array())->links()}}</div>
						<!--//pager-->
					</div>
					<!--//bottom navigation-->


<script>
 
    /*$(window).on('hashchange', function() {
        if (window.location.hash) {
            var page = window.location.hash.replace('#', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            } else {
                getPosts(page);
            }
        }
    });*/
 
   
 
    function getPosts(page) {
		
		var select0 = new Array();
				$('#price option:selected').each(function(){
				  //var $this = $(this);
				  select0.push($(this).val());
				});
			
			
			var select1 = new Array();
			$('#accommFac option:selected').each(function(){
			  //var $this = $(this);
			  select1.push($(this).text());
			});
			
			var select2 = new Array();
			$('#roomFac option:selected').each(function(){
			  //var $this = $(this);
			  select2.push($(this).text());
			});
			
			var select3 = new Array();
			$('#srFac option:selected').each(function(){
			  //var $this = $(this);
			  select3.push($(this).text());
			});
			
			$('.deals').html('<div style=" position: absolute; top: 50%; left: 50%;"><img src="/static/pillo/images/loader.gif"> loading...</div>');
		var postForm = { //Fetch form data
					'price'		:  select0,	
					'accommType'  : $('#accommType').val(),
					'accommFac'  : select1,
					'roomFac'  : select2,
					'srFac'  : select3,
					'starRate' : $('#starRate').val(),
					'userRate' : $('#userRateVal').val()
					};
					
					
        $.ajax({
			type: 'POST',
            url : 'ajaxSearch?page=' + page,
			data: postForm,
            dataType: 'json',
        }).done(function (data) {
			//alert(data);
            $('.deals').html(data);
            location.hash = page;
        })
		
    }
 
    </script>