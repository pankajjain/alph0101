@extends('templates.pillo')

@section('title')
Your Travel Partner
@stop

@section('pageAssets')

<script type="text/javascript" src="{{ URL::to('static/pillo/js/css3-mediaqueries.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('static/pillo/js/jquery.uniform.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('static/pillo/js/jquery.raty.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('static/pillo/js/jquery.prettyPhoto.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('static/pillo/js/selectnav.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('static/pillo/js/scripts.js') }}"></script>
<link rel="stylesheet" href="/static/pillo/chosen/chosen.css">

<script src="/static/pillo/chosen/chosen.jquery.js"> </script>

<script type="text/javascript">
$(document).ready(function() {
	
	//$console.log($( '#slider' ).slider('value'));
        $('dt').each(function() {
            var tis = $(this), state = false, answer = tis.next('dd').hide().css('height','auto').slideUp();
            tis.click(function() {
                state = !state;
                answer.slideToggle(state);
                tis.toggleClass('active',state);
            });
        });
        
        $('.view-type li:first-child').addClass('active');
        
        $('#star').raty({
			score    : 3,
			click: function(score, evt) {
			//alert('ID: ' + $(this).attr('id') + '\nscore: ' + score + '\nevent: ' + evt);
			$('#starRate').val(score);
			starRateSearch(score,'');
		  }
		});
		
		
	});
	
	$(window).load(function () {
	var maxHeight = 0;
			
	$(".three-fourth .one-fourth").each(function(){
	if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
	});
	$(".three-fourth .one-fourth").height(maxHeight);	
	});	

</script>


 <script>
 $(document).ready(function(){
        $('.chosen-select').change(function() {
			
			var select0 = new Array();
			$('#price option:selected').each(function(){
			  //var $this = $(this);
			  select0.push($(this).val());
			});
			
			var select1 = new Array();
			$('#accommFac option:selected').each(function(){
			  //var $this = $(this);
			  select1.push($(this).text());
			});
			
			var select2 = new Array();
			$('#roomFac option:selected').each(function(){
			  //var $this = $(this);
			  select2.push($(this).text());
			});
			
			var select3 = new Array();
			$('#srFac option:selected').each(function(){
			  //var $this = $(this);
			  select3.push($(this).text());
			});
				
				$('.deals').html('<div style=" position: absolute; top: 50%; left: 50%;"><img src="/static/pillo/images/loader.gif"> loading...</div>');
				
		
				//alert($('#ajaxsearch').serialize());
				//alert($('#star').val()); return false;
				var postForm = { //Fetch form data
					'price'		:  select0,		
					'accommType'  : $('#accommType').val(),
					'accommFac'  : select1,
					'roomFac'  : select2,
					'srFac'  : select3,
					'starRate' : $('#starRate').val(),
					'userRate' : $('#userRateVal').val()
					};
					 
                $.ajax({
                        type: 'POST',
                        url: "{{URL('/ajaxSearch')}}",
                       	data: postForm,
						success: function (data) {
                             // alert(data);
							   $('.deals').html(data);
                               
                        }
                });
        });
	});
        </script>
        
        
  <script>
 function priceFilter(price,order){
       
			
			
				var select0 = new Array();
				$('#price option:selected').each(function(){
				  //var $this = $(this);
				  select0.push($(this).val());
				});
			
			
			var select1 = new Array();
			$('#accommFac option:selected').each(function(){
			  //var $this = $(this);
			  select1.push($(this).text());
			});
			
			var select2 = new Array();
			$('#roomFac option:selected').each(function(){
			  //var $this = $(this);
			  select2.push($(this).text());
			});
			
			var select3 = new Array();
			$('#srFac option:selected').each(function(){
			  //var $this = $(this);
			  select3.push($(this).text());
			});
				
				
		$('.deals').html('<div style=" position: absolute; top: 50%; left: 50%;"><img src="/static/pillo/images/loader.gif"> loading...</div>');
				//alert($('#ajaxsearch').serialize());
				//alert($('#star').val()); return false;
				if(price == ''){
				var postForm = { //Fetch form data
					'price'		:  '',	
					'priceOrder' : order,	
					'accommType'  : $('#accommType').val(),
					'accommFac'  : select1,
					'roomFac'  : select2,
					'srFac'  : select3,
					'starRate' : $('#starRate').val(),
					'userRate' : $('#userRateVal').val()
					};
				}else{
						var postForm = { //Fetch form data
					'price'		:  select0,	
					'priceOrder' : order,		
					'accommType'  : $('#accommType').val(),
					'accommFac'  : select1,
					'roomFac'  : select2,
					'srFac'  : select3,
					'starRate' : $('#starRate').val(),
					'userRate' : $('#userRateVal').val()
					};
					}
					 
                $.ajax({
                        type: 'POST',
                        url: "{{URL('/ajaxSearch')}}",
                       	data: postForm,
						success: function (data) {
                             // alert(data);
							   $('.deals').html(data);
                               
                        }
                });
        
 }
        </script>      
        
        
   <script>
		function starRateSearch(score,order){
			//alert(score);
			var select0 = new Array();
			$('#price option:selected').each(function(){
			  //var $this = $(this);
			  select0.push($(this).val());
			});
			
			var select1 = new Array();
			$('#accommFac option:selected').each(function(){
			  //var $this = $(this);
			  select1.push($(this).text());
			});
			
			var select2 = new Array();
			$('#roomFac option:selected').each(function(){
			  //var $this = $(this);
			  select2.push($(this).text());
			});
			
			var select3 = new Array();
			$('#srFac option:selected').each(function(){
			  //var $this = $(this);
			  select3.push($(this).text());
			});
				
				$('.deals').html('<div style=" position: absolute; top: 50%; left: 50%;"><img src="/static/pillo/images/loader.gif"> loading...</div>');
		
				//alert($('#ajaxsearch').serialize());
				//alert($('#star').val()); return false;
			if(score == '')
			{
				var postForm = { //Fetch form data
					'price'		:  select0,	
					'accommType'  : $('#accommType').val(),
					'accommFac'  : select1,
					'roomFac'  : select2,
					'srFac'  : select3,
					'starRate' : '',
					'order'		: order,
					'userRate'  : $('#userRateVal').val()
					};
			}else{
					var postForm = { //Fetch form data
					'price'		:  select0,	
					'accommType'  : $('#accommType').val(),
					'accommFac'  : select1,
					'roomFac'  : select2,
					'srFac'  : select3,
					'starRate' : score,
					'order'		: order,
					'userRate'  : $('#userRateVal').val()
					};
				}
					 
                $.ajax({
                        type: 'POST',
                        url: "{{URL('/ajaxSearch')}}",
                       	data: postForm,
						success: function (data) {
                              // alert(data);
							   $('.deals').html(data);
                               
                        }
                });
			}
		</script>     
        
        
  <script>
			function userRateVal(v,order){
				//alert(v);
			var select0 = new Array();
			$('#price option:selected').each(function(){
			  //var $this = $(this);
			  select0.push($(this).val());
			});
				
			var select1 = new Array();
			$('#accommFac option:selected').each(function(){
			  //var $this = $(this);
			  select1.push($(this).text());
			});
			
			var select2 = new Array();
			$('#roomFac option:selected').each(function(){
			  //var $this = $(this);
			  select2.push($(this).text());
			});
			
			var select3 = new Array();
			$('#srFac option:selected').each(function(){
			  //var $this = $(this);
			  select3.push($(this).text());
			});
				
				$('.deals').html('<div style=" position: absolute; top: 50%; left: 50%;"><img src="/static/pillo/images/loader.gif"> loading...</div>');
		
				//alert($('#ajaxsearch').serialize());
				//alert($('#star').val()); return false;
				if(v == '')
				{
				var postForm = { //Fetch form data
					'price'		:  select0,	
					'accommType'  : $('#accommType').val(),
					'accommFac'  : select1,


					'roomFac'  : select2,
					'srFac'  : select3,
					'starRate' : $('#starRate').val(),
					'userRate'  : '',
					'userRateOrder'		: order,
					};
					
				}else{
					var postForm = { //Fetch form data
					'price'		:  select0,	
					'accommType'  : $('#accommType').val(),
					'accommFac'  : select1,
					'roomFac'  : select2,
					'srFac'  : select3,
					'starRate' : $('#starRate').val(),
					'userRate'  : v,
					'userRateOrder'		: order,
					};
					}
					 
                $.ajax({
                        type: 'POST',
                        url: "{{URL('/ajaxSearch')}}",
                       	data: postForm,
						success: function (data) {
                              // alert(data);
							   $('.deals').html(data);
                               
                        }
                });
				
				}
		</script>   
        
        
        <script>
		
        $(document).on('click', '.pagination a', function (e) {
            getPosts($(this).attr('href').split('page=')[1]);
            e.preventDefault();
        });
    
		</script>
@stop



@section('content')
<div class="main" role="main">		
	<div class="wrap clearfix">
		<!--main content-->
		<div class="content clearfix">			
			<!--sidebar-->
			<aside class="left-sidebar">
            <form name="ajaxsearch" id="ajaxsearch" method="post" action="{{URL('/ajaxSearch')}}">
				<article class="refine-search-results">
					<h2>Refine search results</h2>
					<dl>
						<!--Price (per night)-->
						<dt>Price (per night)</dt>

						<dd>
							<select data-placeholder="Price range" id="price" name="price" class="chosen-select" multiple style="width:200px;" tabindex="4">
						        <option value="1,100">1 - 100</option>
						        <option value="101,200">101 - 200</option>
						        <option value="201,300">201 - 300</option>
						        <option value="301,All">301+</option>
						    </select>
						</dd>
						<!--//Price (per night)-->

						<!--Accommodation type-->
						<dt>Accommodation type</dt>
						<dd>
							<select name="typeid" id="accommType" data-placeholder="Type range" class="chosen-select" multiple style="width:200px;" tabindex="4">
								@foreach(AccommType::all() as $acT)
                                <option value="{{ $acT->id }}">{{ $acT->title }}</option>
                                @endforeach
						    </select>
						</dd>
						<!--//Accommodation type-->

						<!--Star rating-->
						<dt>Star rating</dt>
						<dd>
							<span class="stars-info">3 or more</span>
							<div id="star" ></div>
                            <input type="hidden" name="starRate" id="starRate" value="" />
						</dd>
						<!--//Star rating-->

						<!--User rating-->
						<dt class="active">User rating</dt>
						<dd>
                         <input type="hidden" name="userRateVal" id="userRateVal" value="" />
							<div id="slider"></div>
							<span class="min">0</span><span class="max">5</span>
						</dd>
						<!--//User rating-->

						<!--Hotel facilities-->
						<dt>Accommodation facilities</dt>
						<dd>
							<select name="hotelfacilities" id="accommFac" data-placeholder="Accommodation Facility" class="chosen-select" multiple style="width:200px;" tabindex="4">
							@foreach(Facility::getStandardFacilities() as $fac)<option value="{{ $fac->id }}">{{ $fac->title }}</option>@endforeach
							</select>
						</dd>
						<!--//Hotel facilities-->

						<!--Room facilites-->
						<dt>Room facilites</dt>
						<dd>
							<select name="roomfacilities" id="roomFac" data-placeholder="Room Facility" class="chosen-select" multiple style="width:200px;" tabindex="4">
							@foreach(Facility::getRoomFacilities() as $fac)<option value="{{ $fac->id }}">{{ $fac->title }}</option>@endforeach
							</select>
						</dd>
						<!--//Room facilites-->

						<dt>Sport & Recreation facilities</dt>
						<dd>
							<select name="srFac" id="srFac" data-placeholder="S&R Facility" class="chosen-select" multiple style="width:200px;" tabindex="4">
							@foreach(Facility::getSportAndRecreationFacilities() as $fac)<option value="{{ $fac->id }}">{{ $fac->title }}</option>@endforeach
							</select>
						</dd>

					</dl>
                    
				</article>
             </form>
			</aside>
			<!--//sidebar-->
			
			<!--three-fourth content-->
			<section class="three-fourth">
				<div class="sort-by">
					<h3>Sort by</h3>
					<ul class="sort">
						
						<li>Price <a href="javascript:void(0);" onclick="priceFilter('','asc')" title="ascending" class="ascending">ascending</a><a href="javascript:void(0);" onclick="priceFilter('','desc')"  title="descending" class="descending">descending</a></li>
                        <li>Star <a href="javascript:void(0);" onclick="starRateSearch('','asc')" title="ascending" class="ascending">ascending</a><a href="javascript:void(0);" onclick="starRateSearch('','desc')" title="descending" class="descending">descending</a></li>
						<li>Rating <a href="javascript:void(0);" onclick="userRateVal('','asc')" title="ascending" class="ascending">ascending</a><a href="javascript:void(0);" onclick="userRateVal('','desc')" title="descending" class="descending">descending</a></li>
					</ul>

					<?php /*?><ul class="view-type">
						<li class="location-view"><a href="#" title="location view">location view</a></li>
					</ul><?php */?>
				</div>

				<div class="deals clearfix">
                
                
             
					@foreach($result as $entry)
					<article class="one-fourth promo">
						<?php /*?><div class="ribbon-small">- {{rand(30,89)}}%</div><?php */?>
                        
						<figure>
							@if($entry->fileName!=NULL)
                                @if(HTML::image("https://rqphoto.s3-ap-southeast-1.amazonaws.com/268_$entry->fileName"))
                                   <img alt width="270" height="152" src="https://rqphoto.s3-ap-southeast-1.amazonaws.com/268_{{ $entry->fileName }}"/>
                                @else
                                  <img alt width="270" height="152" src="http://dummyimage.com/270x152/d6d2d6/000&text=No+Image"/>
                                @endif
								
							@else
								 <img alt width="270" height="152" src="http://dummyimage.com/270x152/d6d2d6/000&text=No+Image"/>
							@endif
						</figure>
						<div class="details">
							<h1>@if(strlen($entry->name) > 20)
                            	{{substr($entry->name,0,20)}}...
                            @else
                            	{{$entry->name}}
                            @endif
                            <br />
                             <span class="starOuter">
								<span class="stars">
									@for($i = 0; $i < $entry->starRating; $i++)
									<img src="/static/pillo/images/ico/star.png" alt="">
									@endfor
								</span>
                                </span>
							</h1>
                            
							<span class="address">{{$entry->city}}, {{$entry->province}}</span>
							<span class="rating">{{$entry->starRating}}</span>
							<span class="price" style="text-align: justify;min-height:110px">
                            @if($entry->hotelDescription!=NULL)
                            @if(strlen($entry->hotelDescription) > 150)
                                {{substr($entry->hotelDescription,0,150)}} ...
                             @else
                             	{{$entry->hotelDescription}}
                             @endif 
                        @else 
                        	Overlooking the Aqueduct and Nature Park, Lorem Ipsum Hotel is situated 5 minutes' walk from London's Zoo logical Gardens and a metro station. Click for more info. 
                        @endif
                        </span>
							<a href="{{URL('/hotel/'.$entry->id)}}" title="Book now" class="gradient-button yellow">Browse</a>
						</div>
					</article>
					@endforeach	
                

					<!--bottom navigation-->
					<div class="bottom-nav">
						<!--back up button-->
						<?php /*?><a href="#" class="scroll-to-top" title="Back up">Back up</a> <?php */?>
						<!--//back up button-->

						<!--pager-->
					<!--	<div class="pager">
							<span class="first"><a href="#">First page</a></span>
							<span><a href="#">&lt;</a></span>
							<span class="current">1</span>
							<span><a href="#">2</a></span>
							<span><a href="#">3</a></span>
							<span><a href="#">4</a></span>
							<span><a href="#">5</a></span>
							<span><a href="#">6</a></span>
							<span><a href="#">7</a></span>
							<span><a href="#">8</a></span>
							<span><a href="#">&gt;</a></span>
							<span class="last"><a href="#">Last page</a></span>
                            
                            
						</div>-->
                        
                        <div class="paging2">{{$result->appends(array())->links()}}
                        </div>
						<!--//pager-->
					</div>
					<!--//bottom navigation-->
				</div>
			</section>
			<!--//three-fourth content-->
		</div>
		<!--//main content-->
	</div>
</div>

@stop

@section('inlineJS')
    $(".chosen-select").chosen({no_results_text: "Oops, nothing found!"}); 
@stop

<!--$2y$10$2GjLasH14sRGJnNDIDmR4OgAUueFl7O0rJ/hy0hOLibAmW911scs2-->