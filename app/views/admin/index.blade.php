@extends('templates.metronic')

@section('title')
Super Admin
@stop

@section('sidebar')
@include('admin.adminSideMenu')
@stop

@section('pageAssets')

@stop


@section('pageAsset2')
@stop

@section('content')
@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title">Master Panel</h3>
		<hr>
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon icon-reorder"></i> System Statistic
				</div>	
			</div>
			<div class="portlet-body">
				<table class="table table-bordered table-striped">
					<tbody>
						<tr>
							<td>
								<b>New registration of the week:</b> 
							</td>
							<td>
								<b>Total registration:</b> 
							</td>
						</tr>
						<tr>
							<td>
								<b>Pending Approval:</b> 
							</td>
							<td>
								<b>Disapproved Hotel:</b> 
							</td>
						</tr>
						<tr>
							<td>
								<b>Total Rooms:</b> 
							</td>
							<td>
								<b>Total Vouchers:</b> 
							</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop