@extends('templates.metronic')

@section('title')
Hotel Listing Report
@stop

@section('sidebar')
@include('admin.adminSideMenu')
@stop

@section('pageAssets')
{{ HTML::style('static/metronic/css/print.css') }}
{{ HTML::style('static/metronic/plugins/rateit/rateit.css') }}

@stop

@section('pageAssets2')
{{ HTML::script('static/metronic/plugins/rateit/jquery.rateit.min.js')}}
{{ HTML::script('static/ckeditor/ckeditor.js')}}

<script>
	$(document).ready(function(){
	CKEDITOR.replace( 'content' );
	});
	</script>
    
@stop



@section('content')
@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif

<div class="row-fluid">
	<div class="span12">
	<!-- <h3 class="page-title">Generate Hotel Listing Report</h3>
	<hr> -->
	
	<form action="{{URL::route('update' ,array('id' => $page[0]->id))}}" id="page-form" method="post">
 
    <div class="f-item">
									<label for="message">Page Title</label>
									<input type="text" id="title" name="title" required="required" value="{{$page[0]->title}}">
                                   @if($errors->has('title'))			
                                        @foreach($errors->get('title') as $message)
                                        <div class="help-inline" style="color:red">
                                            {{$message}}
                                        </div>
                                        @endforeach
                                        @endif
								</div>
    
    
    <div class="f-item">
									<label for="message">Page Content</label>
									<textarea id="content" name="content" rows="10" cols="10" required="required">{{$page[0]->content}}</textarea>
                                     @if($errors->has('content'))			
                                    @foreach($errors->get('content') as $message)
                                    <div class="help-inline" style="color:red">
                                        {{$message}}
                                    </div>
                                    @endforeach
                                    @endif
                                  
								</div>
                                
   <div class="f-item">
									<input type="submit" value="Save" id="submit" name="submit" class="gradient-button" />
                                  
								</div>
                                
     </form>
		
	</div>
</div>

<div class="printbtn"></div>
@stop