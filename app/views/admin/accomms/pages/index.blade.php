@extends('templates.metronic')

@section('title')
Pages
@stop

@section('sidebar')
@include('admin.adminSideMenu')
@stop

@section('pageAssets')
{{ HTML::style('static/metronic/css/print.css') }}
{{ HTML::style('static/metronic/plugins/rateit/rateit.css') }}

@stop

@section('pageAssets2')
{{ HTML::script('static/metronic/plugins/rateit/jquery.rateit.min.js')}}
@stop



@section('content')
@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif

<?php //echo "<pre>"; print_r($page); ?>


<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title">Pages</h3>
        
		<hr>
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon icon-reorder"></i> Pages
				</div>
			</div>
			<div class="portlet-body">
				<table class="table">
					<thead>
						<tr>
							<td>Sr.No.</td>
							<td>Title</td>
							<td>Content</td>
							<td>Action</td>
						</tr>
					</thead>
					<tbody>
						@if(count($page) > 0)
                        <?php $i = 1; ?>
						@foreach($page as $pages)
						<tr>
							<td>
								
									{{$i}}
								
							</td>
							<td>{{$pages->title}}</td>
							<td>
								{{strip_tags(substr($pages->content,0,80))}}...
							</td>
							<td>
								<a href="{{URL::route('edit-page' ,array('id' => $pages->id))}}" class="btn blue">
									Edit
									
								</a>
								<?php /*?><a onclick="deletePage(this.id)" id="{{URL::route('delete-page',array('id' => $pages->id))}}"  class="btn red">
									Delete
								</a><?php */?>
							</td>
						</tr>
                        <?php $i++;?>
						@endforeach	
						@else
						<tr>
							<td>There is no any pages.</td>
							<td>-</td>
							<td>-</td>
							<td>-</td>
						</tr>
						@endif	
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<?php /*?><div class="row-fluid"><a href="{{URL::route('createPage')}}">Add Page</a></div><?php */?>
<div class="printbtn"></div>

@stop
@section('inlineJS')
<script>
function deletePage(url){
	
	var r = confirm("Are you sure, you want to delete page?");
	
		if (r == true)
		  {
		 	window.location=url;
		  }
		else
		  {
		  	return false;
		  }
	}
</script>
@stop