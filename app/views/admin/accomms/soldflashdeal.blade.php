@extends('templates.metronic')

@section('title')
Hotel Listing Report
@stop

@section('sidebar')
@include('admin.adminSideMenu')
@stop

@section('pageAssets')
{{ HTML::style('static/metronic/css/print.css') }}
{{ HTML::style('static/metronic/plugins/rateit/rateit.css') }}

@stop

@section('pageAssets2')
{{ HTML::script('static/metronic/plugins/rateit/jquery.rateit.min.js')}}
@stop



@section('content')
@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
	<!-- <h3 class="page-title">Generate Hotel Listing Report</h3>
	<hr> -->
	
	<table class="table">
		<thead>
			<tr>
				<th>Flash Deal Name</th>
                <th>Room</th>
                <th>Hotel</th>
                <th>Buyer Name</th>
                <th>Buyer Email</th>							
                <th>Amount</th>
                <th>Status</th>
                <th>Created_at</th>
			</tr>
		</thead>
		<tbody>
			 @foreach($flashdeals as $flashdeal)
                   <tr>
                   		<td>{{$flashdeal->flashDealName}}</td>
                        <td>{{$flashdeal->roomName}}</td>
                        <td>{{$flashdeal->accommName}}</td>
                        <td>{{$flashdeal->firstName}}&nbsp;{{$flashdeal->lastName}}</td>
                        <td>{{$flashdeal->email}}</td>
                         <td>{{$flashdeal->price}}</td>
                        <td>{{$flashdeal->paymentStatus}}</td>
                        <td>{{$flashdeal->created_at}}</td>
                     </tr>
                   @endforeach    
			</tbody>
		</table>
		
	</div>
</div>
<div class="printbtn">

</div>
@stop