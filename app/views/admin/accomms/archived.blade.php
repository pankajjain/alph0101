@extends('templates.metronic')

@section('title')
Archived Applications
@stop

@section('sidebar')
@include('admin.adminSideMenu')
@stop

@section('pageAssets')

@stop


@section('pageAsset2')
@stop

@section('content')
@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title">Administration Panel</h3>
		<hr>
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon icon-reorder"></i> Archived List
				</div>
			</div>
			<div class="portlet-body">
				<form class="sidebar-search pull-right">
						<div class="input-box">
							<a href="javascript:;" class="remove"></a>
							<input type="text" class="search" placeholder="Search..." />
							<input type="text" class="filter" placeholder="Status Filter" />
						</div>
					</form>
					
				<table class="table table-striped table-bordered ">
					<tbody>
						@if(Accomm::where('isVerified','>','0')->get()->count())
						@foreach(Accomm::where('isVerified','>','0')->orderBy('updated_at', 'DESC')->get() as $accomm)
						<tr>
							<td>
								<a href="{{URL::route('viewAccommodation',array('accomm' => $accomm->getKey()))}}"  class="accomnamelink" >
									{{ucwords($accomm->name)}}
								</a>
							</td>
							<td>
								<?php 
									switch($accomm->isVerified){
										case 0:
											echo "<span class='label label-info'>New Applicant</span>";
											break;
										case 1:
											echo "<span class='label label-success'>Approved</span>";
											break;
										case 2:
											echo "<span class='label label-important'>Failed</span>";
											break;
										case 3:
											echo "<span class='label label-inverse'>Deleted</span>";
											break;
										}
									?>
							</td>
						</tr>
						@endforeach	
						@else
						<tr>
							<td>There is no processed accommodation</td>
							<td>-</td>
						</tr>
						@endif	
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop

@section('inlineJS')
<script>
    $(".search").keyup(function() {
        var value = this.value.toLowerCase().trim();

        $(".table").find("tr").each(function(index) {
            var id = $(this).find("td").first().text().toLowerCase().trim();
            $(this).toggle(id.indexOf(value) !== -1);
        });
    });

    $(".filter").keyup(function() {
        var value = this.value.toLowerCase().trim();
        $(".table").find("tr").each(function(index) {
            var id = $(this).find("td").last().text().toLowerCase().trim();
            $(this).toggle(id.indexOf(value) !== -1);
        });
    });
</script>
@stop