@extends('templates.metronic')

@section('title')
Users Listing
@stop

@section('sidebar')
@include('admin.adminSideMenu')
@stop

@section('pageAssets')

@stop


@section('pageAsset2')
@stop

@section('content')
@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title">Administration Panel</h3>
		<hr>
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon icon-reorder"></i> Users List
				</div>
			</div>
			<div class="portlet-body">
				<form class="sidebar-search pull-right">
						<div class="input-box">
							<a href="javascript:;" class="remove"></a>
							<input type="text" class="search" placeholder="Search Email..." />
							<!-- <input type="text" class="filter" placeholder="Status Filter" /> -->
						</div>
					</form>
					
				<table class="table table-striped table-bordered ">
					<tbody>
						@if(User::get()->count())
						@foreach(User::orderBy('updated_at', 'DESC')->get() as $user)
						<tr>
							<td>
									{{ucwords($user->email)}}
							</td>
							<td>
									{{ucwords($user->firstName)}} 
									{{ucwords($user->lastName)}}
							</td>
							<td>
								<?php 
									switch($user->typeID){
										case 1:
											echo "Partner";
											break;
										case 2:
											echo "Admin";
											break;
										case 3:
											echo "Public";
											break;
										}
									?>
							</td>
						</tr>
						@endforeach	
						@else
						<tr>
							<td>There is no registered user</td>
							<td>-</td>
						</tr>
						@endif	
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop

@section('inlineJS')
<script>
    $(".search").keyup(function() {
        var value = this.value.toLowerCase().trim();

        $(".table").find("tr").each(function(index) {
            var id = $(this).find("td").first().text().toLowerCase().trim();
            $(this).toggle(id.indexOf(value) !== -1);
        });
    });

    $(".filter").keyup(function() {
        var value = this.value.toLowerCase().trim();
        $(".table").find("tr").each(function(index) {
            var id = $(this).find("td").last().text().toLowerCase().trim();
            $(this).toggle(id.indexOf(value) !== -1);
        });
    });
</script>
@stop