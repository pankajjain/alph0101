@extends('templates.metronic')

@section('title')
Room Listing
@stop

@section('sidebar')
@include('admin.adminSideMenu')
@stop

@section('pageAssets')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnm6ykLCH25kF6s7kl_CJ4Red1CcrtrhI&sensor=false"></script>
@stop


@section('pageAssets2')
<link rel="stylesheet" href="/static/metronic/plugins/rateit/rateit.css">
<script src="/static/metronic/plugins/rateit/jquery.rateit.min.js"> </script>
@stop


@section('content')
@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
		<div class="clearfix">
			<h3 class="page-title " style="margin-bottom:0px;">{{ucwords($accomm->name)}}</h3> 

		</div>	
		<hr>
	</div>
</div>
<div class="row-fluid">
	<div class="span6">
		<div class="well" style="min-height:260px;">
			<h3>Basic Information</h3>
			<table class="table">
				<tr>
					<td>Partner Name</td>
					<td>{{ucwords($accomm->user->getFullName())}}</td>
				</tr>
				<tr>
					<td>Accommodation Type</td>
					<td>{{$accomm->accommType->title}}</td>
				</tr>
				<tr>
					<td>Number of rooms</td>
					<td>{{$accomm->numberOfRooms}}</td>
				</tr>
				<tr>
					<td>Star Rating</td>
					<td><div class="rateit" style="" data-rateit-value="{{$accomm->starRating}}"  data-rateit-readonly="true"  ></td>
				</tr>
				<tr>
					<td>Agreement Letter</td>
					<td>
						<div class="btn-group">
							<button id="btnGroupVerticalDrop1" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
								Download
								<i class="icon-arrow-down"></i>
							</button>
							<ul class="dropdown-menu" role="menu" aria-labelledby="btnGroupVerticalDrop1">
								@foreach($accomm->getVerificationDocuments() as $document)
								<li><a href="{{URL::route('downloadVF',array('id' => $document->encryptedKey()))}}">{{$document->getShortName()}}</a></li>
								@endforeach
							</ul>
						</div>
					</td>
				</tr>
			</table>	
		</div>
		


	</div>
	<div class="span6">
		<div class="well" style="min-height:260px;">
			<h3>Contact Details</h3>
			<table class="table">
				<tr>
					<td>Main Contact Name</td>
					<td>{{ucwords($accomm->mainContactName)}}</td>
				</tr>
				<tr>
					<td>Contact Role</td>
					<td>{{$accomm->mainContactRole}}</td>
				</tr>
				<tr>
					<td>Contact Number</td>
					<td>{{$accomm->mainContactMobile}}</td>
				</tr>
				<tr>
					<td>Email Address</td>
					<td>{{$accomm->mainContactEmail}}</td>
				</tr>
				<tr>
					<td>Last Update</td>
					<td>{{$accomm->created_At('d F Y')}}</td>
				</tr>
			</table>	
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span6">
		<div class="well" style="min-height:420px;">
			
			<div class="span3"> <b>Current Status:</b> <br>
				<?php 
				switch($accomm->isVerified){
					case 0:
					echo "<span class='label label-info'>New Applicant</span>";
					break;
					case 1:
					echo "<span class='label label-success'>Approved</span>";
					break;
					case 2:
					echo "<span class='label label-important'>Disapproved</span>";
					break;
					case 3:
					echo "<span class='label label-inverse'>Deleted</span>";
					break;
				}
				?>
			</div>
			<div class="span3" style="text-align: right;float: right;">
				<a href="{{URL::route('approveAccommodation' ,array('id' => $accomm->encryptedKey()))}}" class="btn blue">
					<i class="icon-check"></i> Approve
				</a>
					<!-- <a href="URL::route('sudeleteAccommodation' ,array('id' => $accomm->encryptedKey()))" class="btn grey">
						<i class="icon-trash"></i> Delete
					</a> -->
				</div>
				<br><br>
				<hr>
				{{Form::model($accomm, array('route' => array('disapproveAccommodation', $accomm->id)))}}
				<div class="control-group">
					{{Form::label('remarks',"Remarks",array('class'=>'control-label'))}}<br>
					<div class="controls">
						{{Form::textarea('remarks',null,array('placeholder' => 'Reason of disapprove', 'class' => 'm-wrap span12','rows' => '10' , 'style' => 'width:100%'))}}
						@if($errors->has('remarks'))
						<div class="formErrors">
							<ul>
								@foreach($errors->get('remarks') as $message)
								<li>
									{{$message}}
								</li>
								@endforeach
							</ul>
						</div>
						@endif
					</div>
				</div>
				<button type="submit" class="btn red" style="float: right;"><i class="icon-warning-sign"></i> Disapprove</button>
				{{Form::close()}}
			</div>
		</div>

	<!-- <div class="span6">
		<div class="well" style="min-height:420px;">
			foreach($accomm->getVerificationDocuments() as $document)
				<img src="URL::route('downloadVF',array('id' => $document->encryptedKey()))"/>
			endforeach
		</div>
	</div> -->
	<div class="span6 well">
		<div>
			<address>
				<strong>Address : </strong> {{$accomm->streetAddress}}<br>
				{{$accomm->city}}, {{$accomm->province}}, {{$accomm->getCountry()}}
			</address>
		</div>
		<div id="mapWrapper" style=" height: 360px;margin: 0px; padding: 0px;width:100%;float: right;">
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span12 well">
		<h3>Rooms</h3>
		<table class="table table-condensed">
			<thead>
				<tr>
					<th>Room Name</th>
					<th>Bed Type</th>
					<th>Max Guests</th>
					<th>Price</th>
				</tr>
			</thead>
			@if($accomm->rooms()->count() > 0)
			@foreach(Room::published($accomm->getKey()) as $room)
			<tr>
				<td>{{$room->name}}</td>
				<td>{{$room->bedType}}</td>
				<td>{{$room->maxGuestsAllowed}}</td>
				<td>{{$room->originalPrice}}</td>
			</tr>
			@endforeach
			@else
			<tr>
				<td>No room for '{{ucwords($accomm->name)}}'</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>

			@endif
		</table>
	</div>
</div>

<div class="row-fluid">
	<div class="span12 well">
		<h3>Vouchers</h3>
		<table class="table table-condensed">
			<thead>
				<tr>
					<th>Voucher room</th>
					<th>Original room price</th>
					<th>Discounted price</th>
					<th>Discount percentage</th>
					<th>Valid till</th>
				</tr>
			</thead>
			@if(!$accomm->vouchers()->count())
			<tr>
				<td>No voucher yet.</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			@else
			@foreach($accomm->vouchers()->get() as $voucher)
			<tr>
				<td>{{ucwords(Room::find($voucher->roomID)->name)}}</td>
				<td>{{$voucher->originalPrice}}</td>
				<td>{{$voucher->discountedPrice}}</td>
				<td>{{ceil($voucher->discountPercentage)}}%</td>
				<td>{{date('d M Y',strtotime($voucher->redemptionPeriodTo))}}</td>
			</tr>
			@endforeach
			@endif
		</table>
	</div>
</div>

<div class="row-fluid">
	<div class="span12 well">
		<h3>Flash Deal</h3>
		<table class="table table-condensed">
			<thead>
				<tr>
					<th>Flash deal room</th>
					<th>Original room price</th>
					<th>Discounted price</th>
					<th>Discount percentage</th>
					<th>Valid till</th>
				</tr>
			</thead>
			@if(!$accomm->flashdeals()->count())
			<tr>
				<td>No flashdeal yet.</td>
				<td></td>
				<td></td>
				<td></td>
				<th></th>
			</tr>
			@else
			@foreach($accomm->flashdeals()->get() as $flashdeal)
			<tr>
				<td>{{ucwords(Room::find($flashdeal->roomID)->name)}}</td>
				<td>{{$flashdeal->originalPrice}}</td>
				<td>{{$flashdeal->discountedPrice}}</td>
				<td>{{ceil($flashdeal->discountPercentage)}}%</td>
				<td>{{date('d M Y',strtotime($flashdeal->redemptionPeriodTo))}}</td>
			</tr>
			@endforeach
			@endif
		</table>
	</div>
</div>
@stop

@section('inlineJS')
<script>
function initialize() {
	@if(!empty($accomm->lat))
	var myLatlng = new google.maps.LatLng({{$accomm->lat}},{{$accomm->lng}});
	defaultZoom = 17;
	@else
	var myLatlng = new google.maps.LatLng(-25.363882,131.044922);
	defaultZoom = 8;
	@endif

	var mapOptions = {

		center: myLatlng,
		zoom: defaultZoom,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	document.map = new google.maps.Map(document.getElementById("mapWrapper"), mapOptions);


	@if(!empty($accomm->lat))
	document.marker = new google.maps.Marker({
		position: new google.maps.LatLng({{$accomm->lat}}, {{$accomm->lng}}),
		map: document.map,
		title: '{{$accomm->name}}',
		animation: google.maps.Animation.DROP
	});
	@endif

}

google.maps.event.addDomListener(window, 'load', initialize);

</script>
@stop
@stop