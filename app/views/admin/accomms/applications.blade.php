@extends('templates.metronic')

@section('title')
Hotel Applications
@stop

@section('sidebar')
@include('admin.adminSideMenu')
@stop

@section('pageAssets')

@stop


@section('pageAsset2')
@stop

@section('content')
@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title">Administration Panel</h3>
		<hr>
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon icon-reorder"></i> Pending List
				</div>
			</div>
			<div class="portlet-body">
				<table class="table">
					<thead>
						<tr>
							<td>Accommodation</td>
							<td>Created</td>
							<td>Documents</td>
							<td>Action</td>
						</tr>
					</thead>
					<tbody>
						@if(Accomm::where('isVerified','0')->get()->count())
						@foreach(Accomm::where('isVerified','0')->get() as $accomm)
						<tr>
							<td>
								<a href="{{URL::route('viewAccommodation',array('accomm' => $accomm->getKey()))}}"  class="accomnamelink" >
									{{ucwords($accomm->name)}}
								</a>
							</td>
							<td>{{$accomm->created_at('d F Y')}}</td>
							<td>
								<div class="btn-group">
									<button id="btnGroupVerticalDrop1" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
										Download
										<i class="icon-arrow-down"></i>
									</button>
									<ul class="dropdown-menu" role="menu" aria-labelledby="btnGroupVerticalDrop1">
										@foreach($accomm->getVerificationDocuments() as $document)
										<li><a href="{{URL::route('downloadVF',array('id' => $document->encryptedKey()))}}">{{$document->getShortName()}}</a></li>
										@endforeach
									</ul>
								</div>
							</td>
							<td>
								<a href="{{URL::route('approveAccommodation' ,array('id' => $accomm->encryptedKey()))}}" class="btn blue">
									Pass
									<i class="icon-check"></i>
								</a>
								<a href="{{URL::route('viewAccommodation',array('accomm' => $accomm->getKey()))}}" class="btn red">
									Fail
									<i class="icon-warning-sign"></i>
								</a>
							</td>
						</tr>
						@endforeach	
						@else
						<tr>
							<td>There is no unverified accommodation</td>
							<td>-</td>
							<td>-</td>
							<td>-</td>
						</tr>
						@endif	
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop