<div class="page-sidebar nav-collapse collapse"> 
	<ul class="page-sidebar-menu">
		<li>
			<div class="sidebar-toggler hidden-phone"></div>
		</li>
		<li>
			<br>
		</li>
		<li class="start {{Request::is('admin')? ' active':''}}">
			<a href="{{URL::route('adminIndex')}}">
				<i class="icon-home"></i> 
				<span class="title">Dashboard</span>
				<span class="selected"></span>
			</a>
		</li>
		<li
			{{Request::is('admin/applications')? 'class="active"':''}}
			{{Request::is('admin/archived')? 'class="active"':''}}
			{{(Request::url() == URL::route('users')) ? 'class="active"' : ''}}
			{{(Request::url() == URL::route('activityLog')) ? 'class="active"' : ''}}
		>
			<a href="javascript:;">
				<i class="icon-home"></i> 
				<span class="title">Admin Panel</span>
				<span class="arrow "></span>
			</a>
			<ul class="sub-menu">
				<li {{Request::is('admin/applications')? 'class="active"':''}}>
					<a href="{{URL::route('applications')}}">Applications</a>
				</li>
				<li {{Request::is('admin/archived')? 'class="active"':''}}>
					<a href="{{URL::route('archived')}}">Archived</a>
				</li>
				<li {{(Request::url() == URL::route('users'))? 'class="active"':''}}>
					<a href="{{URL::route('users')}}">Users</a>
				</li>
                
                <li {{(Request::url() == URL::route('pages'))? 'class="active"':''}}>
					<a href="{{URL::route('pages')}}">Pages</a>
				</li>
                
                <li {{(Request::url() == URL::route('soldvoucher'))? 'class="active"':''}}>
					<a href="{{URL::route('soldvoucher')}}">Sold Vouchers</a>
				</li>
                <li {{(Request::url() == URL::route('soldflashdeal'))? 'class="active"':''}}>
					<a href="{{URL::route('soldflashdeal')}}">Sold Flash Deals</a>
				</li>
				<li {{(Request::url() == URL::route('activityLog'))? 'class="active"':''}}>
					<a href="{{URL::route('activityLog')}}">Activity Log</a>
				</li>
			</ul>
		</li>
		<li
			{{Request::is('admin/listing')? 'class="active"':''}}
			{{Request::is('admin/summary')? 'class="active"':''}}
			{{Request::is('admin/getlisting')? 'class="active"':''}}
			{{Request::is('admin/getsummary')? 'class="active"':''}}
		>
			<a href="javascript:;">
				<i class="icon-home"></i> 
				<span class="title">Reporting</span>
				<span class="arrow "></span>
			</a>
			<ul class="sub-menu">
				<li {{Request::is('admin/getlisting')? 'class="active"':''}}{{Request::is('admin/listing')? 'class="active"':''}}>
					<a href="{{URL::route('suReportHotelListing')}}">Hotel Listing</a>
				</li>
				<li {{Request::is('admin/getsummary')? 'class="active"':''}}{{Request::is('admin/summary')? 'class="active"':''}}>
					<a href="{{URL::route('suReportHotelSummary')}}">Hotel Summary</a>
				</li>
                <li {{Request::is('admin/getuserreview')? 'class="active"':''}}{{Request::is('admin/reviewsrating')? 'class="active"':''}}>
					<a href="{{URL::route('userReviewRating')}}">User Reviews & Rating</a>
				</li>
			</ul>
		</li>

		<li 
			{{Request::is('admin/settings')? 'class="active"':''}}
		>
			<a href="{{URL::route('appsettings')}}">
				<i class="icon-building"></i> 
				<span class="title">System Settings</span>
				<span class="arrow "></span>
			</a>
		</li>

	</ul>
</div>