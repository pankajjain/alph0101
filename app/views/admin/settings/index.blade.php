@extends('templates.metronic')

@section('title')
Room Listing
@stop

@section('sidebar')
@include('admin.adminSideMenu')
@stop

@section('pageAssets')

@stop


@section('pageAsset2')
@stop

@section('content')
@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title">Administration Panel</h3>
		<hr>
		<h4>Room Quickly Settings</h4>

		<br>

		<div class="tabbable-custom ">
			<ul class="nav nav-tabs ">
				<li class="active"><a href="#tab_5_1" data-toggle="tab">Agreement Letter</a></li>
				<li class=""><a href="#tab_5_4" data-toggle="tab">Currency Markup</a></li>
				<li class=""><a href="#tab_5_2" data-toggle="tab">Flash Deals</a></li>
				<li class=""><a href="#tab_5_3" data-toggle="tab">Vouchers</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tab_5_1">
					<div class="container">
						@include('admin.settings.tabs.agreements')
					</div>
				</div>
				<div class="tab-pane" id="tab_5_4">
					<div class="container">
						@include('admin.settings.tabs.currency')
					</div>
				</div>
				<div class="tab-pane" id="tab_5_2">
					@include('admin.settings.tabs.flashDeals')
				</div>
				<div class="tab-pane" id="tab_5_3">
					<p>Vouchers</p>
					<p>
						Comming Soon
					</p>
					
				</div>
			</div>
		</div>

	</div>
</div>
@stop