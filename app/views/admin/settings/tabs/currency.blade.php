<?php
$markUp = DB::table('configs')->where('key', "CurrencyMarkup")->select('value')->first();
$markUp = $markUp->value;
$USD = DB::table('currency')->find(1);
$USD = $USD->value;
$MYR = DB::table('currency')->find(22);
$MYR = $MYR->value;
?>

{{Form::open(array('class'=>'form-horizontal','route'=>'updateCurrency'))}}
<div class="control-group">
	{{Form::label('markup','Currency Markup',array('class'=>'control-label'))}}
	<div class="controls">
		{{Form::text('markup',$markUp,array('placeholder' => '0.04', 'class' => 'm-wrap span12'))}}
		@if($errors->has('markup'))
		<div class="formErrors">
			<ul>
				@foreach($errors->get('markup') as $message)
				<li>
					{{$message}}
				</li>
				@endforeach
			</ul>
		</div>
		@endif
		<p class="help-block">Update the currency rates from yahoo! with markup the calculation for the currency.</p>
		<p><b>Current Rate (RM 100):</b>
			<br>MYR: @currency(100, 'MYR')
			<br>USD: @currency(100, 'USD')
			<br>SGD: @currency(100, 'SGD') 
		</p>

	</div>
</div>
{{form::submit("Update",array('class'=>'btn btn-primary pull-right'))}}
{{Form::close()}}