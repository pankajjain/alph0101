<?php
$Ver = Configs::find(6);
$Ver = $Ver->value;
?>

{{Form::open(array('class'=>'form-horizontal','route'=>'uploadVeridoc','files'=>true))}}
<div class="control-group">
	{{Form::label('files','Agreement Letter',array('class'=>'control-label'))}}
	<div class="controls">
		{{ Form::file('files',(array('class' => 'fileupload')))}}    
		@if($errors->has('files'))
		<div class="formErrors">
			<ul>
				@foreach($errors->get('files') as $message)
				<li>
					{{$message}}
				</li>
				@endforeach
			</ul>
		</div>        
		@endif
		<p class="help-block">Please note that maximum file size is 2MB.<br>
		<a href="{{$Ver}}" target="_blank">Download current agreement letter</a></p>

	</div>
</div>
{{form::submit("Upload",array('class'=>'btn btn-primary'))}}
{{Form::close()}}