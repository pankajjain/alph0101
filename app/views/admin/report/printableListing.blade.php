@extends('templates.metronic')

@section('title')
Hotel Listing Report
@stop

@section('sidebar')
@include('admin.adminSideMenu')
@stop

@section('pageAssets')
{{ HTML::style('static/metronic/css/print.css') }}
{{ HTML::style('static/metronic/plugins/rateit/rateit.css') }}

@stop

@section('pageAssets2')
{{ HTML::script('static/metronic/plugins/rateit/jquery.rateit.min.js')}}
@stop

@section('topNavs')
    {{$result->appends(array('hotelStatus' => $hs, 'hotelCountry' => $b, 'hotelState' => $c, 'yearRangeStart' => $d, 'monthRangeStart' => $e, 'yearRangeEnd' => $f, 'monthRangeEnd' => $g ))->links()}}
<a onClick="window.print()" class="btn blue">Print</a>
@stop

@section('content')
@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
	<!-- <h3 class="page-title">Generate Hotel Listing Report</h3>
	<hr> -->
	@if(count($result) < 1)
		<h2>No results matching criteria. Please search again.</h2>
		<a href="{{ URL::previous() }}" class="btn">Reset search criteria</a>
	@else 
	<table class="table">
		<thead>
			<tr>
				<td>Name</td>
				<td>Type</td>
				<td>Rate</td>
				<td>Room</td>
				<td>Location</td>
				<td>Contacts</td>
				<td>Person</td>
			</tr>
		</thead>
		<tbody>
			@foreach($result as $accomm)
			<tr>
				<td>
					{{ucwords($accomm->name)}}
				</td>
				<?php $accommType = AccommType::find($accomm->typeID) ?>
				<td>
					{{ucwords($accommType->title)}}
				</td>
				<td>
					<div class="rateit" data-rateit-value="{{$accomm->starRating}}" data-rateit-resetable="false" data-rateit-ispreset="true" data-rateit-readonly="true"/>
					</td>
					<td>
						{{$accomm->numberOfRooms}}
					</td>
					<td>
						{{$accomm->streetAddress}}<br>{{$accomm->city}}, {{$accomm->zipCode}}<br>{{$accomm->province}}, {{$accomm->country}}
					</td>
					<td>
						Phone: {{$accomm->contactNumber}}<br>Fax: {{$accomm->fax}}<br>Web: {{$accomm->website}}
					</td>
					<td>
						{{$accomm->mainContactMobile}} ({{$accomm->mainContactName}})<br>{{$accomm->mainContactEmail}} ({{$accomm->mainContactRole}})
					</td>

				</tr>
				@endforeach	
			</tbody>
		</table>
		@endif
	</div>
</div>
<div class="printbtn">

</div>
@stop