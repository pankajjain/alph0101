@extends('templates.metronic')

@section('title')
Hotel Listing Report
@stop

@section('sidebar')
@include('admin.adminSideMenu')
@stop

@section('pageAssets')
{{ HTML::style('static/metronic/css/print.css') }}
{{ HTML::style('static/metronic/plugins/rateit/rateit.css') }}

@stop

@section('pageAssets2')
{{ HTML::script('static/metronic/plugins/rateit/jquery.rateit.min.js')}}
@stop

@section('topNavs')
   
<a onClick="window.print()" class="btn blue">Print</a>
@stop

@section('content')
@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<style>

.pos-nag{ margin:auto;}
.pos-nag span{ float:left;;;  margin-right:5px;text-align:center; padding:3px 15px}
span.positive{ background:#cbffcb; color:#309664; border-radius:4px 0 0 4px !important;}
span.negative{ background:#ffdddd; color:#ce281c; border-radius:0 4px 4px 0 !important;}
</style>


<div class="row-fluid">
	<div class="span12">
	<!-- <h3 class="page-title">Generate Hotel Listing Report</h3>
	<hr> -->
    
	
	<table class="table">
		<thead>
			<tr>
            				<th>Hotel</th>
                        	
                            <th>Rating</th>
                            <th>Positive review </th>
                            <th>Negative review </th>
                           
                        </tr>
                      </thead>
                      <tbody> 
						@foreach($review as $rev)
                       
                            <tr>
                            <td >{{$rev->name}}</td>
                          
                            <td>{{round($rev->rat/$rev->ratcount)}}</td>
                            <td>
                            	<div class="pos-nag">
            <span class="positive">+{{  DB::table('reviewsrating')->where('accommID',$rev->accommID)->where('positiveReviews', '<>', '')->count('negativeReviews') }}</span></div>
            
      						</td>
                            <td>
                            	<div class="pos-nag">
            
            <span class="negative">-{{  DB::table('reviewsrating')->where('accommID',$rev->accommID)->where('negativeReviews', '<>', '')->count('negativeReviews') }}</span>
      							</div>
                             </td>
                           
							</tr>
						@endforeach	
			</tbody>
		</table>
		
	</div>
</div>
<div class="printbtn">

</div>
@stop