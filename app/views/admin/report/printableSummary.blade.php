@extends('templates.metronic')

@section('title')
Hotel Listing Report
@stop

@section('sidebar')
@include('admin.adminSideMenu')
@stop

@section('pageAssets')
{{ HTML::style('static/metronic/css/print.css') }}
{{ HTML::style('static/metronic/plugins/rateit/rateit.css') }}

@stop

@section('pageAssets2')
{{ HTML::script('static/metronic/plugins/rateit/jquery.rateit.min.js')}}
@stop

@section('content')
@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
	<!-- <h3 class="page-title">Generate Hotel Listing Report</h3>
	<hr> -->
	<?php 
	$sql = "select province,
	sum(case when isVerified=1 then 1 else 0 end)Approved,
	sum(case when isVerified=0 then 1 else 0 end)Unverified,
	sum(case when isVerified=3 then 1 else 0 end)Deleted,
	sum(case when isVerified=2 then 1 else 0 end)Disapproved from accommodations group by province";
	$stats = DB::select($sql);
	?>

	<div class="portlet box red">
		<div class="portlet-title">
			<div class="caption"><i class="icon-reorder"></i>Summary Report</div>
		</div>
		<div class="portlet-body">
			<table class="table" style="position: relative;margin-top: 0px !important;">
				<thead>
					<tr>
						<td>Province</td>
						<td>Approve</td>
						<td>Disapprove</td>
						<td>Deleted</td>
						<td>Unverified</td>
					</tr>
				</thead>
				<tbody>
					@foreach($stats as $stat)
					<tr>
						<td>{{$stat->province}}</td>
						<td>{{$stat->Approved}}</td>
						<td>{{$stat->Disapproved}}</td>
						<td>{{$stat->Deleted}}</td>
						<td>{{$stat->Unverified}}</td>
					</tr>
					@endforeach	
				</tbody>
			</table>

			<div id="chart_div" style="width: 100%; height: 100%; min-height: 800px;"></div>
		</div>
	</div>
	
</div>
</div>
<div class="printbtn">
	<!-- $accomms->appends(array('hotelCountry' => $b, 'hotelState' => $c, 'yearRangeStart' => $d, 'monthRangeStart' => $e, 'yearRangeEnd' => $f, 'monthRangeEnd' => $g ))->links() -->
	<a onClick="window.print()" class="btn blue">Print</a>
</div>
@stop

@section('inlineJS')
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">

google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
function drawChart() {
	var data = google.visualization.arrayToDataTable([
		['States', 'Approved', 'Disapproved', 'Deleted', 'Unverified'],
		<?php foreach($stats as $stat){ ?>
			<?php echo "['".$stat->province."',".$stat->Approved.",".$stat->Disapproved.",".$stat->Deleted.",".$stat->Unverified."],"; ?>
			<?php } ?>
			]);

	var options = {
		title: 'Hotel Status Summary'
	};

	var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
	chart.draw(data, options);
}
</script>
@stop