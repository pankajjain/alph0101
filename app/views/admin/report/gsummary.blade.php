@extends('templates.metronic')

@section('title')
Hotel Summary Report
@stop

@section('sidebar')
@include('admin.adminSideMenu')
@stop

@section('pageAssets')

@stop


@section('pageAsset2')
@stop

@section('content')
@if(Session::has('message'))
<div class="alert alert-success"> {{Session::get('message')}}</div>
@endif
<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title">Generate Hotel Summary Report</h3>
		<hr>
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon icon-reorder"></i> Hotel Summary Report
				</div>
			</div>
			<div class="portlet-body">
				<div class="container">
				<div class="span3"></div>
				{{Form::open(array('class' => '', 'route' => 'getsuReportHotelSummary', 'method'=>'get'))}}
				
				<div class="span6">
					<div class="control-group">
						{{Form::label('hotelCountry',"Hotel Country",array('class'=>'control-label'))}}
						<div class="controls">
							{{Form::select('hotelCountry',DB::table('countries')->lists('nicename', 'iso'),'MY')}}
							@if($errors->has('hotelCountry'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('hotelCountry') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="control-group">
						{{Form::label('monthRangeStart',"Range of Report (Start)",array('class'=>'control-label'))}}
						<div class="controls">
							{{Form::selectMonth('monthRangeStart',date('n'))}}
							{{Form::selectRange('yearRangeStart', 2013, date('Y'), date('Y'))}}
							@if($errors->has('monthRangeStart'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('monthRangeStart') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
							@if($errors->has('yearRangeStart'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('yearRangeStart') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>

					<div class="control-group">
						{{Form::label('monthRangeEnd',"Range of Report (End)",array('class'=>'control-label'))}}
						<div class="controls">
							{{Form::selectMonth('monthRangeEnd',date('n'))}}
							{{Form::selectRange('yearRangeEnd', 2013, date('Y'), date('Y'))}}
							@if($errors->has('monthRangeEnd'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('monthRangeEnd') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
							@if($errors->has('yearRangeEnd'))
							<div class="formErrors">
								<ul>
									@foreach($errors->get('yearRangeEnd') as $message)
									<li>
										{{$message}}
									</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
					</div>


					<button type="submit" class="btn green"><i class="icon-cogs"></i> Generate</button>
				</div>
				{{Form::close()}}
				<div class="span3"></div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
@stop