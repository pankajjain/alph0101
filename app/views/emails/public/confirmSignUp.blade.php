<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<table width="752" border="0" align="center" cellpadding="10" cellspacing="0" style="border:solid 1px #bababa; border-radius:8px;font-size:14px; color:#555555; font-family:Arial, Helvetica, sans-serif; line-height:18px; behavior: url(PIE.htc)">
  <tr>
    <td align="center" style="border-bottom:solid 1px #bababa"><img src="http://alphreds.azurewebsites.net/static/pillo/images/txt/logo.png" /></td>
  </tr>
  <tr>
    <td><h1 style="color:#7bbe61">Verify your email address</h1>
    <p style="font-size:14px; color:#555555; font-family:Arial, Helvetica, sans-serif" >Dear {{$firstname}} {{$lastname}},</p>
    <p style="font-size:14px; color:#555555; font-family:Arial, Helvetica, sans-serif; line-height:18px" >To complete your registration, please verify email address by clicking below:-</p>
    </td>
  </tr>
  <tr>
   
  </tr>
  <tr>
    <td><a href="{{$link}}">Activate Account<!--<img src="image/rename-btn.png" width="214" height="41" border="0" />--></a></td>
  </tr>
 
  <tr>
    <td height="188" valign="top"><table width="100%" border="0" align="center" cellpadding="10" cellspacing="0" style="background:#f8f8f8; border:solid 1px #dfdfdf; border-radius:8px;font-size:14px; color:#545151; font-family:Arial, Helvetica, sans-serif; line-height:18px; behavior: url(PIE.htc)">
      <tr>
        <td colspan="2"><h4 style="margin:0px 0px 5px 0px; padding:0px">Login Details :</h4></td>
      </tr>
      <tr >
        <td width="21%" valign="top" style="border-bottom:solid 1px #dfdfdf"><strong>Your Username is</strong><br />
          <strong>Your Password is</strong></td>
        <td width="79%" valign="top" style="border-bottom:solid 1px #dfdfdf">:   &nbsp; &nbsp;<a href="{{$email}}" style="color:#0865a0; text-decoration:none">{{$email}} </a> <br />
          : &nbsp;&nbsp; {{$password}}</td>
      </tr>
      <tr>
        <td colspan="2"><a href="http://alphreds.azurewebsites.net/signin"  style="color:#0865a0; text-decoration:none">Login to your profile now</a></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td >Thanks &amp; Regards,<br />
    Room Quickly Team</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="752" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif;">
  <tr>
    <td valign="top"></td>
  </tr>
  <tr>
    <td valign="top"><p style="font-size:13px; color:#0002fe; font-weight:bold;">This is System-generated email, Please do not reply to this message</p></td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
