<?php /*?><style>
.alert-info{
	color: #3a87ad;
	background-color: #d9edf7;
	border-color: #bce8f1;
}

.alert{
	font-size: 18px;
	width: 250px;
	padding: 8px 35px 8px 14px;
	margin-bottom: 20px;
	text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
	background-color: #fcf8e3;
	border: 1px solid #fbeed5;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
}
</style>

Welcome to Roomquickly
<h3>Your accommodation just got updated!</h3>
The new status of your accommodation is<br>
<div class="alert alert-info">{{$status}}</div>
You may login to your partner panel for further information<br>
<div class="alert alert-info">Additional information: {{$remarks}}</div>
For further information, you may contact system admin via admin@roomquickly.com<br>
Thank you for using RoomQuickly.<br><br>
Regard<br>
RoomQuickly<?php */?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<table width="752" border="0" align="center" cellpadding="10" cellspacing="0" style="border:solid 1px #bababa; border-radius:8px;font-size:14px; color:#555555; font-family:Arial, Helvetica, sans-serif; line-height:18px; behavior: url(PIE.htc)">
  <tr>
    <td align="center" style="border-bottom:solid 1px #bababa"><img src="http://alphreds.azurewebsites.net/static/pillo/images/txt/logo.png" /></td>
  </tr>
  <tr>
    <td>
    <p style="font-size:14px; color:#555555; font-family:Arial, Helvetica, sans-serif" >Dear {{$name}},</p>
    
    <h3>Your accommodation just got updated!</h3>
    
    <p style="font-size:14px; color:#555555; font-family:Arial, Helvetica, sans-serif; line-height:18px" >The new status of your accommodation is :- {{$status}}</p>
    </td>
  </tr>
  <tr>
   
  </tr>
  <tr>
    <td><p style="font-size:14px; color:#555555; font-family:Arial, Helvetica, sans-serif; line-height:18px" >You may login to your partner panel for further information</p></td>
  </tr>
 
  <tr>
    <td height="188" valign="top"><table width="100%" border="0" align="center" cellpadding="10" cellspacing="0" style="background:#f8f8f8; border:solid 1px #dfdfdf; border-radius:8px;font-size:14px; color:#545151; font-family:Arial, Helvetica, sans-serif; line-height:18px; behavior: url(PIE.htc)">
      <tr>
        <td colspan="2"><h4 style="margin:0px 0px 5px 0px; padding:0px">Additional information : </h4></td>
      </tr>
    <tr>
    <td><p style="font-size:14px; color:#555555; font-family:Arial, Helvetica, sans-serif; line-height:18px" >{{$remarks}}</p></td>
  </tr>
      <tr>
        <td colspan="2"><p style="font-size:14px; color:#555555; font-family:Arial, Helvetica, sans-serif; line-height:18px" >For further information, you may contact system admin via admin@roomquickly.com</p></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td >Thanks &amp; Regards,<br />
    Room Quickly Team</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="752" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif;">
  <tr>
    <td valign="top"></td>
  </tr>
  <tr>
    <td valign="top"><p style="font-size:13px; color:#0002fe; font-weight:bold;">This is System-generated email, Please do not reply to this message</p></td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
