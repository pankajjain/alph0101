<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Orders Details</title>
</head>

<body style="margin:0px;background:#000;font-size:12px;color:#666;font-family:Arial, Helvetica, sans-serif;line-height:18px;padding:0;margin:0">
<div id="wrapper" style="width:700px;margin:0 auto;">
  <div class="emailer" style="padding:10px ;	background:#fff">
    <div id="header" style="width:100%;height:100px;border-bottom:#ddd 1px solid;	margin-bottom:5px">
      <p style="color:#000000;text-align:center;">If you can't view this email, please <a href="#">click here</a>.</p>
      <div class="logo" style="padding:15px 0 0 0"><a href="http://alphreds.azurewebsites.net"> 
        <!--<img src="images/logo.png" />--> 
        <img src="http://alphreds.azurewebsites.net/static/pillo/images/txt/logo.png" /> </a></div>
    </div>
    @foreach($vouchers as $vouch)
    <div  style="background: none repeat scroll 0 0 #C7C7C7;border-radius: 0 0 4px 4px;box-shadow: 0 3px 4px #CCCCCC;clear: both;min-height: 212px; ">
    
        <div style="float:left; width:50%; border-right: medium double #888888;min-height:212px;"> 
         	<div style="font-size:16px;font-weight:bold;text-align:center">Hotel</div>
            <div style="font-size:12px;float:left;margin:10px 0 0 10px">Name : {{$vouch->accomm->name}}</div>
           <div  class="stars" style="float:left;width:100%;margin:10px 0 0 10px">
                                   <span style=" float: left;"> Rating : </span>
										@for($i = 0; $i < $vouch->accomm->starRating; $i++)
										<img style="float:left" src="http://alphreds.azurewebsites.net/static/pillo/images/ico/star.png" alt="">
										@endfor
									</div>
            <div style="float:left;font-size:12px;margin:10px 0 0 10px">
            Address : 
            @if(strlen($vouch->accomm->streetAddress) > 25)
                                 {{substr($vouch->accomm->streetAddress,0,25)}}...
                                 @else
                                  {{$vouch->accomm->streetAddress}}
                                 @endif<br />  {{$vouch->accomm->city}} , {{$vouch->accomm->country}}</div>
            
         </div>
         <div style="float:left; width:49%;min-height:212px;">
             <div style="font-size:16px;font-weight:bold;text-align:center">Voucher</div>
               <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:100%">Code : fddf45455d5</div>
               <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:100%">Name : {{$vouch->voucherName}}</div>
               <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:100%">Voucher Ammount : {{$vouch->originalPrice}}</div>
               <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:100%">Discount Percentage : {{(int)$vouch->discountPercentage}}</div>
               <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:100%">Discounted Price : {{$vouch->discountedPrice}}</div>
               <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:100%">Duration : {{$vouch->voucherDuration}} Days</div>
                 <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:100%">From : {{$vouch->redemptionPeriodFrom}} To {{$vouch->redemptionPeriodTo}}</div>
                
         	
         </div>
      </div> 
      
      <div  style="background: none repeat scroll 0 0 #C7C7C7;border-radius: 0 0 4px 4px;box-shadow: 0 3px 4px #CCCCCC;clear: both;min-height: 180px; ">
         <div style="float:left; width:50%;border-right: medium double #888888;min-height:180px;">
         	<div style="font-size:16px;font-weight:bold;text-align:center">Room</div>
           <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:100%">Name : {{$vouch->room->name}}</div>
           <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:100%">Bed : {{$vouch->room->bedType}}</div>
           <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:100%">Size : {{$vouch->room->roomSize}} Sqft.</div>
        
        
         </div>
         
         <div style="float:left; width:49%;min-height:180px;">
         	<div style="font-size:16px;font-weight:bold;text-align:center">Codes</div>
            <div style="font-size:12px;float:left;text-align:left;margin:10px 0 0 10px;width:60%">QRCODE : 
           <?php
 $qrsize = '150x150';
             $qrstring = urlencode($orders->voucherCode.",".$user->firstName." ".$user->lastName.",".$vouch->voucherName.",".(int)$vouch->discountPercentage.",".$vouch->redemptionPeriodFrom.",".$vouch->redemptionPeriodTo);
             $qrencoding = "UTF-8";
			 ?>
      <img src="https://chart.googleapis.com/chart?chs=<?php echo $qrsize; ?>&cht=qr&chl=<?php echo $qrstring; ?>&choe=<?php echo $qrencoding; ?>" width="100" />
    </div>
        
            
                <div style="font-size:12px;float:left;text-align:left;margin: 117px 0 0 10px;width:25%">
                
                <a style=" background: linear-gradient(to bottom, #FDD22B 0%, #EFBA01 100%) repeat scroll 0 0 rgba(0, 0, 0, 0); border-radius: 6px;
    color: #FFFFFF; display: inline-block;font: 11px/30px 'OpenSansBold';height: 30px;padding: 0 18px;text-align: center;text-decoration: none;  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.2);text-transform: uppercase;" title="Book now" onclick="window.print()" >Print</a>
            </div>
        
        
        
        </div>
       </div> 
    
    
    @endforeach
     
</div>

<div class="footerbar" style="float:left; width:100%; height:84px; background:url(images/graybg.png) 0 bottom repeat-x; color:#ccc border-top:#b42025 10px solid">
    <p class="footer_left" style="float:left; font-size:11px; line-height:64px; padding-left:10px">Having trouble viewing this email? <a href="#" style="color:#009ec2; font-size:12px">Click Here</a></p>
    <div class="social" style="float:right; padding-top:16px; padding-right:10px"> <a href="#" style="float:left"><img src="images/social_st.png" /></a> <a href="#" style="float:left"><img src="images/social_tw.png" /></a> <a href="#" style="float:left"><img src="images/social_gplus.png" /></a> <a href="#" style="float:left"><img src="images/social_fb.png" /></a> <a href="#" style="float:left"><img src="images/social_in.png" /></a> <a href="#" style="float:left"><img src="images/social_pin.png" /></a> <a href="#" style="float:left"><img src="images/social_you.png" /></a> </div>
  </div>
  <div class="clr"></div>
 </div>
  </body>
</html>