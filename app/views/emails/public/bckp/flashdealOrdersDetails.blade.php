<style>
.alert-info{
	color: #3a87ad;
	background-color: #d9edf7;
	border-color: #bce8f1;
}

.alert{
	font-size: 18px;
	width: 250px;
	padding: 8px 35px 8px 14px;
	margin-bottom: 20px;
	text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
	background-color: #fcf8e3;
	border: 1px solid #fbeed5;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
}
</style>

Welcome to Roomquickly
<h3>Your order has been succesfully created</h3>
Your Order's invoice id is : #{{$invoice}} <br>
<div class="alert alert-info">Payment Status : {{$status}}</div>
<b>FlashDeal Details</b><br>
<div class="alert alert-info">
FlashDeal Name : {{$flashdealName}} <br />
Hotel : {{$accommName}} <br />
Room : {{$roomName}} <br />
FlashDeal Price : {{$flashdealPrice}} <br /> 
Redemption Period From : {{$redemptionPeriodFrom}} <br /> 
Redemption Period To : {{$redemptionPeriodTo}}
</div>
For further information, you may contact system admin via admin@roomquickly.com<br>
Thank you for using RoomQuickly.<br><br>
Regard<br>
RoomQuickly