<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>email-newsletters</title>
</head>

<body style="margin:0px;background:#000;font-size:12px;color:#666;font-family:Arial, Helvetica, sans-serif;line-height:18px;padding:0;margin:0">
<div id="wrapper" style="width:610px;margin:0 auto;">
  <div class="emailer" style="padding:10px ;	background:#fff">
    <div id="header" style="width:100%;height:100px;border-bottom:#ddd 1px solid;	margin-bottom:5px">
      <p style="color:#000000;text-align:center;">If you can't view this email, please <a href="#">click here</a>.</p>
      <div class="logo" style="padding:15px 0 0 0"><a href="http://alphreds.azurewebsites.net"> 
        <!--<img src="images/logo.png" />--> 
        <img src="http://alphreds.azurewebsites.net/static/pillo/images/txt/logo.png" /> </a></div>
    </div>
     @if(count($voucher) > 0)  
    <div class="box" style="width:100%;	border-bottom:#ddd 1px solid;margin-bottom:10px;float:left;">
      <h1 style='background:#f7c718;color:#fff;font:"Lucida Sans Unicode", "Lucida Grande", sans-serif;font-size:20px;padding:6px 15px;'>Newly Added Hotels</h1>
      <ul style="padding:10px 0;float:left; width:100%">
        @foreach($accomm as $entry)
        <li style="float:left; width:100%; padding-bottom:7px">
          <div class="postimg" style="width:150px;float:left; position:relative !important;"> 
          
            <!--<img src="images/hotel.png" width="145" height="87" />--> 
            @if(!empty($entry->photo->first()->fileName))
            
            
            @if(HTML::image("https://rqphoto.s3-ap-southeast-1.amazonaws.com/268_$entry->photo->first()->fileName")) <img style="border:none;outline:none" alt width="145" height="87" src="https://rqphoto.s3-ap-southeast-1.amazonaws.com/268_{{ $entry->photo->first()->fileName }}"/> @else <img style="border:none;outline:none"  alt width="145" height="87" src="http://dummyimage.com/145x87/d6d2d6/000&text=No+Image"/> @endif
            @else <img style="border:none;outline:none"  alt width="145" height="87" src="http://dummyimage.com/145x87/d6d2d6/000&text=No+Image"/> @endif </div>
          <div class="postcontent" style="width:430px;float:right;">
            <h2 style='font:"Trebuchet MS", Arial, Helvetica, sans-serif;font-size:16px;color:#4f4f4f;'> @if(strlen($entry->name) > 20)
              {{substr($entry->name,0,20)}}...
              @else
              {{$entry->name}}
              @endif</h2>
            <div class="rating"> @for($i = 0; $i < $entry->starRating; $i++) <img src="http://alphreds.azurewebsites.net/static/pillo/images/ico/star.png" alt=""> @endfor </div>
            <p style="float:left">{{$entry->starRating}}<br />
              {{$entry->city}}, {{$entry->province}}<br />
              <!--  Original Price $100--></p>
            <a href="http://alphreds.azurewebsites.net/hotel/{{$entry->id}}" style="background:#f6c617; color:#fff; text-transform:uppercase; float:right; padding:5px 15px; border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px; margin:25px 42px 0 0">Browse</a> </div>
        </li>
        @endforeach
      </ul>
    </div>
     @endif
    
    
    
   @if(count($voucher) > 0)  
    <div class="box" style="width:100%;	border-bottom:#ddd 1px solid;margin-bottom:10px;float:left;">
      <h1 style='background:#f7c718;color:#fff;font:"Lucida Sans Unicode", "Lucida Grande", sans-serif;font-size:20px;padding:6px 15px;'>Newly Added Vouchers</h1>
      <ul style="padding:10px 0;float:left; width:100%">
        @foreach($voucher as $v)
        <li style="float:left; width:100%; padding-bottom:7px">
          <div class="postimg" style="width:150px;float:left;"> <span class="presentes" style="background:url('http://alphreds.azurewebsites.net/static/pillo/images/bgr/discount.png') no-repeat;
	width:27px;height:30px;overflow: hidden;padding:3px 0 0; position: absolute;text-align: center;font-size:11px;text-shadow: 0 1px 0 rgba(0, 0, 0, 0.1);z-index:2;">-{{(int)$v->discountPercentage}}%</span> 
            <!--<img src="images/hotel.png" width="145" height="87" />--> 
            @if(!empty($v->room->photos->first()->fileName))
            
            
            @if(HTML::image("https://rqphoto.s3-ap-southeast-1.amazonaws.com/268_$v->accomm->photo->first()->fileName")) <img style="border:none;outline:none" alt width="145" height="87" src="https://rqphoto.s3-ap-southeast-1.amazonaws.com/268_{{ $v->accomm->photo->first()->fileName }}"/> @else <img style="border:none;outline:none"  alt width="145" height="87" src="http://dummyimage.com/145x87/d6d2d6/000&text=No+Image"/> @endif
            @else <img style="border:none;outline:none"  alt width="145" height="87" src="http://dummyimage.com/145x87/d6d2d6/000&text=No+Image"/> @endif </div>
          <div class="postcontent" style="width:430px;float:right;">
            <h2 style='font:"Trebuchet MS", Arial, Helvetica, sans-serif;font-size:16px;color:#4f4f4f;'> @if(strlen($v->voucherName) > 20)
              {{substr($v->voucherName,0,20)}}...
              @else
              {{$v->voucherName}}
              @endif </h2>
            <div class="rating"> @for($j = 0; $j < $v->accomm->starRating; $j++) <img src="http://alphreds.azurewebsites.net/static/pillo/images/ico/star.png" alt=""> @endfor </div>
            <p style="float:left">{{$v->accomm->starRating}}<br />
              @if(strlen($v->accomm->streetAddress) > 25)
              {{substr($v->accomm->streetAddress,0,25)}}...
              @else
              {{$v->accomm->streetAddress}}
              @endif<br />
              {{$v->accomm->city}} , {{$v->accomm->country}}<br />
              <!--  Original Price $100--></p>
            <a href="http://alphreds.azurewebsites.net/voucher/{{$v->id}}" style="background:#f6c617; color:#fff; text-transform:uppercase; float:right; padding:5px 15px; border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px; margin:25px 42px 0 0">Browse</a> </div>
        </li>
        @endforeach
      </ul>
    </div>
    @endif
    
    @if(count($flashdeal) > 0)
    <div class="box" style="width:100%;	border-bottom:#ddd 1px solid;margin-bottom:10px;float:left;">
      <h1 style='background:#f7c718;color:#fff;font:"Lucida Sans Unicode", "Lucida Grande", sans-serif;font-size:20px;padding:6px 15px;'>Newly Added FlashDeals</h1>
      <ul style="padding:10px 0;float:left; width:100%">
        @foreach($flashdeal as $flash)
        <li style="float:left; width:100%; padding-bottom:7px">
          <div class="postimg" style="width:150px;float:left;"> <span class="presentes" style="background:url('http://alphreds.azurewebsites.net/static/pillo/images/bgr/discount.png') no-repeat;
	width:27px;height:30px;overflow: hidden;padding:3px 0 0; position: absolute;text-align: center;font-size:11px;text-shadow: 0 1px 0 rgba(0, 0, 0, 0.1);z-index:2;">-{{(int)$flash->discountPercentage}}%</span> 
            <!--<img src="images/hotel.png" width="145" height="87" />--> 
            @if(!empty($flash->room->photos->first()->fileName))
            
            
            @if(HTML::image("https://rqphoto.s3-ap-southeast-1.amazonaws.com/268_$flash->accomm->photo->first()->fileName")) <img style="border:none;outline:none" alt width="145" height="87" src="https://rqphoto.s3-ap-southeast-1.amazonaws.com/268_{{ $flash->accomm->photo->first()->fileName }}"/> @else <img style="border:none;outline:none"  alt width="145" height="87" src="http://dummyimage.com/145x87/d6d2d6/000&text=No+Image"/> @endif
            @else <img style="border:none;outline:none"  alt width="145" height="87" src="http://dummyimage.com/145x87/d6d2d6/000&text=No+Image"/> @endif </div>
          <div class="postcontent" style="width:430px;float:right;">
            <h2 style='font:"Trebuchet MS", Arial, Helvetica, sans-serif;font-size:16px;color:#4f4f4f;'> @if(strlen($flash->flashDealName) > 20)
              {{substr($flash->flashDealName,0,20)}}...
              @else
              {{$flash->flashDealName}}
              @endif </h2>
            <div class="rating"> @for($j = 0; $j < $flash->accomm->starRating; $j++) <img src="http://alphreds.azurewebsites.net/static/pillo/images/ico/star.png" alt=""> @endfor </div>
            <p style="float:left">{{$flash->accomm->starRating}}<br />
              @if(strlen($flash->accomm->streetAddress) > 25)
              {{substr($flash->accomm->streetAddress,0,25)}}...
              @else
              {{$flash->accomm->streetAddress}}
              @endif<br />
              {{$flash->accomm->city}} , {{$flash->accomm->country}}<br />
              <!--  Original Price $100--></p>
            <a href="http://alphreds.azurewebsites.net/voucher/{{$flash->id}}" style="background:#f6c617; color:#fff; text-transform:uppercase; float:right; padding:5px 15px; border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px; margin:25px 42px 0 0">Browse</a> </div>
        </li>
        @endforeach
      </ul>
    </div>
    
    @endif
    
    <div class="infodiv" style="padding:0 20px; border-bottom:#ddd 1px solid">
      <h2 style="text-transform:uppercase; font-size:20px;  color:#000">Lorem ipsum dolor sit amet!</h2>
      <p style="text-align:justify; padding:5px 0 10px 0">Lorem ipsum dolor sit amet laoreet, feugiat lorem id, volutpat nunc. Ut quis enim tellus. Phasellus vestibulum suscipit ante sed pretium. Vivamus eget malesuada lectus. Vivamus luctus eu sapien a pretium. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras non lacus magna. <a href="#" style="float:right; color:#b42025;"> Read More...</a> </p>
      <div class="clr"></div>
    </div>
    <div class="requast" style=" width:298px; height:46px; margin-left:auto; margin-right:auto; padding:11px 0 30px 0;"><a href="#"><img src="images/request.png" width="298" height="46"/></a> </div>
    <div class="suscribe">
      <p class="suscribe_left" style="float:left; font-size:12px; padding:0px;"><a href="#" style="color:#6f0b0d;">Forward to a friend</a> | <a href="#" style="color:#6f0b0d;">Subscribe</a> | <a href="#" style="color:#6f0b0d;"> Un-Subscribe</a></p>
      <p style="text-align:right;"><span style="color:#b42025">24/7</span> Support number |<span  style="color:#b42025"> INDIA:</span> +1- 555 - 555 - 555</p>
    </div>
  </div>
  <div class="footerbar" style="float:left; width:100%; height:84px; background:url(images/graybg.png) 0 bottom repeat-x; color:#fff; border-top:#b42025 10px solid">
    <p class="footer_left" style="float:left; font-size:11px; line-height:64px; padding-left:10px">Having trouble viewing this email? <a href="#" style="color:#009ec2; font-size:12px">Click Here</a></p>
    <div class="social" style="float:right; padding-top:16px; padding-right:10px"> <a href="#" style="float:left"><img src="images/social_st.png" /></a> <a href="#" style="float:left"><img src="images/social_tw.png" /></a> <a href="#" style="float:left"><img src="images/social_gplus.png" /></a> <a href="#" style="float:left"><img src="images/social_fb.png" /></a> <a href="#" style="float:left"><img src="images/social_in.png" /></a> <a href="#" style="float:left"><img src="images/social_pin.png" /></a> <a href="#" style="float:left"><img src="images/social_you.png" /></a> </div>
  </div>
  <div class="clr"></div>
</div>
</body>
</html>
