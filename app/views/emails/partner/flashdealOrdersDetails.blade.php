<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Orders Details</title>
</head>

<body>
 @foreach($flashdeals as $flash)
<table width="752" border="0" align="center" cellpadding="10" cellspacing="0" style="border:solid 1px #bababa; border-radius:8px;font-size:14px; color:#555555; font-family:Arial, Helvetica, sans-serif; line-height:18px; behavior: url(PIE.htc)">
  <tr>
    <td align="center" style="border-bottom:solid 1px #bababa"><img src="http://alphreds.azurewebsites.net/static/pillo/images/txt/logo.png" width="327" height="108" /></td>
  </tr>
  <tr>
    <td><h1 style="color:#7bbe61">FlashDeal Order Details</h1>
    <p style="font-size:14px; color:#555555; font-family:Arial, Helvetica, sans-serif" >Dear {{$flashdealsUser[0]->firstName}} {{$flashdealsUser[0]->lastName}}</p>
    
    </td>
  </tr>
  
  <tr>
    <td height="120" valign="top"><table width="100%" border="0" align="center" cellpadding="10" cellspacing="0" style="background:#f8f8f8; border:solid 1px #dfdfdf; border-radius:8px;font-size:14px; color:#545151; font-family:Arial, Helvetica, sans-serif; line-height:18px; behavior: url(PIE.htc)">
      <tr>
        <td colspan="2"><h4 style="margin:0px 0px 5px 0px; padding:0px">Order :</h4></td>
      </tr>
      <tr >
        <td width="30%" valign="top" style="border-bottom:solid 1px #dfdfdf">
        <strong>User Name</strong><br />
          <strong>User Email</strong>
         
        </td>
        <td width="70%" valign="top" style="border-bottom:solid 1px #dfdfdf">
          : &nbsp; &nbsp; {{$user->firstName}} {{$user->lastName}}</a> <br />
          : &nbsp;&nbsp; {{$user->email}}
          
         
        </td>
      </tr>
      
    </table></td>
  </tr>
  
  <tr>
    <td height="120" valign="top"><table width="100%" border="0" align="center" cellpadding="10" cellspacing="0" style="background:#f8f8f8; border:solid 1px #dfdfdf; border-radius:8px;font-size:14px; color:#545151; font-family:Arial, Helvetica, sans-serif; line-height:18px; behavior: url(PIE.htc)">
      <tr>
        <td colspan="2"><h4 style="margin:0px 0px 5px 0px; padding:0px">Order :</h4></td>
      </tr>
      <tr >
        <td width="30%" valign="top" style="border-bottom:solid 1px #dfdfdf">
        <strong>Invoice Id</strong><br />
          <strong>Order Status</strong><br />
          <strong>Order Price</strong>
         
        </td>
        <td width="70%" valign="top" style="border-bottom:solid 1px #dfdfdf">
          : &nbsp; &nbsp; #{{$invoice}} <br />
          : &nbsp;&nbsp; {{$status}}<br/>
          : &nbsp; &nbsp; {{$orders->price}}
         
        </td>
      </tr>
      
    </table></td>
  </tr>
  
  <tr>
    <td height="180" valign="top"><table width="100%" border="0" align="center" cellpadding="10" cellspacing="0" style="background:#f8f8f8; border:solid 1px #dfdfdf; border-radius:8px;font-size:14px; color:#545151; font-family:Arial, Helvetica, sans-serif; line-height:18px; behavior: url(PIE.htc)">
      <tr>
        <td colspan="2"><h4 style="margin:0px 0px 5px 0px; padding:0px">FlashDeal Details :</h4></td>
      </tr>
      <tr >
        <td width="30%" valign="top" style="border-bottom:solid 1px #dfdfdf">
        
          <strong>FlashDeal Name</strong><br />
          <strong>FlashDeal Ammount</strong><br />
          <strong>Discount Percentage</strong><br />
          <strong>Discounted Price</strong><br />
          <strong>Duration</strong><br />
          <strong>From</strong>
        </td>
        <td width="70%" valign="top" style="border-bottom:solid 1px #dfdfdf">
          
          : &nbsp;&nbsp; {{$flash->flashDealName}}<br/>
          : &nbsp; &nbsp; {{$flash->originalPrice}}<br />
          : &nbsp; &nbsp; {{(int)$flash->discountPercentage}}<br />
          : &nbsp; &nbsp; {{$flash->discountedPrice}}<br />
          : &nbsp; &nbsp; {{$flash->flashDealDuration}} Days<br />
          : &nbsp; &nbsp; {{$flash->redemptionPeriodFrom}} To {{$flash->redemptionPeriodTo}}
        </td>
      </tr>
      
    </table></td>
  </tr>
  
  <tr>
    <td height="180" valign="top"><table width="100%" border="0" align="center" cellpadding="10" cellspacing="0" style="background:#f8f8f8; border:solid 1px #dfdfdf; border-radius:8px;font-size:14px; color:#545151; font-family:Arial, Helvetica, sans-serif; line-height:18px; behavior: url(PIE.htc)">
      <tr>
        <td colspan="2"><h4 style="margin:0px 0px 5px 0px; padding:0px">Hotel Details :</h4></td>
      </tr>
      <tr >
        <td width="30%" valign="top" style="border-bottom:solid 1px #dfdfdf">
        <strong>Hotel Name</strong><br />
          <strong>Rating</strong><br />
          <strong>Address</strong><br />
          <strong>City</strong><br />
          <strong>Country</strong>
          
        </td>
        <td width="70%" valign="top" style="border-bottom:solid 1px #dfdfdf">
          : &nbsp; &nbsp; {{$flash->accomm->name}} <br />
          : &nbsp;&nbsp; 
          							@for($i = 0; $i < $flash->accomm->starRating; $i++)
										<img style="float:left" src="http://alphreds.azurewebsites.net/static/pillo/images/ico/star.png" alt="">
										@endfor<br/>
          : &nbsp; &nbsp; {{$flash->accomm->streetAddress}}<br />
          : &nbsp; &nbsp; {{$flash->accomm->city}}<br />
          : &nbsp; &nbsp; {{$flash->accomm->country}}
         
        </td>
      </tr>
      
    </table></td>
  </tr>
  
  
  <tr>
    <td height="120" valign="top"><table width="100%" border="0" align="center" cellpadding="10" cellspacing="0" style="background:#f8f8f8; border:solid 1px #dfdfdf; border-radius:8px;font-size:14px; color:#545151; font-family:Arial, Helvetica, sans-serif; line-height:18px; behavior: url(PIE.htc)">
      <tr>
        <td colspan="2"><h4 style="margin:0px 0px 5px 0px; padding:0px">Room Details :</h4></td>
      </tr>
      <tr >
        <td width="30%" valign="top" style="border-bottom:solid 1px #dfdfdf">
        <strong>Room Name</strong><br />
          <strong>Bed</strong><br />
          <strong>Room Size</strong>
          
        </td>
        <td width="70%" valign="top" style="border-bottom:solid 1px #dfdfdf">
          : &nbsp; &nbsp; {{$flash->room->name}} <br />
          : &nbsp;&nbsp; {{$flash->room->bedType}}<br/>
          : &nbsp; &nbsp; {{$flash->room->roomSize}} Sqft.
                 
        </td>
      </tr>
      
    </table></td>
  </tr>
  
  
  <tr>
    <td >Thanks &amp; Regards,<br />
    Room Quickly Team</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
@endforeach
<table width="752" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif;">
  <tr>
    <td valign="top"></td>
  </tr>
  <tr>
    <td valign="top"><p style="font-size:13px; color:#0002fe; font-weight:bold;">This is System-generated email, Please do not reply to this message</p></td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
