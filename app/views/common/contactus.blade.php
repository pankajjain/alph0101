@extends('templates.pillo')

@section('title')
Contact Us
@stop

@section('bodyyield')
onload="initialize()"
@stop

@section('thescripts')

<?php /*?><script src="static/ckeditor/ckeditor.js"></script>
 <script>
	$(document).ready(function(){
	CKEDITOR.replace( 'message' );
	});
	</script><?php */?>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript">
		function initialize() {
			var secheltLoc = new google.maps.LatLng(49.47216, -123.76307);

			var myMapOptions = {
				 zoom: 15
				,center: secheltLoc
				,mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			var theMap = new google.maps.Map(document.getElementById("map_canvas"), myMapOptions);


			var marker = new google.maps.Marker({
				map: theMap,
				draggable: true,
				position: new google.maps.LatLng(49.47216, -123.76307),
				visible: true
			});

			var boxText = document.createElement("div");
			boxText.innerHTML = "<strong>Book Your Travel</strong><br>1400 Pennsylvania Ave,<br>Washington DC";

			var myOptions = {
				 content: boxText
				,disableAutoPan: false
				,maxWidth: 0
				,pixelOffset: new google.maps.Size(-140, 0)
				,zIndex: null
				,closeBoxURL: ""
				,infoBoxClearance: new google.maps.Size(1, 1)
				,isHidden: false
				,pane: "floatPane"
				,enableEventPropagation: false
			};

			google.maps.event.addListener(marker, "click", function (e) {
				ib.open(theMap, this);
			});

			var ib = new InfoBox(myOptions);
			ib.open(theMap, marker);
		}
	</script>
@stop

@section('content')

<div class="main" role="main">		
		<div class="wrap clearfix">
			<!--main content-->
			<div class="content clearfix">

				<!--three-fourth content-->
                
				<section class="three-fourth">
					<h1>Contact us</h1>
					<!--map-->
					<div class="map-wrap">
						<div class="gmap" id="map_canvas"></div>
					</div>
					<!--//map-->
				</section>	
				<!--three-fourth content-->
				
				<!--sidebar-->
				<aside class="right-sidebar lower">
					<!--contact form-->
					<article class="default">
                        @if(Session::has('message'))
                        <div class="alert alert-success"> {{Session::get('message')}}</div>
                        @endif

						<h2>Send us a message</h2>
						<form action="{{URL::route('contactPost')}}" id="contact-form" method="post">
							<fieldset>
								<div class="f-item">
									<label for="name">Your name *</label>
									<input type="text" id="name" name="name" value="" required="required" />
                                    @if($errors->has('name'))			
                                        @foreach($errors->get('name') as $message)
                                        <div class="help-inline" style="color:red">
                                            {{$message}}
                                        </div>
                                        @endforeach
                                        @endif
								</div>
								<div class="f-item">
									<label for="email">Your e-mail *</label>
									<input type="email" id="email" name="email" value=""  required="required" />
                                    @if($errors->has('email'))			
                                    @foreach($errors->get('email') as $message)
                                    <div class="help-inline" style="color:red">
                                        {{$message}}
                                    </div>
                                    @endforeach
                                    @endif
								</div>
								<div class="f-item">
									<label for="message">Your message *</label>
									<textarea id="message" name="content" rows="10" cols="10" required="required"></textarea>
                                    @if($errors->has('content'))			
                                    @foreach($errors->get('content') as $message)
                                    <div class="help-inline" style="color:red">
                                        {{$message}}
                                    </div>
                                    @endforeach
                                    @endif
								</div>
								<input type="submit" value="Send" id="submit" name="submit" class="gradient-button" />
							</fieldset>
						</form>
					</article>
					<!--//contact form-->
					
					<!--contact info-->
					<article class="default">
						<h2>Or contact us directly</h2>
						<p class="phone-green">1- 555 - 555 - 555</p>
						<p class="email-green"><a href="#">booking@mail.com</a></p>
					</article>
					<!--//contact info-->
				</aside>
				<!--//sidebar-->
			</div>
			<!--//main content-->
		</div>
	</div>
    
   
    
@stop

