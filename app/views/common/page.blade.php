@extends('templates.pillo')

@section('title')
{{$page[0]->title}}
@stop

@section('bodyyield')
@stop

@section('thescripts')

@stop

@section('content')
<div class="main" role="main">		
	<div class="wrap clearfix">
		<div class="content clearfix">
			<h2>{{$page[0]->title}}</h2>
			<section class="full">
				<div class="map-wrap">
					{{$page[0]->content}}
				</div>
			</section>
		</div>
	</div>
</div>
@stop